package com.gyenno.me;


import com.gyenno.R;
import com.gyenno.me.MeSportWeek.MyAdapter;
import com.gyenno.testUI.BluetoothTestUIActivity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.AlteredCharSequence;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


public class TabMeFragment extends Fragment implements OnClickListener{
	public static final String TITLE = "title";
	private int touxiang[]={R.drawable.touxiang1,R.drawable.touxiang2,R.drawable.touxiang3,R.drawable.touxiang4,
			R.drawable.touxiang5,R.drawable.touxiang1,R.drawable.touxiang3};
	private String name[]={"Sunny","我","Lisa","Jakey","Tom","Ervin","Emy"};
	
	private ListView lv_me_advice;
	private MyAdapter madapter;
	private ScrollView scroll;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_me, container, false);
		initView(rootView);
		LinearLayout linearLayout =(LinearLayout)rootView.findViewById(R.id.meMain);
		
		/*CharacterView characterView =new CharacterView(rootView.getContext());
		linearLayout.addView(characterView);*/
		
		return rootView;
	}
	private void initView(View rootView) {
		ImageView iv_youyang = (ImageView) rootView.findViewById(R.id.iv_youyang);
		ImageView iv_sheru = (ImageView) rootView.findViewById(R.id.iv_sheru);
		ImageView iv_shuimian = (ImageView) rootView.findViewById(R.id.iv_shuimian);
		ImageView iv_del = (ImageView)rootView.findViewById(R.id.iv_del);
		lv_me_advice = (ListView) rootView.findViewById(R.id.lv_me_advice);
		if(madapter == null)
			madapter = new MyAdapter();
		lv_me_advice.setAdapter(madapter);
		lv_me_advice.setOnItemClickListener(new OnItemClickListener() {//添加条目点击事件
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				//Toast.makeText(getActivity(), "点击了条目"+position, 0).show();
				Intent intent = new Intent(getActivity(),HealthModelActivity.class);
				intent.putExtra("name", name[position]);
				startActivity(intent);
			}
		});
		
		scroll=(ScrollView) rootView.findViewById(R.id.scroll);
		scroll.post(new Runnable() {  
		      //让scrollview跳转到顶部，必须放在runnable()方法中
		    @Override  
		    public void run() {  
		    	scroll.scrollTo(0, 0);  
		     }  
		   });
		
		iv_del.setOnClickListener(this);
		iv_youyang.setOnClickListener(this);
		iv_sheru.setOnClickListener(this);
		iv_shuimian.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.iv_youyang:
			intent = new Intent(getActivity(), SportActivity.class);
			startActivity(intent);
			break;
		case R.id.iv_sheru:
			//intent = new Intent(getActivity(),BluetoothTestUIActivity.class);
			intent = new Intent(getActivity(),IntakeActivity.class);
			startActivity(intent);
			break;
		case R.id.iv_shuimian:
			intent = new Intent(getActivity(),SleepActivity.class);
			startActivity(intent);
			break;
		case R.id.iv_del: //自定义dialog
			/*AlertDialog.Builder builder = new Builder(getActivity());
			View view = View.inflate(getActivity(), R.layout.customdialog, null);
			builder.setView(view);
			builder.show();*/
			
			//自定义dialog类
			/*Dialog dialog = new MyDialog(getActivity(),
                    R.style.MyDialog);
            dialog.show();		*/
			
			//用Activity
			intent = new Intent(getActivity(),DelDialogActivity.class);
			startActivity(intent);
			break;
		}
		
	}
	
	class MyAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return touxiang.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;
			ViewHolder holder;
			if(convertView!=null)
			{
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}
			else
			{
				view = View.inflate(getActivity(), R.layout.advice_item, null);
				holder = new ViewHolder();
				holder.icon = (ImageView) view.findViewById(R.id.icon);
				holder.name = (TextView) view.findViewById(R.id.name);
				view.setTag(holder);
			}			
			//holder.jiantou = (ImageView) view.findViewById(R.id.jiantou);
			
			holder.icon.setImageResource(touxiang[position]);
			holder.name.setText(name[position]+"给你推荐的健康建议");
						
			return view;
		}
	}
	
	public class ViewHolder
	{
		ImageView icon;
		TextView name;
		ImageView jiantou;
	}
}
