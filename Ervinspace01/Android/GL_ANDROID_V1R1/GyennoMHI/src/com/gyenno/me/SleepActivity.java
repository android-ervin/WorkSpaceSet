package com.gyenno.me;

import java.util.ArrayList;
import java.util.List;

import com.gyenno.R;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SleepActivity extends FragmentActivity implements OnClickListener,
	OnPageChangeListener{

	private ViewPager mViewPager;
	private List<Fragment> mTabs = new ArrayList<Fragment>();
	private FragmentPagerAdapter mAdapter;

	private List<View> mTabIndicators = new ArrayList<View>();
	
	private TextView tv_constitute;
	private TextView tv_compare;
	private TextView tv_summary;
	
	ImageView iv_share ;
	ImageView iv_turnnext;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_sleep);
		
		initView();
		initDatas();
		mViewPager.setAdapter(mAdapter);
		setCurrentTab(0);
	}
	
	private void setCurrentTab(int j) {
		for (int i = 0; i < 3; i++) {		
			if (i == j) //选择了当前页
			{
				mTabIndicators.get(i).setSelected(true);
				mTabIndicators.get(i).setBackgroundColor(getResources().getColor(R.color.menu_normal));
			} else {
				mTabIndicators.get(i).setSelected(false);
				mTabIndicators.get(i).setBackgroundColor(Color.TRANSPARENT);
			}
		}	
	}

	private void initDatas() {
		MeSleepConstitute micon = new MeSleepConstitute();
		mTabs.add(micon);
		MeSleepCompare micom = new MeSleepCompare();
		mTabs.add(micom);
		MeSleepSummary misum = new MeSleepSummary();
		mTabs.add(misum);
		//初始化viewpager适配器
		mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
			
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return mTabs.size();
			}			
			@Override
			public Fragment getItem(int arg0) {
				// TODO Auto-generated method stub
				return mTabs.get(arg0);
			}
		};		
		
	}

	private void initView() {
		mViewPager = (ViewPager) findViewById(R.id.sleep_viewpager);
		mViewPager.setOnPageChangeListener(this);
		tv_constitute = (TextView) findViewById(R.id.tv_constitute);
		mTabIndicators.add(tv_constitute);
		tv_compare = (TextView) findViewById(R.id.tv_compare);
		mTabIndicators.add(tv_compare);
		tv_summary = (TextView) findViewById(R.id.tv_summary);
		mTabIndicators.add(tv_summary);
		
		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);
		
		iv_share = (ImageView)findViewById(R.id.iv_share);
		
		
		tv_constitute.setOnClickListener(this);
		tv_compare.setOnClickListener(this);
		tv_summary.setOnClickListener(this);
		
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int arg0) {
		setCurrentTab(arg0);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_constitute:
			setCurrentTab(0);
			iv_share.setVisibility(View.INVISIBLE);
			mViewPager.setCurrentItem(0, false);
			break;
		case R.id.tv_compare:
			setCurrentTab(1);
			iv_share.setVisibility(View.INVISIBLE);
			mViewPager.setCurrentItem(1, false);
			break;
		case R.id.tv_summary:
			setCurrentTab(2);
			iv_share.setVisibility(View.VISIBLE);
			mViewPager.setCurrentItem(2, false);
			break;		
		case R.id.iv_back:
			finish();
			break;
		}
		
	}

}
