package com.gyenno.me;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.gyenno.R;

import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Telephony.ThreadsColumns;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MeIntakeCompare extends Fragment{

	LinearLayout chart;
	GraphicalView chartView;
	
	Handler handler;
/*	XYSeries xySeries;
	XYMultipleSeriesDataset seriesDataset;*/
	private double energy[]={36,40,26,15,39,33,28};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_me_sheru_compare, container, false);
		initView(rootView);	
		return rootView;
	}

	private void initView(View rootView) {
		// TODO Auto-generated method stub
		chart = (LinearLayout) rootView.findViewById(R.id.chart_show);
		handler = new MyHandler();
		
		showChart();
	}

	private void showChart() {
		XYMultipleSeriesDataset mDataSet=getDataSet();
		XYMultipleSeriesRenderer mRefender=getRefender();
		chartView=ChartFactory.getLineChartView(getActivity(), mDataSet, mRefender);
		chart.addView(chartView);
		
	}

	private XYMultipleSeriesRenderer getRefender() {
		XYMultipleSeriesRenderer seriesRenderer=new XYMultipleSeriesRenderer();
		seriesRenderer.setApplyBackgroundColor(true);
		
		//seriesRenderer.setAxesColor(getResources().getColor(R.color.background)); //设置轴的颜色，设为背景色，隐藏
		seriesRenderer.setLabelsTextSize(DensityUtil.sp2px(getActivity(), 15));//设置轴标签文本大小
		seriesRenderer.setLabelsColor(0xFF00C8FF);
		seriesRenderer.setShowAxes(false);  //是否显示坐标轴
		//seriesRenderer.setShowGrid(true);//设置是否在图表中显示网格
		//seriesRenderer.setGridColor(0xFF00C8FF);
		
		seriesRenderer.setMargins(new int[] { 40, 40, 30, 20 });//设置图表四周边距
		seriesRenderer.setYAxisMin(0);//设置Y轴最小值
		seriesRenderer.setYAxisMax(50);//设置Y轴最大值
		seriesRenderer.setXAxisMin(0.5);//设置x轴最小值
        seriesRenderer.setXAxisMax(7.5);//设置x轴最大值
        
        seriesRenderer.setZoomButtonsVisible(false);//缩放按钮隐藏
        seriesRenderer.setZoomEnabled(false,false); //设置x,y轴都不能缩放
        seriesRenderer.setPanEnabled(false,false);//设置x.y不可以移动
        seriesRenderer.setPointSize(DensityUtil.sp2px(getActivity(), 4));//设置点的大小
       
        
        seriesRenderer.setMarginsColor(getResources().getColor(R.color.background)); //设置四周的背景颜色
        seriesRenderer.setClickEnabled(false);
        
        seriesRenderer.setXLabelsAlign(Align.CENTER);
		//seriesRenderer.setYLabelsAlign(Align.RIGHT);
		seriesRenderer.setXLabels(0);//设置X轴显示的刻度标签的个数
		seriesRenderer.addXTextLabel(1, "周日");//x轴数据
		seriesRenderer.addXTextLabel(2, "周一");
		seriesRenderer.addXTextLabel(3, "周二");
		seriesRenderer.addXTextLabel(4, "周三");
		seriesRenderer.addXTextLabel(5, "周四");
		seriesRenderer.addXTextLabel(6, "周五");
		seriesRenderer.addXTextLabel(7, "周六");
		seriesRenderer.setXLabelsColor(0xFF00C8FF);//设置x轴字体颜色
		seriesRenderer.setShowGridX(true); //显示X轴网格
		seriesRenderer.setGridColor(0xFF00C8FF);
		
		seriesRenderer.setYLabels(0); //设置Y轴显示几个坐标
		
		//seriesRenderer.setLegendHeight(0);
		//seriesRenderer.setLegendTextSize(0);
		seriesRenderer.setShowLegend(false); //隐藏图例
		
		
		XYSeriesRenderer xySeriesRenderer=new XYSeriesRenderer();
		xySeriesRenderer.setPointStyle(PointStyle.CIRCLE);//设置点的样式 
		xySeriesRenderer.setFillPoints(true);//设置实心点
		xySeriesRenderer.setChartValuesSpacing(DensityUtil.dip2px(getActivity(), 15));//显示的点的值与图的距离  
		//xySeriesRenderer.setFillBelowLine(true);//填充下方
		
		//xySeriesRenderer.setLineWidth(2.0f);//设置线的宽度
		xySeriesRenderer.setDisplayChartValuesDistance(DensityUtil.dip2px(getActivity(), 15));//必须要设置,不然有的值在低分辨率上不显示
		
		//xySeriesRenderer.setDisplayBoundingPoints(false);		
		//xySeriesRenderer.setPointStrokeWidth(DensityUtil.sp2px(getActivity(), 5));//设置点的线宽度
        xySeriesRenderer.setColor(0xFF00C8FF);//设置点线颜色
        xySeriesRenderer.setDisplayChartValues(true);//设置显示值   
        xySeriesRenderer.setChartValuesTextSize(DensityUtil.sp2px(getActivity(), 13));//设置点值字体大小
        
        
        DecimalFormat df = (DecimalFormat)NumberFormat.getInstance();
        df.applyPattern("####'卡'");
        xySeriesRenderer.setChartValuesFormat(df);
        
        
        seriesRenderer.addSeriesRenderer(xySeriesRenderer);
        //seriesRenderer.clearYTextLabels();
        return seriesRenderer;
	}

	private XYMultipleSeriesDataset getDataSet() {
		XYMultipleSeriesDataset seriesDataset=new XYMultipleSeriesDataset();
		XYSeries xySeries=new XYSeries("");
		/*final Message msg = handler.obtainMessage();
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for(int i =0;i<7;i++)
				{
					try {
						Thread.sleep(300);
						msg.obj = energy[i];
						handler.sendMessage(msg);
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();*/
		xySeries.add(1, 36);
		xySeries.add(2, 30);
		xySeries.add(3, 27);
		xySeries.add(4, 29);
		xySeries.add(5, 34);
		xySeries.add(6, 28);
		xySeries.add(7, 33);
		seriesDataset.addSeries(xySeries);
		return seriesDataset;
	}
	
	public class MyHandler extends Handler
	{
		@Override
		public void handleMessage(Message msg) {		
			super.handleMessage(msg);
		}		
	}
}
