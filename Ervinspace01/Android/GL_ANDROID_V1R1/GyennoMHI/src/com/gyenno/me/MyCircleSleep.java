package com.gyenno.me;

import com.gyenno.R;
import com.gyenno.R.color;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class MyCircleSleep extends View {

	private Context context;
	int innerCircle_blue;
	int ringWidth_blue;
	
	int innerCircle_grey;
	int ringWidth_grey;
	
	int innerCircle_red;
	int ringWidth_red;
	public MyCircleSleep(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	public MyCircleSleep(Context context,AttributeSet attrs)
	{
		super(context, attrs); 
		this.context = context;
		innerCircle_blue = DensityUtil.dip2px(context, 100); //设置内圆半径  
	    ringWidth_blue = DensityUtil.dip2px(context, 55); //设置圆环宽度  
	    
	    innerCircle_grey = DensityUtil.dip2px(context, 95);
	    ringWidth_grey = DensityUtil.dip2px(context, 30);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		//画百分比
		Paint pencil = new Paint();
		pencil.setColor(Color.WHITE);
		pencil.setTextSize(DensityUtil.sp2px(context, 13));
		
		int center = getWidth()/2;  
       
        //蓝色圆环
        Paint pcircle = new Paint();
        pcircle.setAntiAlias(true);
        pcircle.setStrokeWidth(0);
        pcircle.setStyle(Paint.Style.FILL);
        pcircle.setColor(getResources().getColor(R.color.blue1));
        RectF oval = new RectF(center-innerCircle_blue, center-innerCircle_blue-innerCircle_blue/2, center+innerCircle_blue, center+innerCircle_blue-innerCircle_blue/2);// 设置个新的长方形，扫描测量  
        canvas.drawArc(oval, 60, 240, true, pcircle); 
        
        Paint pcircle1 = new Paint();
        pcircle1.setAntiAlias(true);
        pcircle1.setStrokeWidth(1);
        pcircle1.setStyle(Paint.Style.FILL);
        pcircle1.setColor(getResources().getColor(R.color.background));
        RectF oval1 = new RectF(center-innerCircle_blue+ringWidth_blue, center-innerCircle_blue+ringWidth_blue-innerCircle_blue/2, center+innerCircle_blue-ringWidth_blue, center+innerCircle_blue-ringWidth_blue-innerCircle_blue/2);
        canvas.drawArc(oval1, 60, 240, true, pcircle1);
        	//百分比
        canvas.drawText("57%", center-innerCircle_blue+ringWidth_blue/4, center-innerCircle_blue+ringWidth_blue, pencil);
        
       
        //灰色圆环
        Paint pcircle2 = new Paint();
        pcircle2.setAntiAlias(true);
        pcircle2.setStrokeWidth(0);
        pcircle2.setStyle(Paint.Style.FILL);
        pcircle2.setColor(getResources().getColor(R.color.grey));
        RectF oval2 = new RectF(center-innerCircle_grey, center-innerCircle_grey-innerCircle_grey/2, center+innerCircle_grey, center+innerCircle_grey-innerCircle_grey/2);// 设置个新的长方形，扫描测量  
        canvas.drawArc(oval2, 298, 122, true, pcircle2); 
        //  canvas.drawArc(oval2, 250, 360, true, pcircle2); 
        
        Paint pcircle3 = new Paint();
        pcircle3.setAntiAlias(true);
        pcircle3.setStrokeWidth(1);
        pcircle3.setStyle(Paint.Style.FILL);
        pcircle3.setColor(getResources().getColor(R.color.background));
        RectF oval3 = new RectF(center-innerCircle_grey+ringWidth_grey, center-innerCircle_grey+ringWidth_grey-innerCircle_grey/2, center+innerCircle_grey-ringWidth_grey, center+innerCircle_grey-ringWidth_grey-innerCircle_grey/2);
        canvas.drawArc(oval3, 298, 122, true, pcircle3);
        
        	//百分比
        canvas.drawText("43%", center+innerCircle_grey-ringWidth_grey, center-innerCircle_grey+ringWidth_grey, pencil);
        
        
        //画背景圆覆盖半径线
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(getResources().getColor(R.color.background));
        //paint.setColor(Color.RED);
        canvas.drawCircle(center, center-innerCircle_blue/2, ringWidth_blue, paint);
        
        //中间文字
        Paint pencil2 = new Paint();
        pencil2.setColor(getResources().getColor(color.menu_normal));
        pencil2.setTextSize(DensityUtil.sp2px(context, 13));
        float x1 = center-ringWidth_blue/3;
        float y1 = center-innerCircle_blue/2-ringWidth_blue/3;
        canvas.drawText("今天睡眠", x1, y1, pencil2);
        pencil2.setTextSize(DensityUtil.sp2px(context, 20));
        float x2 = center-ringWidth_blue/2;
        float y2 = center-innerCircle_blue/2+ringWidth_blue/4;
        canvas.drawText("7.5小时", x2, y2, pencil2);
        
        
	}
	

	/*public static int dip2px(Context context, float dpValue) {  
        final float scale = context.getResources().getDisplayMetrics().density;  
        return (int) (dpValue * scale + 0.5f);  
    }  */
}
