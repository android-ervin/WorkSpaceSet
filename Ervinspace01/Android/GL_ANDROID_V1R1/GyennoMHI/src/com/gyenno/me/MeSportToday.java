package com.gyenno.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gyenno.R;

public class MeSportToday extends Fragment {
	public static final String TITLE = "Today";
	
	private ImageView iv_turnnext;
	private TextView tv_today;
	
	private View mview;
	private LinearLayout ll_tdone;
	private LinearLayout ll_tdtwo;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_me_youyang_today, container, false);
		initView(rootView);	
		return rootView;
	}

	private void initView(View rootview) {
		iv_turnnext = (ImageView)rootview.findViewById(R.id.iv_turnnext);
		tv_today = (TextView)rootview.findViewById(R.id.tv_today);
		
		ll_tdone = (LinearLayout) rootview.findViewById(R.id.ll_todayone);
		ll_tdtwo = (LinearLayout) rootview.findViewById(R.id.ll_todaytwo);
		
		iv_turnnext.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(0==ll_tdtwo.getVisibility())
				{
					ll_tdtwo.setVisibility(View.INVISIBLE);
					ll_tdone.setVisibility(View.VISIBLE);
				}
				else
				{
					ll_tdtwo.setVisibility(View.VISIBLE);
					ll_tdone.setVisibility(View.INVISIBLE);
				}
				
			}
		});
	}
}
