package com.gyenno.me;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.gyenno.R;
import com.gyenno.R.color;

public class CharacterView extends View {

	public CharacterView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(color.gyennostyle);
		// 设置字体大小
		paint.setTextSize(50);

		// 让画出的图形是空心的
		paint.setStyle(Paint.Style.STROKE);
		// 设置画出的线的 粗细程度
		paint.setStrokeWidth(5);
		// 画出一根线
		canvas.drawLine(150,250, 150, 900, paint);
		
		//属性标记
		canvas.drawText("55岁", 160, 340, paint);
		canvas.drawLine(150,350, 350, 350, paint);
		
		canvas.drawText("女", 160, 440, paint);
		canvas.drawLine(150,450, 350, 450, paint);
		
		canvas.drawText("50KG", 160, 540, paint);
		canvas.drawLine(150,550, 350, 550, paint);
		
		canvas.drawText("158CM", 160, 640, paint);
		canvas.drawLine(150,650, 350, 650, paint);
		// 画圆
		canvas.drawCircle(150, 150, 100, paint);
		// 绘制图片
		canvas.drawBitmap(BitmapFactory.decodeResource(getResources(),
				R.drawable.header), 50, 50, paint);

		canvas.drawBitmap(BitmapFactory.decodeResource(getResources(),
				R.drawable.people), 350, 100, paint);
		
		super.onDraw(canvas);
	}

}
