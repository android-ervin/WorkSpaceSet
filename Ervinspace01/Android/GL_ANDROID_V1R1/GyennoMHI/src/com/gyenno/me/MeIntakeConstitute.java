package com.gyenno.me;

import com.gyenno.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class MeIntakeConstitute extends Fragment implements OnClickListener{

	private ImageView iv_turnnext;
	private LinearLayout ll_goucheng1;
	private LinearLayout ll_goucheng2;
	private LinearLayout ll_goucheng3;
	private LinearLayout ll_goucheng4;
	private Button btn_ok;
	private Button btn_ok2;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_me_sheru_constitute, container, false);
		initView(rootView);	
		return rootView;
	}

	private void initView(View rootView) {
		iv_turnnext = (ImageView) rootView.findViewById(R.id.iv_turnnext);
		ll_goucheng1 = (LinearLayout) rootView.findViewById(R.id.goucheng1);
		ll_goucheng2 = (LinearLayout) rootView.findViewById(R.id.goucheng2);
		ll_goucheng3 = (LinearLayout) rootView.findViewById(R.id.goucheng3);
		ll_goucheng4 = (LinearLayout) rootView.findViewById(R.id.goucheng4);
		btn_ok = (Button) rootView.findViewById(R.id.sheru_ok);
		btn_ok2 = (Button) rootView.findViewById(R.id.sheru_ok2);
		iv_turnnext.setVisibility(View.INVISIBLE);
		
		btn_ok.setOnClickListener(this);
		btn_ok2.setOnClickListener(this);
		iv_turnnext.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.sheru_ok:
			intent = new Intent(getActivity(),IntakeDialogActivity.class);
			//startActivity(intent);
			startActivityForResult(intent, 0);
			break;
		case R.id.sheru_ok2:
			iv_turnnext.setVisibility(View.VISIBLE);
			ll_goucheng2.setVisibility(View.GONE);
			ll_goucheng4.setVisibility(View.VISIBLE);		
			
		case R.id.iv_turnnext:
			if(ll_goucheng4.getVisibility()==0)
			{
				ll_goucheng4.setVisibility(View.GONE);
				ll_goucheng3.setVisibility(View.VISIBLE);
				
			}else{
				ll_goucheng3.setVisibility(View.GONE);
				ll_goucheng4.setVisibility(View.VISIBLE);
			}
			break;
		default:
			break;
		}
		
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case 1: //�����ok
			ll_goucheng1.setVisibility(View.INVISIBLE);
			ll_goucheng2.setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
