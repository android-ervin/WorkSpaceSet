package com.gyenno.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gyenno.R;

public class MeSleepConstitute extends Fragment {

	private GIFView gifview;
	private Button btn_ok;
	private LayoutInflater inflater;
	private LinearLayout ll_sleep;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.inflater = inflater;
		View rootView = inflater.inflate(R.layout.fragment_me_sleep_constitute, null);
		
		initview(rootView);
		return rootView;
	}

	private void initview(View rootView) {
		// TODO Auto-generated method stub
		gifview = (GIFView) rootView.findViewById(R.id.gifview);
		ll_sleep = (LinearLayout) rootView.findViewById(R.id.ll_sleep);
		/*LinearLayout.LayoutParams linearParams =(LinearLayout.LayoutParams) gifview.getLayoutParams();
		linearParams.width = DensityUtil.px2dip(getActivity(), 248*3);
		linearParams.height = DensityUtil.px2dip(getActivity(), 144*3);*/
		btn_ok = (Button) rootView.findViewById(R.id.sleep_ok);
		btn_ok.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				ll_sleep.removeAllViews();
				View secondView = inflater.inflate(R.layout.fragment_me_sleep_constitute2, null);
				ll_sleep.addView(secondView);
			}
		});
	}
}
