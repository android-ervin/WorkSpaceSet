package com.gyenno.me;

import com.gyenno.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.webkit.WebView.FindListener;
import android.widget.ImageView;


public class MeIntakeSummary extends Fragment{

	private ImageView iv_yestoday;
	private ImageView iv_beforyestoday;
	private View ll_yestoday;
	private View ll_beforyestoday;
	private View iv_win;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_me_sheru_summary, null);
		initView(rootView);
		return rootView;
	}

	private void initView(View rootView) {
		iv_beforyestoday = (ImageView) rootView.findViewById(R.id.iv_beforeyestoday);
		iv_yestoday = (ImageView) rootView.findViewById(R.id.iv_yestoday);	
		ll_yestoday = rootView.findViewById(R.id.ll_yestoday);
		ll_beforyestoday = rootView.findViewById(R.id.ll_beforeyestoday);
		iv_win = rootView.findViewById(R.id.iv_win);
		
		ImageAnimationLeft(iv_yestoday);
		ImageAnimationRight(iv_beforyestoday);
		//ImageAnimationRight(iv_win);
		
		
	}
	
	private void ImageAnimationLeft(View view)
	{
		AnimationSet set = new AnimationSet(true);  //������ֵ��
	//	ScaleAnimation sa = new ScaleAnimation(0f, 1.0f, 0f, 1.0f, 0.0f, 0.0f);
		ScaleAnimation sa = new ScaleAnimation(0f, 1.0f, 0f, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f);
		sa.setDuration(3000);
		set.addAnimation(sa);
		AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
		aa.setDuration(3000);
		set.addAnimation(aa);
		
		set.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
				aa.setDuration(2000);
				ll_yestoday.setVisibility(View.VISIBLE);
				ll_yestoday.startAnimation(aa);
				ll_beforyestoday.setVisibility(View.VISIBLE);		
				ll_beforyestoday.startAnimation(aa);
				iv_win.setVisibility(View.VISIBLE);	
				iv_win.startAnimation(aa);
			}
		});
		view.setAnimation(set);
	}
	
	private void ImageAnimationRight(View view)
	{
		AnimationSet set = new AnimationSet(true); // ������ֵ��
		ScaleAnimation sa = new ScaleAnimation(0f, 1.0f, 0f, 1.0f,
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
				1.0f);
		sa.setDuration(3000);
		set.addAnimation(sa);
		AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
		aa.setDuration(3000);
		set.addAnimation(aa);
		view.setAnimation(set);
	}
}
