package com.gyenno.me;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.gyenno.R;

public class HealthModelActivity extends Activity {
	private ImageView iv_back;
	private TextView name;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_healthmodel);
		
		Intent intent = getIntent();
		
		iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		name = (TextView) findViewById(R.id.tv_friendname);
		name.setText(intent.getStringExtra("name"));
		
		
	}

}
