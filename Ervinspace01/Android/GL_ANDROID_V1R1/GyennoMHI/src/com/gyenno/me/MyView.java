package com.gyenno.me;

import java.util.ArrayList; 
import java.util.List; 

import com.gyenno.R;
                                                               
import android.content.Context; 
import android.graphics.Canvas; 
import android.graphics.Color; 
import android.graphics.Paint; 
import android.graphics.Path;
import android.graphics.Point;
import android.os.Handler; 
import android.os.Message; 
import android.util.AttributeSet; 
import android.view.View; 
import android.view.WindowManager;
                                                                 
public class MyView extends View{ 
	//起始坐标
    private int XPoint ; 
    private int YPoint ; 
    
    private int XScale = 90;  //刻度长度 
    
    private Point endpoint;
    private List<Point> data = new ArrayList<Point>(); 
                                                            
    private int window_width;
    private int window_height;
    private Context context;
    
    private Handler handler = new Handler(){ 
        public void handleMessage(Message msg) { 
            if(msg.what == 0x1234){ 
                MyView.this.invalidate(); //调用onDraw函数
            } 
        }; 
    }; 
    public MyView(Context context, AttributeSet attrs) { 
        super(context, attrs); 
      //获取屏幕宽高         (像素)    
        WindowManager wm =  (WindowManager) context.getSystemService("window");
        window_width = wm.getDefaultDisplay().getWidth();
        window_height = wm.getDefaultDisplay().getHeight();
        
        this.context = context;
       
        
        
    } 
                                                                     
    @Override 
    protected void onDraw(Canvas canvas) { 
    	
		int view_width = this.getWidth();
		int view_height = this.getHeight();
		
		int width =  DensityUtil.px2dip(context, view_width); //变为dp;
        int height = DensityUtil.px2dip(context, view_height);
        //int scalex = DensityUtil.dip2px(context, XScale);
        XPoint = (width-20)/6;
        YPoint = height;
		System.out.println("------------>" + width);
		System.out.println("------------>" + height);
		System.out.println("------XPoint------>" + XPoint);
		System.out.println("------YPoint------>" + YPoint);
    	
        super.onDraw(canvas); 
        Paint paint1 = new Paint(); 
        paint1.setStyle(Paint.Style.STROKE); 
        paint1.setAntiAlias(true); //去锯齿 
        paint1.setTextSize(20);
        paint1.setColor(getResources().getColor(R.color.week_line));
       // paint1.setColor(Color.BLACK);
        
        //绘长度图
        for(int i=0;i<7;i++)   
        {
        	//canvas.drawLine(XPoint+(i*XPoint+20), YPoint, XPoint+(i*XPoint+20), YPoint-400, paint);
        	canvas.drawLine(XPoint+(i*XScale), YPoint+245, XPoint+(i*XScale), YPoint-i*30, paint1);
        	endpoint = new Point();
        	endpoint.x = XPoint+(i*XScale);
        	endpoint.y = YPoint-i*30;
        	data.add(endpoint);        	
        }
        System.out.println("total point--->" + data.size());
        for(int i=0;i<7;i++)
        {
        	System.out.println(data.get(i).x+","+data.get(i).y);
        }
        
        //绘折现图
        for(int i=1;i<data.size();i++)
        {
        	
        	canvas.drawLine(data.get(i-1).x, data.get(i-1).y, data.get(i).x, data.get(i).y, paint1);
        }
        //画圆圈
        Paint paint = new Paint(); 
        paint1.setStyle(Paint.Style.FILL);
        paint.setTextSize(20);
        paint.setColor(Color.BLACK);
        for(int i=0;i<data.size();i++)
        {
        	canvas.drawCircle(data.get(i).x, data.get(i).y, 8, paint1);
        	canvas.drawText("1280卡", data.get(i).x-25, data.get(i).y-30, paint);
        }
        
        //手绘填充效果图
      /*  Paint paint = new Paint(); 
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(getResources().getColor(R.color.week_line));
        Path path = new Path();
        //path.moveTo(data.get(0).x, data.get(0).y);
        path.moveTo(XPoint, YPoint+245);
        
        for(int i=0;i<data.size();i++)
        {
        	canvas.drawCircle(data.get(i).x, data.get(i).y, 8, paint);
        	canvas.drawText("1280卡", data.get(i).x-25, data.get(i).y-30, paint1);
        	path.lineTo(data.get(i).x, data.get(i).y);      	
        }
        path.lineTo(data.get(6).x, YPoint+245);
        canvas.drawPath(path, paint);*/
        
    } 
}
