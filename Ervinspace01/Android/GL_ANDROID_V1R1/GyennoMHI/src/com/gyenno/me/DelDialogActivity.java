package com.gyenno.me;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gyenno.R;


public class DelDialogActivity extends Activity {

	private ImageView iv_del_result;
	private TextView tv_del_content;
	private Button btn_ok;
	private Button btn_cancel;
	
	private LinearLayout ll_del;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.customdialog);
		
		/*ll_del = (LinearLayout) findViewById(R.id.ll_del);
		ll_del.setLayoutParams(new LayoutParams((int) (d.getWidth() * 0.7), (int) (d.getWidth() * 0.7));*/
		
		//设置activity显示的宽高
		WindowManager m = getWindowManager();    
	    Display d = m.getDefaultDisplay();  //为获取屏幕宽、高    
	    LayoutParams p = getWindow().getAttributes();  //获取对话框当前的参数值    
	    p.height = (int) (d.getHeight() * 0.6);   //高度设置为屏幕的1.0   
	    p.width = (int) (d.getWidth() * 0.84);    //宽度设置为屏幕的0.8   
	    //p.alpha = 1.0f;      //设置本身透明度  
	    //p.dimAmount = 0.0f;      //设置黑暗度  
	    getWindow().setAttributes(p);   

		
		iv_del_result = (ImageView) findViewById(R.id.iv_del_result);
		tv_del_content = (TextView) findViewById(R.id.tv_del_content);
		btn_ok = (Button) findViewById(R.id.btn_del_ok);
		btn_cancel = (Button) findViewById(R.id.btn_del_cancel);
		
		btn_cancel.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				finish();				
			}
		});
	}
	
		
	
	
}
