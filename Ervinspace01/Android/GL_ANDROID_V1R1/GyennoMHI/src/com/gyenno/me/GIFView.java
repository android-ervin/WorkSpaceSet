package com.gyenno.me;

import java.io.InputStream;

import com.gyenno.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.View;

public class GIFView extends View {

	private Movie mMovie;
	private long movieStart;

	public GIFView(Context context) {
	    super(context);
	    initializeView();
	}

	public GIFView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    initializeView();
	}

	public GIFView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    initializeView();
	}

	private void initializeView() {
	    InputStream is = getContext().getResources().openRawResource(
	            R.drawable.sleep);
	    mMovie = Movie.decodeStream(is);
	    setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	}

	protected void onDraw(Canvas canvas) {
	    canvas.drawColor(Color.TRANSPARENT);
	    super.onDraw(canvas);
	    long now = android.os.SystemClock.uptimeMillis();

	    if (movieStart == 0) {
	        movieStart = (int) now;
	    }
	    if (mMovie != null) {
	        int relTime = (int) ((now - movieStart) % mMovie.duration());
	        mMovie.setTime(relTime);
	        /*System.out.println(mMovie.height());
	        System.out.println(mMovie.width());*/
	        mMovie.draw(canvas, getWidth()/2-mMovie.width()/2 , getHeight()/2-mMovie.height()/2);	               
	        this.invalidate();
	    }
	}
}
