package com.gyenno.me;


import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.gyenno.R;



public class SportActivity extends FragmentActivity implements OnClickListener,
		OnPageChangeListener {

	private ViewPager mViewPager;
	private List<Fragment> mTabs = new ArrayList<Fragment>();
	private String[] mTitles = new String[] { "First Fragment !",
			"Second Fragment !", "Third Fragment !" };
	private FragmentPagerAdapter mAdapter;

	private List<View> mTabIndicators = new ArrayList<View>();
	
	private TextView tv_today;
	private TextView tv_week;
	private TextView tv_month;
	
	ImageView iv_share ;
	ImageView iv_turnnext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_youyang);
		
		//setOverflowButtonAlways();
		//getActionBar().setDisplayShowHomeEnabled(true);
		
		//ActionBar actionBar = getActionBar(); 
		//actionBar.hide();

		initView();
		initDatas();
		mViewPager.setAdapter(mAdapter);
		//initEvent();

		setCurrentTab(0);
		//设置缓存
		mViewPager.setOffscreenPageLimit(0);
		
	}

	private void setCurrentTab(int j) {
		for (int i = 0; i < 3; i++) {		
			if (i == j) //选择了当前页
			{
				mTabIndicators.get(i).setSelected(true);
				mTabIndicators.get(i).setBackgroundColor(getResources().getColor(R.color.menu_normal));
			} else {
				mTabIndicators.get(i).setSelected(false);
				mTabIndicators.get(i).setBackgroundColor(Color.TRANSPARENT);
			}
		}

		
	}

	/*private void initEvent() {
		mViewPager.setOnPageChangeListener(this);
		mViewPager.setOnTouchListener(new OnTouchListener() {
			//屏蔽viewPager的滑动事件
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
		});
	}*/

	private void initDatas() {
		MeSportToday meSportToday = new MeSportToday();
		/*Bundle bundle1 = new Bundle();
		bundle1.putString(MeSportToday.TITLE, mTitles[0]);
		meSportToday.setArguments(bundle1);*/
		mTabs.add(meSportToday);
		
		MeSportWeek meSportWeek = new MeSportWeek();
		/*Bundle bundle2 = new Bundle();
		bundle2.putString(MeSportWeek.TITLE, mTitles[1]);
		meSportWeek.setArguments(bundle2);*/
		mTabs.add(meSportWeek);
		
		MeSportMonth meSportMonth = new MeSportMonth();
		/*Bundle bundle3 = new Bundle();
		bundle3.putString(MeSportMonth.TITLE, mTitles[2]);
		meSportMonth.setArguments(bundle3);*/
		mTabs.add(meSportMonth);
		
		//初始化viewpager适配器
		mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
			
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return mTabs.size();
			}
			
			@Override
			public Fragment getItem(int arg0) {
				// TODO Auto-generated method stub
				return mTabs.get(arg0);
			}
		};
		
	}

	private void initView() {
		mViewPager = (ViewPager) findViewById(R.id.youyang_viewpager);
		mViewPager.setOnPageChangeListener(this); //设置页面监听事件
		tv_today = (TextView) findViewById(R.id.tv_today);
		mTabIndicators.add(tv_today);
		tv_week = (TextView) findViewById(R.id.tv_week);
		mTabIndicators.add(tv_week);
		tv_month = (TextView) findViewById(R.id.tv_month);
		mTabIndicators.add(tv_month);
		
		ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);
		
		iv_share = (ImageView)findViewById(R.id.iv_share);
		
		
		tv_today.setOnClickListener(this);
		tv_week.setOnClickListener(this);
		tv_month.setOnClickListener(this);
		
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int page) {
		// TODO Auto-generated method stub
		System.out.println("---->"+page);
		setCurrentTab(page);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_today:
			setCurrentTab(0);
			iv_share.setVisibility(View.INVISIBLE);
			mViewPager.setCurrentItem(0, false);
			break;
		case R.id.tv_week:
			setCurrentTab(1);
			iv_share.setVisibility(View.VISIBLE);
			mViewPager.setCurrentItem(1, false);
			break;
		case R.id.tv_month:
			setCurrentTab(2);
			iv_share.setVisibility(View.VISIBLE);
			mViewPager.setCurrentItem(2, false);
			break;
		
		case R.id.iv_back:
			finish();
			break;
		}
	}
	
	/*private void setOverflowButtonAlways() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKey = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			menuKey.setAccessible(true);
			menuKey.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
}
