package com.gyenno.me;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import com.gyenno.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MeSportWeek extends Fragment {
	public static final String TITLE = "Week";
	
	private ImageView iv_turnnext;
	private TextView tv_week;
	
	private ListView lv_friends;
	private int touxiang[]={R.drawable.touxiang1,R.drawable.touxiang2,R.drawable.touxiang3,R.drawable.touxiang4,
			R.drawable.touxiang5,R.drawable.touxiang1,R.drawable.touxiang3};
	private String name[]={"Sunny","��","Lisa","Jakey","Tom","Ervin","Emy"};
	
	private MyAdapter madapter;
	private MeSportWeekAdapter mWeekAdapter;
	
	//private ViewPager mViewPager;
	//private List<Fragment> Fragmentlist = new ArrayList<Fragment>();
	private View mview;
	private LinearLayout ll_drawline;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_me_youyang_week, container, false);
		initView(rootView);	
		//initData();
		//mViewPager.setAdapter(mWeekAdapter);
		//mViewPager.setCurrentItem(0, true);
		return rootView;
	}
	/*private void initData() {
		MeSportWeekOne one = new MeSportWeekOne();
		Fragmentlist.add(one);
		MeSportWeekTwo two = new MeSportWeekTwo();
		Fragmentlist.add(two);	
	}*/
	private void initView(View rootview) {
		
		//mViewPager = (ViewPager) rootview.findViewById(R.id.week_pager);
		mview = rootview.findViewById(R.id.draw_view);
		ll_drawline = (LinearLayout) rootview.findViewById(R.id.ll_drawline);
		
		iv_turnnext = (ImageView)rootview.findViewById(R.id.iv_turnnext);
		tv_week = (TextView)rootview.findViewById(R.id.tv_week);		
		
		lv_friends = (ListView) rootview.findViewById(R.id.lv_friends);
		if(madapter == null)
			madapter = new MyAdapter();
		lv_friends.setAdapter(madapter);
		
		iv_turnnext.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), "�л�ҳ��", 0).show();
				if(0==lv_friends.getVisibility())
				{
					lv_friends.setVisibility(View.INVISIBLE);
					ll_drawline.setVisibility(View.VISIBLE);
				}
				else
				{
					lv_friends.setVisibility(View.VISIBLE);
					ll_drawline.setVisibility(View.INVISIBLE);
				}
			}
		});
	}

	public class MeSportWeekAdapter extends FragmentPagerAdapter
	{

		public MeSportWeekAdapter(FragmentManager fm) {
			super(fm);
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int arg0) {
			// TODO Auto-generated method stub
			//return Fragmentlist.get(arg0);
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			//return Fragmentlist.size();
			return 0;
		}
		
	}
	
	class MyAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return touxiang.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view ;
			ViewHolder holder;
			LayoutInflater layoutInflater  = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(convertView == null)
			{
				view = layoutInflater.inflate(R.layout.weeklist_item, null);
				holder = new ViewHolder();
				holder.iv_icon = (ImageView) view.findViewById(R.id.week_icon);
				holder.tv_name = (TextView) view.findViewById(R.id.week_name);
				holder.tv_calsize = view.findViewById(R.id.week_calsize);
				holder.tv_calnumber = (TextView) view.findViewById(R.id.week_calnumber);
				view.setTag(holder);
			//view = View.inflate(getActivity(), R.layout.weeklist_item, null);
			}
			else
			{
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}
							
			holder.iv_icon.setBackgroundResource(touxiang[position]);
			holder.tv_name.setText(name[position]);
			if(position==1)
			{
				holder.tv_calsize.setBackgroundColor(getResources().getColor(R.color.menu_normal));
				LayoutParams lp = holder.tv_calsize.getLayoutParams();
				lp.width = 320;//160px = 80dp
				holder.tv_calsize.setLayoutParams(lp);
			}
			//tv_calnumber.setText(text)
			return view;
		}	
	}
	
	static class ViewHolder{
		ImageView iv_icon;
		TextView tv_name;
		View tv_calsize;
		TextView tv_calnumber;
	}
}
