package com.gyenno.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;


import com.gyenno.R;

public class MeSleepSummary extends Fragment {

	FrameLayout fl_sleep;
	private LayoutInflater inflater;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_me_sleep_summary, null);
		this.inflater = inflater;
		initView(rootView);
		
		System.out.println("------>"+fl_sleep.getHeight());
		return rootView;
	}

	private void initView(View rootView) {
		
		fl_sleep = (FrameLayout) rootView.findViewById(R.id.fl_sleep);
		View ll_view = inflater.inflate(R.layout.sleepsummaryview, null);
		LinearLayout ll_sleep = (LinearLayout) ll_view.findViewById(R.id.ll_sleep);
		LinearLayout.LayoutParams linearParams =(LinearLayout.LayoutParams) ll_sleep.getLayoutParams();		
		linearParams.topMargin = DensityUtil.dip2px(getActivity(), 240);
		//linearParams.bottomMargin = DensityUtil.px2dip(getActivity(), 150);
		fl_sleep.addView(ll_view, linearParams);
		
	}
}
