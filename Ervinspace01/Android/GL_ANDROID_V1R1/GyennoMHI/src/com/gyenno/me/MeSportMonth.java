package com.gyenno.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gyenno.R;
import com.gyenno.utils.CounterView;

public class MeSportMonth extends Fragment {
	public static final String TITLE = "Month";
	private ImageView iv;
	private CounterView counterView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_me_youyang_month, container, false);
		initView(rootView);	
		return rootView;
	}
	private void initView(View rootView) {
		// TODO Auto-generated method stub
		//iv = (ImageView) rootView.findViewById(R.id.ren);
		counterView = (CounterView) rootView.findViewById(R.id.counter);
		counterView.setAutoStart(false);
		counterView.setStartValue(0f);
		counterView.setEndValue(78f);
		counterView.setIncrement(1f);
		counterView.setTimeInterval(30);
		counterView.setSuffix("%");
/*		counter.setPrefix("");
		counter.setText("");*/
		counterView.start();
	}
}
