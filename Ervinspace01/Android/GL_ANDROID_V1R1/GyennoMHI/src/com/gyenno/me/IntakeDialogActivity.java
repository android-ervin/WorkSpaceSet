package com.gyenno.me;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gyenno.R;


public class IntakeDialogActivity extends Activity implements OnClickListener{


	private Button btn_ok;
	private Button btn_cancel;
	
	private ImageView iv1;
	private ImageView iv2;
	private ImageView iv3;
	private ImageView iv4;
	
	private final int RESULT_OK=1;
	private final int RESULT_CANCEL=2;
	
	private static boolean flag=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.sheru_customdialog);
		
		/*ll_del = (LinearLayout) findViewById(R.id.ll_del);
		ll_del.setLayoutParams(new LayoutParams((int) (d.getWidth() * 0.7), (int) (d.getWidth() * 0.7));*/
		
		//设置activity显示的宽高
		WindowManager m = getWindowManager();    
	    Display d = m.getDefaultDisplay();  //为获取屏幕宽、高    
	    LayoutParams p = getWindow().getAttributes();  //获取对话框当前的参数值    
	    p.height = (int) (d.getHeight() * 0.52);   //高度设置为屏幕的1.0   
	    p.width = (int) (d.getWidth() * 0.84);    //宽度设置为屏幕的0.8   
	    //p.alpha = 1.0f;      //设置本身透明度  
	    //p.dimAmount = 0.0f;      //设置黑暗度  
	    getWindow().setAttributes(p);   

		
		btn_ok = (Button) findViewById(R.id.btn_del_ok);
		btn_cancel = (Button) findViewById(R.id.btn_del_cancel);
		btn_cancel.setOnClickListener(this);
		btn_ok.setOnClickListener(this);
		
		iv1 = (ImageView) findViewById(R.id.iv1);
		iv2 = (ImageView) findViewById(R.id.iv2);
		iv3 = (ImageView) findViewById(R.id.iv3);
		iv4 = (ImageView) findViewById(R.id.iv4);
		//iv1.setImageResource(R.drawable.image_meichi_selector);
		iv1.setOnClickListener(this);
		iv2.setOnClickListener(this);
		iv3.setOnClickListener(this);
		iv4.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_del_cancel:
			setResult(RESULT_CANCEL);  
			flag = false;
			finish();
			break;
		case R.id.btn_del_ok:
			if(flag)
			{
				setResult(RESULT_OK, null);
				flag = false;
				finish();
			}
			break;
		case R.id.iv1:
			iv1.setImageResource(R.drawable.meichi_big);
			iv2.setImageResource(R.drawable.meichibao);
			iv3.setImageResource(R.drawable.zhenghao);
			iv4.setImageResource(R.drawable.chiduole);
			flag = true;
			break;
		case R.id.iv2:
			iv2.setImageResource(R.drawable.meichibao_big);
			iv1.setImageResource(R.drawable.meichi);
			iv3.setImageResource(R.drawable.zhenghao);
			iv4.setImageResource(R.drawable.chiduole);
			flag = true;
			break;
		case R.id.iv3:
			iv3.setImageResource(R.drawable.zhenghao_big);
			iv2.setImageResource(R.drawable.meichibao);
			iv1.setImageResource(R.drawable.meichi);
			iv4.setImageResource(R.drawable.chiduole);
			flag = true;
			break;
		case R.id.iv4:
			iv4.setImageResource(R.drawable.chiduole_big);
			iv1.setImageResource(R.drawable.meichi);
			iv2.setImageResource(R.drawable.meichibao);
			iv3.setImageResource(R.drawable.zhenghao);
			flag = true;
			break;
		default:
			break;
		}
		
	}
	
		
	
	
}
