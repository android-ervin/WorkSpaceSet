package com.gyenno.utils;



import java.text.DecimalFormat;

/**
 * Created by prem on 10/28/14.
 *
 * Formats the text to a comma separated decimal with 2dp
 */
public class CommaSeparatedDecimalFormatter implements Formatter {

    private final DecimalFormat format = new DecimalFormat("###");

    @Override
    public  String format(String prefix, String suffix, float value) {
        return prefix + format.format(value) + suffix;
    }
}
