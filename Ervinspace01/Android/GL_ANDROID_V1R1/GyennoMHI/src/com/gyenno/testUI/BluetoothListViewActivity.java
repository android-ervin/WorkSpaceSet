package com.gyenno.testUI;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gyenno.R;
import com.gyenno.application.InitializationAppData;
import com.gyenno.bluetooth.MyBluetoothManager;

public class BluetoothListViewActivity extends Activity implements
		LeScanCallback {

	List<String> data = new ArrayList<String>();
	List<String> dataMap = new ArrayList<String>();
	List<BluetoothDevice> dataDevice = new ArrayList<BluetoothDevice>();
	ListView listView = null;
	private ArrayAdapter<String> adapter = null;

	public static BluetoothDevice device = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetoothlist);

		listView = (ListView) findViewById(R.id.mylist);
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, data);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				device = dataDevice.get(arg2);

				Intent intent = new Intent(BluetoothListViewActivity.this,
						BluetoothStateViewActivity.class);

				startActivity(intent);

			}
		});

		scanBluetooth();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void scanBluetooth() {

		MyBluetoothManager bluetoothManager = InitializationAppData
				.getBluetooth();

		bluetoothManager.scan(true, this);

	}

	@Override
	public void onLeScan(final BluetoothDevice device, int arg1, byte[] arg2) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				int index =dataMap.indexOf(device.getAddress());
				
				if (index != -1) {
					// dataMap.add(index, device.getAddress());
					// dataMap.remove(index + 1);
				} else {
					data.add(device.getAddress() + "----------"
							+ device.getName());
					dataMap.add(device.getAddress());
					dataDevice.add(device);
					adapter.notifyDataSetChanged();
				}

			}
		});
	}

}
