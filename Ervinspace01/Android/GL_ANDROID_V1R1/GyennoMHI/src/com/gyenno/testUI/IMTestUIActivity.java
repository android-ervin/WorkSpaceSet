package com.gyenno.testUI;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.gyenno.R;
import com.gyenno.im.ConnectionService;
import com.gyenno.tools.Utils;

public class IMTestUIActivity extends ListActivity {

	private ConnectionService mService = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_expandable_list_item_1, getData()));

		Intent intent = new Intent(getApplicationContext(),
				ConnectionService.class);
		startService(intent);

		bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	ServiceConnection mServiceConnection = new ServiceConnection() {
		public void onServiceDisconnected(ComponentName arg0) {
			mService = null;
			Utils.log("service连接断开");
		}

		// 打开服务
		public void onServiceConnected(ComponentName arg0, IBinder binder) {
			mService = ((ConnectionService.LocalBinder) binder).getService();
			Utils.log("service已打开");
		}
	};

	private List<String> getData() {

		List<String> data = new ArrayList<String>();
		data.add("登陆主账号");
		data.add("登陆client账号");
		data.add("获取列表");
		data.add("添加好友");
		data.add("发送信息");
		return data;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		switch (position) {
		case 0:
			mService.login();
			break;
		case 1:
			mService.loginClient();
			break;
		case 2:
			//mService.SendMessageToClient();
			break;
		case 3:
			mService.SendMessageToClient();
			break;
		default:
			break;
		}
	}

}
