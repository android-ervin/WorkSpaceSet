package com.gyenno.testUI;

import java.util.ArrayList;
import java.util.List;

import com.gyenno.R;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainTestActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_expandable_list_item_1, getData()));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private List<String> getData() {

		List<String> data = new ArrayList<String>();
		data.add("蓝牙");
		data.add("网络");
		data.add("IM");
		data.add("加密");
		data.add("动画");
		data.add("LBS");
		data.add("信息");
		data.add("数据库");
		data.add("多媒体");
		data.add("webView");

		return data;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);

		Intent intent = null;

		switch (position) {
		case 0:
			intent = new Intent(this, BluetoothTestUIActivity.class);
			startActivity(intent);
			break;
		case 1:
			intent = new Intent(this, NetWorkTestUIActivity.class);
			startActivity(intent);
			break;
		case 2:
			intent = new Intent(this, IMTestUIActivity.class);
			startActivity(intent);
			break;
		case 3:

			break;
		case 4:

			break;
		case 5:

			break;
		case 6:

			break;
		case 7:

			break;
		default:
			break;
		}

	}

}
