package com.gyenno.testUI;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gyenno.R;

public class NetWorkTestUIActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_testui);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_expandable_list_item_1, getData()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private List<String> getData() {

		List<String> data = new ArrayList<String>();
		data.add("httpPost");
		data.add("httpGet");
		data.add("上传文件");
		data.add("下载文件");
		return data;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		switch (position) {
		case 0:
			for (int i = 0; i < 5; i++) {
				//TestHttpRequestManager testHttpRequestManager =new TestHttpRequestManager();
				//Thread thread = new Thread(testHttpRequestManager);
				//thread.start();
			}
			break;

		default:
			break;
		}
	}
}
