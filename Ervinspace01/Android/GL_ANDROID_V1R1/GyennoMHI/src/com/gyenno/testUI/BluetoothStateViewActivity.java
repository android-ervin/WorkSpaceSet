package com.gyenno.testUI;

import java.util.Arrays;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.gyenno.R;
import com.gyenno.application.InitializationAppData;
import com.gyenno.bluetooth.MyBluetoothManager;
import com.gyenno.tools.Utils;

public class BluetoothStateViewActivity extends Activity implements
		OnClickListener {

	private BluetoothDevice device = null;
	private TextView deviceState = null;

	private MyBluetoothManager bluetoothManager = InitializationAppData
			.getBluetooth();
	
	private static final String ACTION = "com.gyenno.broadcastreceiverregister.SENDBROADCAST";  

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetoothstate);

		device = BluetoothListViewActivity.device;

		TextView deviceName = (TextView) findViewById(R.id.deviceName);

		TextView deviceAddress = (TextView) findViewById(R.id.deviceAddress);

		deviceName.setText(device.getName());

		deviceAddress.setText(device.getAddress());

		Button connect = (Button) findViewById(R.id.connectButton);

		connect.setOnClickListener(this);

		deviceState = (TextView) findViewById(R.id.deviceState);

		// device.getBondState();获得绑定状态， 比如说 返回数字12的话 就是已经绑定

		int state = device.getBondState();

		switch (state) {
		case BluetoothDevice.BOND_NONE:
			deviceState.setText("未连接");
			break;
		case BluetoothDevice.BOND_BONDING:
			deviceState.setText("正在连接");
			break;
		case BluetoothDevice.BOND_BONDED:
			deviceState.setText("已连接");
			break;
		default:
			break;
		}
		
		bundReceiveBroadCast();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		bluetoothManager = InitializationAppData.getBluetooth();

		bluetoothManager.connect(device);

	}

	public void preasBlueByteArray(byte[] data) {
		Utils.log("回调" + Arrays.toString(data));
		int messageType = (data[2] & 0xFF);
		Utils.log("messType--->" + messageType);
		switch (messageType) {
		case 21:// 返回设备version

			break;
		case 7:// 返回设备version
			String id = "";
			for (int i = 4; i < 16; i++) {
				String hexStr = Integer.toHexString(data[i] & 0x000000ff);
				id += hexStr.length() == 1 ? "0" + hexStr : hexStr;
			}
			Utils.log("返回设备version" + id);
			break;

		}
	}

	private BroadcastReceiver myReceiver = new BroadcastReceiver() {  
		  
        @Override  
        public void onReceive(Context context, Intent intent) {
        	deviceState.setTextColor(Color.RED);
        	deviceState.setText("已连接");
        }  
  
    };   

	public void bundReceiveBroadCast() {
		
		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION); // 只有持有相同的action的接受者才能接收此广播
		registerReceiver(myReceiver,filter);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(myReceiver);
	}
	
	
}
