package com.gyenno.testUI;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gyenno.R;
import com.gyenno.application.InitializationAppData;
import com.gyenno.application.MyApp;
import com.gyenno.bluetooth.MyBluetoothManager;
import com.gyenno.model.BluetoothConnected;

public class BluetoothConnectedActivity extends Activity implements OnItemClickListener{

	private List<BluetoothConnected> connecteds;
	List<String> data = new ArrayList<String>();
	ListView listView = null;
	private ArrayAdapter<String> adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetoothlist);
		
		connecteds =MyApp.connecteds;
		
		for (int i = 0; i < connecteds.size(); i++) {
			data.add(connecteds.get(i).getDevice().getAddress());
		}
		
		listView = (ListView) findViewById(R.id.mylist);
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, data);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		MyBluetoothManager bluetoothManager = InitializationAppData
				.getBluetooth();
		
		BluetoothGatt mBluetoothGatt=connecteds.get(arg2).getmBluetoothGatt();

		//only for test
		bluetoothManager.sendAlert(new int[] {0,3 },mBluetoothGatt);
		
	}
}
