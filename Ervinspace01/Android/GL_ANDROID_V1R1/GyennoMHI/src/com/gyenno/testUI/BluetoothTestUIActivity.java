package com.gyenno.testUI;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gyenno.R;
import com.gyenno.application.InitializationAppData;
import com.gyenno.bluetooth.MyBluetoothManager;

public class BluetoothTestUIActivity extends ListActivity implements
		LeScanCallback {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_testui);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_expandable_list_item_1, getData()));
		// 初始化蓝牙
		MyBluetoothManager tooth = InitializationAppData.getBluetooth();
		if (tooth == null) {
			InitializationAppData.setBluetooth(this.getTooth());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private List<String> getData() {

		List<String> data = new ArrayList<String>();
		data.add("打开蓝牙");
		data.add("关闭蓝牙");
		data.add("搜索设备");
		data.add("已连接列表");
		return data;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);

		switch (position) {
		case 0:
			openBluetooth();
			break;
		case 1:
			closeBluetooth();
			break;
		case 2:
			scanBluetooth();
			break;
		case 3:
			connectedList();
			break;

		default:
			break;
		}
	}

	private void connectedList() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, BluetoothConnectedActivity.class);

		startActivity(intent);
	}

	public void openBluetooth() {

		MyBluetoothManager bluetoothManager = InitializationAppData
				.getBluetooth();

		bluetoothManager.openBluetooth();

	}

	public void closeBluetooth() {

		MyBluetoothManager bluetoothManager = InitializationAppData
				.getBluetooth();

		bluetoothManager.closeBluetooth();

	}

	public void scanBluetooth() {

		Intent intent = new Intent(this, BluetoothListViewActivity.class);

		startActivityForResult(intent, 0);

	}

	public void pushDataBluetooth() {

		/*
		 * MyBluetoothManager bluetoothManager = InitializationAppData
		 * .getBluetooth();
		 * 
		 * bluetoothManager.sendAlert(new int[] { 0, 3 });
		 */
	}

	// 初始化操作蓝牙的对象
	public MyBluetoothManager getTooth() {
		/* 获取当前系统的android版本号 */
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		MyBluetoothManager tooth = null;
		/* API级别 17对应android 4.2 */
		if (currentapiVersion > 17) {
			// 只支持4.3及以上版本
			tooth = new MyBluetoothManager(this);
		} else {
			// Utility.toast(this, "本机不支持蓝牙低功耗！");
			tooth = null;
		}
		if (tooth != null) {
			BluetoothAdapter mBluetoothAdapter = tooth.getBluetoothAdapter();
			if (mBluetoothAdapter != null)
				tooth.openService();
		}
		return tooth;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case (0): {
			if (resultCode == 0) {
				// TODO Switch tabs using the index.

				MyBluetoothManager bluetoothManager = InitializationAppData
						.getBluetooth();

				bluetoothManager.scan(false, this);
			}
			break;
		}

		}
	}

	@Override
	public void onLeScan(BluetoothDevice arg0, int arg1, byte[] arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		MyBluetoothManager bluetoothManager = InitializationAppData
				.getBluetooth();

		//解绑service
		//待解决
		bluetoothManager.unbindService();
	}
}
