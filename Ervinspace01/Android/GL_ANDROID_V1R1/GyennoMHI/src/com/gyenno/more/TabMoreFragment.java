package com.gyenno.more;

import com.gyenno.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


public class TabMoreFragment extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_more, container, false);
		//initView(rootView);
		LinearLayout linearLayout =(LinearLayout)rootView.findViewById(R.id.moreMain);
		
		DistributeModelView distributeModelView =new DistributeModelView(rootView.getContext());
		
		linearLayout.addView(distributeModelView);
		
		return rootView;
	}
	
}
