package com.gyenno.interact;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;

import com.gyenno.R;


public class TabInteractFragment extends Fragment implements OnItemClickListener
{
	public static final String TITLE = "title";
	
	private ListViewCompat mListView;
	
	private List<MessageItem> mMessageItems = new ArrayList<MessageItem>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		if (getArguments() != null)
		{
			 getArguments().getString(TITLE);
		}
		
		View rootView = inflater.inflate(R.layout.fragment_interact, container, false);
		initView(rootView);
		return rootView;
	}
	
	private void initView(View rootView) {
		mListView = (ListViewCompat) rootView.findViewById(R.id.list);
		if(mMessageItems.size()!=0)
			mMessageItems.clear();
		for (int i = 0; i < 3; i++) {
			MessageItem item = new MessageItem();

			if (i % 3 == 0) {
				item.iconRes = R.drawable.ic_launcher;
				item.title = "腾讯新闻";
				item.msg = "青岛爆炸满月：大量鱼虾死亡";
				item.time = "晚上18:18";
			} else if (i % 3 == 1) {
				item.iconRes = R.drawable.ic_launcher;
				item.title = "团队";
				item.msg = "欢迎xxxx";
				item.time = "12月18日";
			} else if (i % 3 == 2) {
				item.iconRes = R.drawable.ic_launcher;
				item.title = "xxxxxxxxxx";
				item.msg = "aaaa";
				item.time = "看见对方s";
			}
			mMessageItems.add(item);
		}
		Context context =rootView.getContext();
		mListView.setAdapter(new SlideAdapter(context));
		mListView.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		//Log.e(TAG, "onItemClick position=" + position);
	}
	
	
	private class SlideAdapter extends BaseAdapter {

		//private LayoutInflater mInflater;
		private Context context;

		public SlideAdapter(Context context) {
			super();
			this.context = context;
		}

		@Override
		public int getCount() {
			return mMessageItems.size();
		}

		@Override
		public Object getItem(int position) {
			return mMessageItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View itemView = (View) convertView;
			if (convertView == null) {
				itemView = LayoutInflater.from(context).inflate(R.layout.list_item, null);
			} else {
				itemView = (View) convertView.getTag();
			}
			
			ImageView icon = (ImageView) itemView.findViewById(R.id.icon);
			TextView title = (TextView) itemView.findViewById(R.id.title);
			TextView msg = (TextView) itemView.findViewById(R.id.msg);
			TextView time = (TextView) itemView.findViewById(R.id.time);
			MessageItem item = mMessageItems.get(position);
			icon.setImageResource(item.iconRes);
			title.setText(item.title);
			msg.setText(item.msg);
			time.setText(item.time);
			return itemView;
		}
	}

}
