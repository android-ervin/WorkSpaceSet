package com.gyenno;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.gyenno.interact.TabInteractFragment;
import com.gyenno.me.TabMeFragment;
import com.gyenno.setting.TabSettingFragment;

public class MainActivity extends FragmentActivity implements OnClickListener,
		OnPageChangeListener {

	private ViewPager mViewPager;
	private List<Fragment> mTabs = new ArrayList<Fragment>();
	private String[] mTitles = new String[] { "First Fragment !",
			"Second Fragment !", "Third Fragment !" };
	private FragmentPagerAdapter mAdapter;

	private List<View> mTabIndicators = new ArrayList<View>();
	
	private FrameLayout fm1;
	private FrameLayout fm2;
	private FrameLayout fm3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mainelement);
		setOverflowButtonAlways();
		getActionBar().setDisplayShowHomeEnabled(true);
		
		ActionBar actionBar = getActionBar(); 
		actionBar.hide();

		initView();
		initDatas();
		mViewPager.setAdapter(mAdapter);
		initEvent();

		setCurrentTab(0);
		//设置缓存
		mViewPager.setOffscreenPageLimit(0);
		//获得手机分辨率信息
		float scale = this.getResources().getDisplayMetrics().density;
		System.out.println("scale---->"+scale);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int arg0) {
		// TODO Auto-generated method stub
		setCurrentTab(arg0);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.menu_like:
			setCurrentTab(0);
			mViewPager.setCurrentItem(0, false);
			break;
		case R.id.menu_me:
			setCurrentTab(1);
			mViewPager.setCurrentItem(1, false);
			break;
		case R.id.menu_setting:
			setCurrentTab(2);
			mViewPager.setCurrentItem(2, false);
			break;
	/*	case R.id.image_more:
			setCurrentTab(3);
			mViewPager.setCurrentItem(3, false);
			break;*/

		}
	}

	private void setOverflowButtonAlways() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKey = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			menuKey.setAccessible(true);
			menuKey.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initView() {
		mViewPager = (ViewPager) findViewById(R.id.id_viewpager);

		/*ImageView one = (ImageView) findViewById(R.id.image_interact);
		mTabIndicators.add(one);
		ImageView two = (ImageView) findViewById(R.id.image_find);
		mTabIndicators.add(two);
		ImageView three = (ImageView) findViewById(R.id.image_me);
		mTabIndicators.add(three);
		ImageView four = (ImageView) findViewById(R.id.image_more);
		mTabIndicators.add(four);*/
		fm1 = (FrameLayout) findViewById(R.id.menu_like);
		mTabIndicators.add(fm1);
		fm2 = (FrameLayout) findViewById(R.id.menu_me);
		mTabIndicators.add(fm2);
		fm3 = (FrameLayout) findViewById(R.id.menu_setting);
		mTabIndicators.add(fm3);

		fm1.setOnClickListener(this);
		fm2.setOnClickListener(this);
		fm3.setOnClickListener(this);
		/*one.setOnClickListener(this);
		two.setOnClickListener(this);
		three.setOnClickListener(this);*/
		//four.setOnClickListener(this);
	}

	private void initDatas() {

		TabInteractFragment tabInteractFragment = new TabInteractFragment();
		Bundle bundle1 = new Bundle();
		bundle1.putString(TabInteractFragment.TITLE, mTitles[0]);
		tabInteractFragment.setArguments(bundle1);
		mTabs.add(tabInteractFragment);
		
		TabMeFragment tabMeFragment = new TabMeFragment();
		Bundle bundle2 = new Bundle();
		bundle2.putString(TabMeFragment.TITLE, mTitles[1]);
		tabMeFragment.setArguments(bundle2);
		mTabs.add(tabMeFragment);
		
		TabSettingFragment tabFindFragment = new TabSettingFragment();
		mTabs.add(tabFindFragment);

		
		/*TabMoreFragment tabMoreFragment= new TabMoreFragment();
		mTabs.add(tabMoreFragment);*/

		mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

			@Override
			public int getCount() {
				return mTabs.size();
			}

			@Override
			public Fragment getItem(int position) {
				return mTabs.get(position);
			}
		};
	}

	private void initEvent() {

		mViewPager.setOnPageChangeListener(this);

	}

	private void setCurrentTab(int c) {

		for (int i = 0; i < 3; i++) {
			if (i == c) {
				mTabIndicators.get(i).setSelected(true);
				mTabIndicators.get(i).setBackgroundColor(getResources().getColor(R.color.menu_select));
			} else {
				mTabIndicators.get(i).setSelected(false);
				mTabIndicators.get(i).setBackgroundColor(getResources().getColor(R.color.menu_normal));
			}
		}

	}
	

}
