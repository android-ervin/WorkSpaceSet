package com.gyenno.setting;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;


public class DistributeModelView extends View{

	public DistributeModelView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		Paint paint = new Paint();
		
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);
		
		Path path1 = new Path();
		path1.moveTo(350, 400);
		path1.lineTo(100, 800);
		path1.lineTo(600, 800);
		path1.lineTo(350, 400);
		path1.close();
		canvas.drawPath(path1,paint);
		
		Path path2 = new Path();
		path2.moveTo(350, 450);
		path2.lineTo(150, 750);
		path2.lineTo(550, 750);
		path2.lineTo(350, 450);
		path2.close();
		canvas.drawPath(path2,paint);
		
		
		Path path3 = new Path();
		path3.moveTo(350, 500);
		path3.lineTo(200, 700);
		path3.lineTo(500, 700);
		path3.lineTo(350, 500);
		path3.close();
		canvas.drawPath(path3,paint);
		
		super.onDraw(canvas);
	}

}
