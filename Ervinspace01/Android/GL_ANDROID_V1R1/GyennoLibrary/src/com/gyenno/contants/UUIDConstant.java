package com.gyenno.contants;

import java.util.UUID;

public class UUIDConstant {
	// 服务的UUID
	public static final UUID SERVICE_UUID = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");

	// 用于配置 特征的UUID
	public static final UUID CONFIG_CHARACTERISTIC_UUID = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb");

	// 用于配置 描述的UUID
	public static final UUID CONFIG_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

	// 数据传输 特征的UUID
	public static final UUID CAN_WRITE_UUID = UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb");
	
	// 获取蓝牙版本
	public static final UUID VERSION_UUID = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");
	
	// 蓝牙升级
	public static final UUID UPGRADE_UUID = UUID.fromString("f000ffc0-0451-4000-b000-000000000000");
	
	
	public static final UUID CUUID = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
	
	public static final UUID CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
	
}