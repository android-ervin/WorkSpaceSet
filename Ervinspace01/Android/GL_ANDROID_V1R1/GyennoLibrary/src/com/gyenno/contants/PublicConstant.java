package com.gyenno.contants;

public class PublicConstant {
	public static final int CRC16_LENGTH = 2;
	
	/**
	 * toast显示的时间	 */
	public static final int TOAST_SHOW_TIME = 1000;
	
	/**
	 * 得到Galaxy BluetoothGatt对象
	 */
	public static final int GET_GALAXY_BLE_GATT = 0x0001;
	
	/**
	 * 得到其他 BluetoothGatt对象
	 */
	public static final int GET_API18_BLE_GATT = 0x0002;
	
	/**
	 * 发现外围设备
	 */
	public static final int DISCOVER_BLE_DEVICE = 0x0003;
	
	/**
	 * 连接成功
	 */
	public static final int CONNECT_BLE_SUCCESS = 0x0004;
	
	/**
	 * 重连成功
	 */
	public static final int RECONNECT_BLE_SUCCESS = 0x00025;
	
	/**
	 * 连接失败
	 */
	public static final int CONNECT_BLE_FAIL = 0x0005;
	
	/**
	 * 发现设备成功，再次连接
	 */
	public static final int DISCOVER_SERVICE_SUCCESS_GO_CONNECT_AGAIN = 0x0006;
	
	/**
	 * 扫描设备用时
	 */
	public static final int SCAN_BLE_TIME = 10 * 1000;
	
	public static final int  WRITE_BLE_TIME = 30 * 1000;
	
	/**
	 * 与设备交互数据用时基数
	 */
	public static final int SENDMSG = 2 * 1000;
	
	/**
	 * 发现服务用时
	 */
	public static final int DISCOVER_SERVICE_TIME = 4 * 1000;
	
	/**
	 * 等待同步用时
	 */
	public static final int DEVICE_SYN_TIME = 6 * 1000;
	
	/**
	 * 搜索设备结束
	 */
	public static final int BLE_DEVICE_STOP = 0x0007;
	
	/**
	 * 周围没有设备
	 */
	public static final int BLE_DEVICE_NOTFIND = 0x0008;
	
	/**
	 * 是否打开蓝牙
	 */
	public static final int REQUEST_ENABLE_BT = 1;
	
	/**
	 * 蓝牙手环没有返回数据
	 */
	public static final int DEVICE_NOTRESPONSE = 0x0009;
	
	/**
	 * 蓝牙自动同步开始
	 */
	public static final int SYN_START = 0x000A;
	
	/**
	 * 蓝牙同步成功
	 */
	public static final int SYN_SUCCESS = 0x000B;
	
	/**
	 * 刷新界面
	 */
	public static final int  REFESH_UI= 0x00026;
	
	/**
	 * 蓝牙同步失败
	 */
	public static final int SYN_FAIL = 0x000C;
	
	/**
	 * 蓝牙已断开连接
	 */
	public static final int DEVICE_NOTCONN = 0x000D;
	
	/**
	 * 蓝牙已连接
	 */
	public static final int DEVICE_CONNED = 0x000E;
	
	/**
	 * 蓝牙返回id
	 */
	public static final int DEVICE_RESIDOK = 0x000F;
	
	/**
	 * 蓝牙清除信息成功
	 */
	public static final int DEVICE_CLEAROK = 0x0010;
	
	/**
	 * 蓝牙清除信息成功
	 */
	public static final int MACLENGTH = 0x0011;
	
	/**
	 * 蓝牙状态返回正常
	 */
	public static final int DEVICE_RESOK = 0x0012;
	
	/**
	 * 测试断开
	 */
	public static final int DEVICE_NOTCONNN = 0x0014;
	
	/**
	 * 去发现服务
	 */
	public static final int DISCOVER_SERVICES = 0x0015;
	
	/**
	 * 打开服务
	 */
	public static final int OPEN_SERVICE = 0x0016;
	
	public static final int  OPEN_SERVICE_SEND = 0x0024;
	
	/**
	 * 刷新界面
	 */
	public static final int FLESH_VIEW = 0x0017;
	
	/**
	 * 调用断开连接
	 */
	public static final int DEVICE_DISCONN = 0x0018;
	
	/**
	 * close连接
	 */
	public static final int DEVICE_CLOSE = 0x0023;
	
	/**
	 * 升级成功
	 */
	public static final int UP_SUCCESS = 0x0019;
	
	/**
	 * 升级失败
	 */
	public static final int UP_FAIL = 0x001A;
	
	/**
	 * 无需升级
	 */
	public static final int EARLYNEW = 0x001B;
	
	/**
	 * 无需同步
	 */
	public static final int SYNEARLYNEW = 0x001F;
	
	/**
	 * 不可升级
	 */
	public static final int DONOTUPGRADE = 0x001C;
	
	/**
	 * 蓝牙升级
	 */
	public static final int BLUETOOTH_UP = 0x001D;
	
	
	/**
	 * 蓝牙异常
	 */
	public static final int BLUETOOTH_ERROR = 0x001E;
	
	/**
	 * 蓝牙异常
	 */
	public static final int UPLOAD = 0x0020;
	
	/**
	 * 返回版本正常
	 */
	public static final int DEVICE_RESVERSIONOK = 0x0021;
	
	/**
	 * 没有返回版本号
	 */
	public static final int DEVICE_VERSIONNOTRESPONSE = 0x0022;
	
	/**
	 * 上传结束
	 */
	public static final int UPLOAD_SUC = 0x0027;
	
	/**
	 * 上传失败
	 */
	public static final int UPLOAD_FAIL = 0x0028;
	
	/**
	 * 正在同步
	 */
	public static final int SYNING = 0x0029;
	
	/**
	 * 上传失败
	 */
	public static final int UPLOAD_START = 0x002A;
	
	/**
	 * 未发现
	 */
	public static final int UPLOAD_NO_FOUND = 0x002B;
	
	/**
	 * 上传准备
	 */
	public static final int UPLOAD_READY = 0x002C;
	
	/**
	 * 显示隐藏QQ绑定用户
	 */
	public static final int SHOW_HIDDEN = 0x002D;
	
}

