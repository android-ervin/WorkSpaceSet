package com.gyenno.enums;
/**
 * 消息类型的枚举
 */
public enum EnumMessageType {
	MSG_NOTIFY(0x00), MSG_UPDATE(0x01), MSG_SYNC(0x02), MSG_USER(0x03), MSG_ALARM(0x04), MSG_UPDATE_DATA(0x05), MSG_SYNC_DATA(0x06), MSG_DEVICE_ID(0x07), MSG_STATUE(0x08), MSG_SLEEP(0x0b), MSG_TIME(0x0c);
	private int type;

	EnumMessageType(int type) {
		this.type = type;
	}

	public int getType() {
		return this.type;
	}
}
