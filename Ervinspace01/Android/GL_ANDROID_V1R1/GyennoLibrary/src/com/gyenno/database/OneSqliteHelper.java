package com.gyenno.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gyenno.tools.Alarm;
import com.gyenno.tools.DayData;
import com.gyenno.tools.Utils;


public class OneSqliteHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "One.db";
	private static final int DATABASE_VERSION = 1;
	private static final String ONE_SYN_TABLE = "synchronize";
	private static final String ONE_ALARM = "alarm";
	private Object mObj = new Object();

	public OneSqliteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE " + ONE_SYN_TABLE + " ( " + "_id INTEGER PRIMARY KEY AUTOINCREMENT ,"
				+ " date VARCHAR(128) NOT NULL ," + " data VARCHAR(256) NOT NULL ,"
				+ "day_kms INTEGER NOT NULL, " + "day_sleeps INTEGER NOT NULL ,"
				+ "day_calories INTEGER NOT NULL ," + "day_steps INTEGER NOT NULL , "
				+ "time_slot VARCHAR(64) NOT NULL ," + "ds_deep INTEGER NOT NULL ,"
				+ "ds_point DOUBLE NOT NULL" + " ) ";
		db.execSQL(sql);
		String sqlAlarm = "CREATE TABLE " + ONE_ALARM + " ( " + "_id INTEGER PRIMARY KEY AUTOINCREMENT ,"
				+ " hour INTEGER NOT NULL , " + " minute INTEGER NOT NULL , "
				+ "enable_one INTEGER NOT NULL ," + "enable_two INTEGER NOT NULL ,"
				+ "enable_three INTEGER NOT NULL ," + "enable_four INTEGER NOT NULL ,"
				+ "enable_five INTEGER NOT NULL ," + "enable_six INTEGER NOT NULL ,"
				+ "enable_seven INTEGER NOT NULL ," + " type INTEGER NOT NULL" + " ) ";
		db.execSQL(sqlAlarm);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql = "DROP TABLE IF EXISTS " + ONE_SYN_TABLE;
		String sqlAlarm = "DROP TABLE IF EXISTS " + ONE_ALARM;

		db.execSQL(sql);
		db.execSQL(sqlAlarm);
		this.onCreate(db);
	}

	/**
	 * 
	 * @param date
	 *            日期
	 * @param data
	 *            data
	 * @param day_kms
	 *            公里数
	 * @param day_sleeps
	 *            睡眠时长
	 * @param day_calories
	 *            消耗卡路里数
	 * @param day_steps
	 *            总步数
	 * @return the row ID of the newly inserted row, or -1 if an error occurred
	 */
	public void insert(String date, String data, int day_kms, int day_sleeps, int day_calories,
			int day_steps, String timeSlot, int deepTime, double sleepPoint) {
		synchronized (mObj) {
			boolean falg = isDataExists(date);
			if (falg) {
				updateOneData(date, data, day_kms, day_sleeps, day_calories, day_steps, timeSlot, deepTime,
						sleepPoint);
			} else {
				SQLiteDatabase db = this.getWritableDatabase();
				ContentValues cv = new ContentValues();
				cv.put("date", date);
				cv.put("data", data);
				cv.put("day_kms", day_kms);
				cv.put("day_sleeps", day_sleeps);
				cv.put("day_calories", day_calories);
				cv.put("day_steps", day_steps);
				cv.put("time_slot", timeSlot);
				cv.put("ds_deep", deepTime);
				cv.put("ds_point", sleepPoint);
				db.insert(ONE_SYN_TABLE, null, cv);
				db.close();
			}
		}
	}

	public void updateOneData(String date, String data, double day_kms, int day_sleeps, int day_calories,
			int day_steps, String timeSlot, int deepTime, double sleepPoint) {
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put("date", date);
			cv.put("data", data);
			cv.put("day_kms", day_kms);
			cv.put("day_sleeps", day_sleeps);
			cv.put("day_calories", day_calories);
			cv.put("day_steps", day_steps);
			cv.put("time_slot", timeSlot);
			cv.put("ds_deep", deepTime);
			cv.put("ds_point", sleepPoint);
			String whereClause = "date=?";
			String whereArgs[] = new String[] { date };
			db.update(ONE_SYN_TABLE, cv, whereClause, whereArgs);
			db.close();
		}
	}

	/**
	 * 查询单日的 data Json 字符串
	 * 
	 * @param date
	 * @return A Cursor object, which is positioned before the first entry
	 */
	public String queryData(String date) {
		String data = "";
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String columns[] = new String[] { "data" };
			String selection = "date=?";
			String selectionArgs[] = new String[] { date };
			Cursor result = db.query(ONE_SYN_TABLE, columns, selection, selectionArgs, null, null, null);
			for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
				data = result.getString(result.getColumnIndex("data"));
			}
			db.close();
		}
		return data;
	}

	/**
	 * 判断数据是否已经存在
	 * 
	 * @return true：已存在 false：还没有
	 */
	public boolean isDataExists(String date) {
		boolean flag = false;
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String selection = "date=?";
			String selectionArgs[] = new String[] { date };
			Cursor result = db.query(ONE_SYN_TABLE, null, selection, selectionArgs, null, null, null);
			if (result.getCount() > 0) {
				flag = true;
			} else {
				flag = false;
			}
			db.close();
		}
		return flag;
	}
	
	/**
	 * 第一天数据
	 * @return
	 */
	public DayData queryFirstRecord() {
		DayData daydata = null;
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String sql = "SELECT * FROM synchronize ORDER BY date ASC LIMIT 1";
			Cursor result = db.rawQuery(sql, null);
			if (result.moveToFirst()) {
				daydata = new DayData();
				daydata.setData(result.getString(result.getColumnIndex("data")));
				daydata.setDate(result.getString(result.getColumnIndex("date")));
				daydata.setms(result.getInt(result.getColumnIndex("day_kms")));
				daydata.setSleeps(result.getInt(result.getColumnIndex("day_sleeps")));
				daydata.setCalories(result.getInt(result.getColumnIndex("day_calories")));
				daydata.setSteps(result.getInt(result.getColumnIndex("day_steps")));
				daydata.setTimeSlot(result.getString(result.getColumnIndex("time_slot")));
			}
			db.close();
		}
		return daydata;
	}
	
	/**
	 * 获取  第一条  数据的 日期
	 * @return
	 */
	public String queryFirstDay() {
		String firstDay=null;
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String sql = "SELECT date FROM synchronize ORDER BY date ASC LIMIT 1";
			Cursor result = db.rawQuery(sql, null);
			if (result.moveToFirst()) {
				firstDay=result.getString(result.getColumnIndex("date"));
			}
			db.close();
		}
		return firstDay;
	}
	
	/**
	 *  获取  最后一条  数据的 日期
	 * @return
	 */
	public String queryLastDay() {
		String lastDay=null;
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String sql = "SELECT date FROM synchronize ORDER BY date DESC LIMIT 1";
			Cursor result = db.rawQuery(sql, null);
			if (result.moveToFirst()) {
				lastDay=result.getString(result.getColumnIndex("date"));
			}
			db.close();
		}
		return lastDay;
	}

	/**
	 * 返回最后一行数据记录
	 * 
	 * @return
	 */
	public DayData queryLastRecord() {
		DayData daydata = null;
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String sql = "SELECT * FROM synchronize ORDER BY date DESC LIMIT 1";
			Cursor result = db.rawQuery(sql, null);
			if (result.moveToFirst()) {
				daydata = new DayData();
				daydata.setData(result.getString(result.getColumnIndex("data")));
				daydata.setDate(result.getString(result.getColumnIndex("date")));
				daydata.setms(result.getInt(result.getColumnIndex("day_kms")));
				daydata.setSleeps(result.getInt(result.getColumnIndex("day_sleeps")));
				daydata.setCalories(result.getInt(result.getColumnIndex("day_calories")));
				daydata.setSteps(result.getInt(result.getColumnIndex("day_steps")));
				daydata.setTimeSlot(result.getString(result.getColumnIndex("time_slot")));
			}
			db.close();
		}
		return daydata;
	}

	/**
	 * 根据日期查询 当天 总步数、总公里数、、、、
	 * 
	 * @param date
	 * @return
	 */
	public DayData queryDayConstant(String date) {
		DayData daydata = new DayData();
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String columns[] = new String[] { "date","data","day_kms", "day_sleeps", "day_calories", "day_steps",
					"time_slot", "ds_point", "ds_deep" };
			String selection = "date=?";
			String selectionArgs[] = new String[] { date };
			Cursor result = db.query(ONE_SYN_TABLE, columns, selection, selectionArgs, null, null, null);
			if (result.moveToFirst()) {
				daydata.setDate(result.getString(result.getColumnIndex("date")));
				daydata.setData(result.getString(result.getColumnIndex("data")));
				daydata.setms(result.getInt(result.getColumnIndex("day_kms")));
				daydata.setSleeps(result.getInt(result.getColumnIndex("day_sleeps")));
				daydata.setCalories(result.getInt(result.getColumnIndex("day_calories")));
				daydata.setSteps(result.getInt(result.getColumnIndex("day_steps")));
				daydata.setTimeSlot(result.getString(result.getColumnIndex("time_slot")));
				daydata.setDsPoint(result.getDouble(result.getColumnIndex("ds_point")));
				daydata.setDsSleep(result.getInt(result.getColumnIndex("ds_deep")));
			}
			db.close();
		}
		return daydata;
	}

	public DayData queryDaySleep(String date) {
		DayData daydata = null;
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String columns[] = new String[] { "data", "time_slot" };
			String selection = "date=?";
			String selectionArgs[] = new String[] { date };
			Cursor result = db.query(ONE_SYN_TABLE, columns, selection, selectionArgs, null, null, null);
			if (result.moveToFirst()) {
				daydata = new DayData();
				daydata.setData(result.getString(result.getColumnIndex("data")));
				daydata.setTimeSlot(result.getString(result.getColumnIndex("time_slot")));
			}
			db.close();
		}
		return daydata;
	}

	public DayData queryAllConstant(String date) {
		DayData daydata = new DayData();
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String selection = "date=?";
			String selectionArgs[] = new String[] { date };
			Cursor result = db.query(ONE_SYN_TABLE, null, selection, selectionArgs, null, null, null);
			for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
				daydata.calories = result.getInt(result.getColumnIndex("day_calories"));
				daydata.data = result.getString(result.getColumnIndex("data"));
				daydata.date = result.getString(result.getColumnIndex("date"));
				daydata.dsPoint = result.getDouble(result.getColumnIndex("ds_point"));
				daydata.dsSleep = result.getInt(result.getColumnIndex("ds_deep"));
				daydata.sleeps = result.getInt(result.getColumnIndex("day_sleeps"));
				daydata.steps = result.getInt(result.getColumnIndex("day_steps"));
				daydata.ms = result.getInt(result.getColumnIndex("day_kms"));
	
			}
			db.close();
		}
		return daydata;
	}

	/**
	 * 删除数据 delete one row by Id
	 * 
	 * @param id
	 */
	public void deleteOneRow(int id) {
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String wherecaluse = "_id=?";
			String whereargs[] = new String[] { String.valueOf(id) };
			db.delete(ONE_SYN_TABLE, wherecaluse, whereargs);
			db.close();
		}
	}

	/**
	 * delete all data
	 */
	public void deleteAll() {
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(ONE_SYN_TABLE, null, null);
			db.delete(ONE_ALARM, null, null);
			db.close();
		}
	}

	/**
	 * 根据 周数 查出当周每天的 卡路里数和入眠点
	 * 
	 * @param indexWeek
	 * @return
	 */
	public ArrayList<DayData> queryWeekData() {
		ArrayList<DayData> list = new ArrayList<DayData>();
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String columns[] = new String[] { "date", "day_calories", "ds_point", "day_kms", "day_steps",
					"day_sleeps", "ds_deep" };
			String selection = "date=?";
			// 获取当周已过去的日期 包括今天
			ArrayList<String> days = Utils.getUsedDayOfWeek();
			DayData simpleData = new DayData();
			simpleData.setDsPoint(-1.0);
			for (int i = 0; i < days.size(); i++) {
				String[] selectionArgs = new String[] { days.get(i) };
				Cursor result = db.query(ONE_SYN_TABLE, columns, selection, selectionArgs, null, null, null);
				if (result.getCount() == 0) {
					list.add(simpleData);
				} else {
					for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
						DayData daydata = new DayData();
						daydata.setDate(result.getString(result.getColumnIndex("date")));
						daydata.setCalories(result.getInt(result.getColumnIndex("day_calories")));
						daydata.setDsPoint(result.getDouble(result.getColumnIndex("ds_point")));
						daydata.setms(result.getInt(result.getColumnIndex("day_kms")));
						daydata.setSteps(result.getInt(result.getColumnIndex("day_steps")));
						daydata.setSleeps(result.getInt(result.getColumnIndex("day_sleeps")));
						daydata.setDsSleep(result.getInt(result.getColumnIndex("ds_deep")));
						daydata.setDsPoint(result.getDouble(result.getColumnIndex("ds_point")));
						list.add(daydata);
					}
				}
			}
			db.close();
		}
		return list;
	}

	/**
	 * 根据月份查出 当月所有卡路里数
	 * 
	 * @param indexMonth
	 * @return
	 */
	public ArrayList<DayData> queryMonthData(String date) {
		ArrayList<DayData> list = new ArrayList<DayData>();
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String columns[] = new String[] { "date", "day_calories", "ds_point", "day_kms", "day_steps",
					"day_sleeps", "ds_deep" };
			String selection = "date LIKE ?";
			String[] selectionArgs = new String[] { "%" + date + "%" };
			Cursor result = db.query(ONE_SYN_TABLE, columns, selection, selectionArgs, null, null, "date");
			for (result.moveToFirst(); !result.isAfterLast(); result.moveToNext()) {
				DayData daydata = new DayData();
				daydata.setDate(result.getString(result.getColumnIndex("date")));
				daydata.setCalories(result.getInt(result.getColumnIndex("day_calories")));
				daydata.setDsPoint(result.getDouble(result.getColumnIndex("ds_point")));
				daydata.setms(result.getInt(result.getColumnIndex("day_kms")));
				daydata.setSteps(result.getInt(result.getColumnIndex("day_steps")));
				daydata.setSleeps(result.getInt(result.getColumnIndex("day_sleeps")));
				daydata.setDsSleep(result.getInt(result.getColumnIndex("ds_deep")));
				list.add(daydata);
			}
			db.close();
		}
		return list;
	}

	// -----------------------------以下是闹钟数据库操作方法---------------------------------//
	/**
	 * 新增加一个闹钟
	 * 
	 * @param hour
	 * @param minute
	 * @param enable
	 *            0 表示开启 ，1表示关闭
	 */
	// public void insertAlarm(int hour, int minute, int enable, int type) {
	// SQLiteDatabase db = this.getWritableDatabase();
	// ContentValues cv = new ContentValues();
	// cv.put("hour", hour);
	// cv.put("minute", minute);
	// cv.put("enable", enable);
	// cv.put("type", type);
	// db.insert(ONE_ALARM, null, cv);
	// db.close();
	// }

	public void insertAlarm(int hour, int minute, int enable1, int enable2, int enable3, int enable4,
			int enable5, int enable6, int enable7, int type) {
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put("hour", hour);
			cv.put("minute", minute);
			cv.put("enable_one", enable1);
			cv.put("enable_two", enable2);
			cv.put("enable_three", enable3);
			cv.put("enable_four", enable4);
			cv.put("enable_five", enable5);
			cv.put("enable_six", enable6);
			cv.put("enable_seven", enable7);
			cv.put("type", type);
			db.insert(ONE_ALARM, null, cv);
			db.close();
		}
	}

	// 删除闹钟
	public void deleteAlarm(int hour, int minute) {
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String whereClause = "hour=? AND minute=?";
			String whereArgs[] = new String[] { String.valueOf(hour), String.valueOf(minute) };
			db.delete(ONE_ALARM, whereClause, whereArgs);
			db.close();
		}
	}

	/**
	 * 删除所有闹钟
	 * 
	 * @param hour
	 * @param minute
	 */
	public void deleteAllAlarm() {
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(ONE_ALARM, null, null);
			db.close();
		}
	}

	/**
	 * 更新闹钟状态
	 * 
	 * @param hour
	 * @param minute
	 * @param enable
	 */
	public void updateAlarm(int hour, int minute, int enable, int type) {
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put("enable", enable);
			cv.put("type", type);
			String whereClause = "hour=? AND minute=?";
			String[] whereArgs = new String[] { String.valueOf(hour), String.valueOf(minute) };
			db.update(ONE_ALARM, cv, whereClause, whereArgs);
			db.close();
		}
	}

	/**
	 * 查询所有闹钟、用于显示
	 * 
	 * @return
	 */
	public ArrayList<Alarm> QueryAllAlarm() {
		ArrayList<Alarm> list = new ArrayList<Alarm>();
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.query(ONE_ALARM, null, null, null, null, null, null);
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
				Alarm alarm = new Alarm();
				alarm.setHour(cursor.getInt(cursor.getColumnIndex("hour")));
				alarm.setMinute(cursor.getInt(cursor.getColumnIndex("minute")));
				alarm.setEnable_one(cursor.getInt(cursor.getColumnIndex("enable_one")));
				alarm.setEnable_two(cursor.getInt(cursor.getColumnIndex("enable_two")));
				alarm.setEnable_three(cursor.getInt(cursor.getColumnIndex("enable_three")));
				alarm.setEnable_four(cursor.getInt(cursor.getColumnIndex("enable_four")));
				alarm.setEnable_five(cursor.getInt(cursor.getColumnIndex("enable_five")));
				alarm.setEnable_six(cursor.getInt(cursor.getColumnIndex("enable_six")));
				alarm.setEnable_seven(cursor.getInt(cursor.getColumnIndex("enable_seven")));
				alarm.setAllType(cursor.getInt(cursor.getColumnIndex("type")));
				list.add(alarm);
			}
			db.close();
		}
		return list;
	}

	/**
	 * 查询所有 已经打开的 闹钟，用于发送命令
	 * 
	 * @return
	 */
	// public ArrayList<Alarm> QueryAllEnableAlarm() {
	// ArrayList<Alarm> list = new ArrayList<Alarm>();
	// SQLiteDatabase db = this.getWritableDatabase();
	// Cursor cursor = db.query(ONE_ALARM, null, null, null, null, null, null);
	// for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
	// if (cursor.getInt(cursor.getColumnIndex("enable")) == 0) {
	// Alarm alarm = new Alarm();
	// alarm.setHour(cursor.getInt(cursor.getColumnIndex("hour")));
	// alarm.setMinute(cursor.getInt(cursor.getColumnIndex("minute")));
	// alarm.setEnable(cursor.getInt(cursor.getColumnIndex("type")));
	// list.add(alarm);
	// }
	// }
	// db.close();
	// return list;
	// }

	/**
	 * true 闹钟存在 false 不存在
	 * 
	 * @return
	 */
	public boolean QueryIfExistsAlarm(int hour, int minute) {
		boolean falg = false;
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String selection = "hour=? AND minute=?";
			String[] selectionArgs = new String[] { String.valueOf(hour), String.valueOf(minute) };
			Cursor cursor = db.query(ONE_ALARM, null, selection, selectionArgs, null, null, null);
			if (cursor.getCount() > 0) {
				falg = true;
			} else {
				falg = false;
			}
			db.close();
		}
		return falg;
	}

	/**
	 * 查找指定闹钟
	 * 
	 * @param hour
	 * @param minute
	 * @return
	 */
	public Alarm QuerySingleAlarm(int hour, int minute) {
		Alarm alarm = new Alarm();
		synchronized (mObj) {
			SQLiteDatabase db = this.getWritableDatabase();
			String selection = "hour=? AND minute=?";
			String[] selectionArgs = new String[] { String.valueOf(hour), String.valueOf(minute) };
			Cursor cursor = db.query(ONE_ALARM, null, selection, selectionArgs, null, null, null);
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
				alarm.setHour(cursor.getInt(cursor.getColumnIndex("hour")));
				alarm.setMinute(cursor.getInt(cursor.getColumnIndex("minute")));
				alarm.setHour(cursor.getInt(cursor.getColumnIndex("enable")));
				alarm.setType(cursor.getInt(cursor.getColumnIndex("type")));
			}
			db.close();
		}
		return alarm;
	}

}
