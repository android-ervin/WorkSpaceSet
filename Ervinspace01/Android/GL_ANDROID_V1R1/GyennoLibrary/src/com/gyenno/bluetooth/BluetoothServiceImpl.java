package com.gyenno.bluetooth;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import com.gyenno.application.MyApp;
import com.gyenno.contants.PublicConstant;
import com.gyenno.contants.UUIDConstant;
import com.gyenno.model.BluetoothConnected;
import com.gyenno.model.BluetoothMsg;
import com.gyenno.tools.CRCUtil;
import com.gyenno.tools.MessageFactoryUtil;
import com.gyenno.tools.Utils;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BluetoothServiceImpl extends Service implements BluetoothService {
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothManager mBluetoothManager;
	private BluetoothDevice mBluetoothDevice;// 固定Device
												// 在多个设备连接的时候，这里需要业务，保证其互斥。
	private Queue<Object> heartbeatQueue;// 心跳队列
	private String mBluetoothDeviceAddress;
	public BluetoothGatt mBluetoothGatt = null;
	public BluetoothGattService OadService = null;
	protected Timer upTimer;
	protected static Timer syn;
	protected Timer mMonitorTimer;
	protected Handler mActivityHandler;
	public int size;
	public int mUpdateType;
	public byte[] buff;
	protected boolean mBusy = false;
	private static SharedPreferences sp = null;
	public BluetoothService mService;
	private Handler mHandler;

	private static final Queue<Object> sWriteCharacteristicQueue = new ConcurrentLinkedQueue<Object>();// writeCharacteristic队列
	private static boolean sIsWriting = false;

	// 10秒后停止查找搜索.
	private static final long SCAN_PERIOD = 10000;
	private static final String ACTION = "com.gyenno.broadcastreceiverregister.SENDBROADCAST";

	@Override
	public void onCreate() {
		super.onCreate();
		mHandler = new Handler();
		init();
	}

	// 初始化获取本地蓝牙 - 蓝牙适配器
	protected void init() {
		Utils.log("启动服务...");
		mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = mBluetoothManager.getAdapter();
	}

	private final IBinder binder = new LocalBinder();

	public class LocalBinder extends Binder {
		public BluetoothService getService() {
			return BluetoothServiceImpl.this;
		}
	}

	protected IBinder getBinder() {
		return binder;
	}

	@Override
	// 获取本地蓝牙设备 -- 蓝牙适配器
	public BluetoothAdapter getBluetoothAdapter(Context context) {
		BluetoothManager bluetoothManager = (BluetoothManager) context
				.getSystemService(Context.BLUETOOTH_SERVICE);
		if (!context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_BLUETOOTH_LE)) {
			Utils.toast(context, "不支持BLE设备");
		}
		return bluetoothManager.getAdapter();
	}

	@Override
	// 扫描设备
	public void scan(boolean flag,
			final BluetoothAdapter.LeScanCallback mLeScanCallback) {
		if (flag) {
			Utils.log("扫描设备...");
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					mBluetoothAdapter.stopLeScan(mLeScanCallback);
				}
			}, SCAN_PERIOD);

			mBluetoothAdapter.startLeScan(mLeScanCallback);
		} else {
			Utils.log("停止扫描设备...");
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}

	}

	// 打开蓝牙
	public void openBluetooth() {

		if (!mBluetoothAdapter.isEnabled()) {
			// 蓝牙为关闭，开启蓝牙
			mBluetoothAdapter.enable();
		}

	}

	// 关闭蓝牙
	public void closeBluetooth() {

		if (mBluetoothAdapter.isEnabled()) {
			// 蓝牙为开启，关闭蓝牙
			mBluetoothAdapter.disable();
		}

	}

	@Override
	// 过滤蓝牙设备 得到需要的设备
	public BluetoothGattCharacteristic openBLENotification(
			BluetoothGatt BluetoothGatt) {
		BluetoothGattService service = BluetoothGatt
				.getService(UUIDConstant.SERVICE_UUID);

		if (service == null) {
			OadService = BluetoothGatt.getService(UUIDConstant.UPGRADE_UUID);
			// 服务失效
			return null;
		} else {
			BluetoothGattCharacteristic characteristic = service
					.getCharacteristic(UUIDConstant.CONFIG_CHARACTERISTIC_UUID);
			if (characteristic != null) {
				BluetoothGatt.setCharacteristicNotification(characteristic,
						true);// 为给定的特征打开通知
				BluetoothGattDescriptor clientConfig = characteristic
						.getDescriptor(UUIDConstant.CONFIG_DESCRIPTOR_UUID);
				if (clientConfig != null) {
					clientConfig
							.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
					// BluetoothGatt.writeDescriptor(clientConfig);
					Utils.log("配置成功，可以进行和蓝牙模块的数据传递了！");
					// startMonitorEmail();
					BluetoothGattCharacteristic canWriteDataCharacteristic = service
							.getCharacteristic(UUIDConstant.CAN_WRITE_UUID);

					return canWriteDataCharacteristic;
				}
			}

			return null;
		}

	}

	@Override
	// 两个设备通过BLE通信，首先需要建立GATT连接。这里我们是Android设备作为client端，连接GATT Server
	// 函数成功，返回 BluetoothGatt 对象，它是GATT profile的封装。通过这个对象，我们就能进行GATT
	// Client端的相关操作。
	// BluetoothGattCallback 用于传递一些连接状态及结果。
	public boolean connect() {
		if (mBluetoothGatt == null) {
			Utils.log("mBluetoothGatt == null");
		} else {
			Utils.log("mBluetoothGatt != null");
			mBluetoothGatt.connect();
		}
		return true;
	}

	@Override
	// 连接远程设备
	public boolean connect( BluetoothDevice mDevice) {
		mBluetoothDevice = mDevice;
		mBluetoothGatt = mDevice.connectGatt(this, false, mGattCallback);
		if (mBluetoothGatt != null) {
			BluetoothConnected bluetoothConnected = new BluetoothConnected(
					mDevice, mBluetoothGatt);
			if (!MyApp.findDeviceUpdate(mDevice, bluetoothConnected)) {
				MyApp.connecteds.add(bluetoothConnected);
			}
		}
		return true;
	}

	@Override
	// 断开设备
	public void disConnect(BluetoothDevice mDevice) {
		Utils.log("执行断开..");
		if (mBluetoothGatt != null) {
			mBluetoothGatt.disconnect();
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
	}

	@Override
	// 获取BluetoothGatt
	// 继承BluetoothProfile，通过BluetoothGatt可以连接设备（connect）,发现服务（discoverServices），并把相应地属性返回到BluetoothGattCallback
	public void getGatt() {

	}

	@Override
	// 获取设备连接状态
	public boolean getProfileState(BluetoothDevice mDevice) {
		int connectionState = mBluetoothManager.getConnectionState(mDevice,
				BluetoothProfile.GATT);
		boolean flag = connectionState == BluetoothProfile.STATE_CONNECTED;
		if (flag)
			Utils.log("设备已连接上！");
		else
			Utils.log("设备未连接上！");
		return flag;
	}

	// @Override
	// 执close断开
	public void close() {
		if (mBluetoothGatt != null) {
			mBluetoothGatt.close();
			mBluetoothGatt = null;
			Utils.log("执close断开..close................");
		}
	}

	@Override
	// 搜索连接设备所支持的service
	public void gattDiscoverServices(BluetoothDevice device) {
		mBluetoothGatt.discoverServices();
	}

	@Override
	// 对外围蓝牙写数据
	public void writeData(byte[] data, BluetoothGatt BluetoothGatt) {
		// edit by peter
		BluetoothGattCharacteristic canWriteDataCharacteristic = openBLENotification(BluetoothGatt);
		if (canWriteDataCharacteristic != null && BluetoothGatt != null) {
			canWriteDataCharacteristic.setValue(data);
			BluetoothGatt.writeCharacteristic(canWriteDataCharacteristic);
		}
	}

	@Override
	// 对外围蓝牙写数据
	public boolean writeOadData(int pos, byte[] data) {
		boolean result = false;
		if (!checkGatt())
			return result;

		mBusy = true;
		List<BluetoothGattCharacteristic> charListOad = null;
		BluetoothGattCharacteristic characteristic = null;
		if ((pos == 0 || pos == 1) && data != null) {
			charListOad = OadService.getCharacteristics();
			if (charListOad.size() == 2) {
				characteristic = charListOad.get(pos);
				if (characteristic != null) {
					characteristic.setValue(data);
					result = mBluetoothGatt.writeCharacteristic(characteristic);
				}
			}
		}
		return result;
	}

	@Override
	// 获取数据
	public int getOadCharacteristicsSize() {
		int size = 0;
		if (OadService != null && OadService.getCharacteristics() != null) {
			size = OadService.getCharacteristics().size();
		}
		return size;
	}

	@Override
	// 启用通知
	public boolean enableNotification(int pos, boolean enable) {
		boolean result = false;
		if (!checkGatt())
			return result;
		List<BluetoothGattCharacteristic> charListOad = null;
		BluetoothGattCharacteristic characteristic = null;
		if (pos == 0 || pos == 1) {
			charListOad = OadService.getCharacteristics();
			if (charListOad.size() == 2) {
				characteristic = charListOad.get(pos);
				if (!mBluetoothGatt.setCharacteristicNotification(
						characteristic, enable)) {
					return result;
				}
				BluetoothGattDescriptor clientConfig = characteristic
						.getDescriptor(UUIDConstant.CLIENT_CHARACTERISTIC_CONFIG);
				if (clientConfig == null)
					return result;

				if (enable) {
					clientConfig
							.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
				} else {
					clientConfig
							.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
				}

				mBusy = true;
				result = mBluetoothGatt.writeDescriptor(clientConfig);
			}
		}
		return result;
	}

	// 启用通知 指定GATT
	public boolean enableNotification(boolean enable,
			BluetoothGatt BluetoothGatt,
			BluetoothGattCharacteristic WriteDataCharacteristic) {
		boolean result = BluetoothGatt.setCharacteristicNotification(
				WriteDataCharacteristic, enable);
		System.out.println("notifi--->" + result);
		if (result == false) {
			return result;
		} else {
			result = false;
		}
		BluetoothGattDescriptor clientConfig = WriteDataCharacteristic
				.getDescriptor(UUIDConstant.CLIENT_CHARACTERISTIC_CONFIG);
		if (clientConfig == null)
			return result;

		if (enable) {
			clientConfig
					.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		} else {
			clientConfig
					.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
		}

		mBusy = true;
		result = mBluetoothGatt.writeDescriptor(clientConfig);

		return result;
	}

	// 检测BluetoothAdapter是否为空
	public boolean checkGatt() {
		if (mBluetoothAdapter == null) {
			return false;
		}
		if (mBluetoothGatt == null) {
			return false;
		}
		if (mBusy) {
			return false;
		}
		return true;
	}

	@Override
	// 连接超时重连
	public boolean waitIdle(int delay) {
		delay /= 10;
		while (--delay > 0) {
			if (mBusy)
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			else
				break;
		}
		return delay > 0;
	}

	@Override
	// 检测BluetoothAdapter是否存在
	public boolean hasGatt() {
		if (mBluetoothGatt != null) {
			return true;
		}
		return false;
	}

	@Override
	// 连接设备
	public boolean connect(BluetoothDevice mDevice, int i,
			BluetoothGattCallback mGattCallback) {
		String address = mDevice.getAddress();
		Utils.log("ADDRESS：" + address);
		if (mBluetoothAdapter == null || address == null) {
			return false;
		}
		final BluetoothDevice device = mBluetoothAdapter
				.getRemoteDevice(address);
		int connectionState = mBluetoothManager.getConnectionState(device,
				BluetoothProfile.GATT);
		if (connectionState == BluetoothProfile.STATE_CONNECTED
				&& mBluetoothGatt != null) {
		}

		if (connectionState == BluetoothProfile.STATE_DISCONNECTED) {
			Utils.log("mBluetoothDeviceAddress:%s", mBluetoothDeviceAddress);
			Utils.log("address:%s", address);
			if (mBluetoothDeviceAddress != null
					&& address.equals(mBluetoothDeviceAddress)
					&& mBluetoothGatt != null) {
				if (mBluetoothGatt.connect()) {
					Utils.log("couuect----------");
					return true;
				} else {
					return false;
				}
			}

			if (device == null) {
				return false;
			}

			mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
			Utils.log("couuect++++++++++++++");
			mBluetoothDeviceAddress = address;
			getGatt();
		} else {
			return false;
		}
		return false;
	}

	@Override
	// 重要标记
	// 维持一个心跳包，保持蓝牙连接
	public void autoSyn(final BluetoothGatt BluetoothGatt) {
		final Timer mKeepConnect = new Timer();
		mKeepConnect.schedule(new TimerTask() {
			byte[] msgLength = CRCUtil.getCRCMsg(new int[] { 9, 252, 0, 1, 2,
					3, 4 });

			@Override
			public void run() {
				if (!MyApp.mainSwitch) {
					Utils.log("==>bar code:" + Arrays.toString(msgLength));
					// write(msgLength, BluetoothGatt);

					Boolean state = getProfileState(BluetoothGatt.getDevice());
					if (!state) {
						System.out.println("设备连接不上");
						mKeepConnect.cancel();
					} else {
						writeData(msgLength, BluetoothGatt);
					}

				}
			}
		}, PublicConstant.WRITE_BLE_TIME, PublicConstant.WRITE_BLE_TIME);
		// 将timer放入队列中，防止被系统回收
		heartbeatQueue.add(mKeepConnect);
	}

	@Override
	// 开始监听电子邮件
	public void startMonitorEmail() {
		if (mMonitorTimer != null) {
			mMonitorTimer.cancel();
			mMonitorTimer = null;
		}
		mMonitorTimer = new Timer();
		mMonitorTimer.schedule(new TimerTask() {
			public void run() {
				boolean isMontior = sp.getBoolean("monitor_open_email", true);
				String email = sp.getString("monitor_email", "");
				if (isMontior && Utils.checkEmail(email)) {
					// Utils.monitorEmail();
				} else {
					cancel();
				}
			}
		}, 10000, 150000);
	}

	@Override
	// 停止监听电子邮件
	public void stopMonitorEmail() {
		if (mMonitorTimer != null) {
			mMonitorTimer.cancel();
			mMonitorTimer = null;
		}
	}

	@Override
	// 设置更新类型
	public void setUpdateTyep(int type) {
		mUpdateType = type;
	}

	@Override
	// 警报提醒
	public void sendAlert(int[] type, BluetoothGatt BluetoothGatt) {
		byte[] data = null;
		switch (type[0]) {
		case 0:// 普通消息
			data = MessageFactoryUtil.getGeneralMsg(type[1]);
			Utils.log("普通消息" + Arrays.toString(data));
			// write(data, BluetoothGatt);
			writeData(data, BluetoothGatt);
			break;
		case 1:// 升级准备消息

			break;
		case 2:
			break;
		case 3:// 用户信息改变
			data = MessageFactoryUtil.getMeterMsg(type[1]);
			Utils.log("用户信息" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
			break;
		case 13:// 同步准备消息
			data = MessageFactoryUtil.getSynBef(0);
			Utils.log("同步准备消息" + Arrays.toString(data));
			// edit by peter
			// synIsBreak(data, 1);
			break;
		case 11:// 每次修改睡眠目标后;
			data = MessageFactoryUtil.getSleepTargetMsg(type[1], type[2]);
			Utils.log("更改入眠目标后" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
			break;
		case 12:// 同步时间
			data = MessageFactoryUtil.getSynchronizeTimeMsg();
			Utils.log("同步时间" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
			break;
		case 4:// 每次闹钟设置发生改变时;
			data = MessageFactoryUtil.getAlarmChangeMsg(type);
			Utils.log("闹钟改变" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
			break;
		case 254:// 待机消息
			data = MessageFactoryUtil.getStandbyMsg(type[1]);
			Utils.log("待机消息" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
			break;
		case 20:// 获取version
			data = MessageFactoryUtil.getVersion();
			Utils.log("获取version消息" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
			break;
		case 23:// Clear Data
			data = MessageFactoryUtil.getClear(type[1]);
			Utils.log("清除屏幕信息" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
		case 24:
			data = MessageFactoryUtil.getCustomSetting();
			Utils.log("Custom Setting" + Arrays.toString(data));
			writeData(data, BluetoothGatt);
			break;
		default:
			break;
		}
	}

	@Override
	// 设置Handler
	public void setActivityHandler(Handler mHandler) {
		if (mActivityHandler == mHandler) {
			return;
		}
		mActivityHandler = mHandler;
	}

	public void searchService(final BluetoothDevice device) {
		new Thread() {
			public void run() {
				gattDiscoverServices(device);
			};
		}.start();

		// 这里比较郁闷，用Handler.postDelayed达不到断开连接后发现服务的效果，只能使用定时任务了
		prepareDisconnect(device);
	}

	private void prepareDisconnect(BluetoothDevice device) {
		Timer timer = new Timer();
		timer.schedule(new MyTask(device), PublicConstant.DISCOVER_SERVICE_TIME);
	}

	class MyTask extends TimerTask {
		BluetoothDevice device;

		public MyTask(BluetoothDevice device) {
			this.device = device;
		}

		@Override
		public void run() {
			disConnect(device);
		}
	}

	@Override
	public void onDestroy() {
		System.out.println("peter------>BluetoothOnDestroy");
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return getBinder();
	}

	public byte[] subBytes(byte[] src, int begin, int count) {
		byte[] bs = new byte[count];
		for (int i = begin; i < begin + count; i++)
			bs[i - begin] = src[i];
		return bs;
	}

	@Override
	// 自动连接
	public void connect(String macAddress) {
		// TODO Auto-generated method stub

	}

	// 蓝牙写信息同步队列
	// writeCharacteristic队列
	@SuppressWarnings("all")
	private synchronized void write(byte[] msg, BluetoothGatt BluetoothGatt) {
		if (sWriteCharacteristicQueue.isEmpty() && !sIsWriting) {
			BluetoothMsg bluetoothMsg = new BluetoothMsg(msg, BluetoothGatt);
			doWrite(bluetoothMsg);
		} else {
			BluetoothMsg bluetoothMsg = new BluetoothMsg(msg, BluetoothGatt);
			sWriteCharacteristicQueue.add(bluetoothMsg);
		}
	}

	private synchronized void nextWrite() {
		if (!sWriteCharacteristicQueue.isEmpty() && !sIsWriting) {
			doWrite(sWriteCharacteristicQueue.poll());
		}
	}

	private synchronized void doWrite(Object o) {

		if (o instanceof BluetoothMsg) {
			sIsWriting = true;
			BluetoothMsg bluetoothMsg = (BluetoothMsg) o;
			writeData(bluetoothMsg.getMsg(), bluetoothMsg.getBluetoothGatt());
		}
	}

	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// TODO Auto-generated method stub
			super.onCharacteristicChanged(gatt, characteristic);
			// 蓝牙通信回调
			// preasBlueByteArray(characteristic.getValue());
			System.out.println("onCharacteristicChanged"
					+ characteristic.getValue());
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO Auto-generated method stub
			super.onCharacteristicRead(gatt, characteristic, status);
			System.out.println("onCharacteristicRead");
			Utils.log("version = " + Arrays.toString(characteristic.getValue()));
			String value = "";
			for (byte charvalue : characteristic.getValue()) {
				value = value
						+ String.format(Locale.US, "%c", charvalue & 0xff);
				Utils.log("value = " + value);
			}
			Utils.log("version = " + characteristic.getStringValue(1));
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO Auto-generated method stub
			super.onCharacteristicWrite(gatt, characteristic, status);
			System.out.println("onCharacteristicWrite---->" + status
					+ "<------data-->" + characteristic.getValue());
			sIsWriting = false;
			nextWrite();
		}

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			// TODO Auto-generated method stub
			super.onConnectionStateChange(gatt, status, newState);

			if (newState == BluetoothProfile.STATE_CONNECTED) {
				if (status == BluetoothGatt.GATT_SUCCESS) {
					// 这里需要发现设备
					gattDiscoverServices(mBluetoothDevice);
				} else {
					System.out.println("onConnectionGATTFailed<----status--->"+status+"<----newState---->"+newState);
					final BluetoothDevice device= gatt.getDevice();
					if (device==null) {
						System.out.println("设备为空");
					}else {
						new Thread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								Utils.ThreadSleep(500);
								connect(device);
							}
						}).start();
						
					}
				}

			} else if (newState == BluetoothProfile.STATE_CONNECTING) {
				System.out
						.println("onConnectionStateChange----->STATE_CONNECTING");
			} else {
				
				System.out.println("onConnectionFailed<----status--->"+status+"<----newState---->"+newState);
				final BluetoothDevice device= gatt.getDevice();
				if (device==null) {
					System.out.println("设备为空");
				}else {
					new Thread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							Utils.ThreadSleep(500);
							connect(device);
						}
					}).start();
				}
			}
		}

		@Override
		public void onDescriptorRead(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			// TODO Auto-generated method stub
			super.onDescriptorRead(gatt, descriptor, status);
			System.out.println("onDescriptorRead");
			sIsWriting = false;
			nextWrite();
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			// TODO Auto-generated method stub
			super.onDescriptorWrite(gatt, descriptor, status);
			System.out.println("onDescriptorWrite--->status" + status
					+ "data-->" + descriptor.getValue());
		}

		@Override
		public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
			// TODO Auto-generated method stub
			super.onReadRemoteRssi(gatt, rssi, status);
			System.out.println("onReadRemoteRssi");
		}

		@Override
		public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
			// TODO Auto-generated method stub
			super.onReliableWriteCompleted(gatt, status);
			System.out.println("onReliableWriteCompleted");
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			// TODO Auto-generated method stub
			super.onServicesDiscovered(gatt, status);
			System.out.println("onServicesDiscovered");
			Utils.log("回调onServicesDiscovered()->status:%s", status);
			if (status == BluetoothGatt.GATT_SUCCESS) {
				Utils.log("成功发现服务...");
				// 连接成功
				Intent intent = new Intent();
				intent.setAction(ACTION);
				sendBroadcast(intent);// 传递过去
				System.out.println("成功发送广播");
				// 维持一个心跳包
				autoSyn(gatt);

			} else {
				Utils.log("发现服务失败...");
			}
		}
	};

}
