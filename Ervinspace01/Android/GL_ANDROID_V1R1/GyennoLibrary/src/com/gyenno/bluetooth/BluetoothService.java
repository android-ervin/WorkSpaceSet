package com.gyenno.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.os.Handler;
public interface BluetoothService {
	// 获取本地蓝牙设备 -- 蓝牙适配器
	public BluetoothAdapter getBluetoothAdapter(Context context);

	// 扫描设备
	public void scan(boolean flag,
			BluetoothAdapter.LeScanCallback mLeScanCallback);

	// 打开蓝牙广播
	public BluetoothGattCharacteristic openBLENotification(BluetoothGatt BluetoothGatt);

	// 两个设备通过BLE通信，首先需要建立GATT连接。这里我们是Android设备作为client端，连接GATT Server
	public boolean connect();

	// 连接远程设备
	public boolean connect(BluetoothDevice mDevice);

	// 断开设备
	public void disConnect(BluetoothDevice mDevice);

	// 获取BluetoothGatt
	// 继承BluetoothProfile，通过BluetoothGatt可以连接设备（connect）,发现服务（discoverServices），并把相应地属性返回到BluetoothGattCallback
	public void getGatt();

	// 判断设备是否连接上
	public boolean getProfileState(BluetoothDevice mDevice);

	// 执close断开
	public void close();

	// 关闭外围蓝牙
	public void gattDiscoverServices(BluetoothDevice device);

	// 对外围蓝牙写数据
	public void writeData(byte[] data,BluetoothGatt BluetoothGatt);

	// 对外围蓝牙写数据
	public boolean writeOadData(int pos, byte[] data);

	// 获取数据
	public int getOadCharacteristicsSize();

	// 启用通知
	public boolean enableNotification(int pos, boolean enable);

	// 检测BluetoothAdapter是否为空
	public boolean checkGatt();

	// 连接超时重连
	public boolean waitIdle(int delay);

	// 检测BluetoothAdapter是否存在
	public boolean hasGatt();

	// 连接设备
	public boolean connect(BluetoothDevice mDevice, int i,BluetoothGattCallback mGattCallback);

	// 自动连接设备
	public void connect(String macAddress);

	// 自动同步数据
	public void autoSyn(BluetoothGatt BluetoothGatt);

	// 开始监听电子邮件
	public void startMonitorEmail();

	// 停止监听电子邮件
	public void stopMonitorEmail();

	// 设置更新类型
	public void setUpdateTyep(int type);

	// 发出警报提醒
	public void sendAlert(int[] type,BluetoothGatt BluetoothGatt);

	// 设置Handler
	public void setActivityHandler(Handler mHandler);

	// 开始上传数据至服务器
	// 蓝牙服务不能写网络操作
	//public void startSync();

	// 开始同步数据
	// 蓝牙服务不能写网络操作
	//public void startUpload();

	// 打开蓝牙
	public void openBluetooth();

	// 关闭蓝牙
	public void closeBluetooth();
}
