package com.gyenno.bluetooth;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import com.gyenno.tools.Utils;

public class MyBluetoothManager {

	private BluetoothService mService = new BluetoothServiceImpl();
	private Context context;

	public MyBluetoothManager() {

	}

	public MyBluetoothManager(Context context) {
		this.context = context;
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
	// 获取本地蓝牙设备 -- 蓝牙适配器
	public BluetoothAdapter getBluetoothAdapter() {
		return mService.getBluetoothAdapter(context);
	}

	// 打开蓝牙操作的服务(service)
	public void openService() {
		final Intent bindIntent = new Intent(context,
				BluetoothServiceImpl.class);
		// startService()方法启用服务，调用者与服务之间没有关连
		context.startService(bindIntent);
		// bindService()方 法启用服务，调用者与服务绑定在了一起，调用者一旦退出，服务也就终止，大有“不求同时生，必须同时死”的特点
		context.bindService(bindIntent, mServiceConnection,
				Context.BIND_AUTO_CREATE);
	}

	// 蓝牙打开
	public void openBluetooth() {
		if (mService == null) {
			openService();
		}

		mService.openBluetooth();
	}

	// 蓝牙关闭
	public void closeBluetooth() {
		if (mService == null) {
			openService();
		}
		mService.closeBluetooth();
	}

	// 服务
	ServiceConnection mServiceConnection = new ServiceConnection() {
		public void onServiceDisconnected(ComponentName arg0) {
			mService = null;
			Utils.log("service连接断开");
		}

		// 打开服务
		public void onServiceConnected(ComponentName arg0, IBinder binder) {
			mService = ((BluetoothServiceImpl.LocalBinder) binder).getService();
			Utils.log("service已打开");
		}
	};

	// 停止蓝牙扫描
	public void connectBLE(int position) {
		if (mService == null) {
			openService();
		}
		mService.scan(false, null);
	}

	// 蓝牙扫描
	public void scan(boolean flag, BluetoothAdapter.LeScanCallback callBack) {
		if (mService == null) {
			openService();
		}
		mService.scan(flag, callBack);
	}

	// 获取蓝牙设备
	public void connect(BluetoothDevice device) {
		if (mService == null) {
			openService();
		}
		mService.connect(device);
	}

	// 关闭蓝牙设备
	public void close() {
		if (mService == null) {
			openService();
		}
		mService.close();
	}

	// 获取中央BluetoothGatt
	public void connect() {
		if (mService == null) {
			openService();
		}
		mService.connect();
	}

	public void autoSyn(BluetoothGatt BluetoothGatt) {
		if (mService == null) {
			openService();
		}
		mService.autoSyn(BluetoothGatt);
	}

	// 监视电子邮件
	public void startMonitorEmail() {
		if (mService == null) {
			openService();
		}
		mService.startMonitorEmail();
	}

	public void stopMonitorEmail() {
		if (mService == null) {
			openService();
		}
		mService.stopMonitorEmail();
	}

	public void connect(String macAddress) {
		if (mService == null) {
			openService();
		}
		mService.connect(macAddress);
	}

	public void setUpdateTyep(int type) {
		if (mService == null) {
			openService();
		}
		mService.setUpdateTyep(type);
	}

	public void sendAlert(int[] type,BluetoothGatt BluetoothGatt) {
		if (mService == null) {
			openService();
		}
		mService.sendAlert(type,BluetoothGatt);
	}

	public void disConnect(BluetoothDevice device) {
		if (mService == null) {
			openService();
		}
		mService.disConnect(device);
	}

	public void setHandler(Handler mHandler) {
		if (mService == null) {
			openService();
		}
		mService.setActivityHandler(mHandler);
	}

	public int getOadCharacteristicsSize() {
		if (mService == null) {
			openService();
		}
		return mService.getOadCharacteristicsSize();
	}

	public boolean writeOadData(int type, byte[] data) {
		if (mService == null) {
			openService();
		}
		return mService.writeOadData(type, data);
	}

	public boolean enableNotification(int pos, boolean enable) {
		// TODO Auto-generated method stub
		if (mService == null) {
			openService();
		}
		return mService.enableNotification(pos, enable);
	}

	public boolean waitIdle(int delay) {
		if (mService == null) {
			openService();
		}
		return mService.waitIdle(delay);
	}

	public boolean hasGatt() {
		if (mService == null) {
			openService();
		}
		return mService.hasGatt();
	}

	public void openBLENotification(BluetoothGatt BluetoothGatt) {
		if (mService == null) {
			openService();
		}
		mService.openBLENotification(BluetoothGatt);
	}

	public void gattDiscoverServices(final BluetoothDevice device) {
		if (mService == null) {
			openService();
		}
		//是否需要另外开启线程？
		mService.gattDiscoverServices(device);
	}
	
	public void unbindService() {
		context.unbindService(mServiceConnection);
	}
}
