package com.gyenno.im;

import java.io.ByteArrayInputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.gyenno.tools.EncryptUtil;
import com.yzx.tools.CustomLog;

public abstract class AbsRestClient {

	// public static String ip = "172.16.10.32";
	// public static int port = 8080;
	public static String ip = "api.ucpaas.com";
	// public static String ip = "113.31.89.144";

	public static String version = "2014-06-30";

	public StringBuffer getStringBuffer() {
		StringBuffer sb = new StringBuffer("https://");
		sb.append(ip);// .append(":").append("443");//.append("/rest");// /impl
		return sb;
	}

	public DefaultHttpClient getDefaultHttpClient() {
		DefaultHttpClient httpclient = new DefaultHttpClient();

		return httpclient;
	}

	/**
	 * 
	 * @param version
	 * @param auth
	 * @param sig
	 * @param account
	 * @return String login
	 */
	public abstract String login(String username, String password);

	public abstract String queryAccountsInfo(String account, String token);

	public String getSignature(String accountSid, String authToken,
			String timestamp, EncryptUtil encryptUtil) throws Exception {
		String sig = accountSid + authToken + timestamp;
		String signature = encryptUtil.md5Digest(sig);
		return signature;
	}

	public HttpResponse post(String accountSid, String timestamp, String url,
			DefaultHttpClient httpclient, EncryptUtil encryptUtil, String body)
			throws Exception {

		HttpPost httppost = new HttpPost(url);
		CustomLog.v("BODY:" + body);
		CustomLog.v("REST_URL:" + url);
		httppost.setHeader("Accept", "application/json");
		httppost.setHeader("Content-Type", "application/json;charset=utf-8");
		String src = accountSid + ":" + timestamp;
		String auth = encryptUtil.base64Encoder(src);
		httppost.setHeader("Authorization", auth);
		if (body != null && body.length() > 0) {
			BasicHttpEntity requestBody = new BasicHttpEntity();
			requestBody.setContent(new ByteArrayInputStream(body
					.getBytes("UTF-8")));
			requestBody.setContentLength(body.getBytes("UTF-8").length);
			httppost.setEntity(requestBody);
		}
		// 执行客户端请求
		return httpclient.execute(httppost);
	}

	public HttpResponse get(String accountSid, String timestamp, String url,
			DefaultHttpClient httpclient, EncryptUtil encryptUtil)
			throws Exception {
		CustomLog.v("REST_GET_URL:" + url);
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("Accept", "application/json");
		httpGet.setHeader("Content-Type", "application/json;charset=utf-8");
		String src = accountSid + ":" + timestamp;
		String auth = encryptUtil.base64Encoder(src);
		httpGet.setHeader("Authorization", auth);
		// 执行客户端请求
		return httpclient.execute(httpGet);
	}
}
