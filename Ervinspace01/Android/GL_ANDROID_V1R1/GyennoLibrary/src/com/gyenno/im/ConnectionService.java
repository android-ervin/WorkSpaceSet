package com.gyenno.im;

import java.util.ArrayList;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import com.yzx.api.UCSCall;
import com.yzx.api.UCSMessage;
import com.yzx.api.UCSService;
import com.yzx.listenerInterface.CallStateListener;
import com.yzx.listenerInterface.ConnectionListener;
import com.yzx.listenerInterface.MessageListener;
import com.yzx.listenerInterface.UcsReason;
import com.yzx.tcp.packet.UcsMessage;
import com.yzx.tools.CustomLog;

public class ConnectionService extends Service implements ConnectionListener,
		CallStateListener, MessageListener {

	private final IBinder binder = new LocalBinder();

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return binder;
	}

	public class LocalBinder extends Binder {
		public ConnectionService getService() {
			return ConnectionService.this;
		}
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		// 添加连接监听器
		UCSService.addConnectionListener(ConnectionService.this);
		// 添加电话监听器
		UCSCall.addCallStateListener(ConnectionService.this);
		// 添加消息监听器
		UCSMessage.addMessageListener(ConnectionService.this);
		// 初始化SDK
		UCSService.init(this, true);
	}

	@Override
	public void onDownloadAttachedProgress(String arg0, String arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceiveUcsMessage(UcsReason arg0, UcsMessage arg1) {
		// TODO Auto-generated method stub

		System.out.println("onReceiveUcsMessage---->收到消息信息啦----->"+arg1.getMsg());
	}

	@Override
	public void onSendFileProgress(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSendUcsMessage(UcsReason arg0, UcsMessage arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	@SuppressWarnings("all")
	public void onUserState(ArrayList arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAlerting(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnswer(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCallBackSuccess() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDialFailed(String arg0, UcsReason arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onHangUp(String arg0, UcsReason arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onIncomingCall(String arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionFailed(UcsReason arg0) {
		// TODO Auto-generated method stub
		System.out.println("登陆失败");
	}

	@Override
	public void onConnectionSuccessful() {
		// TODO Auto-generated method stub
		System.out.println("登陆成功");
	}

	// 登陆主账号
	public void login() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				JsonReqClient client = new JsonReqClient();
				String json = client.login("peter19911027@126.com",
						"ml19911027");
				CustomLog.v(DfineAction.TAG_TCP, "RESULT:" + json);
			}
		}).start();

	}

	// 登陆client账号
	public void loginClient() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				UCSService.connect("20821be56d3ba3b14f889520104f0cd6",
						"9b8f12923f25ed927d24210e867c42c9", "76425018336597",
						"2889c73d");

			}
		}).start();

	}

	public void SendMessageToClient() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				UCSMessage.sendUcsMessage("76425018336597", "哈哈哈，这是一条消息", null,
						1);

			}
		}).start();

	}

}
