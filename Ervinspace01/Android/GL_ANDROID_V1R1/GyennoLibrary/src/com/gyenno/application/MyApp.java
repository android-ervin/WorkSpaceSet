package com.gyenno.application;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.os.Handler;

import com.gyenno.model.BluetoothConnected;

public class MyApp extends Application {

	// Utils 类中判断的日期，时间，handler。暂时先放在这里，这里用全局类不好
	public static Calendar startCalender;
	public static Calendar endCalendar;
	public static int startMin;
	public static int endMin;
	public static boolean isSameDay;
	public static int year, month, day;
	public static JSONArray array;
	public static boolean mainSwitch;
	public static Handler handler;
	public static Handler mUploadAndSyncHand;

	// 已连接设备的集合
	public static List<BluetoothConnected> connecteds = new ArrayList<BluetoothConnected>();

	public static Boolean findGattUpdate(BluetoothGatt mBluetoothGatt,BluetoothConnected bluetoothConnected) {
		for (int i = 0; i < connecteds.size(); i++) {
			BluetoothConnected bluetoothConnect = connecteds.get(i);
			if (bluetoothConnect.getmBluetoothGatt() == mBluetoothGatt) {
				connecteds.remove(i);
				connecteds.add(bluetoothConnected);
				return true;
			}
		}
		return false;
	}
	
	public static Boolean findDeviceUpdate(BluetoothDevice mDevice,BluetoothConnected bluetoothConnected) {
		for (int i = 0; i < connecteds.size(); i++) {
			BluetoothConnected bluetoothConnect = connecteds.get(i);
			if (bluetoothConnect.getDevice().getAddress() .equals(mDevice.getAddress()) ) {
				connecteds.remove(i);
				connecteds.add(bluetoothConnected);
				return true;
			}
		}
		return false;
	}
	
	public static BluetoothDevice findDevice(BluetoothGatt mBluetoothGatt) {
		for (int i = 0; i < connecteds.size(); i++) {
			BluetoothConnected bluetoothConnect = connecteds.get(i);
			if (bluetoothConnect.getmBluetoothGatt() == mBluetoothGatt) {
				
				return bluetoothConnect.getDevice();
			}
		}
		return null;
	}
}
