package com.gyenno.application;

import java.io.File;

import android.content.SharedPreferences;
import android.graphics.Typeface;

import com.gyenno.bluetooth.MyBluetoothManager;
import com.gyenno.database.OneSqliteHelper;
import com.gyenno.tools.CustomSetting;
import com.gyenno.tools.UsersInfo;

/**
 * 初始化 app的全局变量
 * 
 * @author xh
 * 
 */
public class InitializationAppData {
	private static UsersInfo usersinfo;// 用户信息
	private static MyBluetoothManager bluetooth;//蓝牙信息
	private static Typeface cnfont;//字体样式
	private static Typeface nofont;//字体样式
	private static SharedPreferences sp;//保存信息
	private static OneSqliteHelper helper;//本地数据库信息
	private static String daySqlite;//数据库日期
	private static String selectDay;//查询库日期
	private static File cache;//文件目录
	private static CustomSetting mCustomSetting;//震动自定义
	private static String app_id;//APP_ID
	public static UsersInfo getUsersinfo() {
		return usersinfo;
	}
	public static void setUsersinfo(UsersInfo usersinfo) {
		InitializationAppData.usersinfo = usersinfo;
	}
	public static MyBluetoothManager getBluetooth() {
		return bluetooth;
	}
	public static void setBluetooth(MyBluetoothManager bluetooth) {
		InitializationAppData.bluetooth = bluetooth;
	}
	public static Typeface getCnfont() {
		return cnfont;
	}
	public static void setCnfont(Typeface cnfont) {
		InitializationAppData.cnfont = cnfont;
	}
	public static Typeface getNofont() {
		return nofont;
	}
	public static void setNofont(Typeface nofont) {
		InitializationAppData.nofont = nofont;
	}
	public static SharedPreferences getSp() {
		return sp;
	}
	public static void setSp(SharedPreferences sp) {
		InitializationAppData.sp = sp;
	}
	public static OneSqliteHelper getHelper() {
		return helper;
	}
	public static void setHelper(OneSqliteHelper helper) {
		InitializationAppData.helper = helper;
	}
	public static String getDaySqlite() {
		return daySqlite;
	}
	public static void setDaySqlite(String daySqlite) {
		InitializationAppData.daySqlite = daySqlite;
	}
	public static String getSelectDay() {
		return selectDay;
	}
	public static void setSelectDay(String selectDay) {
		InitializationAppData.selectDay = selectDay;
	}
	public static File getCache() {
		return cache;
	}
	public static void setCache(File cache) {
		InitializationAppData.cache = cache;
	}
	public static CustomSetting getmCustomSetting() {
		return mCustomSetting;
	}
	public static void setmCustomSetting(CustomSetting mCustomSetting) {
		InitializationAppData.mCustomSetting = mCustomSetting;
	}
	public static String getApp_id() {
		return app_id;
	}
	public static void setApp_id(String app_id) {
		InitializationAppData.app_id = app_id;
	}
}
