package com.gyenno.tools;

import java.io.Serializable;

public class DayData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3403084833590894425L;
	public String date;
	public String data;
	public int ms;
	public int sleeps;
	public int calories;
	public int steps;
	public String timeSlot; // 活动起始终止时间段
	public int dsSleep;
	public double dsPoint;

	public DayData() {
		super();
	}

	public DayData(String date, String data, int ms, int sleeps, int calories, int steps, String timeSlot,
			int dsSleep, double dsPoint) {
		super();
		this.date = date;
		this.data = data;
		this.ms = ms;
		this.sleeps = sleeps;
		this.calories = calories;
		this.steps = steps;
		this.timeSlot = timeSlot;
		this.dsSleep = dsSleep;
		this.dsPoint = dsPoint;
	}

	/**
	 * 日期
	 * 
	 * @return
	 */
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * 获取Data
	 * 
	 * @return
	 */
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	/**
	 * 获取公里数
	 * 
	 * @return
	 */
	public int getms() {
		return ms;
	}

	public void setms(int ms) {
		this.ms = ms;
	}

	/**
	 * 获取每天睡眠时长
	 * 
	 * @return
	 */
	public int getSleeps() {
		return sleeps;
	}

	public void setSleeps(int sleeps) {
		this.sleeps = sleeps;
	}

	/**
	 * 获取每天消耗卡路里
	 * 
	 * @return
	 */
	public int getCalories() {
		return calories;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	/**
	 * 获取每天走的步数
	 * 
	 * @return
	 */
	public int getSteps() {
		return steps;
	}

	public void setSteps(int steps) {
		this.steps = steps;
	}

	/**
	 * 获取每天的活动时间轴
	 * 
	 * @return
	 */
	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	/**
	 * 获取每天深度睡眠时长
	 * 
	 * @return
	 */
	public int getDsSleep() {
		return dsSleep;
	}

	public void setDsSleep(int dsSleep) {
		this.dsSleep = dsSleep;
	}

	/**
	 * 获取每天 入眠点
	 * 
	 * @return
	 */
	public double getDsPoint() {
		return dsPoint;
	}

	public void setDsPoint(double dsPoint) {
		this.dsPoint = dsPoint;
	}

	@Override
	public String toString() {
		return "DayData [date=" + date + " kms=" + ms + ", sleeps=" + sleeps + ", calories=" + calories
				+ ", steps=" + steps + ", timeSlot=" + timeSlot + ", dsSleep=" + dsSleep + ", dsPoint="
				+ dsPoint + ", data=" + data + "]";
	}

}
