package com.gyenno.tools;

public class ConstantFinal {
	public final static boolean DEBUG = false;//log打印开关
	public final static String TAG = "ONE_WATCH";//log标识
	//public static final String PATH = "http://app.gyenno.com/serviceApp/";
	public static final String PATH = "http://192.168.0.114:8080/serviceApp/";//http请求地址
	public static final int SUCCESS = 1000;//成功
	public static final int TOLOGINACTIVITY = 1001;//首页欢迎界面如果未登录 再次启动跳转到登录页面activity
	public static final int TOMAINACTIVITY = 1002;//首页欢迎界面如果已登录 再次启动跳转到主页面activity
	public static final int TOSEARCHACTIVITY = 1003;//首页欢迎界面如果已登录但未绑定 再次启动跳转到搜索蓝牙页面activity
	public static final int QQSKDLOGIN = 1004;//qq sdk 登录成功后跳转的 Handler
	public static final int AUTHORIZATIONFAIL = 1005;//qq sdk 登录授权失败
	public static final int SAVEQFAIL = 1006;//qq sdk 登录获取信息失败
	public static final int PASSERROR = 1007;//普通用户登录用户名或者密码错误
	public static final int LOGINFAIL = 1008;//普通用户登录失败
	public static final int SENDPASSWORDSUCCESS = 1009;//发送密码成功
	public static final int PASSRESETFAIL = 1010;//密码重置失败
	public static final int PASSRESETUSERERROR = 1011;//密码重置用户不存在
	public static final int REQUESTERROR = 1012;//请求错误
	public static final int CONNECTIONFAIL = 1013;//连接错误
	public static final int GETTARGETSPORTSFAIL = 1014;//获取运动目标失败
	public static final int SETGOALFAIL = 1015;//设置运动目标失败
	public static final int GETTARGETSLEEPINGFAIL = 1016;//获取睡眠目标失败
	public static final int TYPE_CUSTOM = 0x0;//目标类型
	public static final int TYPE_LEVEL_ONE = 0x1;//目标类型1
	public static final int TYPE_LEVEL_TWO = 0x2;//目标类型2
	public static final int TYPE_LEVEL_THREE = 0x3;//目标类型3
	public static final int TYPE_LEVEL_FOUR = 0x4;//目标类型4
	public static final int LEVEL_ONE_GOAL_STEP = 3000;//目标类型1默认值
	public static final int LEVEL_TWO_GOAL_STEP = 5000;//目标类型2默认值
	public static final int LEVEL_THREE_GOAL_STEP = 8000;//目标类型3默认值
	public static final int LEVEL_FOUR_GOAL_STEP = 10000;//目标类型4默认值
}
