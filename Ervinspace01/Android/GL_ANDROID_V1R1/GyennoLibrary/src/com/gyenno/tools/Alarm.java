package com.gyenno.tools;

public class Alarm {

	// 0表示非选中, 非勾选
	private int hour, minute, enable_one, enable_two, enable_three, enable_four, enable_five, enable_six,
			enable_seven, type;

	public Alarm() {
	}

	/**
	 * 
	 * @param hour 时
	 * @param minute 分
	 * @param enable_one 星期一
	 * @param enable_two 星期二
	 * @param enable_three 星期三
	 * @param enable_four 星期四
	 * @param enable_five 星期五
	 * @param enable_six 星期六
	 * @param enable_seven 星期日 
	 * @param type 智能闹钟状态（0非打开，1打开）
	 */
	public Alarm(int hour, int minute, int enable_one, int enable_two, int enable_three, int enable_four,
			int enable_five, int enable_six, int enable_seven, int type) {
		super();
		this.hour = hour;
		this.minute = minute;
		this.enable_one = enable_one;
		this.enable_two = enable_two;
		this.enable_three = enable_three;
		this.enable_four = enable_four;
		this.enable_five = enable_five;
		this.enable_six = enable_six;
		this.enable_seven = enable_seven;
		this.type = type;
	}
	
	public boolean getEnable()
	{
		int enable = (type >> 16 & 0xff);
		boolean result = true;
		if(enable == 1)//0=enable 1=disable
		{
			result = false;
		}
		return result;
	}
	
	public void setEnable(boolean enable)
	{
		int value = (1 << 16);
		if(enable)
		{
			type &= (~value);
		}
		else
		{
			type |= value;
		}
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getEnable_one() {
		return enable_one;
	}

	public void setEnable_one(int enable_one) {
		this.enable_one = enable_one;
	}

	public int getEnable_two() {
		return enable_two;
	}

	public void setEnable_two(int enable_two) {
		this.enable_two = enable_two;
	}

	public int getEnable_three() {
		return enable_three;
	}

	public void setEnable_three(int enable_three) {
		this.enable_three = enable_three;
	}

	public int getEnable_four() {
		return enable_four;
	}

	public void setEnable_four(int enable_four) {
		this.enable_four = enable_four;
	}

	public int getEnable_five() {
		return enable_five;
	}

	public void setEnable_five(int enable_five) {
		this.enable_five = enable_five;
	}

	public int getEnable_six() {
		return enable_six;
	}

	public void setEnable_six(int enable_six) {
		this.enable_six = enable_six;
	}

	public int getEnable_seven() {
		return enable_seven;
	}

	public void setEnable_seven(int enable_seven) {
		this.enable_seven = enable_seven;
	}
	
	public int getAllType()
	{
		return type;
	}
	
	public void setAllType(int type)
	{
		this.type = type;
	}

	public int getType() {
		return type & 0xff;
	}

	public void setType(int type) {
		if(type == 1)
		{
			this.type |= type;
		}
		else if(type == 0)
		{
			this.type &= (~type);
		}
	}
	
	public boolean isSame(String time) {
		return (hour + ":" + minute).equals(time);
	}

	@Override
	public String toString() {
		return "Alarm [hour=" + hour + ", minute=" + minute + ", enable_one=" + enable_one + ", enable_two="
				+ enable_two + ", enable_three=" + enable_three + ", enable_four=" + enable_four
				+ ", enable_five=" + enable_five + ", enable_six=" + enable_six + ", enable_seven="
				+ enable_seven + ", type=" + type + "]";
	}
}
