package com.gyenno.tools;
public class ConstantPool {
	/**
	 * ���ʷ�������������
	 */
	public static final int OK = 100;
	public static final int REQUEST = 50;
	public static final int FACE = 49;
	public static final int MIN = 60;
	public static final float CALORIE = 1.25f;
	public static final float METER = 0.3048f;
	public static final int IS_NEED_UPDATE = 10000; // ѯ���Ƿ���Ҫ����
	public static final int UPGRADE_FIRMWARE = 10001; // ����İ汾����
	public static final int FINDPASSWORD = 10002; // 找回密码
	public static final int DOWNLOADRESPACKAGE = 10004;
	public static final int DOWNLOADIMAGEPACKAGE = 10005;
	public static final int DOWNLOADBLUEPACKAGE = 10006;
	public static final float AXISSCALE = 23.0f; // X/Y 轴刻度的字体大小
	public static final int QQ = 101;

	/**
	 * 一周的毫秒数
	 */
	public static final long WEEK_MS = 7 * 86400000;
	public static final long DAY_MS = 86400000;
	public static final int UPLOADREQUEST = 10002;
	public static final int UPLOAD = 10003;
	public static final int UNBIND = 25;
	public static final int REQUESTCODE = 5;
	public static final int RESULTCODE = 10;
	public static final int REQUESTFAIL = -1;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// ����
	public static final int PHOTOZOOM = 2; // ����
	public static final int PHOTORESOULT = 3;// ���
	public static final String IMAGE_UNSPECIFIED = "image/*";
	public static final String TEMP = "temp.png";
	public static final int UNBINDUSER = 26;
	// 设置sharedpreference
	public static final String SETTING_PREFS = "setting_prefs";

	public static final String[] MONTH = new String[] { "01", "", "", "04", "", "", "", "08", "", "", "", "12",
			"", "", "", "16", "", "", "", "20", "", "", "", "24", "", "", "", "28", "", "", "31" };
	public static final String[] HOUR = new String[] { "1h", "", "", "4h", "", "", "", "8h", "", "", "", "12h",
			"", "", "", "16h", "", "", "", "20h", "", "", "", "24h" };

	public static final String[] UNITCALORIE = new String[] { "0.5", "1.0", "1.5", "2.0", "2.5" };
	public static final String[] SLEEPLENGTH = new String[] { "", "2h", "4h", "6h", "8h", "10h", "12h" };
	public static final String[] SLEEPTIME = new String[] { "", "20:00", "22:00", "00:00", "02:00", "04:00",
			"06:00" };
}
