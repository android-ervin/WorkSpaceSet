package com.gyenno.tools;


public class CustomSetting{
	private int vibrationNumber_phone;
	private int vibrationNumber_EMS;
	private int vibrationNumber_mail;
	private int vibrationNumber_schedule;
	private int vibrationNumber_lowPowe;
	private int vibrationNumber_breakOff;
	private int vibrationNumber_weiXin;
	private int vibrationNumber_qq;
	private int vibrationNumber_clock;
	private int vibrationNumber_Normal;
	public int getVibrationNumber_phone() {
		return vibrationNumber_phone;
	}
	public void setVibrationNumber_phone(int vibrationNumber_phone) {
		this.vibrationNumber_phone = vibrationNumber_phone;
	}
	public int getVibrationNumber_EMS() {
		return vibrationNumber_EMS;
	}
	public void setVibrationNumber_EMS(int vibrationNumber_EMS) {
		this.vibrationNumber_EMS = vibrationNumber_EMS;
	}
	public int getVibrationNumber_mail() {
		return vibrationNumber_mail;
	}
	public void setVibrationNumber_mail(int vibrationNumber_mail) {
		this.vibrationNumber_mail = vibrationNumber_mail;
	}
	public int getVibrationNumber_schedule() {
		return vibrationNumber_schedule;
	}
	public void setVibrationNumber_schedule(int vibrationNumber_schedule) {
		this.vibrationNumber_schedule = vibrationNumber_schedule;
	}
	public int getVibrationNumber_lowPowe() {
		return vibrationNumber_lowPowe;
	}
	public void setVibrationNumber_lowPowe(int vibrationNumber_lowPowe) {
		this.vibrationNumber_lowPowe = vibrationNumber_lowPowe;
	}
	public int getVibrationNumber_breakOff() {
		return vibrationNumber_breakOff;
	}
	public void setVibrationNumber_breakOff(int vibrationNumber_breakOff) {
		this.vibrationNumber_breakOff = vibrationNumber_breakOff;
	}
	public int getVibrationNumber_weiXin() {
		return vibrationNumber_weiXin;
	}
	public void setVibrationNumber_weiXin(int vibrationNumber_weiXin) {
		this.vibrationNumber_weiXin = vibrationNumber_weiXin;
	}
	public int getVibrationNumber_qq() {
		return vibrationNumber_qq;
	}
	public void setVibrationNumber_qq(int vibrationNumber_qq) {
		this.vibrationNumber_qq = vibrationNumber_qq;
	}
	public int getVibrationNumber_clock() {
		return vibrationNumber_clock;
	}
	public void setVibrationNumber_clock(int vibrationNumber_clock) {
		this.vibrationNumber_clock = vibrationNumber_clock;
	}
	public int getVibrationNumber_Normal() {
		return vibrationNumber_Normal;
	}
	public void setVibrationNumber_Normal(int vibrationNumber_Normal) {
		this.vibrationNumber_Normal = vibrationNumber_Normal;
	}
}
