package com.gyenno.tools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.gyenno.application.InitializationAppData;


/**
 * 工具类
 * @author xh
 *
 */
public class Tools {
	/**
	 * 获取HTTP请求的返回值
	 * @param inStream
	 * @return
	 * @throws IOException
	 */
	public static String read(InputStream inStream) throws IOException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		inStream.close();
		String result = new String(outStream.toByteArray(), "UTF-8");
		outStream.close();
		return result;
	}
	/**
	 * 获取图片信息
	 * @param path
	 * @return
	 */
	public static String getImage(String path) {
		String urlPath = path;
		if(!urlPath.endsWith(".png")){
			urlPath = urlPath + ".png";
		}
		File cache = InitializationAppData.getCache();
		File localFile = new File(cache, MD5.getMD5(urlPath)
				+ urlPath.substring(urlPath.lastIndexOf(".")));
		Utils.log("localFile = " + localFile.getAbsolutePath());
		if (localFile.exists()) {
			return localFile.getAbsolutePath();//Uri.fromFile(localFile);
		} else {
			HttpURLConnection conn;
			try {
				conn = (HttpURLConnection) new URL(path).openConnection();
				conn.setConnectTimeout(5000);
				conn.setRequestMethod("GET");
				if (conn.getResponseCode() == 200) {
					FileOutputStream outStream = new FileOutputStream(localFile);
					InputStream inputStream = conn.getInputStream();
					byte[] buffer = new byte[1024];
					int len = 0;
					while ((len = inputStream.read(buffer)) != -1) {
						outStream.write(buffer, 0, len);
					}
					inputStream.close();
					outStream.close();
					return localFile.getAbsolutePath();//Uri.fromFile(localFile);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
