package com.gyenno.tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.gyenno.application.InitializationAppData;
import com.gyenno.application.MyApp;
import com.gyenno.contants.PublicConstant;
import com.gyenno.database.OneSqliteHelper;

public class Utils {

	final static boolean DEBUG = true;
	final static String TAG = "gyenno";

	public static final int UPDATE_TYPE_REPAIR = 0x01;
	public static final int UPDATE_TYPE_NORMAL = 0x02;

	// private static MyBluetoothManager tooth =
	// InitializationAppData.getBluetooth();
	// private static SharedPreferences sp = InitializationAppData.getSp();
	private static OneSqliteHelper helper = InitializationAppData.getHelper();

	// private static CustomSetting customSetting =
	// InitializationAppData.getmCustomSetting();

	/**
	 * 打印日志
	 */
	public static void log(String s, Object... args) {
		if (DEBUG) {
			if (args != null && args.length > 0) {
				s = String.format(Locale.US, s, args);
			}

			Log.i(TAG, s);
		}

	}

	/**
	 * 弹出提示 使用静态成员变量保证toast不延时
	 */
	public static Toast toast;

	public static void toast(Context c, String s, Object... args) {
		s = String.format(Locale.US, s, args);
		if (toast == null) {
			toast = Toast.makeText(c, "", PublicConstant.TOAST_SHOW_TIME);
			toast.setGravity(Gravity.CENTER, 0, 0);
		}
		toast.setText(s);
		toast.show();
	}

	public static boolean checkEmail(String email) {
		boolean flag = false;

		if (email == null || email.length() == 0) {
			return flag;
		}
		try {
			// String check =
			// "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			String check = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
			// Pattern regex = Pattern.compile(check);
			// Matcher matcher = regex.matcher(email);
			flag = email.matches(check);
			// flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 得到当周已过去的日期集合
	 * 
	 * @return
	 */
	public static ArrayList<String> getUsedDayOfWeek() {
		ArrayList<String> allDay = new ArrayList<String>();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		int indexInWeek = (cal.get(Calendar.DAY_OF_WEEK) - 1 + 6) % 7;

		cal.add(Calendar.DAY_OF_YEAR, -indexInWeek);

		for (int i = 0; i <= indexInWeek; i++) {
			allDay.add(sf.format(cal.getTime()));
			cal.add(Calendar.DAY_OF_YEAR, 1);
		}
		return allDay;
	}

	public static void setSqlData() {
		new Thread() {
			public void run() {
				Calendar calendar = Calendar.getInstance();
				int mYear = calendar.get(Calendar.YEAR);
				int mMonth = calendar.get(Calendar.MONTH) + 1;
				int mDay = calendar.get(Calendar.DAY_OF_MONTH);
				MyApp.endMin = calendar.get(Calendar.HOUR_OF_DAY) * 60
						+ calendar.get(Calendar.MINUTE);
				String date = Utils.getDate(mYear) + "-"
						+ Utils.getDate(mMonth) + "-" + Utils.getDate(mDay);
				helper.insert(date,
						Utils.setSqlData(0, MyApp.endMin, new JSONArray()), 0,
						0, 0, 0, "[]", 0, -1);
			}
		}.start();
	}

	public static String getDate(int i) {
		return i < 10 ? "0" + i : i + "";
	}

	public static String setSqlData(int startMin, int endMin, JSONArray array) {
		try {
			for (int i = startMin; i < endMin; i++) {
				JSONObject object = new JSONObject();
				object.put("m", i);
				object.put("f", 0);
				object.put("e", 0);
				array.put(object);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return array.toString();
	}

	public static int setstartCalender(String date, String arr) {
		MyApp.endCalendar = Calendar.getInstance();
		String[] calen = date.split("-");
		MyApp.year = Integer.parseInt(calen[0]);
		MyApp.month = Integer.parseInt(calen[1]);
		MyApp.day = Integer.parseInt(calen[2]);

		MyApp.startCalender = Calendar.getInstance();
		MyApp.startCalender.set(MyApp.year, MyApp.month - 1, MyApp.day, 0, 0);
		long ms = MyApp.endCalendar.getTimeInMillis()
				- MyApp.startCalender.getTimeInMillis();
		int dayCount = (int) (ms / ConstantPool.DAY_MS);
		try {
			MyApp.array = new JSONArray(arr);
			MyApp.startMin = MyApp.array.length();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (MyApp.startMin > 1440) {
			MyApp.startMin = 1440;
		}

		if (dayCount > 3) {// 大于两周
			String arrayStr = setSqlData(MyApp.startMin, 1440, MyApp.array);
			String newDate = Utils.getDate(MyApp.year) + "-"
					+ Utils.getDate(MyApp.month) + "-"
					+ Utils.getDate(MyApp.day);
			helper.insert(newDate, arrayStr, 0, 0, 0, 0, "[]", 0, -1);
			// if (dayCount < 31 * ConstantPool.WEEK_MS) {// 少于一个月
			// arrayStr = setSqlData(0, 1440, new JSONArray());
			for (int i = dayCount; i > 3; i--) {
				MyApp.startCalender.add(Calendar.DAY_OF_MONTH, 1);
				MyApp.year = MyApp.startCalender.get(Calendar.YEAR);
				MyApp.month = MyApp.startCalender.get(Calendar.MONTH) + 1;
				MyApp.day = MyApp.startCalender.get(Calendar.DAY_OF_MONTH);
				newDate = Utils.getDate(MyApp.year) + "-"
						+ Utils.getDate(MyApp.month) + "-"
						+ Utils.getDate(MyApp.day);
				helper.insert(newDate, "[]", 0, 0, 0, 0, "[]", 0, -1);
			}
			MyApp.startCalender.add(Calendar.DAY_OF_MONTH, 1);
			MyApp.year = MyApp.startCalender.get(Calendar.YEAR);
			MyApp.month = MyApp.startCalender.get(Calendar.MONTH) + 1;
			MyApp.day = MyApp.startCalender.get(Calendar.DAY_OF_MONTH);
			MyApp.array = new JSONArray();
			MyApp.startMin = 0;
			// }
			ms = MyApp.endCalendar.getTimeInMillis()
					- MyApp.startCalender.getTimeInMillis();
		}
		int length = (int) (ms / 60000) - MyApp.startMin;
		if (length < 1) {
			if (MyApp.handler != null) {
				MyApp.handler.sendEmptyMessage(PublicConstant.SYNEARLYNEW);
			}
			if (MyApp.mUploadAndSyncHand != null) {
				MyApp.mUploadAndSyncHand
						.sendEmptyMessage(PublicConstant.SYNEARLYNEW);
			}
		} else {
			MyApp.mainSwitch = true;
			MyApp.isSameDay = getisSameDate();
			Utils.log("startMin" + MyApp.startMin);
			// edit by peter
			// tooth.sendAlert(new int[] { 13 });
		}
		return length;
	}

	public static boolean getisSameDate() {
		int mYear = MyApp.endCalendar.get(Calendar.YEAR);
		int mMonth = MyApp.endCalendar.get(Calendar.MONTH) + 1;
		int mDay = MyApp.endCalendar.get(Calendar.DAY_OF_MONTH);
		MyApp.endMin = MyApp.endCalendar.get(Calendar.HOUR_OF_DAY) * 60
				+ MyApp.endCalendar.get(Calendar.MINUTE);
		Utils.log("start date = " + MyApp.year + "-" + MyApp.month + "-"
				+ MyApp.day);
		Utils.log("end date = " + mYear + "-" + mMonth + "-" + mDay);
		Utils.log("endMin" + MyApp.endMin);
		if (mYear == MyApp.year && mMonth == MyApp.month && mDay == MyApp.day) {
			return true;
		}
		return false;
	}

	/**
	 * 线程休眠
	 * 
	 * @param ms
	 *            单位：毫秒
	 */
	public static void ThreadSleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static boolean getBigDate(String date, String date2) {
		String[] calen = date.split("-");
		int year = Integer.parseInt(calen[0]);
		int month = Integer.parseInt(calen[1]);
		int day = Integer.parseInt(calen[2]);

		String[] calendar = date2.split("-");
		int year2 = Integer.parseInt(calendar[0]);
		int month2 = Integer.parseInt(calendar[1]);
		int day2 = Integer.parseInt(calendar[2]);
		return year > year2 && month > month2 && day > day2;
	}

}
