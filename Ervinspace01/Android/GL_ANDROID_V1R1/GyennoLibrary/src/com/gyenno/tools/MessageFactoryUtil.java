package com.gyenno.tools;

import com.gyenno.application.InitializationAppData;
import com.gyenno.contants.PublicConstant;
import com.gyenno.enums.EnumMessageType;

public class MessageFactoryUtil {
	private static CustomSetting customSetting = InitializationAppData
			.getmCustomSetting();

	public static byte[] getGeneralMsg(int messageValue) {
		int[] timeArray = StandardMsgUtil.getTime();
		int timeArraylength = timeArray.length;

		int msgLength = 4 + timeArraylength;

		int[] msgs = new int[msgLength];
		msgs[0] = msgLength + PublicConstant.CRC16_LENGTH;
		msgs[1] = EnumMessageType.MSG_NOTIFY.getType();
		msgs[2] = HandleSequenceId.getSequenceId();
		msgs[3] = messageValue;

		for (int i = 0; i < timeArraylength; i++) {
			msgs[i + 4] = timeArray[i];
		}
		return CRCUtil.getCRCMsg(msgs);
	}

	// 准备同步消息
	public static byte[] getSynBef(int type) {
		int[] msgs = new int[4];
		msgs[0] = 6;
		msgs[1] = 13;
		msgs[2] = HandleSequenceId.getSequenceId();
		msgs[3] = type;
		return CRCUtil.getCRCMsg(msgs);
	}

	// 同步数据消息
	public static byte[] getSynMsg(int year, int month, int day, int minute) {
		/*
		 * 名称 Bit数 备注 包头 8 0x7E，表示为数据包 Total_Length8
		 * 消息总长度，包括自己和CRC16，不包括包头：Total_Length = 9; MessageType 8
		 * 消息类型：MessageType = 2; SequenceId 8 流水号：每次加1； Start_Date 24 开始年月日
		 * Start_Minute16 开始分钟：一天24*60个分钟，按顺序排列 CRC 16 CRC16，不包括自己和包头
		 */

		int[] msgs = new int[8];
		msgs[0] = 10;
		msgs[1] = EnumMessageType.MSG_SYNC.getType();
		msgs[2] = HandleSequenceId.getSequenceId();
		msgs[3] = year - 2000;
		msgs[4] = month;
		msgs[5] = day;
		msgs[6] = (byte) ((minute >> 8) & 0xFF);
		msgs[7] = (byte) (minute & 0xFF);
		return CRCUtil.getCRCMsg(msgs);
	}

	// 用户信息改变
	public static byte[] getMeterMsg(int height) {
		int[] msgs = new int[10];
		msgs[0] = 12;
		msgs[1] = EnumMessageType.MSG_USER.getType();
		msgs[2] = HandleSequenceId.getSequenceId();
		msgs[3] = (int) (height * 0.3);
		msgs[4] = (int) (height * 0.3);
		msgs[5] = (int) (height * 0.4);
		msgs[6] = (int) (height * 0.55);
		msgs[7] = (int) (height * 0.78);
		msgs[8] = (int) (height * 0.78);
		msgs[9] = (int) (height * 0.78);
		return CRCUtil.getCRCMsg(msgs);
	}

	/**
	 * 生成每次修改睡眠目标后的 消息
	 * 
	 * @return
	 */
	public static byte[] getSleepTargetMsg(int hour, int minute) {
		int[] msgLength = new int[5];
		msgLength[0] = 7;
		msgLength[1] = EnumMessageType.MSG_SLEEP.getType();
		msgLength[2] = HandleSequenceId.getSequenceId();
		msgLength[3] = hour;
		msgLength[4] = minute;
		return CRCUtil.getCRCMsg(msgLength);
	}

	/**
	 * 拼接同步时间消息
	 * 
	 * @return
	 */
	public static byte[] getSynchronizeTimeMsg() {
		int[] timeArray = StandardMsgUtil.getTime();
		int timeArraylength = timeArray.length;
		int[] msgLength = new int[9];
		msgLength[0] = 11;
		msgLength[1] = EnumMessageType.MSG_TIME.getType();
		msgLength[2] = HandleSequenceId.getSequenceId();
		for (int i = 0; i < timeArraylength; i++) {
			msgLength[i + 3] = timeArray[i];
		}
		return CRCUtil.getCRCMsg(msgLength);
	}

	public static byte[] getAlarmChangeMsg(int[] data) {
		int[] msgLength = new int[3 + data.length];
		msgLength[0] = msgLength.length + 2;
		msgLength[1] = 4;
		msgLength[2] = HandleSequenceId.getSequenceId();
		msgLength[3] = data.length / 3;
		for (int x = 1; x < data.length; x++) {
			msgLength[x + 3] = data[x];
		}
		return CRCUtil.getCRCMsg(msgLength);
	}

	public static byte[] getStandbyMsg(int type) {

		int[] msgLength = new int[4];
		msgLength[0] = 6;
		msgLength[1] = 254;
		msgLength[2] = HandleSequenceId.getSequenceId();
		msgLength[3] = type;
		return CRCUtil.getCRCMsg(msgLength);
	}

	public static byte[] getUpgradePrepareMsg(int tag, int version) {

		int[] msgLength = new int[8];
		msgLength[0] = 10;
		msgLength[1] = 1;
		msgLength[2] = HandleSequenceId.getSequenceId();
		msgLength[3] = tag;
		msgLength[4] = (byte) ((version >> 24) & 0xFF);
		msgLength[5] = (byte) ((version >> 16) & 0xFF);
		msgLength[6] = (byte) ((version >> 8) & 0xFF);
		msgLength[7] = (byte) (version & 0xFF);
		return CRCUtil.getCRCMsg(msgLength);
	}

	public static byte[] getUpgradeMsg(int total, int position, byte[] data,
			int type) {
		int[] msgLength = new int[6 + data.length];
		msgLength[0] = msgLength.length + 2;
		msgLength[1] = type;
		msgLength[2] = HandleSequenceId.getSequenceId();
		int h = (total << 12) + position;
		msgLength[3] = (byte) ((h >> 16) & 0xFF);
		msgLength[4] = (byte) ((h >> 8) & 0xFF);
		msgLength[5] = (byte) (h & 0xFF);
		for (int i = 0; i < data.length; i++) {
			msgLength[i + 6] = data[i];
		}
		return CRCUtil.getCRCMsg(msgLength);
	}

	public static byte[] getVersion() {
		int[] msgLength = new int[7];
		msgLength[0] = 9;
		msgLength[1] = 20;
		msgLength[2] = HandleSequenceId.getSequenceId();
		msgLength[3] = 0;
		msgLength[4] = 0;
		msgLength[5] = 0;
		msgLength[6] = 0;
		return CRCUtil.getCRCMsg(msgLength);
	}

	public static byte[] getClear(int type) {
		int[] msgLength = new int[4];
		msgLength[0] = 0x06;
		msgLength[1] = 0x17;
		msgLength[2] = HandleSequenceId.getSequenceId();
		msgLength[3] = type & 0xFF;
		return CRCUtil.getCRCMsg(msgLength);
	}

	public static byte[] getCustomSetting() {
		int[] msgLength = new int[13];
		msgLength[0] = 0x0F;
		msgLength[1] = 0x18;
		msgLength[2] = HandleSequenceId.getSequenceId();
		msgLength[3] = customSetting.getVibrationNumber_phone() & 0xFF;

		msgLength[4] = customSetting.getVibrationNumber_EMS() & 0xFF;
		msgLength[5] = customSetting.getVibrationNumber_mail() & 0xFF;
		msgLength[6] = customSetting.getVibrationNumber_schedule() & 0xFF;
		msgLength[7] = customSetting.getVibrationNumber_lowPowe() & 0xFF;
		msgLength[8] = customSetting.getVibrationNumber_breakOff() & 0xFF;
		msgLength[9] = customSetting.getVibrationNumber_weiXin() & 0xFF;
		msgLength[10] = customSetting.getVibrationNumber_qq() & 0xFF;
		msgLength[11] = customSetting.getVibrationNumber_clock() & 0xFF;
		msgLength[12] = customSetting.getVibrationNumber_Normal() & 0xFF;

		return CRCUtil.getCRCMsg(msgLength);
	}

}
