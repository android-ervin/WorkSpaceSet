package com.gyenno.tools;

import java.io.Serializable;

public class DataState  implements Serializable{
	private static final long serialVersionUID = 1L;
	private int status;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
