package com.gyenno.tools;

public class HandleSequenceId {
	private static byte sequenceId = 0;

	public static byte getSequenceId() {
		sequenceId = sequenceId > 126 ? 0 : sequenceId;
		return sequenceId++;
	}
}
