package com.gyenno.tools;

import java.util.Calendar;

/**
 * 得到可以发给蓝牙硬件的消息
 */
public class StandardMsgUtil {

	public static int[] getTime(){
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		int[] currentTime = {year-2000, month, day, hour, minute, second};
		return currentTime;
	}
	
	public static int[] getDate(){
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int[] currentTime = {year-2000, month, day};
		return currentTime;
	}
	
}
