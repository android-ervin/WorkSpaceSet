package com.gyenno.tools;

import java.io.Serializable;

/**
 * 
 * @author xh
 *
 */
public class UsersInfo implements Serializable {
	
	private static final long serialVersionUID = 3091383864411711543L;
	private String accountNumber;
	private String nickname;
	private String height;
	private String bodyWeight;
	private String age;
	private String imageUrl;
	private String macAddress;
	private int sex;
	private int heightType;
	private int status;
	private String productNumber;
	private String isBindUser;
	
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(String bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getHeightType() {
		return heightType;
	}

	public void setHeightType(int heightType) {
		this.heightType = heightType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	public String getIsBindUser() {
		return isBindUser;
	}

	public void setIsBindUser(String isBindUser) {
		this.isBindUser = isBindUser;
	}

	@Override
	public String toString() {
		return "MyInfo [accountNumber=" + accountNumber + ", nickname=" + nickname + ", height=" + height
				+ ", bodyWeight=" + bodyWeight + ", age=" + age + ", imageUrl=" + imageUrl + ", sex=" + sex
				+ ", heightType=" + heightType + ", status=" + status + ", productNumber=" + productNumber
				+ "]";
	}
	
}
