package com.gyenno.tools;

import java.util.Arrays;

import android.text.TextUtils;

/**
 * 根据消息得到crc的
 */
public class CRCUtil {
	private static final int CRCTALBES[] = { 0x0000, 0xCC01, 0xD801, 0x1400,
			0xF001, 0x3C00, 0x2800, 0xE401, 0xA001, 0x6C00, 0x7800, 0xB401,
			0x5000, 0x9C01, 0x8801, 0x4400 };

	/*
	private static final int DATA[] = { 0x0c, 0x00, 0x00, 0x00, 0x14, 0x01,
			0x05, 0x14, 0x34, 0x15 };
	*/		

	// public static void main(String[] args) {
	// getCRCMsg(DATA);
	// }
	public static byte[] getCRCMsg(int[] data) {
		int i = 0;
		int crc = com_crc(data);

		byte[] allData = new byte[data.length + 3];

		allData[0] = 0x7e;

		for (i = 0; i < data.length; i++) {
			allData[i + 1] = (byte) (data[i] & 0xFF);
		}
		allData[i + 1] = (byte) ((crc >> 8) & 0xFF);
		allData[i + 2] = (byte) (crc & 0xFF);
		return allData;
	}

	private static int com_crc(int[] msg) {
		int crc = 0xFFFF;
		int i;
		short chChar;

		for (i = 0; i < msg.length; i++) {
			chChar = (short) msg[i];
			crc = CRCTALBES[(chChar ^ crc) & 15] ^ (crc >> 4);
			crc = CRCTALBES[((chChar >> 4) ^ crc) & 15] ^ (crc >> 4);
		}
		System.out.println("crc:" + crc);
		return crc;
	}

	private static int com_crc(byte[] msg) {
		int crc = 0xFFFF;
		int i;
		short chChar;

		for (i = 0; i < msg.length; i++) {
			chChar = (short) msg[i];
			crc = CRCTALBES[(chChar ^ crc) & 15] ^ (crc >> 4);
			crc = CRCTALBES[((chChar >> 4) ^ crc) & 15] ^ (crc >> 4);
		}
		System.out.println("crc:" + crc);
		return crc;
	}

	public static String CRC_CCITT(int flag, String str) {
		int crc = 0x00;
		int polynomial = 0xA001; // 0x1021;
		byte[] bytes = hexStringToBytes(str);// = str.getBytes();
		switch (flag) {
		case 1:
			crc = 0x00;
			break;
		case 2:
			crc = 0xFFFF;
			break;
		case 3:
			crc = 0x1D0F;
			break;

		}
		for (int index = 0; index < bytes.length; index++) {
			byte b = bytes[index];
			for (int i = 0; i < 8; i++) {
				boolean bit = ((b >> (7 - i) & 1) == 1);
				boolean c15 = ((crc >> 15 & 1) == 1);
				crc <<= 1;
				if (c15 ^ bit)
					crc ^= polynomial;
			}
		}
		crc &= 0xffff;
		str = Integer.toHexString(crc);
		return str;

	}

	public static boolean crc(byte[] msg) {
		byte[] data = Arrays.copyOfRange(msg, 1, msg.length - 2);
		int i = com_crc(data);
		int len = ((msg[(msg.length - 2)] & 0xff) << 8)
				+ (msg[msg.length - 1] & 0xff);
		return len == i;
	}

	/**
	 * 
	 * @param hexString
	 * @return
	 */
	public static byte[] hexStringToBytes(String hexString) {
		if (TextUtils.isEmpty(hexString)) {
			return null;
		}

		hexString = hexString.toUpperCase();
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toCharArray();
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
		}
		return d;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

}
