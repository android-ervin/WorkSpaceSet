package com.gyenno.network;

import java.io.File;
import java.util.Map;

import com.gyenno.tools.FormFile;

/**
 * http请求模块接口定义 
 * @author xh
 *
 */
public interface HttpRequestInterface {
	/**
	 * http post请求
	 * @param params
	 * @param path
	 * @param encoding
	 * @return
	 */
	public String httpClientPostRequest(Map<String, String> params, String path,String encoding);
	/**
	 * http post请求
	 * @param params
	 * @param path
	 * @param encoding
	 * @param files
	 * @return
	 */
	public String httpClientPostRequestImages(Map<String, String> params, String path,String encoding,FormFile[] files);
	/**
	 * http get请求
	 * @param params
	 * @param path
	 * @param encoding
	 * @return
	 */
	public String httpClientGetRequest(Map<String, String> params, String path,String encoding);
	/**
	 * 固件升级下载
	 * @param params
	 * @param path
	 * @param file
	 * @return
	 */
	public String httpClientDownRequest(Map<String, String> params, String path, File file);
}
