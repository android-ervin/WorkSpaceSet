package com.gyenno.network;

import java.io.File;
import java.util.Map;

import com.gyenno.tools.FormFile;



/**
 * @titleHttp 模块工厂
 * @description 获取http模块 外部无需关心接口中的具体实现
 * @author xh
 */
public class HttpFactory{
	public static HttpRequestInterface method = HttpRequestInterfaceImpl
			.getSinglehttprequest(); 
	// http调用方式requestMode post方式 get方式
	public static String sendHttpClientRequest(String requestMode,
			Map<String, String> params, String path, String encoding) {
		String res = "";
		if (null != requestMode && !"".equals(requestMode)
				&& "post".trim().equals(requestMode)) {
			res = method.httpClientPostRequest(params, path, encoding);
		}else if (null != requestMode && !"".equals(requestMode)
				&& "get".trim().equals(requestMode)) {
			res = method.httpClientGetRequest(params, path, encoding);
		}
		return res;
	}
	// http调用方式requestMode post方式 get方式 有图片的请求
	public static String sendHttpClientRequestImages(String requestMode,
			Map<String, String> params, String path, String encoding,FormFile[] files) {
		String res = "";
		if (null != requestMode && !"".equals(requestMode)
				&& "post".trim().equals(requestMode)) {
			res = method.httpClientPostRequestImages(params, path, encoding,files);
		}
		return res;
	}
	//http调用方式 下载升级固件
	public static String sendHttpClientRequestDown(Map<String, String> params, String path, File file){
		String res = "";
		if (null != path && !"".equals(path)){
			res = method.httpClientDownRequest(params, path, file);
		}
		return res;
	}
}
