package com.gyenno.network;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;

import com.gyenno.tools.ConstantFinal;
import com.gyenno.tools.FormFile;
import com.gyenno.tools.Tools;
import com.gyenno.tools.Utils;

/**
 * http请求模块接口实现类
 * @author xh
 * @description http请求为饿汉式单例模式
 * 1、单例类只能有一个实例。
 * 2、单例类必须自己自己创建自己的唯一实例。
 * 3、单例类必须给所有其他对象提供这一实例。
 * 
 */
public class HttpRequestInterfaceImpl implements HttpRequestInterface{
	
	//私有的默认构造子  
	private HttpRequestInterfaceImpl() {}  
	//已经自行实例化   
	private static final HttpRequestInterfaceImpl singleHttpRequest = new HttpRequestInterfaceImpl();  
	//静态工厂方法   
	public static HttpRequestInterfaceImpl getSinglehttprequest() {
		return singleHttpRequest;
	}

	@Override
	public String httpClientPostRequest(Map<String, String> params,
			String path, String encoding) {
		// TODO Auto-generated method stub
		String res="";
		Utils.log("path = " + path);
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();// 请求参数
		if (params != null && !params.isEmpty()) {
			for (Map.Entry<String, String> entry : params.entrySet()) {
				pairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
		}
		UrlEncodedFormEntity entity;
		try {
			entity = new UrlEncodedFormEntity(pairs, encoding);
			HttpPost httpPost = new HttpPost(ConstantFinal.PATH + path);
			httpPost.setEntity(entity);
			DefaultHttpClient client = new DefaultHttpClient();
			//请求超时 
			client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
			//读取超时 
			client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);
			HttpResponse response = client.execute(httpPost);
			 // 获取状态码
            int resCode = response.getStatusLine().getStatusCode();
			if (resCode == 200) {
				Utils.log("200");
				HttpEntity responseHttpEntity = response.getEntity();
				InputStream in = responseHttpEntity.getContent();
				res = Tools.read(in);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public String httpClientGetRequest(Map<String, String> params, String path,
			String encoding) {
		// TODO Auto-generated method stub
		String res = "";
		try {
			StringBuilder urlBuilder = new StringBuilder();
			urlBuilder.append(ConstantFinal.PATH + path);
			if (null != params) {
				urlBuilder.append("?");
				Iterator<Entry<String, String>> iterator = params.entrySet()
						.iterator();
				while (iterator.hasNext()) {
					Entry<String, String> param = iterator.next();
					urlBuilder
							.append(URLEncoder.encode(param.getKey(), "UTF-8"))
							.append('=')
							.append(URLEncoder.encode(param.getValue(), "UTF-8"));
					if (iterator.hasNext()) {
						urlBuilder.append('&');
					}
				}
			}
			// 创建HttpClient对象
			DefaultHttpClient client = new DefaultHttpClient();
			// 发送get请求创建HttpGet对象
			HttpGet getMethod = new HttpGet(urlBuilder.toString());
			HttpResponse response = client.execute(getMethod);
			// 获取状态码
			int resCode = response.getStatusLine().getStatusCode();
			if (resCode == 200) {
				//StringBuilder builder = new StringBuilder();
				// 获取响应内容
				HttpEntity responseHttpEntity = response.getEntity();
				InputStream in = responseHttpEntity.getContent();
				res = Tools.read(in);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public String httpClientPostRequestImages(Map<String, String> params,
			String path, String encoding, FormFile[] files) {
		// TODO Auto-generated method stub
		final String BOUNDARY = "---------------------------7da2137580612"; //
		final String endline = "--" + BOUNDARY + "--\r\n";//
		String res = "";
		String result = "";
		int fileDataLength = 0;
		for (FormFile uploadFile : files) {
			StringBuilder fileExplain = new StringBuilder();
			fileExplain.append("--");
			fileExplain.append(BOUNDARY);
			fileExplain.append("\r\n");
			fileExplain.append("Content-Disposition: form-data;name=\""
					+ uploadFile.getParameterName() + "\";filename=\""
					+ uploadFile.getFilname() + "\"\r\n");
			fileExplain.append("Content-Type: " + uploadFile.getContentType()
					+ "\r\n\r\n");
			fileDataLength += fileExplain.length();
			if (uploadFile.getInStream() != null) {
				fileDataLength += uploadFile.getFile().length();
			} else {
				fileDataLength += uploadFile.getData().length;
			}
			fileDataLength += "\r\n".length();
		}

		StringBuilder textEntity = new StringBuilder();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			textEntity.append("--");
			textEntity.append(BOUNDARY);
			textEntity.append("\r\n");
			textEntity.append("Content-Disposition: form-data; name=\""
					+ entry.getKey() + "\"\r\n\r\n");
			textEntity.append(entry.getValue());
			textEntity.append("\r\n");
		}
		int dataLength = textEntity.toString().getBytes().length
				+ fileDataLength + endline.getBytes().length;
		URL url;
		try {
			url = new URL(ConstantFinal.PATH + path);
			int port = url.getPort() == -1 ? 80 : url.getPort();
			Socket socket = new Socket(InetAddress.getByName(url.getHost()),
					port);
			OutputStream outStream = socket.getOutputStream();
			String requestmethod = "POST " + url.getPath() + " HTTP/1.1\r\n";
			outStream.write(requestmethod.getBytes());
			String accept = "Accept: image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*\r\n";
			outStream.write(accept.getBytes());
			String language = "Accept-Language: zh-CN\r\n";
			outStream.write(language.getBytes());
			String contenttype = "Content-Type: multipart/form-data; boundary="
					+ BOUNDARY + "\r\n";
			outStream.write(contenttype.getBytes());
			String contentlength = "Content-Length: " + dataLength + "\r\n";
			outStream.write(contentlength.getBytes());
			String alive = "Connection: Keep-Alive\r\n";
			outStream.write(alive.getBytes());
			String host = "Host: " + url.getHost() + ":" + port + "\r\n";
			outStream.write(host.getBytes());
			outStream.write("\r\n".getBytes());
			outStream.write(textEntity.toString().getBytes());
			for (FormFile uploadFile : files) {
				StringBuilder fileEntity = new StringBuilder();
				fileEntity.append("--");
				fileEntity.append(BOUNDARY);
				fileEntity.append("\r\n");
				fileEntity.append("Content-Disposition: form-data;name=\""
						+ uploadFile.getParameterName() + "\";filename=\""
						+ uploadFile.getFilname() + "\"\r\n");
				fileEntity.append("Content-Type: "
						+ uploadFile.getContentType() + "\r\n\r\n");
				outStream.write(fileEntity.toString().getBytes());
				if (uploadFile.getInStream() != null) {
					byte[] buffer = new byte[1024];
					int len = 0;
					while ((len = uploadFile.getInStream()
							.read(buffer, 0, 1024)) != -1) {
						outStream.write(buffer, 0, len);
					}
					uploadFile.getInStream().close();
				} else {
					outStream.write(uploadFile.getData(), 0,
							uploadFile.getData().length);
				}
				outStream.write("\r\n".getBytes());
			}
			outStream.write(endline.getBytes());
			outStream.flush();
			InputStream in = socket.getInputStream();
			res = Tools.read(in);
			result = res.toString();
			result = result.substring(result.indexOf("{"),
					result.indexOf("}") + 1);
			outStream.close();
			socket.close();
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String httpClientDownRequest(Map<String, String> params,
			String path, File file) {
		// TODO Auto-generated method stub
		if (file.exists()) {
			file.delete();
			Utils.log("localFile.exists()");
		}else {
			Utils.log("localFile = null");
		}
		
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		if (params != null && !params.isEmpty()) {
			for (Map.Entry<String, String> entry : params.entrySet()) {
				pairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
		}
		UrlEncodedFormEntity entity;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
			HttpPost httpPost = new HttpPost(ConstantFinal.PATH + path);
			httpPost.setEntity(entity);
			DefaultHttpClient client = new DefaultHttpClient();
			client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
			client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 6000);
			HttpResponse response = client.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == 200) {
				FileOutputStream outStream = new FileOutputStream(file);
				HttpEntity responseHttpEntity = response.getEntity();
				InputStream inputStream = responseHttpEntity.getContent();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, len);
				}
				inputStream.close();
				outStream.close();
				return "OK";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
