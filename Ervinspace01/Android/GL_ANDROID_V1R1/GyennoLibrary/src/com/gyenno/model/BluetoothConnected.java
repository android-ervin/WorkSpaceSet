package com.gyenno.model;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;

public class BluetoothConnected {

	private BluetoothDevice device;
	private BluetoothGatt mBluetoothGatt;
	
	public BluetoothConnected(BluetoothDevice device,
			BluetoothGatt mBluetoothGatt) {
		super();
		this.device = device;
		this.mBluetoothGatt = mBluetoothGatt;
	}
	public BluetoothDevice getDevice() {
		return device;
	}
	public void setDevice(BluetoothDevice device) {
		this.device = device;
	}
	public BluetoothGatt getmBluetoothGatt() {
		return mBluetoothGatt;
	}
	public void setmBluetoothGatt(BluetoothGatt mBluetoothGatt) {
		this.mBluetoothGatt = mBluetoothGatt;
	}
}
