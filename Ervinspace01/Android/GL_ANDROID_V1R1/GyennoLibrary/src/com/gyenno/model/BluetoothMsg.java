package com.gyenno.model;

import android.bluetooth.BluetoothGatt;

public class BluetoothMsg {

	byte[] msg;
	BluetoothGatt BluetoothGatt;
	
	public byte[] getMsg() {
		return msg;
	}
	public void setMsg(byte[] msg) {
		this.msg = msg;
	}
	public BluetoothGatt getBluetoothGatt() {
		return BluetoothGatt;
	}
	public void setBluetoothGatt(BluetoothGatt bluetoothGatt) {
		BluetoothGatt = bluetoothGatt;
	}
	public BluetoothMsg(byte[] msg,
			android.bluetooth.BluetoothGatt bluetoothGatt) {
		super();
		this.msg = msg;
		BluetoothGatt = bluetoothGatt;
	}
	
}
