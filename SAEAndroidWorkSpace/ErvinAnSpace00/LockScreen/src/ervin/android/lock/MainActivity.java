package ervin.android.lock;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends ActionBarActivity {

	private ComponentName mComponentName;
	private DevicePolicyManager mDevicePolicyManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Translucent_NoTitleBar);
		super.onCreate(savedInstanceState);
		
		this.mDevicePolicyManager = ((DevicePolicyManager)getSystemService("device_policy"));
		this.mComponentName = new ComponentName(this, AdminRecevier.class);

		if (mDevicePolicyManager.isAdminActive(mComponentName)) {
			finish();
			mDevicePolicyManager.lockNow();
		} else {
			
			author();
			finish();
		}

	}	
	private void author() {
		Intent localIntent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
		localIntent.putExtra("android.app.extra.DEVICE_ADMIN",
				this.mComponentName);
		localIntent.putExtra("android.app.extra.ADD_EXPLANATION",
				getString(R.string.auther_explain));
		//startActivityForResult(localIntent, 1);
		startActivity(localIntent);
//		finish();
	}

//	protected void onDestroy() {
//		super.onDestroy();
//		System.exit(0);
//	}
}
