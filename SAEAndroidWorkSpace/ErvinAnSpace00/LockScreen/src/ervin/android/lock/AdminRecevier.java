package ervin.android.lock;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

public class AdminRecevier extends DeviceAdminReceiver
{
  static SharedPreferences getSamplePreferences(Context paramContext)
  {
    return paramContext.getSharedPreferences(DeviceAdminReceiver.class.getName(), 0);
  }
  public CharSequence onDisableRequested(Context paramContext, Intent paramIntent)
  {
    return "取消激活，一键锁屏功能失败";
  }

  public void onEnabled(Context paramContext, Intent paramIntent)
  {
    Toast.makeText(paramContext, "授权成功，您可以使用一键锁屏了", 0).show();
  }
}