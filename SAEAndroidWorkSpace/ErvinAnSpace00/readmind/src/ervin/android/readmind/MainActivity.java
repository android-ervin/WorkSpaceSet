package ervin.android.readmind;
import java.util.Random;

import ervin.android.scalepic.scalePicture;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.text.AlteredCharSequence;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {	
	private static final Random rgenerator = new Random();

	private static final Integer[] mImageBefor = 
	    { R.drawable.b1, R.drawable.b2, R.drawable.b3, R.drawable.b4, R.drawable.b5, 
		R.drawable.b6,R.drawable.b7 ,R.drawable.b8, R.drawable.b9,R.drawable.b10};
	private static final Integer[] mImageAfter =
		{R.drawable.a1,R.drawable.a2,R.drawable.a3,
		R.drawable.a4,R.drawable.a5,R.drawable.a6,R.drawable.a7,R.drawable.a8,R.drawable.a9,
		R.drawable.a10};
	
	private ImageView iv;
	private Button btn_read;
	
	private static int i = 0;
	private int index;
	private static boolean flag = false;
	private static boolean isend = false;
	
	private MediaPlayer mPlayer;
	private Animation iv_mation;
	private AlertDialog.Builder first_builder ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);

		btn_read = (Button) findViewById(R.id.btn_read);
		iv = (ImageView) findViewById(R.id.iv);
		index = rgenerator.nextInt(mImageBefor.length);
		iv.setImageBitmap(scalePicture.getScalePic(this, mImageBefor[index],1.0f,0,0));
		
		iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(flag && isend)
				{
					index = rgenerator.nextInt(mImageBefor.length);
					iv.setImageBitmap(scalePicture.getScalePic(MainActivity.this, mImageBefor[index], 1.0f, 0, 0));
					btn_read.setVisibility(View.VISIBLE);
					flag = false;	
					isend = false;
				}
			}
		});
//		//Toast.makeText(this, R.string.first, Toast.LENGTH_LONG).show();
//		AlertDialog.Builder builder =  new Builder(this);
//		builder.setTitle(R.string.tips).setIcon(android.R.drawable.ic_dialog_info)
//		.setMessage(R.string.first).setPositiveButton(android.R.string.ok, null).show();
		showFirstDialog();
		
	}

	public void read(View view) {
		i++;
		flag = true;
		// 显示after的图片
		PlayMp3();
		iv_mation = setAnimation();
		iv_mation.setAnimationListener(new myAnimationListener());
		iv.setAnimation(iv_mation);
		iv.setImageBitmap(scalePicture.getScalePic(this, mImageAfter[index],
				1.0f, 0, 0));
		btn_read.setVisibility(View.GONE);

	}
	
	private void PlayMp3() {
		mPlayer =  MediaPlayer.create(this, R.raw.duxinshu);
		mPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				mPlayer.release();			
			}
		});
		try {	
			mPlayer.stop();  //不加会报illegalStateException
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.prepare();
			mPlayer.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			LayoutInflater li = LayoutInflater.from(this);
			View view = li.inflate(R.layout.custom_dialog, null);
			ImageView iv_dialog = (ImageView) view.findViewById(R.id.iv_dialog);
			TranslateAnimation animation = new TranslateAnimation(300f, 300f, 10f, 300f);
			//animation.setRepeatCount(5);
			iv_dialog.setAnimation(animation);
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(R.string.action_settings);
			builder.setView(view);
			builder.setPositiveButton(android.R.string.ok, null);
			builder.show();
		}
		if(id == R.id.action_help)
		{
			helpInfo(this);
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	private Animation setAnimation()
	{
		// Scaling
		Animation scale = new ScaleAnimation(0.1f, 1.0f, 0.1f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		scale.setDuration(3000);
		return scale;
	}
	private class myAnimationListener implements AnimationListener
	{

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			Toast.makeText(MainActivity.this, R.string.toast, Toast.LENGTH_SHORT).show();
			isend = true;
		}
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}		
	}	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == event.KEYCODE_BACK)
		{
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(R.string.tips);
			builder.setIcon(android.R.drawable.ic_dialog_info);
			builder.setMessage(R.string.tips_info);
			builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
					int mypid = android.os.Process.myPid();
					android.os.Process.killProcess(mypid);
				}
			});
			builder.setNegativeButton(android.R.string.cancel, null);
			builder.show();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void showFirstDialog()
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_dialog, null);		
		first_builder = new Builder(this);
		first_builder.setTitle(R.string.tips);
		first_builder.setView(view);
		first_builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				helpInfo(MainActivity.this);				
				}			
		});
		first_builder.show();	
	}

	private void helpInfo(Context context) {
		LayoutInflater li = LayoutInflater.from(context);
		View view = li.inflate(R.layout.custom_firstdialog, null);
		AlertDialog.Builder builder = new Builder(context);
		builder.setTitle(R.string.rule);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setView(view);
		builder.setPositiveButton(android.R.string.ok, null).show();
	}
}


