package com.ervin.ervinandroiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DemoActivity extends Activity
{

	private Button btn2;
	private TextView tv1;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.demoactivity);
		
		btn2=(Button)findViewById(R.id.button2);
		btn2.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(DemoActivity.this, MainActivity.class);
				
				DemoActivity.this.finish();
				String str="";
				str=intent.getStringExtra("Username");
				str+=intent.getStringExtra("Password");
				
			    tv1=(TextView)findViewById(R.id.textView4);
			    tv1.setText(str);
			    startActivity(intent);
			}
			
		});
	}
	
}
