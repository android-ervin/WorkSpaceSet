package ervin.java.suanfatest;

public class ArrayTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Game game = new Game();
		for(int i=0;i<game.position.length;i++)
			System.out.println(game.position[i]);
	}

	
}
class Game {

	private final String initstr ="360000000004230800000004200"
			+"070460003820000014500013020"
			+"001900000007048300000000045";
	public int[] position;
	public  int[] shudu = new int[9*9];
	
	//三维数组，最后一维用来存放每一个单元格已经不可用的数字的数组
	private int used[][][]=new int[9][9][];
	
	public Game() {
		shudu = fromString(initstr);
		markPosition();
	}
	public int[] fromString(String str)
	{
		int[] init = new int[str.length()];
		for(int i=0;i<str.length();i++)
		{
			init[i]=str.charAt(i)-'0';
		}
		return init;
	}
	private int getTile(int x,int y)
	{
		return shudu[9*y + x]; //字符串横向排列
	}
	//标记初始数字的位置
	public void markPosition()
	{
		int count=0;
		for(int i:shudu)
		{
			if(i!=0)
				count++;
		}
		position = new int[count];
		int index=0;
		for(int i=0;i<shudu.length;i++)
		{
			if(shudu[i]!=0)
				position[index++]=i;
		}
	}
	//根据九宫格坐标判断是否是初始的数字位置
	public boolean isThePosition(int x,int y)
	{
		int temp = 9*y + x;
		for(int i=0;i<position.length;i++)
		{
			if(temp!=position[i])
				return true;
			else
				return false;
		}
		return true;
	}
}