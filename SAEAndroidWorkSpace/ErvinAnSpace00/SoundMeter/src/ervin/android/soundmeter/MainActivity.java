package ervin.android.soundmeter;


import ervin.adnroid.soundmeter.R;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class MainActivity extends ActionBarActivity {

	 private ProgressDialog mDlg;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			
	}
	  @Override
	    protected void onStart() {
	        super.onStart();
	        mDlg = ProgressDialog.show(MainActivity.this, "", getString(R.string.loading), true, false);
	        startActivity(new Intent(MainActivity.this, SoundMeter.class));
	        finish();
	    }

	    @Override
	    protected void onDestroy() {
	        super.onDestroy();
	    }

}
