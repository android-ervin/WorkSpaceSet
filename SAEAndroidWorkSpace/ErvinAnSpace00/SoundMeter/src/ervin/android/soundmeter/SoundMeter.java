package ervin.android.soundmeter;


import ervin.adnroid.soundmeter.R;
import ervin.android.scalepic.scalePicture;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class SoundMeter extends Activity{

	 final private int RATE_COUNT = 6;

	    final private int MENU_CALIBRATE = 1;
	    final private int MENU_MANUAL = 2;
	    final private int MENU_SETTINGS = 3;
	    final private int MENU_ABOUT = 4;

	    static int a = 0;
	    static int b = 0;
	    static float c = 0.0F;
	    static float d = 0.0F;
	    static boolean e = false;
	    static boolean f = false;
	    static boolean g = false;
	    static boolean h = false;
	    private static final int k = 200;
//	    private com.google.ads.AdView i = null;
//	    private net.daum.adam.publisher.AdView j = null;
	    private Handler mHandler = new Handler();               //m
	    private SoundPlay mSoundPlay;         //aj n
	    private Recorder mRecorder;         //ah o
	    private SharedPreferences mPref;    //p
	    private Runnable mUpdateTimer = new Runnable() {        //q
	        @Override
	        public void run() {

	            mRecorder.SoundDB();
	            //递归调用，不停的调用run方法
	            mHandler.postDelayed(mUpdateTimer, 200L);
	        }
	    };

	    static TextView mSoundDB;
	    static ImageView mImageMeter;
	    static ImageView mImageNeedle;
	    static ImageView mImageWheel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sound);
		
		mRecorder = new Recorder(getApplicationContext());
		mSoundPlay = new SoundPlay(getApplicationContext());
		mSoundDB = (TextView) findViewById(R.id.text_sounddb);
		mImageMeter = (ImageView)findViewById(R.id.iv_meter);
		mImageNeedle=(ImageView)findViewById(R.id.iv_needle);
		//mImageWheel =(ImageView)findViewById(R.id.iv_wheel);
		//Bitmap metershow = Bitmap.createScaledBitmap(meter, (int)(meter.getWidth()*0.8), (int)(meter.getHeight()*0.8), false);
		mImageMeter.setImageBitmap(scalePicture.getScalePic(this, R.drawable.sound_meter,1.0f,0,0));
		mImageNeedle.setImageBitmap(scalePicture.getScalePic(this, R.drawable.sound_needle,0.8f,60f,3f));
		//mImageWheel.setImageBitmap(scalePicture.getScalePic(this, R.drawable.sound_wheel, 0.2f,30f,30f));
		
		mPref = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor localEditor = mPref.edit();
		int RateCnt = mPref.getInt("smartcount", 0);
		boolean bool = mPref.getBoolean("smartcomment", true);
		RateCnt = RateCnt + 1;
		localEditor.putInt("smartcount", RateCnt);
		localEditor.commit();
		//当用户打开应用次数大于6次之后，每再打开3次就弹框提示打分
		if ((bool) && (RateCnt >= RATE_COUNT)
				&& ((RateCnt - RATE_COUNT) % 3 == 0))
			new ShowDialog().RateDialog(this).show();
		setVolumeControlStream(3);
	}
	
    protected void onDestroy()
    {
        super.onDestroy();

        if (Build.VERSION.SDK_INT < 14)
            System.exit(0);
    }

    protected void onResume()
    {
        super.onResume();
    }

    protected void onStart()
    {
        super.onStart();

        //mRecorder.setSoundView(mSoundView);
        mRecorder.RecorderInit();
        //开启线程收集声音分贝数
        mHandler.postDelayed(mUpdateTimer, 200L);

        mSoundPlay.a();
    }

    protected void onStop()
    {
        super.onStop();
        mRecorder.RecorderRel();

        mHandler.removeCallbacks(mUpdateTimer);
    }

    public boolean onOptionsItemSelected(MenuItem menuitem) {

        ShowDialog dlg = new ShowDialog();

        switch (menuitem.getItemId()) {
            case MENU_CALIBRATE:

                break;

            case MENU_MANUAL:

                break;

            case MENU_SETTINGS:

                break;

            case MENU_ABOUT:
                dlg.AboutDialog(this).show();
                break;

            default:
                break;
        }

        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        int id = 0;
        menu.add(Menu.NONE, MENU_CALIBRATE, id, R.string.menu_calibrate);
        menu.add(Menu.NONE, MENU_MANUAL, id, R.string.menu_manual);
        menu.add(Menu.NONE, MENU_SETTINGS, id, R.string.menu_settings);
        menu.add(Menu.NONE, MENU_ABOUT, id, R.string.menu_about);

        return super.onCreateOptionsMenu(menu);
    }

    class ShowDialog {

        private String getVersionName() throws Exception {
            PackageManager packageManager = getPackageManager();
            PackageInfo packInfo = null;
            if (packageManager != null) {
                packInfo = packageManager.getPackageInfo(getPackageName(),0);
            }
            String version = null;
            if (packInfo != null) {
                version = packInfo.versionName;
            }
            return version;
        }

        public Dialog AboutDialog(Context paramContext)
        {
            AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);

            try {
                localBuilder.setTitle(paramContext.getString(R.string.app_name) + ' ' + getVersionName());
            } catch (Exception e) {
                e.printStackTrace();
            }
            localBuilder.setIcon(R.drawable.ic_launcher);
            localBuilder.setMessage(
                    paramContext.getString(R.string.about_developed_by) + ' ' + paramContext.getString(R.string.about_developer_name) + '\n' +
                            paramContext.getString(R.string.about_msg) + '\n' +
                            paramContext.getString(R.string.about_developer_email)
            );
            localBuilder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            localBuilder.setNeutralButton(R.string.about_more_apps, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            localBuilder.setNegativeButton(R.string.about_send_email, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            return localBuilder.create();
        }

        public Dialog RateDialog(Context paramContext) {

            final SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
            final AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
            localBuilder.setTitle(R.string.rate_title);
            localBuilder.setIcon(R.drawable.ic_launcher);
            localBuilder.setMessage(paramContext.getString(R.string.rate_msg));
            localBuilder.setCancelable(false);

            localBuilder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + getPackageName()));
                    //startActivity(localIntent);
                    localEditor.putBoolean("smartcomment", false);
                    localEditor.commit();
                }
            });

            localBuilder.setNeutralButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            localBuilder.setNegativeButton(R.string.rate_never, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    localEditor.putBoolean("smartcomment", false);
                    localEditor.commit();
                }
            });

            return localBuilder.create();
        }
    }

}
