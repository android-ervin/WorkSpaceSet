package ervin.android.mediaplayer;

import java.io.IOException;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends Activity implements OnClickListener {

	private EditText et;
	private MediaPlayer mPlayer;
	private Button btn1 ;
	private SurfaceView sv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);

		et = (EditText) findViewById(R.id.et);
		sv = (SurfaceView)findViewById(R.id.sv);
		btn1 = (Button)findViewById(R.id.btn1);
		btn1.setOnClickListener(this);
	}
	

	public void play() {
		// TODO Auto-generated method stub
		String path = et.getText().toString().trim();
		try {
			mPlayer = new MediaPlayer();
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setDisplay(sv.getHolder());
			mPlayer.setDataSource(path);
			mPlayer.prepareAsync();
			mPlayer.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn1:
			play();
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mPlayer.release();
		mPlayer = null;
	}

}
