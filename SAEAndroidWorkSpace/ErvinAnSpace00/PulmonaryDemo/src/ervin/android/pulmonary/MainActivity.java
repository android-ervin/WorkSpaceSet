package ervin.android.pulmonary;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.MediaRecorder.AudioSource;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.IOException;

public class MainActivity extends Activity {

private final static int SAMPLERATEINHZ = 8000;
	
	private AudioRecord mAudio;
	private int bufferSizeInBytes;
	private SoundMeter sm;
	private Handler handler;
	private double volume;
	private TextView tv ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);		
		
		tv = (TextView)findViewById(R.id.tv);
		sm = new SoundMeter();
		handler = new MyHandler();		
		
		}

	public  class MyHandler extends Handler
	{
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if(msg!=null)
			{
				volume = (Double) msg.obj;
				tv.setText("�ֱ���"+volume);
			}
			System.gc();
			stop();
		}
	}

	public void iniRecoder() {
		if (mAudio == null) {
			try {
				bufferSizeInBytes = AudioRecord.getMinBufferSize(
						SAMPLERATEINHZ, AudioFormat.CHANNEL_IN_MONO,
						AudioFormat.ENCODING_PCM_16BIT);
				System.out.println("minBufferSize" + bufferSizeInBytes);
				mAudio = new AudioRecord(AudioSource.MIC, SAMPLERATEINHZ,
						AudioFormat.CHANNEL_IN_MONO,
						AudioFormat.ENCODING_PCM_16BIT, bufferSizeInBytes);
				mAudio.startRecording();
				System.out.println("start recoder");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else
		{
			System.out.println("mAudio is not null!");
		}
	}
	
	public void stop() {
        if (mAudio != null) {
        	mAudio.stop();
        	mAudio.release();
        	mAudio = null ;
        }
    }
	public double getAmplitude() {
		//iniRecoder();
        short[] buffer = new short[bufferSizeInBytes];
        mAudio.read(buffer, 0, bufferSizeInBytes);
        int max = 0;
        for (short s : buffer)
        {
            if (Math.abs(s) > max)
            {
                max = Math.abs(s);
            }
        }
        buffer = null;
        return max;
    }
	
	
	public void click(View view)
	{
		//sm.stop();
		stop();
		System.gc();
		//System.out.println("�ֱ���"+volume);
	}
	public void start(View view)
	{
		iniRecoder();
//		try {
//			sm.start();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		new Thread(new TestThread()).start();
		
	}
	public class TestThread implements Runnable
	{
		public void run() {
			//volume = sm.getAmplitude();
			double level =getAmplitude();
			Message msg = handler.obtainMessage();
			msg.obj = level;
			handler.sendMessage(msg);	
		}
	}
}

