package ervin.android.cc;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ChooseChanceActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn1=null;
	private TextView tv1=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        btn1=(Button)findViewById(R.id.choose);
        tv1=(TextView)findViewById(R.id.show);
        
        btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Random r=new Random();
				int result=r.nextInt();
				if((result%2)==0)
				{
					tv1.setText("go ahead with brave!");
				}
				else
				{
					tv1.setText("you should be think more about it!");
				}
			}
		});
    }
}