package ervin.android.prj;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DemoActivity extends Activity {

	TextView tvshow;
	TextView tvinput;
	Button btnadd;
	Button btnsub;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		tvshow = (TextView) findViewById(R.id.textView2);
		tvinput = (TextView) findViewById(R.id.textView4);

		btnadd = (Button) findViewById(R.id.button1);
		btnadd.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				String str = (String) tvinput.getText();
				float value = Float.parseFloat(str) * 1000;
				value = (value + 1) / 1000;
				String text = Float.toString(value);
				tvinput.setText(text);
			}
		});

		btnsub = (Button) findViewById(R.id.button2);
		btnsub.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				String str = (String) tvinput.getText();
				float value = Float.parseFloat(str) * 1000;
				if (value != 1) {
					value = (value - 1) / 1000;
					String text = Float.toString(value);
					tvinput.setText(text);
				} else
					Toast.makeText(DemoActivity.this, "out of limit",
							Toast.LENGTH_SHORT).show();
			}
		});

		Button btn1 = (Button) findViewById(R.id.button3);
		btn1.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				btnsub.setEnabled(true);
				btnadd.setEnabled(true);
			}
		});

		Button btn2 = (Button) findViewById(R.id.button4);
		btn2.setOnClickListener(new myListener());
		
		Button btn5 = (Button) findViewById(R.id.button5);
		btn5.setOnClickListener(new myListener());
	}

	class myListener implements OnClickListener {
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.button4:
				btnsub.setEnabled(false);
				btnadd.setEnabled(false);
				break;
			case R.id.button5:
				Intent intent=new Intent(DemoActivity.this,IOControl.class);
				startActivity(intent);
			default:
				break;
			}
			
			
			
		}
	}
}