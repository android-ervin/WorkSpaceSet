package ervin.android.uri;

import java.util.ArrayList;
import java.util.List;

import ervin.android.smsmodel.SmsModel;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.format.Time;

public class SmsProvider {
	public final static Uri CONVERSATION_URI_ALL = Uri.parse("content://sms");
	public final static Uri CONVERSATION_URI_INBOX = Uri.parse("content://sms/inbox");
	public final static Uri CONVERSATION_URI_OUTBOX = Uri.parse("content://sms/outbox");
	public final static Uri CONVERSATION_URI_SENT = Uri.parse("content://sms/sent");
	public final static Uri CONVERSATION_URI_DRAFT = Uri.parse("content://sms/draft");
	public final static Uri CONVERSATION_URI = Uri.parse("content://sms/conversations");
	
	//查询sms一张表中的数据
	public static List<SmsModel>  getSMS(Context context)
	{
		List<SmsModel> smsModels = new ArrayList<SmsModel>();
		ContentResolver cr = context.getContentResolver();
		Cursor cursor = cr.query(CONVERSATION_URI_ALL, new String[]{"address","date","body"}, null, null, null);
		while(cursor.moveToNext())
		{
			String address = cursor.getString(cursor.getColumnIndex("address"));
			String date = cursor.getString(cursor.getColumnIndex("date"));
			String body = cursor.getString(cursor.getColumnIndex("body"));

			SmsModel sms = new SmsModel(address,date,body);
			System.out.println(sms.toString());
			smsModels.add(sms);
		}
		return smsModels;
	}
	
	public void getSMSByMultiTable(Context context)
	{
		/**
		 * 其中前三个是该Uri下默认的内部返回字段，也就是当projectin为null时自动返回的字段
		 * 后面两个是拼接字段，根据源代码（telephonyProvider）,将sms表和（sms表和thead表联立查询后）的表做联合查询
		 * 		拼接字段中必须有‘AS’或者‘as’
		 */
		String[] projectin = {"thread_id","snippet","msg_count",
				"sms.address as address",
				"sms.date as date"};
		
		QueryHandler mQueryHandler = new QueryHandler(context.getContentResolver());
		mQueryHandler.startQuery(0, null, CONVERSATION_URI, projectin, null, null, null);//调用完此方法后，将会执行回调函数onQueryComplete.该方法在子线程中完成。
	}
	/*异步执行查询短信数据（不在主线程中执行）*/
	public class QueryHandler extends AsyncQueryHandler 
	{

		public QueryHandler(ContentResolver cr) {
			super(cr);
		}
		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) { //此cursor就是查询后的结果
			super.onQueryComplete(token, cookie, cursor);
			String[] names =  cursor.getColumnNames();
			while(cursor.moveToNext())
			{
				for (String name : names) {
					System.out.println(name +"="+cursor.getString(cursor.getColumnIndex(name)));
				}
			}
			
		}	
	}
	
	public String getContactNamebyAddress(Context context, String address) {
		//根据电话号码找联系人
		//Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_URI, Uri.encode(phoneNumber));
		ContentResolver cr_contact = context.getContentResolver();
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,Uri.encode(address));
		String name = "";
		Cursor contactcursor = cr_contact.query(uri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
		if(contactcursor.moveToFirst())
			name = contactcursor.getString(0);
		contactcursor.close();
		return name;
	}
	
	public String formatDate(Context context, long msg_date) {
		
		Time time = new Time();
		time.setToNow();//变为当前时间
		time.hour = 0;
		time.minute = 0;
		time.second = 0 ;
		long starttime = time.toMillis(false); //将今天的起始时间YY--MM--DD 00:00:00赋给起始时间变量
		String datestr="";
		if((msg_date - starttime>0) && (msg_date - starttime)< DateUtils.DAY_IN_MILLIS)
		{
			//show time
			datestr = DateFormat.getTimeFormat(context).format(msg_date);
		}else{
			//show date
			datestr = DateFormat.getDateFormat(context).format(msg_date);
		}
		return datestr;
	}
	
}
