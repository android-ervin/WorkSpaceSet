package ervin.android.sms;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

public class SMSActivity extends TabActivity {
	TabHost tabHost=null;
	Intent intent = null;
	TabSpec spec = null;
	private SharedPreferences sp = null ;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        sp = getSharedPreferences("config", MODE_PRIVATE);
        tabHost=getTabHost();
        setActivity();
        
        String tabname = sp.getString("tabid", "");
        if("".equals(tabname))
        {
        	tabHost.setCurrentTab(0);
        }else
        {
        	tabHost.setCurrentTabByTag(tabname);
        }
        
        tabHost.setOnTabChangedListener(new OnTabChangeListener() {
			
			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				Editor edit = sp.edit();
				edit.putString("tabid", tabId);
				edit.commit();
			}
		});
    }
    public void setActivity()
    {
    	intent = new Intent(this,Conversation.class);
    	spec = tabHost.newTabSpec("Conversation").setIndicator("会话",
    			getResources().getDrawable(android.R.drawable.ic_menu_myplaces)).setContent(intent);
    	tabHost.addTab(spec);
    	
    	intent = new Intent(this,Folder.class);
    	spec = tabHost.newTabSpec("Folder").setIndicator("文件",
    			getResources().getDrawable(android.R.drawable.ic_menu_save)).setContent(intent);
    	tabHost.addTab(spec);
    	
    	intent = new Intent(this,Conversation.class);
    	spec = tabHost.newTabSpec("Group").setIndicator("群组",
    			getResources().getDrawable(android.R.drawable.ic_menu_search)).setContent(intent);
    	tabHost.addTab(spec);
    	
    }
}