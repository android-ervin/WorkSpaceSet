package ervin.android.sms;

import java.net.URI;
import java.util.HashMap;

import ervin.android.uri.SmsProvider;
import android.app.Activity;
import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class Folder extends ListActivity {

	private ListView mlist;
	private String[] strname = new String[]{"收件箱","发件箱","草稿箱","已发送"};
	private int[] pic = new int[]{android.R.drawable.ic_dialog_email,android.R.drawable.ic_dialog_dialer,
			android.R.drawable.ic_menu_delete,android.R.drawable.ic_menu_agenda};
	private final int SMS_INBOX =0;
	private final int SMS_RECEVIE=1;
	private final int SMS_DRAFT = 2;
	private final int SMS_SEND =3;
	
	private folderAdapter mAdapter;
	private HashMap<Integer, Integer> sizeMap = new HashMap<Integer, Integer>();
	private ContentObserver observer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		//listActivity中就有一个listview，不用写布局了
		mlist = getListView();
		mAdapter = new folderAdapter();
		mlist.setAdapter(mAdapter);
		mlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(sizeMap.get(position)!=0)
				{
					Intent intent = new Intent(getApplicationContext(),FolderDetail.class);//不能用this?可以
					intent.putExtra("type", position);
					intent.putExtra("title", strname[position]);
					startActivity(intent);	
				}
			}
		});
		
		observer =  new myObserver(new Handler());
		startQuery();
	}
	
	//设置一个内容观察者，观察短信到来信息，用来更新显示列表
	public class myObserver extends ContentObserver
	{
		public myObserver(Handler handler) {
			super(handler);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onChange(boolean selfChange) {
			// TODO Auto-generated method stub
			super.onChange(selfChange);
			//当观察到有短信到来的时候
			startQuery();
		}		
		
	}
	@Override
	protected void onStart() {
		super.onStart();
		//Uri uri=SmsProvider.CONVERSATION_URI;
		Uri uri = Uri.parse("content://mms-sms");
		getContentResolver().registerContentObserver(uri, true, observer);
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		getContentResolver().unregisterContentObserver(observer);
	}
	
	private void initMap()
	{
		for(int i =0 ;i<pic.length;i++)
			sizeMap.put(i, 0);
	}
	
	private void startQuery() {
		// TODO Auto-generated method stub
		QueryHandler qHandler = new QueryHandler(getContentResolver());
		Uri uri=null;
		//不同的条目使用不同的uri
		for(int i=0;i<pic.length;i++)
		{
			switch (i) {
			case SMS_INBOX:
				uri =SmsProvider.CONVERSATION_URI_INBOX;
				break;
			case SMS_RECEVIE:
				uri =SmsProvider.CONVERSATION_URI_OUTBOX;
				break;
			case SMS_DRAFT:
				uri =SmsProvider.CONVERSATION_URI_DRAFT;
				break;
			case SMS_SEND:
				uri =SmsProvider.CONVERSATION_URI_SENT;
				break;
			}
			qHandler.startQuery(i, null, uri, null, null, null, null);
		}
		
	}

	public class QueryHandler extends AsyncQueryHandler{

		public QueryHandler(ContentResolver cr) {
			super(cr);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
			// TODO Auto-generated method stub
			//其中token为传过来的i的值，也就是查的第几个uri
			sizeMap.put(token, cursor.getCount());
			mAdapter.notifyDataSetChanged();
			super.onQueryComplete(token, cookie, cursor);
			
		}		
		
	} 

	public class folderAdapter extends BaseAdapter
	{
		@Override
		public int getCount() {
			
			//返回ListView中一共有多少条数据
			return pic.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view =null;
			FolderItemViews fiv=null;
			//找到控件
			//为了优化代码，不用每次都执行查找空间findViewById的代码，可以做缓存处理
			if(convertView !=null)
			{
				view = convertView;
				fiv = (FolderItemViews) view.getTag();
			}else
			{
				//用三个参数的infater方法是因为需要用到布局文件中的父窗口指定的65dp的高度
				view= getLayoutInflater().inflate(R.layout.folder_item, parent, false);
				//view = View.inflate(Folder.this, R.layout.folder_item, parent);
				fiv = new FolderItemViews();
				fiv.iv = (ImageView) view.findViewById(R.id.iv);
				fiv.tv_name = (TextView)view.findViewById(R.id.tv_name);
				fiv.tv_size =(TextView)view.findViewById(R.id.tv_size); 
				view.setTag(fiv);
			}	
			
			//绑定数据
			fiv.iv.setImageResource(pic[position]);
			fiv.tv_name.setText(strname[position]);
				//需要等到查询完毕后，绑定条数.初始状态都为0
			fiv.tv_size.setText(sizeMap.get(position)+"");
			return view;
		}	
	}
	
	private class FolderItemViews
	{
		private ImageView iv;
		private TextView tv_name;
		private TextView tv_size;
	}
}
