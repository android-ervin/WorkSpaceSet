package ervin.android.sms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import ervin.android.smsmodel.SmsModel;
import ervin.android.uri.SmsProvider;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.ContactsContract.PhoneLookup;
import android.text.AlteredCharSequence;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Conversation extends Activity implements OnClickListener{

	private static final int MENU_DELETE = 0;
	private static final int MENU_SEARCH = 1;
	private static final int MENU_BACK = 2;
	
	private Button btn_new_msg=null;
	private Button btn_all_select=null;
	private Button btn_cancel_select=null;
	private Button btn_delete=null;
	
	private ListView listview=null;
	private LinearLayout  lineredit=null;
	private TextView tv=null;
	
	private QueryHandler mQueryHandler=null;
	private SmsProvider sProvider=null;
	
	private List<SmsModel> smss = null ;
	private SmsModel sms=null;
	private ConversationAdapter cursoradapter = null ;
	private ProgressDialog pd =null;
	
	MenuItem menu_delete; 
	MenuItem menu_search;
	MenuItem menu_back;
//	private List<String> smsidlist = new ArrayList<String>();
	//?为什么用list数据会变成原来的两倍
	private HashSet<String> smsidlist = new HashSet<String>();
	
	
	public static int test_i = 0;
	public boolean delete = true ;
	
	//用cursoradapter 时必须要用_id这个字段
	private String[] projectin = {"sms.thread_id as _id","snippet","msg_count",
			"sms.address as address",
			"sms.date as date"};
	
	//定义显示模式:编辑状态和显示状态
	private enum DISPLAYMODE{
		eList,eEdit
	}
	private DISPLAYMODE mode = DISPLAYMODE.eList;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conversation);
		
		initView();
		startquery();
		
		cursoradapter = new ConversationAdapter(this, null);
		listview.setAdapter(cursoradapter);
		
		
		/**
		 * 监听listView中那条被选中了
		 */
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.i("Conversation", "条目"+position+"被点击了");
				Log.i("Conversation", "id"+id+"被点击了");
				CheckBox cb = (CheckBox) view.findViewById(R.id.checkbox);
				if(mode == DISPLAYMODE.eEdit){
					if(smsidlist.contains(Long.toString(id)))
					{
						smsidlist.remove(Long.toString(id));
						cb.setChecked(false);
					}else{
						smsidlist.add(Long.toString(id));
						cb.setChecked(true);
					}
					//btn显示状态规则
					if(smsidlist.size() < cursoradapter.getCount()) //表示没有全选
					{
						btn_all_select.setEnabled(true);
						if(smsidlist.size()==0)//相当编辑模式
						{
							btn_cancel_select.setEnabled(false);
							btn_delete.setEnabled(false);
						}else{
							btn_cancel_select.setEnabled(true);
							btn_delete.setEnabled(true);
						}
						
					}else{
						btn_all_select.setEnabled(false);
						
					}
				}
				if(mode == DISPLAYMODE.eList) //查看短信详细信息
				{
					//1,看原生应用是怎么跳转到短信详细信息界面
					//2,跳转到自己的详细界面
					Intent intent = new Intent(Conversation.this,ConversationDetail.class);
					intent.putExtra("id", Long.toString(id));
					startActivity(intent);
				}
			}
		});
	}
	//静态生成menu用到onCreateOptionsMenu(Menu menu)
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu_delete = menu.add(0, MENU_DELETE, Menu.NONE, "删除");
		menu_search = menu.add(0, MENU_SEARCH, Menu.NONE, "搜索");
		menu_back = menu.add(0, MENU_BACK, Menu.NONE, "返回");
		return super.onCreateOptionsMenu(menu);
	}
	//这里要用到动态生成menu,即不同的模式下显示不同的菜单
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if(mode == DISPLAYMODE.eList)
		{
			menu_delete.setVisible(true);
			menu_search.setVisible(true);
			menu_back.setVisible(false);
			if(cursoradapter.getCount()>0)
			{
				menu_delete.setEnabled(true);
			}else
				menu_delete.setEnabled(false);
		}else{
			menu_delete.setVisible(false);
			menu_search.setVisible(false);
			menu_back.setVisible(true);
		}
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//Log.i("Conversation", item.getItemId()+"被点击了");
		switch (item.getItemId()) {
		case MENU_DELETE:
			changeMode(DISPLAYMODE.eEdit);
			break;
		case MENU_SEARCH:
		
			break;
		case MENU_BACK:
			changeMode(DISPLAYMODE.eList);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	private void changeMode(DISPLAYMODE mode) {
		
		this.mode = mode;
		if(mode == DISPLAYMODE.eEdit)
		{
			btn_new_msg.setVisibility(View.GONE);
			//linerlayout必须也显示
			lineredit.setVisibility(View.VISIBLE);
			
			btn_all_select.setVisibility(View.VISIBLE);
			btn_cancel_select.setVisibility(View.VISIBLE);
			btn_delete.setVisibility(View.VISIBLE);
			
			btn_all_select.setEnabled(true);
			btn_cancel_select.setEnabled(false);
			btn_delete.setEnabled(false);
			//注意：由于上面布局发生了改变，listview进行了重绘，所以会调用重新cursoradapter中的bindview方法
		}else
		{
			btn_new_msg.setVisibility(View.VISIBLE);
			lineredit.setVisibility(View.GONE);
			btn_delete.setVisibility(View.GONE);
			smsidlist.clear();
		}
		
	}
	
	
	/*查询数据*/
	private void startquery() {
		mQueryHandler =  new QueryHandler(getContentResolver());
		mQueryHandler.startQuery(0, null, SmsProvider.CONVERSATION_URI, projectin, null, null, "date Desc");
	}
	
	/*异步执行查询短信数据（不在主线程中执行）*/
	public class QueryHandler extends AsyncQueryHandler 
	{

		public QueryHandler(ContentResolver cr) {
			super(cr);
		}
		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) { //此cursor就是查询后的结果
			super.onQueryComplete(token, cookie, cursor);
			
			//当查询完毕的时候 cursor发生改变
			cursoradapter.changeCursor(cursor);  //The new cursor to be used，还是调用notifyDataSetChanged();
		}	
	}
	/*初始化参数*/
	private void initView() {
		btn_new_msg=(Button) findViewById(R.id.btn_new_msg);
		btn_all_select=(Button)findViewById(R.id.bt_all_select);
		btn_cancel_select=(Button)findViewById(R.id.bt_cancel_select);
		btn_delete=(Button)findViewById(R.id.btn_delete);
		
		btn_new_msg.setOnClickListener(this);
		btn_all_select.setOnClickListener(this);
		btn_cancel_select.setOnClickListener(this);
		btn_delete.setOnClickListener(this);
		
		listview=(ListView) findViewById(R.id.listview);
		lineredit=(LinearLayout) findViewById(R.id.edit);
		tv=(TextView)findViewById(R.id.tv);
		
		//隐藏布局
		lineredit.setVisibility(View.GONE);//This view is invisible, and it doesn't take any space for layout purposes.
		btn_delete.setVisibility(View.GONE);
		
		listview.setEmptyView(tv);//Sets the view to show if the adapter is empty.
		sProvider = new SmsProvider();
		//mQueryHandler=new QueryHandler(getContentResolver());
	}
	/**
	 * 获得短信数据的方法：
	 * getContentResvolr.query()
	 * managerquery();返回一个cursor,不用手动去管理cursor,让activity帮我们管理
	 */

	/*为ListView设置Adapter(里面应该实现了接收系统的短信广播，有短信到来的时候也会执行bindview方法)
	 * a column named "_id" or this class will not work.*/
	private class ConversationAdapter extends CursorAdapter
	{
		private LayoutInflater mInflater; //声明一个布局填充器
		
		public ConversationAdapter(Context context, Cursor c) {
			super(context, c);
			// TODO Auto-generated constructor stub
			/**
			 * 有三种方式初始化布局填充器。
			 * 1，mInflater=getLayoutInflater()
			 * 2，mInflater=(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)
			 * 3，mInflater=LayoutInflater.from(context);
			 */
			mInflater=LayoutInflater.from(context);
			
		}
		
		/**
		 * 给item绑定数据，该cursor是否是循环读取数据？根据一共有m条目，然后调用m次getview方法一条一条的将view显示在界面上
		 * 			直到界面满了为止,只要界面发生了改变，listview进行了重绘，就会调用该方法，如果界面没发生改变但是需要
		 * 		listview更新操作，可以用.notifyDataSetChanged()方法，那么又会执行getview方法了
		 */
		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			test_i++;
			//找到控件
			CheckBox cb = (CheckBox) view.findViewById(R.id.checkbox);
			ImageView photo=(ImageView) view.findViewById(R.id.photo);
			TextView name = (TextView) view.findViewById(R.id.tv_name);
			TextView body = (TextView) view.findViewById(R.id.tv_body);
			TextView date = (TextView) view.findViewById(R.id.tv_date);
			
			String id = cursor.getString(0);
			String snippet = cursor.getString(1);
			int msg_count = cursor.getInt(2);
			String address = cursor.getString(3);
			long msg_date = cursor.getLong(4);
			if(mode == DISPLAYMODE.eList)
			{
				cb.setVisibility(View.GONE);
			}else{
				cb.setVisibility(View.VISIBLE);
				//当编辑模式的时候（根据list集合中存放的id值）判断哪些cb被选中了
				if(smsidlist.contains(id))
				{
					cb.setChecked(true);
				}else
					cb.setChecked(false);
			}
			
			String number = sProvider.getContactNamebyAddress(context, address);
			//有联系人的号码时显示联系人姓名
			if(TextUtils.isEmpty(number))
			{
				photo.setImageResource(R.drawable.icon);
				if(msg_count>1)
				{
					name.setText(address+"("+msg_count + ")");
				}else{
					name.setText(address);
				}
			}else
			{
				photo.setImageResource(R.drawable.icon);
				if(msg_count>1)
				{
					name.setText(number+"("+msg_count + ")");
				}else{
					name.setText(number);
				}
			}
			
			
			body.setText(snippet);
			//设置时间，如果是今天的时间显示时间，如果不是，显示日期，并且保持与系统风格一致
			//得到的时间与今天初始时间比较，小于一天的毫秒数
			String datestr = sProvider.formatDate(context, msg_date);
			date.setText(datestr);
			
			Log.i("Conversation", "这是第"+test_i+"次调用bindview方法");
			Log.i("Conversation",id);
		}
	
		/**
		 * 创建ListView的Item布局,执行一次
		 */
		@Override
		public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
			View view=mInflater.inflate(R.layout.conversation_items, arg2,false);//Inflate a new view hierarchy from the specified xml resource.
			return view;
		}

	}
	/**
	 * 一般的listview的adpter创建方法
	 * @author acer
	 *
	 */
	public class myAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return smss.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			sms=smss.get(position);
			View view = View.inflate(getApplicationContext(), R.layout.conversation_items, null);
			
			TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
			tv_name.setText(sms.getAddress());
			TextView tv_date = (TextView)view.findViewById(R.id.tv_date);
			tv_date.setText(sms.getDate());
			TextView tv_body = (TextView)view.findViewById(R.id.tv_body);
			tv_body.setText(sms.getBody());
			return view;
		}
		
	}
	/**
	 * btn点击事件
	 */
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btn_new_msg:
			Intent intent = new Intent(Conversation.this,NewMessage.class);
			startActivity(intent);
			break;

		case R.id.bt_all_select:
			
			for(int i = 0 ;i<cursoradapter.getCount();i++)
			{
				Cursor cursor =(Cursor) cursoradapter.getItem(i);//源码中返回值是cursor,也就是每一个sms对象（包含id,snippe,...）
				smsidlist.add(cursor.getString(0));//每一个cursor取第一个字段id,把id放入集合中
				Log.i("Conversation", "List"+cursor.getString(0));
			}
			cursoradapter.notifyDataSetChanged(); //listview界面更新，通知adapter去适配
			
			btn_all_select.setEnabled(false);
			btn_cancel_select.setEnabled(true);
			btn_delete.setEnabled(true);
			
			break;
		case R.id.bt_cancel_select:
			
			smsidlist.clear();
			changeMode(DISPLAYMODE.eList);
			cursoradapter.notifyDataSetChanged(); 
			
			break;
		case R.id.btn_delete:
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle("删除");
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage("您确定要删除所选择的的会话吗？");
			builder.setCancelable(false); //屏蔽回退键
			builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					//
					//设置processbar
					pd = new ProgressDialog(Conversation.this);
					pd.setTitle("删除");
					pd.setIcon(android.R.drawable.ic_dialog_alert);
					pd.setCancelable(false); //屏蔽回退键
					pd.setMax(smsidlist.size());
					pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);//样式
					pd.setButton("取消", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// 点击取消后，停止删除
							delete = false ;
							pd.dismiss();
						}
					});
					//开启一个删除短信的线程
					new Thread(new myRunnable()).start();
					pd.show();
				}

			});
			builder.setNegativeButton(android.R.string.cancel,new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			builder.show();
			break;
		}
	}
	private myHandler mHandler = new myHandler();
	public class myRunnable implements Runnable
	{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			for(int i =0;i<smsidlist.size();i++)
			{
				if(!delete)
				{
					break;
				}
				//Cursor cursor = (Cursor) cursoradapter.getItem(i);
				ArrayList<String> list = new ArrayList<String>(smsidlist);
				Uri uri = Uri.withAppendedPath(SmsProvider.CONVERSATION_URI, list.get(i)); //根据回话id也就是查询出的_id来删除短信内容
				getContentResolver().delete(uri, null,null);
				//
				pd.incrementProgressBy(1);//可以在子线程中更新
				SystemClock.sleep(2000); 
			}
			pd.dismiss();
			
			//删除完毕后，通知UI界面更新
			
			Message msg = mHandler.obtainMessage();//google recommend
			msg.obj = "DeleteFinish";
			mHandler.sendMessage(msg);
			
			smsidlist.clear();
		}
		
	}
	public class myHandler extends Handler
	{
		@Override
		public void handleMessage(Message msg) {

			if("DeleteFinish".equals(msg.obj))
			{
				changeMode(DISPLAYMODE.eList);
				Log.i("Conversation","mode changed");
				delete = true;
			}
			super.handleMessage(msg);
		}
	}
	//处理返回键
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(mode == DISPLAYMODE.eEdit)
		{
			if(keyCode == event.KEYCODE_BACK)
			{
				changeMode(DISPLAYMODE.eList);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
