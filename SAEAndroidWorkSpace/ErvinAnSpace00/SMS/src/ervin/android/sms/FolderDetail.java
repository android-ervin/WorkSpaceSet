package ervin.android.sms;

import ervin.android.uri.SmsProvider;
import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FolderDetail extends Activity {

	private Button btn_new_msg ;
	private ListView listview;
	private int type;
	private MyAdapter mAdapter;
	
	private MyQueryHandler mQueryHandler;
	
	private SmsProvider sp = null;
	
	private final int SMS_INBOX =0;
	private final int SMS_RECEVIE=1;
	private final int SMS_DRAFT = 2;
	private final int SMS_SEND =3;
	
	private final String[] projection = new String[]{"sms.thread_id as _id","body",
			"sms.address as address",
			"sms.date as date"};
	private final int TV_BODY = 1;
	private final int TV_NAME =2;
	private final int TV_DATE =3;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.folderdetail);
		
		sp= new SmsProvider();
		btn_new_msg = (Button) findViewById(R.id.btn_new_msg);
		listview = (ListView) findViewById(R.id.listview);
		
		Intent intent = getIntent();
		type = intent.getIntExtra("type", 0);
		String title = intent.getStringExtra("title");
		setTitle(title);
		btn_new_msg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent= new Intent(FolderDetail.this,NewMessage.class);
				startActivity(intent);
			}
		});
		
		mQueryHandler = new MyQueryHandler(getContentResolver());
		mAdapter = new MyAdapter(this, null);
		listview.setAdapter(mAdapter);
		startQuery();
	}
	
	private void startQuery() {
		Uri uri=null;
		switch (type) {
		case SMS_INBOX:
			uri =SmsProvider.CONVERSATION_URI_INBOX;
			break;
		case SMS_RECEVIE:
			uri =SmsProvider.CONVERSATION_URI_OUTBOX;
			break;
		case SMS_DRAFT:
			uri =SmsProvider.CONVERSATION_URI_DRAFT;
			break;
		case SMS_SEND:
			uri =SmsProvider.CONVERSATION_URI_SENT;
			break;
		}
		mQueryHandler.startQuery(0, null, uri, projection, null, null, "sms.date"+" desc");
	}
	public class MyQueryHandler extends AsyncQueryHandler
	{

		public MyQueryHandler(ContentResolver cr) {
			super(cr);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
			// TODO Auto-generated method stub
			super.onQueryComplete(token, cookie, cursor);
			mAdapter.changeCursor(cursor);//最终会调用mAdapter.notifyDataSetChanged();
			
		}
		
	}
	

	public class MyAdapter extends CursorAdapter
	{
		LayoutInflater inflater;
		Views views ;
		public MyAdapter(Context context, Cursor c) {
			super(context, c);
			inflater = getLayoutInflater().from(context);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			//第一次运行的时候执行
			//View view = inflater.inflate(R.layout.conversation_items, parent, false);
			View view = inflater.inflate(R.layout.conversation_items, null);
			views = new Views();
			views.iv = (ImageView) view.findViewById(R.id.photo);
			views.tv_name = (TextView) view.findViewById(R.id.tv_name);
			views.tv_date = (TextView) view.findViewById(R.id.tv_date);
			views.tv_body = (TextView) view.findViewById(R.id.tv_body);
			view.setTag(views);
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			
			views = (Views) view.getTag();
			
			views.iv.setBackgroundResource(R.drawable.icon);
			views.tv_body.setText(cursor.getString(TV_BODY));
			
			String address = sp.getContactNamebyAddress(context, cursor.getString(TV_NAME));
			if(address!=""){
				views.tv_name.setText(address);
			}else
			{
				views.tv_name.setText(cursor.getString(TV_NAME));
			}
			
			
			String date = sp.formatDate(context, cursor.getLong(TV_DATE));
			views.tv_date.setText(date);
			
		}		
	}
	public class Views 
	{
		private ImageView iv;
		private TextView tv_name;
		private TextView tv_date;
		private TextView tv_body;
	}
}
