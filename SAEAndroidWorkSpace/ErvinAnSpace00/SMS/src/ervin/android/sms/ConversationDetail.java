package ervin.android.sms;

import ervin.android.uri.SmsProvider;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ConversationDetail extends Activity {

	private String sms_id;
	private Cursor cursor_smsdetail;
	
	private ImageView iv_photo ;
	private TextView tv_name ;
	private TextView tv_number ;
	private ListView listview;
	
	private SmsProvider sProvider;
	private myAdapter mAdapter ;
	private ConversationDetailView cdview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conversationdetail);
		sms_id = getIntent().getStringExtra("id");
		sProvider = new SmsProvider();
		
		//给一个空cursor，当查询短信完毕时调用notifyDataSetChanged();
		mAdapter= new myAdapter(this, cursor_smsdetail);
		
		
		iv_photo = (ImageView) findViewById(R.id.detail_photo);
		tv_name =(TextView) findViewById(R.id.detail_name);
		tv_number = (TextView) findViewById(R.id.detail_number);
		listview = (ListView) findViewById(R.id.detail_smslist);
		
		iv_photo.setImageResource(R.drawable.icon);
		
		//查询短信      这里也可以使用异步查询（）
		ContentResolver cr = getContentResolver();
		cursor_smsdetail = cr.query(SmsProvider.CONVERSATION_URI_ALL, null, "thread_id=?", new String[]{sms_id}, null);
		if(cursor_smsdetail!=null)
		{
			mAdapter.changeCursor(cursor_smsdetail);
		}
		listview.setAdapter(mAdapter);
		/*System.out.println(sms_id);
		while(cursor_smsdetail.moveToNext())
		{
			for (String name : cursor_smsdetail.getColumnNames()) {
				System.out.println(name + "="+cursor_smsdetail.getString(cursor_smsdetail.getColumnIndex(name)));
			}
		}*/
		stratBindView();
	}
	private void stratBindView() {
		//一个会话可能有好几条数据，随便取一条取出address
		Cursor cursor = (Cursor) mAdapter.getItem(0);

		String address = cursor.getString(cursor.getColumnIndex("address"));
		String contact_name = sProvider.getContactNamebyAddress(this, address);
		if(TextUtils.isEmpty(contact_name))
		{
			tv_name.setText(address);
			tv_number.setText("");
		}else{
			tv_name.setText(contact_name);
			tv_number.setText(address);
		}
		
		
	}
	public class ConversationDetailView 
	{
		TextView tv_type ;
		TextView tv_date ;
		TextView tv_body ;
	}
	public class myAdapter extends CursorAdapter
	{
		private LayoutInflater mInflater;
		public myAdapter(Context context, Cursor c) {
			super(context, c);
			mInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view = mInflater.inflate(R.layout.conversationdetail_items,null);
			cdview = new ConversationDetailView();
			cdview.tv_type = (TextView) view.findViewById(R.id.detail_type);
			cdview.tv_date = (TextView) view.findViewById(R.id.detail_date);
			cdview.tv_body = (TextView) view.findViewById(R.id.detail_body);
			
			view.setTag(cdview);
			return view;
		}

		@Override
		//cursorAdapter会根据getCount的多少来调用多少次的bindView(getView)方法，将cursor中的数据一条一条显示在界面，
		//直到界面填满为止
		public void bindView(View view, Context context, Cursor cursor) {
			// TODO Auto-generated method stub
			ConversationDetailView views = (ConversationDetailView) view.getTag();
			
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			long date = cursor.getLong(cursor.getColumnIndex("date"));
			String body = cursor.getString(cursor.getColumnIndex("body"));
			//处理类型
			switch (type) {
			case 1:
				views.tv_type.setText("接收于：");
				break;
			case 2:
				views.tv_type.setText("发送于：");
				break;
			}
			//处理日期
			String datestr = sProvider.formatDate(context, date);
			views.tv_date.setText(datestr);
			
			views.tv_body.setText(body);
			
		}
		
	}
}
