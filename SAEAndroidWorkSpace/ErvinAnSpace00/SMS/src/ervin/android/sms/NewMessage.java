package ervin.android.sms;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class NewMessage extends Activity {

	private EditText et_number;
	private EditText et_content;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_message);
		
		et_number = (EditText) findViewById(R.id.msg_num);
		et_content = (EditText) findViewById(R.id.msg_content);
	}
	
	public void send(View view)
	{
		String msg_number = et_number.getText().toString();
		String msg_content = et_content.getText().toString();
		if(!TextUtils.isEmpty(msg_number)&&!TextUtils.isEmpty(msg_content))
		{
			SmsManager sm = SmsManager.getDefault();
			//当短信很长时候，分批发送
			ArrayList<String> parts = sm.divideMessage(msg_content);
			sm.sendMultipartTextMessage(msg_number, null, parts, null, null);
			//将发送的短信插入数据库中
			Uri uri = Uri.parse("content://sms/sent");
			ContentValues values = new ContentValues();
			values.put("address", msg_number);
			values.put("body", msg_content);
			getContentResolver().insert(uri, values);
			
			Toast.makeText(this, "短信发送成功", 1).show();
			finish();		
		}
	}
}
