package ervin.android.sensor;

import java.util.ArrayList;
import java.util.List;

import net.youmi.android.AdManager;
import net.youmi.android.banner.AdSize;
import net.youmi.android.banner.AdView;
import net.youmi.android.diy.banner.DiyAdSize;
import net.youmi.android.diy.banner.DiyBanner;
import ervin.android.scalepic.scalePicture;
import ervin.android.sensor.R.drawable;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.AlteredCharSequence;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity {

	private List<Sensor> sensorList;
	private static ImageView iv ;
	private myListener listener;
	private SensorManager sm;
	private Sensor sensor ;
	private Bitmap bitmap ;
	
	private final static String APPID="9ebb2ddc9d6508c2";
	private final static String APPSECRET ="3974ab5f733d526b";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);

		AdManager.getInstance(this).init(APPID, APPSECRET, false);
		// 获取要嵌入迷你广告条的布局
		RelativeLayout adLayout=(RelativeLayout)findViewById(R.id.AdLayout);

		// Demo 1 迷你Banner : 宽满屏，高32dp
		// 传入宽度满屏，高度为 32dp 的 DiyAdSize 来定义迷你 Banner
		DiyBanner banner = new DiyBanner(this, DiyAdSize.SIZE_MATCH_SCREENx32);

		// Demo 2 迷你Banner : 宽320dp，高32dp
		// 传入高度为32dp 的 DiyAdSize 来定义迷你 Banner
		//DiyBanner banner = new DiyBanner(this, DiyAdSize.SIZE_320x32);

		// 将积分 Banner 加入到布局中
		adLayout.addView(banner);
		
		iv = (ImageView) findViewById(R.id.iv);
		iv.setBackgroundColor(Color.TRANSPARENT);
		bitmap = scalePicture.getScalePic(MainActivity.this, R.drawable.compass3);
		iv.setImageBitmap(bitmap);
//		iv.setBackgroundResource(R.drawable.compass_item12);
		sensorRegist();	
		sensorList =  sm.getSensorList(Sensor.TYPE_ALL);
		for (Sensor sensor : sensorList) {
			System.out.println("名称"+sensor.getName()+"厂商"+sensor.getVendor());
		}
		
		
	}
	
	public void sensorRegist() {
		sm = (SensorManager) getSystemService(SENSOR_SERVICE);
		sensor = sm.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		listener = new myListener();
		sm.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_GAME);
	}
	
	public class myListener implements SensorEventListener
	{
		float startangle = 0;
		@Override
		//values[0]: Azimuth, angle between the magnetic north direction and the y-axis, around the z-axis (0 to 359). 0=North, 90=East, 180=South, 270=West 
		public void onSensorChanged(SensorEvent event) {
			// TODO Auto-generated method stub
			float[] value =event.values;
			float angle = value[0];//与正北的夹角
			System.out.println("方向:"+angle);
			RotateAnimation ra = new RotateAnimation(startangle, angle, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			ra.setDuration(1300);
			iv.startAnimation(ra);
			startangle = -angle;
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
			
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			 
			 AlertDialog.Builder builder = new Builder(this);
			 builder.setTitle("设置");
			 builder.setMessage("是否更换图片");
			 builder.setNegativeButton("取消", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			 builder.setPositiveButton("确定", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					sensorUnregist();
					AlertDialog.Builder builder_singleselect = new Builder(MainActivity.this);
					builder_singleselect.setTitle("请选择");
					String[] items = new String[]{"图片一","图片二","图片三","默认图片"};
					builder_singleselect.setSingleChoiceItems(items, -1, new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							switch (which) {
							case 0:
								bitmap = scalePicture.getScalePic(MainActivity.this, R.drawable.images);
								iv.setImageBitmap(bitmap);
								sensorRegist();
								dialog.dismiss();
								break;
							case 1:
								bitmap = scalePicture.getScalePic(MainActivity.this, R.drawable.compass1);
								iv.setImageBitmap(bitmap);
								sensorRegist();
								dialog.dismiss();
								break;
							case 2:
								bitmap = scalePicture.getScalePic(MainActivity.this, R.drawable.compass2);
								iv.setImageBitmap(bitmap);
								sensorRegist();
								dialog.dismiss();
								break;
							case 3:
								bitmap = scalePicture.getScalePic(MainActivity.this, R.drawable.compass3);
								iv.setImageBitmap(bitmap);
								sensorRegist();
								dialog.dismiss();
								break;
							}
						}
					});
					builder_singleselect.create().show();
				}
			});
			 builder.create().show();
			return true;
		}
		
		if(id == R.id.action_sensor)
		{
			Intent intent = new Intent(this,SensorList.class);
		
			startActivity(intent);
		}
//		if(id == R.id.action_theme)
//		{
//			AlertDialog.Builder builder = new Builder(this);
//			String[] items = new String[]{"透明悬浮","Holo白"};
//			builder.setSingleChoiceItems(items, -1, new OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
//					switch (which) {
//					case 0:
//						MainActivity.this.setTheme(android.R.style.Theme_Translucent_NoTitleBar);
//						break;
//					case 1:
//						MainActivity.this.setTheme(android.R.style.Theme_Holo_Light);
//						break;
//					}
//					dialog.dismiss();
//				}
//			});
//			builder.create().show();
			
//		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		sensorUnregist();
	}

	public void sensorUnregist() {
		
		if (sm != null && listener != null) {
			sm.unregisterListener(listener);
			listener = null;
			sm = null;
		}
	}
}
