package ervin.android.sensor;

import java.util.List;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SensorList extends Activity {

	private ListView lv;
	private List<Sensor> sensorlist ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor_list);
		
		SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
		sensorlist =  sm.getSensorList(Sensor.TYPE_ALL);
		
		lv = (ListView) findViewById(R.id.lv);
		lv.setAdapter(new myAdapter());
	}
	
	public class myAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return sensorlist.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Sensor sensor = sensorlist.get(position);
			View view = View.inflate(SensorList.this, R.layout.sensor_list_item, null);
			TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
			tv_name.setText(sensor.getName());
			TextView tv_vendor = (TextView)view.findViewById(R.id.tv_vendor);
			tv_vendor.setText(sensor.getVendor());
			return view;
		}
		
	}
}
