package ervin.android.tab;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

/*官方文档标准格式*/
public class TabLayoutActivity extends TabActivity {
	
	SharedPreferences sp=null;  //用来保存首选项，保存用户最后的操作界面,单例模式
	TabHost tabHost=null;
	Intent intent = null;
	TabSpec spec = null;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		tabHost = this.getTabHost();
		sp=getSharedPreferences("config", MODE_PRIVATE);
		
		this.setActivity();
		//给Tab设置监听器
		tabHost.setOnTabChangedListener(new OnTabChangeListener() {
			
			@Override
			public void onTabChanged(String arg0) {
				
				//String tag=tabHost.getCurrentTabTag();//获得当前的tag
				Editor editor=sp.edit();
				editor.putString("tag", arg0);
				editor.commit();  //保存信息在首选项中 或者用apply() 
				//System.out.println("arg0--->" + arg0 +"...tag--->" + tag);
			}
		});
	
		
		String tag=sp.getString("tag", "");//Retrieve a String value from the preferences.获得保存在sp中的信息；
		if("".equals(tag))
		{
			tabHost.setCurrentTab(1);
		}
		else
			tabHost.setCurrentTabByTag(tag);
	}	
		
	private void setActivity()
	{
		/**
		 * 定义第一个tab
		 */
		// Create an Intent to launch an Activity for the tab (to be reused)
		intent = new Intent().setClass(this, ArtistsActivity.class); //
		// Initialize a TabSpec for each tab and add it to the TabHost
		spec =tabHost.newTabSpec("artists").setIndicator("艺术").setContent(intent);
		// res.getDrawable(R.drawable.ic_tab_artists)) .setContent(intent);
		tabHost.addTab(spec);
		
		/**
		 * 定义第二个tab
		 */
		// Create an Intent to launch an Activity for the tab (to be reused)
		intent = new Intent().setClass(this, AlbumsActivity.class); //
		// Initialize a TabSpec for each tab and add it to the TabHost
		spec =tabHost.newTabSpec("albums").setIndicator("图片").setContent(intent);
		// res.getDrawable(R.drawable.ic_tab_artists)) .setContent(intent);
		tabHost.addTab(spec);
		
		/**
		 * 定义第三个tab
		 */
		// Create an Intent to launch an Activity for the tab (to be reused)
		intent = new Intent().setClass(this, SongsActivity.class); //
		// Initialize a TabSpec for each tab and add it to the TabHost
		spec =tabHost.newTabSpec("songs").setIndicator("歌曲").setContent(intent);
		// res.getDrawable(R.drawable.ic_tab_artists)) .setContent(intent);
		tabHost.addTab(spec);
		System.out.println("is running");
		//tabHost.setCurrentTab(2);
	}	
}