package ervin.android.shudu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

//初始化数独数组
public class InitUtils {

	//判断是否copy到目录中 data/data/ervin.android.shudu/files/shudu.db
	public void CopyFile(Context context)
	{
		try {
			InputStream is = context.getAssets().open("shudu.txt");
			File file = new File(context.getFilesDir(), "shudu.txt");
			if(file.exists() && file.length()!=0)
			{
				
			}else{
				FileOutputStream fos = new FileOutputStream(file);
				byte[] b = new byte[1024];
				int len = 0;
				while((len = is.read(b))!=-1)
				{
					fos.write(b, 0, len);
				}
				is.close();
				fos.flush();
			}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<String> getInitStr(Context context)
	{
		File file = new File(context.getFilesDir(), "shudu.txt");
		InputStream is;
		List<String> initstr = new ArrayList<String>();
		String line;
		try {
			is = new FileInputStream(file);
			BufferedReader br = new  BufferedReader(new InputStreamReader(is));
			while((line = br.readLine())!=null)
			{
				initstr.add(line);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return initstr;
	}
}
