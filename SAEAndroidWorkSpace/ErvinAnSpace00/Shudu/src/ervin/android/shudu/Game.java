package ervin.android.shudu;

import java.util.List;
import java.util.Random;

import android.content.Context;
import android.os.SystemClock;

public class Game {

//	public final String[] init ={"360000000004230800000004200"
//			+"070460003820000014500013020"
//			+"001900000007048300000000045","390750000000000060064028000"
//			+"000000005002903800700000000"
//			+"000260940020000000000094073"};
//	private final String initstr ="360000000004230800000004200"
//			+"070460003820000014500013020"
//			+"001900000007048300000000045";
	private static int[] position;
	public static int[] shudu = new int[9*9];
	
	//三维数组，最后一维用来存放每一个单元格已经不可用的数字的数组
	private int used[][][]=new int[9][9][];
	
	//计数
	private long startTime = 0L;
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	static List<String> initStr;
	static InitUtils initutils;
	
	public Game() {
		
	}
	public void init(Context context)
	{
		Random r = new Random();
		int i = r.nextInt(9);
		initutils = new InitUtils();
		initStr = initutils.getInitStr(context);
		String str= initStr.get(i);
		System.out.println("----->"+str);
		
		shudu = fromString(str);
		getAllUsedTiles();
		markPosition();
	}

	public int[] fromString(String str)
	{
		int[] init = new int[str.length()];
		for(int i=0;i<str.length();i++)
		{
			init[i]=str.charAt(i)-'0';
		}
		return init;
	}
	private int getTile(int x,int y)
	{
		return shudu[9*y + x]; //字符串横向排列
	}
	//标记初始数字的位置
	public void markPosition()
	{
		int count=0;
		for(int i:shudu)
		{
			if(i!=0)
				count++;
		}
		position = new int[count];
		int index=0;
		for(int i=0;i<shudu.length;i++)
		{
			if(shudu[i]!=0)
				position[index++]=i;
		}
	}
	//根据九宫格坐标判断是否是初始的数字位置,true表示不是
	public boolean isThePosition(int x,int y)
	{
		int temp = 9*y + x;
		for(int value:position)
		{
			if(value==temp)
				return false;
		}
		return true;
	}	
	
	//根据九宫格坐标得到该位置初始的数字
	public String getTileString(int x,int y)
	{
		int v = getTile(x,y);
		if(v==0)
		{
			return "";
		}else
			return String.valueOf(v);
	}
	//计算获得所有九宫格的不可用数字
	public void getAllUsedTiles()
	{
		for(int i=0;i<9;i++)
		{
			for(int j=0;j<9;j++)
			{
				used[i][j]=getUsedTiles(i,j);//用一个一维数组存放每一个单元格中已经不能填写的数字
			}
		}
	}
	//根据九宫格坐标得到当前九宫格内哪些数字不可用
	public int[] getUsedTilesCoor(int x,int y)
	{
		return used[x][y];
	}
	//根据九宫格坐标得到该位置不可以用哪些数字
	public int[] getUsedTiles(int x,int y)
	{
		int usedtemp[]=new int[9];//下标0-8
		//y方向,x坐标相同
		for(int i=0;i<9;i++)
		{
//			if(i==y)
//				continue;
			int temp = getTile(x,i);
			if(temp!=0)
				usedtemp[temp-1] = temp;		
		}
		//x方向，y坐标相同
		for(int i = 0;i<9;i++)
		{
//			if(i==x)
//				continue;
			int temp = getTile(i,y);
			if(temp!=0)
				usedtemp[temp-1] = temp;
		}
		//小九宫格内
		//每一个小九宫格的起始九宫格坐标
		int startx=(x/3)*3;
		int starty=(y/3)*3;
		for(int i=startx;i<startx+3;i++)
		{
			for(int j=starty;j<starty+3;j++)
			{
//				if(i==x&&j==y)
//					continue;
				int temp=getTile(i,j);
				if(temp!=0)
					usedtemp[temp-1]=temp;
			}
		}
		//计算出有多少个已经用过的数字
		int index=0;
		for(int i:usedtemp)
		{
			if(i!=0)
				index++;
		}
		
		int[] usedto = new int[index];
		int tempindex=0;
		for(int i=0;i<9;i++)
		{
			if(usedtemp[i]!=0)
				usedto[tempindex++]=usedtemp[i];
		}
		return usedto;
	}
	//根据九宫格坐标和填写数字更新shudu数组
	public boolean updateShudu(int x,int y,int value)
	{
		//判断所选数字是否是不可以用的数字（前面已经过滤了，这里再做判断有点重复）
		int temp[] = getUsedTiles(x, y);//获得该单元格已经用过的数字
		if(value!=0){
			for(int tile:temp)
			{
				if(tile == value)
					return false;
			}
		}
		setValue(x, y, value);
		getAllUsedTiles();
		return true;
	}
	public void updateShuduValue(int x,int y,int value)
	{
		setValue(x, y, value);
		getAllUsedTiles();
	}
	public void setValue(int x,int y,int value)
	{
		shudu[9*y + x]=value;//将当前位置设置为指定的值
	}
	
	public void startTimer()
	{
		startTime = SystemClock.uptimeMillis();
	}
	public String stopTimer()
	{
		timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
		updatedTime = timeSwapBuff + timeInMilliseconds;
		int secs = (int) (updatedTime / 1000);
		int mins = secs / 60;
		secs = secs % 60;
		int milliseconds = (int) (updatedTime % 1000);
		
		return "用时："+"" + mins + "分:"
		+ String.format("%02d", secs) + "秒:"
		+ String.format("%03d", milliseconds);
	}
	
	//检查shudu数组中是否全部有数字，true表示用户完成了数独
	public boolean isFinished()
	{
		for(int value : shudu)
		{
			if(value==0)
				return false;
		}
		return true;
	}
}
