package ervin.android.shudu;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity {

	private Game game;
	private InitUtils init;
	private MyView view;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.fragment_main);
		init = new InitUtils();
		init.CopyFile(this);		
		view = new MyView(this);
		
		setContentView(view);
		
		game = new Game();

	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if(id==R.id.action_settings)
		{
			LayoutInflater li = LayoutInflater.from(this);
			View view = li.inflate(R.layout.help_dialog, null);
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(R.string.action_settings);
			builder.setIcon(R.drawable.help);
			builder.setView(view);
			builder.setPositiveButton(android.R.string.ok, null).show();
		}
		if(id == R.id.action_change)
		{
			//main的game实例改变了position数组，但是view的game实例却得不到改变的position数组 ,因此需要把position变为静态的
			game.init(this); 
			view.invalidate(); // 重绘，调用onDraw方法
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == event.KEYCODE_BACK)
		{
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(R.string.tips);
			builder.setIcon(android.R.drawable.ic_dialog_info);
			builder.setMessage(R.string.tips_info);
			builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
					int mypid = android.os.Process.myPid();
					android.os.Process.killProcess(mypid);
				}
			});
			builder.setNegativeButton(android.R.string.cancel, null);
			builder.show();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onStart() {
		game.startTimer();
		super.onStart();
	}
	
	@Override
	protected void onDestroy() {
		String text = game.stopTimer();
		Toast.makeText(this, text, 1).show();
		super.onDestroy();
	}

}
