package ervin.android.shudu;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.text.AlteredCharSequence;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

public class MyView extends View{

	//系统屏幕高度和宽度
	private float width;
	private float height;
	//每一个单元格的宽和高
	private float cellwidth;
	private float cellheight;
	private Game game;
	
	private Context context;
	
	private int[] unused;
	private static int count=0;
	private Rect rect = null;
	
	//用户点击的九宫格坐标
	private int touchx;
	private int touchy;
	public MyView(Context context) {
		super(context);
		WindowManager wm = (WindowManager) context.getSystemService("window");
		width = wm.getDefaultDisplay().getWidth();
		height = wm.getDefaultDisplay().getHeight();
		game=new Game();
		game.init(context);
		
		this.context=context;
		
	}

	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {

		cellwidth = w / 9;
		cellheight = h / 9;
		super.onSizeChanged(w, h, oldw, oldh);
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction()!=MotionEvent.ACTION_DOWN)
			return super.onTouchEvent(event);
		// 获得用户触摸屏幕的真实屏幕坐标,并转化为九宫格坐标
		touchx = (int) (event.getX() / cellwidth);
		touchy = (int) (event.getY() / cellheight);
		System.out.println("coordinate："+touchx+","+touchy);
		
		int used[] = game.getUsedTilesCoor(touchx, touchy);
		for(int i=0;i<used.length;i++)
		{
			System.out.println(used[i]);
		}
		int temp[]=new int[]{1,2,3,4,5,6,7,8,9};
		unused=new int[9-used.length];
		int index=0;
		int flag=1;
		//根据不可用元素得到可以用的元素
		for(int i=0;i<9;i++)
		{
			for(int j=0;j<used.length;j++)
			{
				if(temp[i]!=used[j])
				{
					flag*=1;			
				}else{
					flag*=-1;
					break;
				}
			}
			if(flag>0){
				unused[index++] = temp[i];
			}
			flag=1;
		}
		//将整形数组转换为字符串数组
		String usedstr[]=new String[unused.length];
		for (int i = 0; i < unused.length; i++)
		{
			usedstr[i]=String.valueOf(unused[i]);
		}
		//判断是否点击的是起始的数字	
		if(game.isThePosition(touchx, touchy))
			selectDialog(touchx,touchy,context,usedstr);
		//test
		//game.getUsedTiles(0, 2);
		return true;		
	}
	private void selectDialog(int x,int y,Context context,String[] usednumber) {
		AlertDialog.Builder selectDialog = new Builder(context);
		selectDialog.setTitle("请选择数字");
		
		selectDialog.setSingleChoiceItems(usednumber, -1, new dialogListener(x,y));
		selectDialog.setPositiveButton("清除", new DialogInterface.OnClickListener() {		
			@Override
			//清除该单元格上的数字
			public void onClick(DialogInterface dialog, int which) {				
				game.updateShuduValue(touchx, touchy, 0);
				dialog.dismiss();
				invalidate();
			}
		});
		selectDialog.show();
	}
	public class dialogListener implements DialogInterface.OnClickListener
	{
		int coordx=0;
		int coordy=0;
		public dialogListener(int x,int y) {
			super();
			coordx = x;
			coordy = y;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			System.out.println("点击的数字是："+unused[which]);
			game.updateShuduValue(coordx, coordy, unused[which]);
			dialog.dismiss();
			//invalidate();//更新view,重绘view,重新调用onDraw方法
			
			rect= new Rect();
			rect.left = (int) (coordx*cellwidth);
			rect.top =(int) (coordy*cellheight);
			rect.right=(int) ((coordx+1)*cellwidth);
			rect.bottom = (int) ((coordy+1)*cellheight);
			invalidate(rect);//局部刷新
		}
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {

		if(game.isFinished())
		{
			String timestr = game.stopTimer();
			AlertDialog.Builder builder = new Builder(context);
			builder.setTitle("恭喜你");
			builder.setMessage("太棒了，您" + timestr);
			builder.setPositiveButton("换一组", new DialogInterface.OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});
			builder.show();
		}
		
		Paint paint = new Paint();
		//paint.setStyle(Paint.Style.STROKE); //设置画笔为空心的
		paint.setColor(getResources().getColor(R.color.shudu_background));
		canvas.drawRect(0, 0, width, height, paint);
		
		Paint lightpaint = new Paint();
		lightpaint.setColor(getResources().getColor(R.color.shudu_light));
		Paint hilitepaint = new Paint();
		hilitepaint.setColor(getResources().getColor(R.color.shudu_hilite));
		Paint darkpaint = new Paint();
		darkpaint.setColor(getResources().getColor(R.color.shude_dark));
		
		for(int i =0;i<9;i++)
		{
			canvas.drawLine(0, i*cellheight, width, i*cellheight, lightpaint);
			canvas.drawLine(0, i*cellheight+1, width, i*cellheight+1, hilitepaint);
			
			canvas.drawLine(i*cellwidth, 0, i*cellwidth, height, lightpaint);
			canvas.drawLine(i*cellwidth+1, 0, i*cellwidth+1, height, hilitepaint);
		}
		
		for(int i=0;i<9;i++)
		{
			if(i%3!=0)
			{
				continue;
			}
			canvas.drawLine(0, i*cellheight, width, i*cellheight, darkpaint);
			canvas.drawLine(0, i*cellheight+1, width, i*cellheight+1, hilitepaint);
			canvas.drawLine(i*cellwidth, 0, i*cellwidth, height, darkpaint);
			canvas.drawLine(i*cellwidth+1, 0, i*cellwidth+1, height, hilitepaint);
		}
		
		Paint numberpaint = new Paint();
		numberpaint.setColor(Color.BLACK);
		numberpaint.setTextSize(cellheight*0.75f);
		numberpaint.setTextAlign(Paint.Align.CENTER);
		numberpaint.setStyle(Paint.Style.STROKE); 
		
		Paint selectpaint = new Paint();
		selectpaint.setColor(Color.BLUE);
		selectpaint.setTextSize(cellheight*0.75f);
		selectpaint.setTextAlign(Paint.Align.CENTER);
		selectpaint.setStyle(Paint.Style.STROKE); 
		//canvas.drawText("1", 3*cellwidth+cellwidth/2, 36, numberpaint);
		
//		Paint currenttpaint = new Paint();
//		currenttpaint.setColor(Color.RED);
//		currenttpaint.setTextSize(cellheight*0.75f);
//		currenttpaint.setTextAlign(Paint.Align.CENTER);
//		currenttpaint.setStyle(Paint.Style.STROKE);
		
		FontMetrics fm = numberpaint.getFontMetrics();
		float x = cellwidth/2; //让字符在单元格内x方向上居中
		float y = cellheight/2 - (fm.ascent + fm.descent)/2;//ascent是负数，ascent比descent多,让字符在单元格内y方向上居中
		//i为宽，j为高
		//
		for(int i=0;i<9;i++)
		{
			for(int j=0;j<9;j++)
			{
				if(!game.isThePosition(i, j))
				{
					canvas.drawText(game.getTileString(i, j), i * cellwidth + x, j
						* cellheight + y, numberpaint);
				}else
					canvas.drawText(game.getTileString(i, j), i * cellwidth + x, j
							* cellheight + y, selectpaint);
			}
		}	
//		if(count!=1)
//			canvas.drawText(game.getTileString(touchx, touchy), touchx * cellwidth + x, touchy
//				* cellheight + y, currenttpaint);
//		
		super.onDraw(canvas);	
	}
}
