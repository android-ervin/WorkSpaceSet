package ervin.android.scalepic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.BitmapFactory.Options;
import android.view.WindowManager;

public class scalePicture {

	public static Bitmap getScalePic(Context context,int resid)
	{
		WindowManager wm = (WindowManager) context.getSystemService("window");
		int view_width = wm.getDefaultDisplay().getWidth();
		int view_height = wm.getDefaultDisplay().getHeight();
		System.out.println("系统宽度："+view_width+",系统高度："+view_height);
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resid);
		System.out.println("图片宽度："+bitmap.getWidth()+",图片高度："+bitmap.getHeight());
		Bitmap scalebitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
		Canvas canvas = new Canvas(scalebitmap);
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		Matrix matrix = new Matrix();
		//matrix.setScale(bitmap.getWidth()/view_width, bitmap.getHeight()/view_height);
		matrix.setScale(1.0f, 1.0f);
		canvas.drawBitmap(bitmap, matrix, paint);
		System.out.println("缩放后图片宽度："+scalebitmap.getWidth()+",缩放后图片高度："+scalebitmap.getHeight());
		return scalebitmap ;
		
//		//获得图片的详细信息的类 option
//				BitmapFactory.Options opts = new Options();
//				opts.inJustDecodeBounds = true; //设置为true时就就不会把原图直接加载到内存中
//				Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resid, opts);
//				//获得图片的真实宽高
//				int imageHeight = opts.outHeight;
//				int imageWidth =opts.outWidth;
//				//获得真实手机屏幕宽高
//				WindowManager wm = (WindowManager) context.getSystemService("window") ;
//				int screenHeight = wm.getDefaultDisplay().getHeight();
//				int screenWidth = wm.getDefaultDisplay().getWidth();
//				
//				//计算缩放比例
//				if(imageHeight > screenHeight && imageWidth > screenWidth)
//				{	
//					int scaleX = imageHeight / screenHeight ;
//					int scaleY = imageWidth / screenWidth;
//					if(scaleX > scaleY)
//					{
//						opts.inSampleSize = scaleX ;
//					}else
//						opts.inSampleSize = scaleY ;
//				}	
//				opts.inJustDecodeBounds = false ;
//				Bitmap scalebitmap = BitmapFactory.decodeResource(context.getResources(), resid, opts);
//				System.out.println("缩放后图片宽度："+scalebitmap.getWidth()+",缩放后图片高度："+scalebitmap.getHeight());
//				return scalebitmap ;
	}
}
