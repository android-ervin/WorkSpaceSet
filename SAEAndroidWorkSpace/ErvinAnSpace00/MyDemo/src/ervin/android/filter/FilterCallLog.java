package ervin.android.filter;

import ervin.android.mydemo.R;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

public class FilterCallLog extends Activity {

	private EditText et_input ;
	private ListView log_list ;
	private myAdapter madapter ;
	
	private final static String[] projection = new String[]{Calls._ID,Calls.NUMBER,Calls.DATE,Calls.TYPE};
	private final static int NUMBER=1;
	private final static int DATE=2;
	private final static int TYPE=3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter);
		
		et_input = (EditText) findViewById(R.id.tv_input);
		log_list = (ListView) findViewById(R.id.calllog_list);
		
		madapter = new myAdapter(this, null);
		log_list.setAdapter(madapter);
		et_input.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				//当text改变的时候开始过滤
				Filter filter = madapter.getFilter();
				//filter.filter(constraint, listener);因为listview继承了AbsListView 
				//而AbsListView 又实现了Filter.FilterListener，所以可以直接写listview的对象
				filter.filter(s.toString(), log_list);
				
			}
		});
	}

	public class myAdapter extends CursorAdapter
	{

		public myAdapter(Context context, Cursor c) {
			super(context, c);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = getLayoutInflater().inflate(R.layout.filter_list, null);
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TextView tv_number = (TextView) view.findViewById(R.id.tv_number);
			TextView tv_date = (TextView) view.findViewById(R.id.tv_date);
			TextView tv_type = (TextView) view.findViewById(R.id.tv_type);
			
			String number = cursor.getString(NUMBER);
			long date = cursor.getLong(DATE);
			int type = cursor.getInt(TYPE);
			
			tv_number.setText(number);
			String date_str = DateFormat.getDateFormat(context).format(date);
			tv_date.setText(date_str);
			
			switch (type) {
			case Calls.INCOMING_TYPE:
				tv_type.setText("incoming");
				break;
			case Calls.MISSED_TYPE:
				tv_type.setText("missed");
				break;
			case Calls.OUTGOING_TYPE:
				tv_type.setText("outgoing");
				break;
			default:
				break;
			}
			
		}

		@Override
		public Cursor runQueryOnBackgroundThread(CharSequence constraint) {

			if(TextUtils.isEmpty(constraint))
			{
				return null;
			}else
			{
				Uri uri = Calls.CONTENT_URI;
				String selection = Calls.NUMBER + " like '%" + constraint +"%'";
				Cursor cursor = getContentResolver().query(uri, projection, selection, null, Calls.DATE + " desc");
				return cursor;
			}
		}		
	}
}
