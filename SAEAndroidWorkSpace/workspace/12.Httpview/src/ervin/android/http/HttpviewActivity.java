package ervin.android.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class HttpviewActivity extends Activity {
    /** Called when the activity is first created. */
	
	String imageUrl ="http://10.0.2.2:80/pin.png";//"http://pics.htwed.com/2014/02/11/52f97e395286c.jpg!l";// "http://i.pbase.com/o6/92/229792/1/80199697.uAs58yHk.50pxCross_of_the_Knights_Templar_svg.png";  
	Bitmap bmImg;  
	ImageView imView;  
	private ProgressDialog dialog=null;    
	Button button1;  
	Button btn2;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        imView = (ImageView) findViewById(R.id.imageView1);  
        
        dialog=new ProgressDialog(this);
        dialog.setTitle("提示");
        dialog.setMessage("正在下载，请稍后...");
        dialog.setCancelable(false);//不显示
        button1=(Button)findViewById(R.id.button1);
        btn2=(Button)findViewById(R.id.button2);
        button1.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				new myThread().start();
				dialog.show();
			}
		});
        
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				Intent intent=new Intent(HttpviewActivity.this,Mywebview.class);
				startActivity(intent);
			}
		});
    }
    Handler myhandHandler=new Handler()
    {

		@Override
		public void handleMessage(Message msg)
		{
			// TODO Auto-generated method stub
			Bitmap bm=(Bitmap) msg.obj;
			imView.setImageBitmap(bm);  
			if(msg.what==1)
			{
				dialog.dismiss();
				button1.setVisibility(View.INVISIBLE);
			}
			
			super.handleMessage(msg);
		}
    	
    };
    class myThread extends Thread
    {

		@Override
		public void run()
		{
			// TODO Auto-generated method stub
			URL myFileUrl =null;
			try
			{
				myFileUrl =new URL(imageUrl);
			} catch (MalformedURLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {  
	    		 HttpURLConnection conn = (HttpURLConnection) myFileUrl  
	    		 	.openConnection();  
	    		 //conn.setDoInput(true);  
	    		 //conn.connect();  
	    		 InputStream is = conn.getInputStream();  
	    		 Bitmap bitmap = BitmapFactory.decodeStream(is);  
	    		 is.close();  
	    		 
	    		 Message msg=myhandHandler.obtainMessage();
	    		 msg.obj=bitmap;
	    		 msg.what=1;
	    		 myhandHandler.sendMessage(msg);
	    	 } 
	    	 catch (IOException e) {  
	    		 e.printStackTrace();  
	    	 }  
			super.run();
		}
    	
    }
    
}