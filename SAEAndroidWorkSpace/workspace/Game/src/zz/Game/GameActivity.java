package zz.Game;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;

public class GameActivity extends Activity {
    /** Called when the activity is first created. */
    public static int i=0;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        FrameLayout frameLayout =(FrameLayout)findViewById(R.id.mylayout);
        final Zzview zzview  =new Zzview(this);
        
        zzview.setOnTouchListener(new OnTouchListener() {

			
			public boolean onTouch(View v, MotionEvent event) {
				zzview.x = event.getX(); // 设置小兔子显示位置的X坐标
				zzview.y = event.getY(); // 设置小兔子显示位置的Y坐标
				zzview.invalidate(); // 重绘zzview组件
				return true;
			}
		});
		frameLayout.addView(zzview); // 将zzview添加到布局管理器中
		System.out.println("第" + i++ +"次调用");
	}
        
    
}