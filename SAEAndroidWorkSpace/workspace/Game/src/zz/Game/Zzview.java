package zz.Game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class Zzview extends View
{
	public float x ; //X坐标
	public float y ;//Y坐标
	public Zzview(Context context)//重写构造函数
	{
		super(context);
		// TODO Auto-generated constructor stub
		x=29;
		y=130;
		
	}
	@Override
	public void draw(Canvas canvas)//重写父类View的draw方法
	{
		// TODO Auto-generated method stub
		super.draw(canvas);
		
		Paint paint =new Paint(); //实例化一个Paint对象
		Bitmap bitmap =BitmapFactory.decodeResource(this.getResources(), R.drawable.rabbit);
		
		canvas.drawBitmap(bitmap, x, y, paint); // 绘制小兔子
		if (bitmap.isRecycled()) { // 判断图片是否回收
			bitmap.recycle(); // 强制回收图片
		}
	}
	
	
	
}
