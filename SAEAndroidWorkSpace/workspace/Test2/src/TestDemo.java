class Person
{
	public void fun1()
	{
		System.out.println("F.function1");
	}
	public void fun2()
	{
		System.out.println("F.function2");
	}
}
class student extends Person
{
	public void fun1()
	{
		System.out.println("S.function3");
	}
	public void fun3()
	{
		System.out.println("S.function4");
	}
}
public class TestDemo
{

	public static void main(String[] args)
	{
		
		Person p=new student();   //调用子类中复写父类的方法
//		Person p=new Person(); //调用父类中的方法
		//student p=new student(); //调用子类中的方法
		
		student s=(student)p;//进过子类实例化后的父类对象才可以强制转换为这个子类的类型
		p.fun1();
		p.fun2();
		s.fun1();
		s.fun2();
	}

}
