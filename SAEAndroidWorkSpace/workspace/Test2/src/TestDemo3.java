
public class TestDemo3
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		A a=new B();
		a.functionA1();//虽然a是由B类来实例化的，但是a的引用类是A类，而A类中并没有functionB1方法
		
		
		D d =null;
		
		C c2=d.functionD();
		C c=a.functionA2();
	}

}
class A 
{
	public void functionA1()
	{
		System.out.println("this is classA's functionA1");
	}
	public C functionA2()
	{
		return null;
	}
}
class B extends A
{
	public void functionB1()
	{
		System.out.println("this is classB's functionB1");
	}
}

interface C
{
	public abstract void functionC();
}
interface D
{
	public abstract C functionD();
}
//class E implements D
//{
//
//	@Override
//	public C functionD()
//	{
//		// TODO Auto-generated method stub
//		return null;
//	}
//}