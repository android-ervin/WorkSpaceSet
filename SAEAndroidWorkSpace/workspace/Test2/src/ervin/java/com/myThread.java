package ervin.java.com;

public class myThread
{

	public static void main(String[] args)
	{
		//ThreadDemo td=new ThreadDemo();
		//td.run();
		//td.start();//启动线程
	//	new ThreadDemo().run();
		 
		ThreadOne t=new ThreadOne();
		new Thread(t).start();
		for(int i=0;i<30;i++)
		{
			System.out.println("执行main线程");
		}
			
	}

}
/*第一种开启多线程的方法：继承Thread类，覆写run方法，在主进程中new一个该类的对象后启动线程（使用start方法）*/
class ThreadDemo extends Thread
{
	public void run()
	{
		for(int i=0;i<30;i++)
		{
			System.out.println("执行ThreadDemo线程");
		}
	}
}

/*第二种开启多线程的方法：实现Runnable接口。实现该接口中的抽象方法run,在主进程中new一个该对象，
 *然后调用Thread类中的Thread（Runnable target）构造函数，将生成的对象传入到该构造函数的参数中后，启动线程 （使用start方法）*/
class ThreadOne implements Runnable
{

	@Override
	public void run()
	{
		
		for(int i=0;i<30;i++)
		{
			System.out.println("执行ThreadDemo线程");
		}
	}
}