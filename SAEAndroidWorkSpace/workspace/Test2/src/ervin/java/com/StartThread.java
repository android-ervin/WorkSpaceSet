package ervin.java.com;

public class StartThread extends Thread
{

	public static void main(String[] args)
	{
		/*实例化一个线程对象*/
		StartThread myThread=new StartThread();
		myThread.setName("myThread");
		System.out.println("该线程是否启动："+myThread.isAlive());
		myThread.start();                                        //去执行复写的run方法
		System.out.println("该线程是否启动："+myThread.isAlive());
		
		/*实例化一个接口线程对象,后台线程开启后执行run方法（死循环），会随着主线程结束而被终止掉*/
		BackThread bThread=new BackThread();
		Thread thread=new Thread(bThread); 
		thread.setName("BackThread");
		thread.setDaemon(true);                                  //将该线程设置为后台线程
		thread.start();
		
		for(int i=0;i<5;i++)
			System.out.println("this is Thread:"+currentThread().getName());
	}

	@Override
	public void run()
	{
		for(int i=0;i<5;i++)
			System.out.println("this is Thread:"+currentThread().getName());
	}
	

}
class BackThread implements Runnable
{

	@Override
	public void run()
	{
		while(true)
			System.out.println("this is Thread:"+Thread.currentThread().getName());
		
	}
	
}