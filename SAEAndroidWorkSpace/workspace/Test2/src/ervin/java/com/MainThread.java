package ervin.java.com;

public class MainThread
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Thread t=Thread.currentThread();//主线程不用去start(),会报错
//		t.start();
		System.out.println("current thread is:"+Thread.currentThread().getName());
		System.out.println("1:current thread whether interrupt:"+t.isInterrupted());
		
		t.interrupt();
		System.out.println("2:current thread whether interrupt:"+t.isInterrupted());
		
		try
		{
			/*让线程开始休眠，但是此时线程已经被中断，所以会抛出中断异常。*/
			Thread.sleep(2000);//sleep为父类Thread的静态方法，所以直接用父类名称去调用静态方法（同CurrentThread）
			System.out.println("线程没有中断");
		} catch (InterruptedException e)
		{
			// TODO: handle exception
			System.out.println("线程已经中断");
		}
		/*因为sleep抛出了异常，所以它清除了中断标志位*/
		System.out.println("3:current thread whether interrupt:"+t.isInterrupted());
	}

}
