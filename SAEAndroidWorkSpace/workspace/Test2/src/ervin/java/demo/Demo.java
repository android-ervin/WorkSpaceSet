package ervin.java.demo;

public class Demo
{	
	/*字符串改变了，但是字符数组没变？？，可以监控
	 * 一个是值类型，一个是对象类型，字符串是引用类型，简单数据类型是值类型*/
	String str=new String("good");
	char[]ch={'a','b','c'};  
	public static void main(String args[]){  
		Demo ex=new Demo();  
	    ex.change(ex.str,ex.ch);  
	    System.out.print(ex.str+" and ");  
	    System.out.print(ex.ch);  
	}     
	/*值传递，还是引用传递，值传递不会改变原始值，引用传递会改变原来的值*/
	//实际运行结果好像是str是值传递，而数组ch是引用传递,但是资料上写的是String和数组都是引用类型？查查String类型就明白了
	public void change(String str,char ch1[]){  
	    str="test ok";  
	    ch1[0]='g';  
	}  
}
