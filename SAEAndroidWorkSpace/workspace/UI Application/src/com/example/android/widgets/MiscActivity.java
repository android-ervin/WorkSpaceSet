package com.example.android.widgets;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MiscActivity extends Activity implements RatingBar.OnRatingBarChangeListener  {
//RatingBar.OnRatingBarChangeListener  SeekBar.OnSeekBarChangeListener 

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.misc_activity);
		
		TextView mTextView01 = (TextView)findViewById(R.id.TextView01);
		TextView mTextView02 = (TextView)findViewById(R.id.TextView02);
		RatingBar mRatingBar01 = (RatingBar)findViewById(R.id.RatingBar01);
		SeekBar mSeekBar01 = (SeekBar)findViewById(R.id.SeekBar01);
	
		mTextView01.setText("Use Rating Bar to express: \n luminance=" + mRatingBar01.getProgress());
		
		mTextView02.setText("Use Seek Bar to express: \n volume=" + mSeekBar01.getProgress());

		mRatingBar01.setOnRatingBarChangeListener(this);
	
		mSeekBar01.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
		
			public void onProgressChanged(SeekBar mSeekBar01, int progress, boolean fromTouch) {
				final TextView mTextView02 = (TextView)findViewById(R.id.TextView02);
				mTextView02.setText("Use Seek Bar to express: \n volume=" + mSeekBar01.getProgress());
			}
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
		});		
	}

	public void onRatingChanged(RatingBar mRatingBar01, float rating, boolean fromTouch) {
		final TextView mTextView01 = (TextView)findViewById(R.id.TextView01);
		mTextView01.setText("Use Rating Bar to express: \n luminance=" + mRatingBar01.getProgress());
	}
}

