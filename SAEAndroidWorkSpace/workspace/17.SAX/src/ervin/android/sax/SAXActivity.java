package ervin.android.sax;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class SAXActivity extends Activity {
    /** Called when the activity is first created. */
	Button DownloadBtn=null;
	Button PraseBtn=null;
	static String XMLStr="";
	static String Path="http://10.0.2.2:81//Workers1.xml";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //从网上下载XML文件
        DownloadBtn=(Button)findViewById(R.id.button1);
        DownloadBtn.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				new myThread().start();	
			}
		});
        //开始解析XML文件
        PraseBtn=(Button)findViewById(R.id.button2);
        PraseBtn.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				try
				{
					SAXParserFactory factory=SAXParserFactory.newInstance();
					XMLReader reader=factory.newSAXParser().getXMLReader();
					/***以上为解析XML固定写法**/
					reader.setContentHandler(new SAXPrase());//为reader对象设置内容处理器
					reader.parse(new InputSource(new StringReader(XMLStr)));//开始解析XML
					
				} catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
			}
		});
        
    }
    myHandler handler=new myHandler();
    class myThread extends Thread
    {
    	URL url=null;
    	String ResultStr="";
    	String LineStr="";
    	@Override
    	public void run()
    	{
    		// TODO Auto-generated method stub
    		super.run();
    		try
			{
				url=new URL(Path);
			} catch (Exception e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		Message msg=handler.obtainMessage();
    		try
    		{
    			HttpURLConnection conn=(HttpURLConnection)url.openConnection();
    			InputStream inputStream=conn.getInputStream();
    			BufferedReader bReader=new BufferedReader(new InputStreamReader(inputStream));
    			while((LineStr=bReader.readLine())!=null)
    			{
    				ResultStr+=LineStr;
    			}
    			inputStream.close();
    			msg.obj=ResultStr;
    			handler.sendMessage(msg);
    			
    		} catch (IOException e)
    		{
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}	
    }
    class myHandler extends Handler
    {

    	@Override
    	public void handleMessage(Message msg)
    	{
    		// TODO Auto-generated method stub
    		super.handleMessage(msg);
    		XMLStr=(String) msg.obj;
			System.out.println("----->"+XMLStr);
    	}	
    }

}