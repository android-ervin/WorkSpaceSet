package ervin.android.tabs;



import ervin.android.ui.R;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class Tabs extends TabActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		TabHost tabHost=getTabHost();
		Intent tab1intent=new Intent(this,ItemList.class);
		
		TabHost.TabSpec spec1=tabHost.newTabSpec("TabOne");
		Resources res=getResources();
		spec1.setIndicator("TabOne", res.getDrawable(android.R.drawable.stat_sys_download));
		spec1.setContent(tab1intent);
		
		tabHost.addTab(spec1);
	}
}
