package ervin.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MyUIActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn1;
	private ImageView image;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab1);
        image=(ImageView)findViewById(R.id.imageView1);
        btn1=(Button)findViewById(R.id.button1);
        /**
         * @text document
         * @author acer
         * */
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				image.setImageResource(R.drawable.sakura01);
			}
		});
    }
}