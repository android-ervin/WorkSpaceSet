package com.gaosheng.foodandroid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.gaosheng.data.remoteData;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class rwtsform extends Activity {

	String ssqbh="",zfrybh="",zfryxm="",zfrybhid="",password="";
	ListView listview;
	Button btnout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView(R.layout.rwtsform);
        
        Bundle bundle=this.getIntent().getExtras();
        ssqbh=bundle.getString("ssqbh");
        zfrybh=bundle.getString("zfrybh");
        zfrybhid=bundle.getString("zfrybhid");
        zfryxm=bundle.getString("zfryxm");
        password=bundle.getString("password");
        this.setTitle(this.getTitle()+"--"+zfryxm);
        
        
        listview=(ListView)findViewById(R.id.listView1);
        btnout=(Button)findViewById(R.id.btnrwtsout);
        btnout.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
        String data=remoteData.Rwts(zfrybhid, password);
        if (data.indexOf("|")!=-1)
        {  
        	String[] rwtsarr=data.split("=");
             List<HashMap<String, Object>> rwts = new ArrayList<HashMap<String,Object>>();   
             for(int i=0;i<=rwtsarr.length-1;i++)
             {   
                 HashMap<String, Object> item = new HashMap<String, Object>(); 
                 String[] rwtsitem=rwtsarr[i].split("\\|");
                 item.put("title", rwtsitem[0]);   
                 item.put("memo", rwtsitem[1]);   
                 item.put("memodate",rwtsitem[2]);   

                 rwts.add(item);   
             }   
             
            SimpleAdapter adapter = new SimpleAdapter(this, rwts,R.layout.rwtslistview,    
                     new String[]{"title", "memo", "memodate"}, new int[]{R.id.rwtstextview1,R.id.rwtstextview2,R.id.rwtstextview3});   
          
            listview.setAdapter(adapter);   


        }
        
        
        

    }
}

