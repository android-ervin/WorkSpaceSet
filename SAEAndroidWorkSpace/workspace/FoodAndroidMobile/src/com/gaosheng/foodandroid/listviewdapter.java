package com.gaosheng.foodandroid;

import java.util.ArrayList;
import java.util.Map;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class listviewdapter extends BaseAdapter  {
    Context context;
    ArrayList<Map<String,Object>> listData;
    public listviewdapter(Context context,ArrayList<Map<String,Object>> listData){
    	this.context=context;
    	this.listData=listData;
    }
	public int getCount() {
	    
		return listData.size();
	}

	public Object getItem(int position) {
		return listData.get(position);
	}
	public void dataChanged()
	{
	    this.notifyDataSetChanged(); 
	}

	public long getItemId(int position) {
		return position;
	}


	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	LayoutInflater mInflater=LayoutInflater.from(context);
	convertView=mInflater.inflate(R.layout.content,null);
	TextView textview1=(TextView)convertView.findViewById(R.id.textViewsign);
	TextView textview2=(TextView)convertView.findViewById(R.id.textViewname);
	CheckBox textview3 = (CheckBox) convertView.findViewById(R.id.checkBox1);
	textview3.setOnClickListener(new OnClickListener() {
        

        public void onClick(View v) {
            CheckBox checkBox = (CheckBox) v;
            int position = (Integer) checkBox.getTag();
            for(int i = 0; i < listData.size();i++)
            {
               if(i== position)
               {
                   listData.get(i).put("check", true);   
               }
               else 
               {
                   listData.get(i).put("check", false);
               }
               dataChanged();
            }
            
        }
    });
	textview3.setTag(position);
	
	textview1.setText((String)listData.get(position).get("rgin"));
	textview2.setText((String)listData.get(position).get("name"));
	textview3.setChecked((Boolean)listData.get(position).get("check"));
	return convertView;
	}
	

}
