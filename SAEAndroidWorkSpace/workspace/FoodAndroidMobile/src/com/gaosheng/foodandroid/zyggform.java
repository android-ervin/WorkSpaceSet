package com.gaosheng.foodandroid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.gaosheng.data.remoteData;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;


public class zyggform extends Activity {
	String ssqbh="",zfrybh="",zfryxm="",zfrybhid="",password="";
	ListView listview;
	Button btnout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView(R.layout.rwtsform);
        
        Bundle bundle=this.getIntent().getExtras();
        ssqbh=bundle.getString("ssqbh");
        zfrybh=bundle.getString("zfrybh");
        zfrybhid=bundle.getString("zfrybhid");
        zfryxm=bundle.getString("zfryxm");
        password=bundle.getString("password");
        this.setTitle(this.getTitle()+"--"+zfryxm);
        
        
        listview=(ListView)findViewById(R.id.listView1);
        btnout=(Button)findViewById(R.id.btnrwtsout);
        btnout.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
        String data=remoteData.Zygg(zfrybhid, password,ssqbh);
        if (data.indexOf("|")!=-1)
        {  
        	String[] zyggarr=data.split("=");
             List<HashMap<String, Object>> zygg = new ArrayList<HashMap<String,Object>>();   
             for(int i=0;i<=zyggarr.length-1;i++)
             {   
                 HashMap<String, Object> item = new HashMap<String, Object>(); 
                 String[] zyggitem=zyggarr[i].split("\\|");
                 item.put("title", zyggitem[0]);   
                 item.put("memo", zyggitem[1]);   
                 item.put("memodate",zyggitem[2]);   

                 zygg.add(item);   
             }   
            //创建SimpleAdapter适配器将数据绑定到item显示控件上   
            SimpleAdapter adapter = new SimpleAdapter(this, zygg,R.layout.rwtslistview,    
                     new String[]{"title", "memo", "memodate"}, new int[]{R.id.rwtstextview1,R.id.rwtstextview2,R.id.rwtstextview3});   
            //实现列表的显示   
            listview.setAdapter(adapter);   

}
    }
}
