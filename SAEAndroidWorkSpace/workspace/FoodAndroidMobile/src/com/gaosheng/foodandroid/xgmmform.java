package com.gaosheng.foodandroid;

import com.gaosheng.data.remoteData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class xgmmform extends Activity {
	EditText passwordedit1,passwordedit2;

	Button btnpassword,btnpasswordreturn;
	String ssqbh="",zfrybh="",zfryxm="",zfrybhid="",password="";
	Intent intent;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView(R.layout.modifypasswordform);
        intent=this.getIntent();
        Bundle bundle=this.getIntent().getExtras();
        ssqbh=bundle.getString("ssqbh");
        zfrybh=bundle.getString("zfrybh");
        zfrybhid=bundle.getString("zfrybhid");
        zfryxm=bundle.getString("zfryxm");
        password=bundle.getString("password");
        this.setTitle(this.getTitle()+"--"+zfryxm);

        passwordedit1=(EditText)findViewById(R.id.passwordeditText1);
        passwordedit2=(EditText)findViewById(R.id.passwordeditText2);
        btnpassword=(Button)findViewById(R.id.btnpassword);
        btnpasswordreturn=(Button)findViewById(R.id.btnpasswordreturn);
        btnpasswordreturn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				xgmmform.this.setResult(RESULT_CANCELED,intent);
				finish();
			}
		});
        btnpassword.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String newpass1,newpass2;
				newpass1=passwordedit1.getText().toString();
				newpass2=passwordedit2.getText().toString();
				if (newpass1.equals(""))
				{
					remoteData.ShowDialog("请输入新密码", xgmmform.this);
					return;
				}
				if (!newpass1.equals(newpass2))
				{
					remoteData.ShowDialog("密码确认错误", xgmmform.this);
					return;
				}
				String result=remoteData.ModifyPassword(zfrybhid, password, newpass1);
				remoteData.ShowDialog("修改密码成功！正在退出系统", xgmmform.this);
				xgmmform.this.setResult(RESULT_OK,intent);
				finish();
				
			}
		});
    }
}
