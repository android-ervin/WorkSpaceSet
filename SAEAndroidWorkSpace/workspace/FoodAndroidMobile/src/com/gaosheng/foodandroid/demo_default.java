package com.gaosheng.foodandroid;



import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class demo_default extends Activity  {
	String ssqbh="",zfrybh="",zfryxm="",zfrybhid="",password="";
    //声明TabHost对象  
    TabHost mTabHost; 
    /** Called when the activity is first created. */
  
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_page);
        Bundle bundle=this.getIntent().getExtras();
        ssqbh=bundle.getString("ssqbh");
        zfrybh=bundle.getString("zfrybh");
        zfrybhid=bundle.getString("zfrybhid");
        zfryxm=bundle.getString("zfryxm");
        password=bundle.getString("password");
        mTabHost = (TabHost) findViewById(R.id.tabhost);;  
        mTabHost.setup();
        mTabHost.addTab(mTabHost.newTabSpec("tab_test1")
                .setIndicator("基本项")
                .setContent(R.id.layout_tab1));  
        mTabHost.addTab(mTabHost.newTabSpec("tab_test2")
                .setIndicator("其他项")
                .setContent(R.id.layout_tab2));  
    
        for(int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) 
        { 
        	mTabHost.getTabWidget().getChildAt(i).getLayoutParams().height = 50;
        	
        }
        TextView test=(TextView)findViewById(R.id.textViewname);
     
        mTabHost.setCurrentTab(0);  
        Button but=(Button)findViewById(R.id.buttonselect);
        but.setOnClickListener(myAdapterBtnListener);
        
    }
	private View.OnClickListener myAdapterBtnListener = new View.OnClickListener() {


		public void onClick(View v) {
			Log.v("ssss", "myAdapter");
			Bundle bundle=new Bundle();
	    	bundle.putString("ssqbh", ssqbh);
			bundle.putString("zfrybh", zfrybh);
			bundle.putString("password", password);
			bundle.putString("zfrybhid", zfrybhid);
			bundle.putString("zfryxm", zfryxm);
	    	Intent intent=new Intent();
	    	intent.putExtras(bundle);
			intent.setClass(demo_default.this, ssss.class);
			startActivity(intent);
		}
	};
	
}
