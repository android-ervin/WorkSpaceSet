package com.gaosheng.foodandroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class imageAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater mInflater;
/*	static class ViewHolder
	{
	   ImageView image;
	   TextView textview;
	}*/
	private int[] imagearr={R.drawable.rwts,R.drawable.zygg,R.drawable.tjjcx,R.drawable.rcjg,
			                R.drawable.zdhd,R.drawable.xgmm,R.drawable.rjgx,R.drawable.tcxt};
	private String[] strarr={"任务提示","重要公告","添加剂查询","日常监管","重大活动","修改密码","软件更新","退出系统"};
	private String value;
	public imageAdapter(Context c,String str)
	{
		context=c;
		value=str;
		mInflater=LayoutInflater.from(c);
	}
	public int getCount() {
		// TODO Auto-generated method stub
		return imagearr.length;
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}


	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView imageview;
		TextView textview;
		if (convertView==null)
		{
			convertView=mInflater.inflate(R.layout.maingridviewitem, null);
		//	convertView.setLayoutParams(new GridView.LayoutParams(120,120));
			imageview=(ImageView)convertView.findViewById(R.id.gridviewimageView1);
			textview=(TextView)convertView.findViewById(R.id.gridviewtextView1);
			textview.setTextColor(Color.BLACK);
	/*		imageview.setLayoutParams(new GridView.LayoutParams(180,180));
			imageview.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageview.setPadding(20,20,20,20);*/
		}
		else
		{
			imageview=(ImageView)convertView.findViewById(R.id.gridviewimageView1);
			textview=(TextView)convertView.findViewById(R.id.gridviewtextView1);
		}
		imageview.setImageResource(imagearr[position]);
		textview.setText(strarr[position]);

		return convertView;
	}
	public   void messagebox(String message)
	{
		AlertDialog.Builder builder=new AlertDialog.Builder(context);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		AlertDialog alert=builder.create();
		alert.show();
	}	
}


