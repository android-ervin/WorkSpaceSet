package com.gaosheng.foodandroid;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.client.ClientProtocolException;

import com.gaosheng.foodandroid.R;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class rjgxform extends Activity {
    ProgressBar pb;
    TextView tv;
    int   fileSize;
    int   downLoadFileSize;
    Button btnrjgx,btnrjgxreturn;
	TextView textviewrjgx;
    String fileEx,fileNa,filename;
    String urlStr="http://www.gfsms.cn/food/FoodAndroidMobile.apk";   
    String fileName="FoodAndroidMobile.apk"; 
    String SDCard; 
    Thread thread;
    private Handler handler = new Handler()
      {
        @Override
        public void handleMessage(Message msg)
        {//定义一个Handler，用于处理下载线程与UI间通讯
          if (!Thread.currentThread().isInterrupted())
          {
            switch (msg.what)
            {
              case 0:
                pb.setMax(fileSize);
              case 1:
                pb.setProgress(downLoadFileSize);
                int result = downLoadFileSize * 100 / fileSize;
                tv.setText("下载进度："+result + "%");
                break;
              case 2:
                Toast.makeText(rjgxform.this, "文件下载完成", 1).show();
                break;
 
              case -1:
                String error = msg.getData().getString("error");
                Toast.makeText(rjgxform.this, error, 1).show();
                break;
            }
          }
          super.handleMessage(msg);
        }
      };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView(R.layout.rjgxform);
        pb=(ProgressBar)findViewById(R.id.pbrjgx1);
        tv=(TextView)findViewById(R.id.textviewrjgx1);
        SDCard=Environment.getExternalStorageDirectory()+"/";      
        btnrjgx=(Button)findViewById(R.id.btnrjgx);
        btnrjgxreturn=(Button)findViewById(R.id.btnrjgxreturn);
        btnrjgx.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				thread.start();
                
				
			}
		});
        btnrjgxreturn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rjgxform.this.setResult(RESULT_CANCELED);
				finish();
				
			}
		});
        thread=new Thread(){
            public void run(){
                try {
                    down_file(urlStr,SDCard);
                    //下载文件，参数：第一个URL，第二个存放路径
                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                File file1 = new File(SDCard+"/"+fileName);
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(Intent.ACTION_VIEW);     //浏览网页的Action(动作)
                String type = "application/vnd.android.package-archive";
                intent.setDataAndType(Uri.fromFile(file1), type);  //设置数据类型
                startActivity(intent);
				rjgxform.this.setResult(RESULT_OK);
				finish();
            }
        };
 
 
    }
    public void down_file(String url,String path) throws IOException{
        //下载函数       
        filename=url.substring(url.lastIndexOf("/") + 1);
        //获取文件名
        URL myURL = new URL(url);
        URLConnection conn = myURL.openConnection();
        conn.connect();
        InputStream is = conn.getInputStream();
        this.fileSize = conn.getContentLength();//根据响应获取文件大小
        if (this.fileSize <= 0) throw new RuntimeException("无法获知文件大小 ");
        if (is == null) throw new RuntimeException("stream is null");
        FileOutputStream fos = new FileOutputStream(path+filename);
        //把数据存入路径+文件名
        byte buf[] = new byte[1024];
        downLoadFileSize = 0;
        sendMsg(0);
        do
          {
            //循环读取
            int numread = is.read(buf);
            if (numread == -1)
            {
              break;
            }
            fos.write(buf, 0, numread);
            downLoadFileSize += numread;
 
            sendMsg(1);//更新进度条
          } while (true);
        sendMsg(2);//通知下载完成
        try
          {
            is.close();
          } catch (Exception ex)
          {
            Log.e("tag", "error: " + ex.getMessage(), ex);
          }
 
    }
    private void sendMsg(int flag)
    {
        Message msg = new Message();
        msg.what = flag;
        handler.sendMessage(msg);
    }     

}
	/*
	Button btnrjgx,btnrjgxreturn;
	TextView textviewrjgx;
	ProgressBar pbrjgx;
	
	int fileSize;
    String urlStr="http://www.gfsms.cn/food/FoodAndroidMobile.apk";   
    String fileName="FoodAndroidMobile.apk"; 
	int downLoadFileSize;
    URL url;   
    HttpURLConnection conn;   
    String SDCard=Environment.getExternalStorageDirectory()+"";   
    String pathName;//+"/"+fileName;//文件存储路径   
    Thread thread;
	private Handler handler=new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			if (!Thread.currentThread().isInterrupted())
			{
				switch(msg.what)
				{
				case 0:
					pbrjgx.setMax(fileSize);

				case 1:
					pbrjgx.setProgress(downLoadFileSize);
					int result=downLoadFileSize*100/fileSize;
					textviewrjgx.setText(result+"%");
					break;
				case 2:
					Toast.makeText(rjgxform.this, "文件下载完成", 1).show();
					break;
				case -1:
					String error=msg.getData().getString("error");
					Toast.makeText(rjgxform.this, error,1).show();
					break;
				}
			}
			super.handleMessage(msg);
		}
	};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView(R.layout.rjgxform);
        btnrjgx=(Button)findViewById(R.id.btnrjgx);
        btnrjgxreturn=(Button)findViewById(R.id.btnrjgxreturn);
        thread=new Thread(){
        	public void run()
        	{
	            urlStr="http://www.gfsms.cn/food/FoodAndroidMobile.apk";   
	            fileName="FoodAndroidMobile.apk";   
	            OutputStream output=null;   
	            try {   

	                url=new URL(urlStr);   
	                conn=(HttpURLConnection)url.openConnection();   
	                SDCard=Environment.getExternalStorageDirectory()+"";   
	                pathName=SDCard+"/"+"FoodDownload";//+"/"+fileName;//文件存储路径   
	                  
	                File file=new File(pathName); 
	                if (!file.exists())
	                	file.mkdir();
	                InputStream input=conn.getInputStream();   
	                fileSize=conn.getContentLength();
                    output=new FileOutputStream(pathName+"/"+fileName);   
                    byte[] buffer=new byte[8*1024];   
                    int count = 0;
                    downLoadFileSize=0;
                    sendMsg(0);
                    while ((count = input.read(buffer)) != -1)
                    {
                    	output.write(buffer, 0, count);
                    	downLoadFileSize+=count;
                    	sendMsg(1);
                    }
                    sendMsg(2);
                    output.close();
                    input.close();

                    File file1 = new File(pathName+"/"+fileName);
                    Intent intent = new Intent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setAction(Intent.ACTION_VIEW);     //浏览网页的Action(动作)
                    String type = "application/vnd.android.package-archive";
                    intent.setDataAndType(Uri.fromFile(file1), type);  //设置数据类型
                    startActivity(intent);
    				rjgxform.this.setResult(RESULT_OK);
    				finish();

	               }   
	            catch (MalformedURLException e) {   
	                e.printStackTrace();   
	            }
	            catch (FileNotFoundException e)
	            {
	            	e.printStackTrace();
	            }
	            catch (IOException e) {   
	                e.printStackTrace();   
	            }finally{   
	                try {   
	                        output.close();   
	                      //  System.out.println("success");   
	                    } catch (IOException e) {   
	                     //   System.out.println("fail");   
	                   //     e.printStackTrace();   
	                    }   
	            }   
	
        	}
        };
        btnrjgx.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				thread.start();
                
				
			}
		});
        btnrjgxreturn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rjgxform.this.setResult(RESULT_CANCELED);
				finish();
				
			}
		});
    }
    private void sendMsg(int flag)

    {

    Message msg = new Message();

    msg.what = flag;

    handler.sendMessage(msg);

    } 


}
*/