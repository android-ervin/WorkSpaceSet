package com.gaosheng.foodandroid;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TabHost;


public class rcjgtab extends Activity  {

    //声明TabHost对象  
    TabHost mTabHost; 
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mTabHost = (TabHost) findViewById(R.id.tabhost);;  
        mTabHost.setup();
        mTabHost.addTab(mTabHost.newTabSpec("tab_test1")
                .setIndicator("基本项")
                .setContent(R.id.layout_tab1));  
        mTabHost.addTab(mTabHost.newTabSpec("tab_test2")
                .setIndicator("其他项")
                .setContent(R.id.layout_tab2));  
    
        for(int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) 
        { 
        	mTabHost.getTabWidget().getChildAt(i).getLayoutParams().height = 50;
        }
        mTabHost.setCurrentTab(1);  
    }	
	
}