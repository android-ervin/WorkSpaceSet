package com.gaosheng.foodandroid;

import com.gaosheng.data.remoteData;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.os.Bundle;


public class mainform extends Activity {
	
	private GridView gridview;
	String ssqbh="",zfrybh="",zfryxm="",zfrybhid="",password="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );//取消标题设置
        setContentView(R.layout.mainmenu);
        Bundle bundle=this.getIntent().getExtras();
        ssqbh=bundle.getString("ssqbh");
        zfrybh=bundle.getString("zfrybh");
        zfrybhid=bundle.getString("zfrybhid");
        zfryxm=bundle.getString("zfryxm");
        password=bundle.getString("password");
        // this.setTitle(this.getTitle()+"--"+zfryxm);//设置标题
        gridview=(GridView)findViewById(R.id.gridView1);
        
        imageAdapter imgadapter=new imageAdapter(mainform.this,"ok");
        
        gridview.setAdapter(imgadapter);
        OnItemClickListener l=new OnItemClickListener(){

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				switch(arg2)
				{
				    case 0:
				    	Bundle bundle=new Bundle();
				    	bundle.putString("ssqbh", ssqbh);
						bundle.putString("zfrybh", zfrybh);
						bundle.putString("password", password);
						bundle.putString("zfrybhid", zfrybhid);
						bundle.putString("zfryxm", zfryxm);
				    	Intent intent=new Intent();
				    	intent.putExtras(bundle);
				    	intent.setClass(mainform.this, rwtsform.class);
				    	startActivity(intent);
				    	break;
				    case 1:
				    	Bundle bundle1=new Bundle();
				    	bundle1.putString("ssqbh", ssqbh);
						bundle1.putString("zfrybh", zfrybh);
						bundle1.putString("password", password);
						bundle1.putString("zfrybhid", zfrybhid);
						bundle1.putString("zfryxm", zfryxm);
				    	Intent intent1=new Intent();
				    	intent1.putExtras(bundle1);
				    	intent1.setClass(mainform.this, zyggform.class);
				    	startActivity(intent1);
				    	break;
				    case 3:
				    	Bundle bundle3=new Bundle();
				    	bundle3.putString("ssqbh", ssqbh);
				    	bundle3.putString("zfrybh", zfrybh);
				    	bundle3.putString("password", password);
				    	bundle3.putString("zfrybhid", zfrybhid);
				    	bundle3.putString("zfryxm", zfryxm);
				    	Intent intent3=new Intent();
				    	intent3.putExtras(bundle3);
				    	intent3.setClass(mainform.this, rcjgform.class);
				    	startActivity(intent3);
				    	break;
				    case 5:
				    	Bundle bundle5=new Bundle();
				    	bundle5.putString("ssqbh", ssqbh);
				    	bundle5.putString("zfrybh", zfrybh);
				    	bundle5.putString("password", password);
				    	bundle5.putString("zfrybhid", zfrybhid);
				    	bundle5.putString("zfryxm", zfryxm);
				    	Intent intent5=new Intent();
				    	intent5.putExtras(bundle5);
				    	intent5.setClass(mainform.this, xgmmform.class);
				    	startActivityForResult(intent5,0);
				    	break;
				    case 6:
				    	Intent intent6=new Intent();
				    	intent6.setClass(mainform.this, rjgxform.class);
				    	startActivityForResult(intent6,0);
				    	break;
				    case 7:
				    	finish();
					default:
						break;
				}
			}};
        gridview.setOnItemClickListener(l);
       
        
        
    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data)
    {
    	switch(resultCode)
    	{
    	case RESULT_OK:
    		finish();
    		break;
    	default:
    		break;
    	
    	}
    }
}
