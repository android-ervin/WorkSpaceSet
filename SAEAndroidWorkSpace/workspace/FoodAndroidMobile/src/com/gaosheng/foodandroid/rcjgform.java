package com.gaosheng.foodandroid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.gaosheng.data.remoteData;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;


public class rcjgform extends Activity {
	String ssqbh="",zfrybh="",zfryxm="",zfrybhid="",password="";
	ListView listview;
	Button butjc;
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        requestWindowFeature( Window.FEATURE_NO_TITLE );
	        setContentView(R.layout.rcjgform);
			 Bundle bundle=this.getIntent().getExtras();
		        ssqbh=bundle.getString("ssqbh");
		        zfrybh=bundle.getString("zfrybh");
		        zfrybhid=bundle.getString("zfrybhid");
		        zfryxm=bundle.getString("zfryxm");
		        password=bundle.getString("password");
		        listview=(ListView)findViewById(R.id.rcjglistview);
		        butjc=(Button)findViewById(R.id.buttonrcjg);
		        butjc.setOnClickListener(new OnClickListener() {		
				public void onClick(View v) {
				        String data=remoteData.GetWjsjc(zfrybhid, password);
				        if (data.indexOf("|")!=-1)
				        {  
				        	String[] jcarr=data.split("=");
				             List<HashMap<String, Object>> jc = new ArrayList<HashMap<String,Object>>();  
				             for(int i=0;i<=jcarr.length-1;i++)
				             {   
				                 HashMap<String, Object> item = new HashMap<String, Object>(); 
				                 String[] jcitem=jcarr[i].split("\\|");
				                 item.put("bsh", jcitem[0]);   
				                 item.put("qymc", jcitem[1]);   
				                 item.put("jckssj",jcitem[2]);   
				                 jc.add(item); 
				             }   
				            SimpleAdapter adapter = new SimpleAdapter(rcjgform.this, jc,R.layout.rwtslistview1,    
				                     new String[]{"bsh", "qymc", "jckssj"}, new int[]{R.id.textViewbsh,R.id.textViewqymc,R.id.textViewjckssj});   
				            listview.setAdapter(adapter);  
				        }
				}
			});
	        }
	   
	   public boolean onCreateOptionsMenu(Menu menu) {

	        menu.add(Menu.NONE, Menu.FIRST + 1, 1, "新增检查").setIcon(

	        android.R.drawable.ic_menu_delete);

	        menu.add(Menu.NONE, Menu.FIRST + 2, 2, "从事人员健康证检查").setIcon(

	        android.R.drawable.ic_menu_edit);

	        menu.add(Menu.NONE, Menu.FIRST + 3, 3, "修改检查").setIcon(

	        android.R.drawable.ic_menu_help);

	        menu.add(Menu.NONE, Menu.FIRST + 4, 4, "从业人员日常卫生检查").setIcon(

	        android.R.drawable.ic_menu_add);

	        menu.add(Menu.NONE, Menu.FIRST + 5, 5, "删除记录").setIcon(

	        android.R.drawable.ic_menu_info_details);

	        menu.add(Menu.NONE, Menu.FIRST + 6, 6, "继续检查").setIcon(

	        android.R.drawable.ic_menu_send);
	        menu.add(Menu.NONE, Menu.FIRST + 7, 7, "结束检查").setIcon(

	                android.R.drawable.ic_menu_send);  
	        menu.add(Menu.NONE, Menu.FIRST + 8, 8, "附加功能").setIcon(

	                        android.R.drawable.ic_menu_send);
	        menu.add(Menu.NONE,Menu.FIRST + 9, 9,"退出");

	        return true;

	    }
	    public boolean onOptionsItemSelected(MenuItem item) {       
	    	switch (item.getItemId()) {     
	    	case Menu.FIRST + 1:onClick(null);
	    	break;        
	    	case Menu.FIRST + 2:Toast.makeText(this, "保存菜单被点击了", Toast.LENGTH_LONG).show(); 
	    	break; 
	    	case Menu.FIRST + 3:Toast.makeText(this, "帮助菜单被点击了", Toast.LENGTH_LONG).show();            
	    	break;
	    	case Menu.FIRST + 4:Toast.makeText(this, "添加菜单被点击了", Toast.LENGTH_LONG).show();  
	    	break;
	    	case Menu.FIRST + 5:Toast.makeText(this, "详细菜单被点击了", Toast.LENGTH_LONG).show();           
	    	break;
	    	case Menu.FIRST + 6: Toast.makeText(this, "发送菜单被点击了", Toast.LENGTH_LONG).show(); 
	    	break; }        
	    	return false;    
	    	}
		public void onClick(View v) {
			Log.v("demo_default", "myAdapter");
			Bundle bundle=new Bundle();
	    	bundle.putString("ssqbh", ssqbh);
			bundle.putString("zfrybh", zfrybh);
			bundle.putString("password", password);
			bundle.putString("zfrybhid", zfrybhid);
			bundle.putString("zfryxm", zfryxm);
	    	Intent intent=new Intent();
	    	intent.putExtras(bundle);
			intent.setClass(rcjgform.this, demo_default.class);
			startActivity(intent);
		}

}
