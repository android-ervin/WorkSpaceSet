package com.gaosheng.foodandroid;

import com.gaosheng.data.remoteData;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;



public class FoodAndroidMobileActivity extends Activity {
    /** Called when the activity is first created. */
	public Button btnlogin,btnout;
	public EditText edtuser,edtpass;
	private ArrayAdapter<String> adapter;
	public Spinner spinner;
	private String m_area="";
	private final static String[] area={
	"南昌市监督管理局","南昌市东湖区分局","南昌市南昌县分局","南昌市新建县分局",
	"南昌市安义县分局","南昌市进贤县分局","南昌市高新区分局","南昌市西湖区分局",
	"南昌市青云谱区分局",	"南昌市湾里区分局","南昌市青山湖分局","南昌食品化妆品监督所"};
	
	private final static String[] areano={
		"01","02","03","04","05","06","09","10","11","12","13","15"
	};

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView(R.layout.main);
        btnlogin=(Button)findViewById(R.id.btnlogin);
        btnout=(Button)findViewById(R.id.btnout);
        spinner=(Spinner)findViewById(R.id.spinner1);
        edtuser=(EditText)findViewById(R.id.edtuser);
        edtpass=(EditText)findViewById(R.id.edtpass);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,area);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

       	  public void onNothingSelected(AdapterView<?> parent) 
        	  {
              }
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
		
			
		}
        
        });
        btnlogin.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String zfrybh=edtuser.getText().toString();
				String password=edtpass.getText().toString();
				m_area=areano[spinner.getSelectedItemPosition()];
				
				String zfrybhid,zfryxm;
				
				if (m_area.equals(""))
				{
					remoteData.ShowDialog("请选择分局",FoodAndroidMobileActivity.this);
					return;
				}
				if (zfrybh.equals(""))
				{
					
					remoteData.ShowDialog( "请输入用户编号",FoodAndroidMobileActivity.this);
					return;
				}
				if (password.equals(""))
				{
					remoteData.ShowDialog( "请输入密码",FoodAndroidMobileActivity.this);
					return;
				}
				String result=remoteData.Login(m_area, zfrybh, password);
				if (result.indexOf("|")==-1)
				{
					remoteData.ShowDialog("用户名或密码错误！",FoodAndroidMobileActivity.this);
					return;
				}
				else
				{
	    			Intent intent=new Intent();
			    	Bundle bundle=new Bundle();
					
					String[] zfryarr=result.split("\\|");
					zfrybhid=zfryarr[0];
					zfryxm=zfryarr[1];
					bundle.putString("ssqbh", m_area);
					bundle.putString("zfrybh", zfrybh);
					bundle.putString("password", password);
					bundle.putString("zfrybhid", zfrybhid);
					bundle.putString("zfryxm", zfryxm);
					intent.putExtras(bundle);
					intent.setClass(FoodAndroidMobileActivity.this, mainform.class);
					startActivity(intent);
					finish();
				}
			}
        	
        });
        
        btnout.setOnClickListener(new Button.OnClickListener()
        {

			public void onClick(View v)
			{
				// TODO Auto-generated method 
				
				finish();
				
			}
		});
   




        

    }

}
