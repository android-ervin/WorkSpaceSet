package com.gaosheng.data;




import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;



public class remoteData {

	private static final String METHOD_NAME = "ProcessData";   
    private static final String NAMESPACE = "http://tempuri.org/";   
    private static final String URL = "http://www.gfsms.cn/food/DataAccess.asmx";
    private static String SOAP_ACTION="http://tempuri.org/ProcessData";



	private static String JiaMiString(String str)//加密字符串
	{
		return str;
	}
	private static String JieMiString(String str)//解密字符串
	{
		return str;
	}
	private static String GetRemoteData(String str)//调用远程服务，交互相关数据
	{
		String result="";
		str=JiaMiString(str);
		///////////和远程服务交互
		try {  
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME); 
            request.addProperty("str",str);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            
            envelope.dotNet = true;  
            envelope.setOutputSoapObject(request);  
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);  
            androidHttpTransport.call(SOAP_ACTION, envelope);  
            Object ReturnStr = (Object) envelope.getResponse();  
            result=ReturnStr.toString();   
        } catch (Exception e) {  
            result=e.getMessage();
        }  

		////////////
		return JieMiString(result);
	}
	public static String Login(String area,String user,String password)//用户登录
	{
		String str="login|"+area+"|"+user+"|"+password;
		String result=GetRemoteData(str);
		return result;
	}
	
	public static String Rwts(String zfrybhid,String password)//获取任务提示信息
	{
		String str="rwts|"+zfrybhid+"|"+password;
		String result=GetRemoteData(str);
		return result;
	}
	
	public static String Zygg(String zfrybhid,String password,String ssqbh)//获取重要公告信息
	{
	   
		String str="zygg|"+zfrybhid+"|"+password+"|"+ssqbh;
		String result=GetRemoteData(str);
		return result;
	}
	public static String ModifyPassword(String zfrybhid,String password,String newpassword)  //修改密码
	{
		String result="0";
		String str="xgmm|"+zfrybhid+"|"+password+"|"+newpassword;
		result=GetRemoteData(str);
		return result;
	}

	
	public static String GetWjsjc(String zfrybhid,String password)//获取未检查完成企业
	{
		String result="0";
		String str="wjsjc|"+zfrybhid+"|"+password;
		result=GetRemoteData(str);
		return result;
	}
	
	public static String Getqy(String zfrybhid,String password,String ssqbh,String flag,String nr)//发送查询企业信息请求
	{
		String result="0";
		String str="";
		if(flag=="0")
		{
			str="getqy|"+zfrybhid+"|"+password+"|"+ssqbh+"|"+"0"+"|"+nr;
		}
		else if(flag=="1")
		{
			str="getqy|"+zfrybhid+"|"+password+"|"+ssqbh+"|"+"1"+"|"+nr;
		}
		result=GetRemoteData(str);
		return result;
			
	}
	
	public static String Getzfry(String zfrybhid,String password,String ssqbh) //查找执法人员信息
	{
		String result="0";
		String str="";
		str="getzfry|"+zfrybhid+"|"+password+"|"+ssqbh;
		result=GetRemoteData(str);
		return result ;
	}
	
	
	public static String Insertjcjlzb (String zfrybhid,String password,String qybhid,String jckssj ,String jcjssj,
			String sxry,String sfcfwjy, String sfgghdmj,String bz,String xkzflag,String qycheckedman)//保存新检查
	{
		String result="0";
		String str="";
		str="insertjcjlzb|"+zfrybhid+"|"+password+"|"+qybhid+"|"+jckssj+"|"+jcjssj+"|"+sxry+"|"+
		sfcfwjy+"|"+sfgghdmj+"|"+ bz+"|"+ xkzflag+"|"+ qycheckedman;
		result=GetRemoteData(str);
		return result;
	}

	
	public static String Modifyjc(String zfrybhid,String password,String qybhid,String jckssj,String jcjssj,
			String sxry,String sfcfwjy,String sfgghdmj,String bz,String xkzflag,String qycheckedman)//修改检查
	{
		String result="0";
		String str="";
		str="modifyjc|"+zfrybhid+"|"+password+"|"+qybhid+"|"+jckssj+"|"+jcjssj+"|"+sxry+"|"+
		sfcfwjy+"|"+sfgghdmj+"|"+bz+"|"+xkzflag+"|"+qycheckedman;
		result=GetRemoteData(str);
		return result;
	}
	
	public static String Getqyjcxmlb(String zfrybhid,String password,String qybhid )//查询某企业的检查项目类别
	{
		String result="0";
		String str="";
		str="getqyjcxmlb|"+zfrybhid+"|"+password+"|"+qybhid;
		result=GetRemoteData(str);
				return result;
	}
	
	
	public static String Getqyjcxm(String zfrybhid,String password,String zbbsh,String jcxmlb,String flag)
	{//查询某检查类别该企业检查项目或通用检查项目
		String result="0";
		String str="";
		if(flag=="0")
		{
			str="getqyjcxm|"+zfrybhid+"|"+password+"|"+zbbsh+"|"+jcxmlb+"|"+"0";
		}
		else if(flag=="1")
		{
			str="getqyjcxm|"+zfrybhid+"|"+password+"|"+zbbsh+"|"+jcxmlb+"|"+"1";
		}
		result=GetRemoteData(str);
		return result;
	}
	
	public static String Savejc(String zfrybhid,String password,String bsh,String sfhg,String txxm1,
			String txxm2,String txxm3,String memo)//保存检查信息
	{
		String result="0";
		String str="";
		str="savejc|"+zfrybhid+"|"+password+"|"+bsh+"|"+sfhg+"|"+txxm1+"|"+txxm2+"|"+txxm3+"|"+memo;
		result=GetRemoteData(str);
		return result;
	}
	
	
	public static String Insertjcjlvideo(String zfrybhid,String password,String zbbsh,String xmlbbh,
			String filetype,String img)//监管取证新增
	{
		String result="0";
		String str="";
		str="insertjcjlvideo|"+zfrybhid+"|"+password+"|"+zbbsh+"|"+xmlbbh+"|"+filetype+"|"+img;
		result=GetRemoteData(str);
		return result;
	}
	
	
	public static String Getjcjlvideo(String zfrybhid,String password,String zbbsh,String xmlbbh )//监管取证查询
	{
		String result="0";
		String str="";
		str="insertjcjlvideo|"+zfrybhid+"|"+password+"|"+zbbsh+"|"+xmlbbh;
		result=GetRemoteData(str);
		return result;
	}
	
	
	public static String Getimg(String zfrybhid,String password,String autobh)//监管取证删除
	{
		String result="0";
		String str="";
		str="insertjcjlvideo|"+zfrybhid+"|"+password+"|"+autobh;
		result=GetRemoteData(str);
		return result;
	}
	

	public static String Deletejcjlzb(String zfrybhid,String password,String zbbsh)//删除检查
	{
		String result="0";
		String str="";
		str="deletejcjlzb|"+zfrybhid+"|"+password+"|"+zbbsh;
		result=GetRemoteData(str);
		return result;
	}
	
	public static String Endjcjl(String zfrybhid,String password,String zbbsh,String jcjssj)//结束检查
	{
		String result="0";
		String str="";
		str="endjcjl|"+zfrybhid+"|"+password+"|"+zbbsh+"|"+jcjssj;
		result=GetRemoteData(str);
		return result;
	}
	
	public  static  void ShowDialog(String message,Context context)
	{
		AlertDialog.Builder builder=new AlertDialog.Builder(context);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		AlertDialog alert=builder.create();
		alert.show();
	}
}
