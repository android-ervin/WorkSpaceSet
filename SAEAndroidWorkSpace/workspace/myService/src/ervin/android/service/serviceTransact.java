package ervin.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public class serviceTransact extends Service
{

	@Override
	public IBinder onBind(Intent arg0)
	{
		// TODO Auto-generated method stub
		return new myBinder();
	}
	class myBinder extends Binder
	{
		protected boolean onTransact(int code, Parcel data, Parcel reply,
				int flags) throws RemoteException
		{
			System.out.println("Service code-->"+code);
			String str=data.readString();//其中data是客户端通过Transact方法发送过来的数据，可以根据该数据返回响应，将结果放入reply中
			reply.writeString("Service reply");//返回结果放入reply参数里回发给客户端
			
			System.out.println("from activity-->"+str);
			// TODO Auto-generated method stub
			return super.onTransact(code, data, reply, flags);
		}
	}
}
