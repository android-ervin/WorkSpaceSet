package ervin.android.service;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MyServiceActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button btn1=(Button)findViewById(R.id.button1);
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				Intent intent=new Intent(MyServiceActivity.this,serviceTransact.class);
				bindService(intent, conn, BIND_AUTO_CREATE);
			}
		});
        
        Button btn2=(Button)findViewById(R.id.button2);
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
//				Intent intent=new Intent(MyServiceActivity.this,serviceTransact.class);
//				stopService(intent);
				serviceTransact myservice=new serviceTransact();
				myservice.stopSelf();
			}
		});
    }
    
    ServiceConnection conn=new ServiceConnection()
	{
		
		public void onServiceDisconnected(ComponentName arg0)
		{
			// TODO Auto-generated method stub

			System.out.println("解除绑定成功");
		}
		
		public void onServiceConnected(ComponentName arg0, IBinder binder)
		{
			// TODO Auto-generated method stub
			System.out.println("绑定成功");
			
			Binder activityBinder=(Binder)binder;
			
			Parcel data=Parcel.obtain();
			Parcel reply=Parcel.obtain();
			
			
			try
			{
				data.writeString("activit data");//先写数据到Parcel中
				activityBinder.transact(0, data, reply, 0);//将Parcel中的数据发送到服务器端Service上面，同时等待接受Service返回的结果
				String str=reply.readString();//接收返回的结果放在Parcel中（不能放在Transact方法前面）
				System.out.println("from service-->"+str);
			} catch (RemoteException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};
}