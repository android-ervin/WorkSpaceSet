package ervin.android.menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MyMenuActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		
		menu.add(0, 1, 1, R.string.update);
		menu.add(0, 2, 2, R.string.about);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId()==1)
			Toast.makeText(this, "Click update", Toast.LENGTH_SHORT).show();
		else 
			Toast.makeText(this, "Click about", Toast.LENGTH_SHORT).show();
		
		return super.onOptionsItemSelected(item);
	}
	

}