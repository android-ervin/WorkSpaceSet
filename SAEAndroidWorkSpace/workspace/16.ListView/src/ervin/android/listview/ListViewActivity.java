package ervin.android.listview;


import android.app.ListActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListViewActivity<View> extends ListActivity {
    /** Called when the activity is first created. */
	static final String [] COUNTRIES={"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra",
	    "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina",};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);
        //NO.1
       // setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, COUNTRIES));
        //NO.2
        String[] countries = getResources().getStringArray(R.array.countries_array);
        setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, countries));

        ListView lv = getListView();
        lv.setTextFilterEnabled(true);//����ʱƥ���ַ�

        lv.setOnItemClickListener(new OnItemClickListener()
        {
         /* public void onItemClick(AdapterView<?> parent, View view,int position, long id)
          {
            // When clicked, show a toast with the TextView text
            
          }maybe the version is different,parament name is different*/

          public void onItemClick(AdapterView<?> arg0, android.view.View arg1,int arg2, long arg3)
          {
			// TODO Auto-generated method stub
        	  Toast.makeText(getApplicationContext(),((TextView) arg1).getText(),Toast.LENGTH_SHORT).show();
          }
        });
        
    }
}