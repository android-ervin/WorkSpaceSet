package ervin.android.com;


import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class RegisterActivity extends Activity
{
	private TextView tv1=null;
	private TextView tv2=null;
	private TextView tv3=null;
	
	private String TAG="A2";
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		
		tv1=(TextView)findViewById(R.id.textView1);
		tv2=(TextView)findViewById(R.id.textView2);
		tv3=(TextView)findViewById(R.id.textView3);
		
		Intent intent =this.getIntent();
		tv1.setText("用户名为："+intent.getStringExtra("username"));
		tv2.setText("密码为："+intent.getStringExtra("password"));
		tv3.setText("用户邮箱为："+intent.getStringExtra("email"));
		
		Button btn=(Button)findViewById(R.id.button1);
		btn.setOnClickListener(new btnOnclick());
		
		
	}
	class btnOnclick implements OnClickListener
	{

		public void onClick(View arg0)
		{
			//Intent intent=new Intent(RegisterActivity.this,ActivityActivity.class);
			//startActivity(intent);
			setResult(1, RegisterActivity.this.getIntent());
			finish();//必须要加这个函数，不然不会结束该Activity
		}
		
	}
	@Override
	protected void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i(TAG,"onDestroy");
		System.out.println("Register-->onDestroy");
	}
	@Override
	protected void onPause()
	{
		// TODO Auto-generated method stub
		super.onPause();
		Log.i(TAG,"onPause");
		System.out.println("Register-->onPause");
	}
	@Override
	protected void onRestart()
	{
		// TODO Auto-generated method stub
		super.onRestart();
		Log.i(TAG,"onRestart");
		System.out.println("Register-->onRestart");
	}
	@Override
	protected void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();
		Log.i(TAG,"onResume");
		System.out.println("Register-->onResume");
	}
	@Override
	protected void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		Log.i(TAG,"onStart");
		System.out.println("Register-->onStart");
	}
	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
		super.onStop();
		Log.i(TAG,"onStop");
		System.out.println("Register-->onStop");
	}
	
	
}
