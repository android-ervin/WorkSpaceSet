package ervin.android.com;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityActivity extends Activity {
    /** Called when the activity is first created. */
    
//    private EditText et_user=null;
//    private EditText et_password=null;
//    private EditText et_email=null;
	  private Button btn=null;
	  private String TAG="A1";
	  
	  final int CODE=1;
	  public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.i(TAG,"onCreat");
		System.out.println("Main-->onCreat");
        
        final EditText et_user=(EditText)findViewById(R.id.editText1);
        final EditText et_password=(EditText)findViewById(R.id.editText2);
        final EditText et_email=(EditText)findViewById(R.id.editText3);
        btn=(Button)findViewById(R.id.button1);
        btn.setOnClickListener(new View.OnClickListener()
		{
			
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				 //if(!"".equals(et_user)&&!"".equals(et_password)&&!"".equals(et_email))
				 if(et_user.getText().toString()!="" && et_password.getText().toString()!="" && et_email.getText().toString()!="")
				 {
					 if(et_password.getText().length()<=6)
					 {
						 Toast.makeText(ActivityActivity.this, "密码太过简单", Toast.LENGTH_SHORT).show();
					 }
					 else 
					 {
						 /*启动另一个Activity*/
//						 Intent intent=new Intent(ActivityActivity.this,RegisterActivity.class);
//						 startActivity(intent);
						 
						 
						 /*向另一个Activity中传入数据*/
						 Intent intent =new Intent(ActivityActivity.this, RegisterActivity.class);
						 //intent.setClass(ActivityActivity.this, RegisterActivity.class)
						 intent.putExtra("username", et_user.getText().toString());
						 intent.putExtra("password", et_password.getText().toString());
						 intent.putExtra("email", et_email.getText().toString());
						 //startActivity(intent);
						 startActivityForResult(intent, CODE);
					 }
				 }
				 else 
				 {
					 Toast.makeText(ActivityActivity.this, "用户信息输入不完整",Toast.LENGTH_LONG).show();
				 }
			}
		});
        	
        }
	  
    @Override
    /*从上一个Activity中返回后会执行该函数*/
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
    	Log.i(TAG,"onActivityResult");
    	System.out.println("Main-->onActivityResult");
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==CODE && resultCode==CODE)
		{
			Toast.makeText(ActivityActivity.this, "Return", Toast.LENGTH_SHORT);
			((EditText)findViewById(R.id.editText2)).setText("");//清空密码
		}
	}

	protected void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i(TAG,"onDestroy");
		System.out.println("Main-->onDestroy");
	}
	@Override
	protected void onPause()
	{
		// TODO Auto-generated method stub
		super.onPause();
		Log.i(TAG,"onPause");
		System.out.println("Main-->onPause");
	}
	@Override
	protected void onRestart()
	{
		// TODO Auto-generated method stub
		super.onRestart();
		Log.i(TAG,"onRestart");
		System.out.println("Main-->onRestart");
	}
	@Override
	protected void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();
		Log.i(TAG,"onResume");
		System.out.println("Main-->onResume");
	}
	@Override
	protected void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		Log.i(TAG,"onStart");
		System.out.println("Main-->onStart");
	}
	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
		super.onStop();
		Log.i(TAG,"onStop");
		System.out.println("Main-->onStop");
	}
				        	
}