package ervin.android.webservice;

import ervin.android.remotedata.RemoteData;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class WebserviceActivity extends Activity {
    /** Called when the activity is first created. */
	Button btn=null;
	TextView eText=null;
	String result="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
        btn=(Button)findViewById(R.id.button1);
        eText=(TextView)findViewById(R.id.textView2);
        btn.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				result=RemoteData.getWebserviceData("Ervin", "1234");
				System.out.println("------->" + result);
				eText.setText(result);
			}
		});
        
    }
}