package com.zz.SoundPool;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;

public class SoundPool extends Activity 
{
    /** Called when the activity is first created. */
    //MediaPlayer MP;
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        MediaPlayer MP=new MediaPlayer();
        MP=MediaPlayer.create(this, R.raw.sound);
        setContentView(R.layout.main);
        MP.start();
    }

}