package ervin.android.Bc;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class BroadcastActivity extends Activity {
    /** Called when the activity is first created. */
    
    private static final String ACTION_SMS="android.provider.Telephony.SMS_RECEIVED";
    
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
        Button btncall=(Button)findViewById(R.id.button1);
        Button btnsms=(Button)findViewById(R.id.button2);
        btncall.setOnClickListener(new calllistener());
        btnsms.setOnClickListener(new calllistener());
        
        Button btnbroad=(Button)findViewById(R.id.button3);
        btnbroad.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				ReceiverB rbB=new ReceiverB();
				IntentFilter filter=new IntentFilter();
				filter.addAction(ACTION_SMS);
				BroadcastActivity.this.registerReceiver(rbB, filter);
			}
		});
        
    }
    
    class calllistener implements OnClickListener
    {
    	EditText et=(EditText)findViewById(R.id.editText1);
		public void onClick(View v)
		{
			switch (v.getId())
			{
			case R.id.button1:
				String editStr=et.getText().toString();
				Intent intent=new Intent();
				intent.setAction(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" +editStr));
				startActivity(intent);
				break;
			case R.id.button2:
				String editStr2=et.getText().toString();
				Intent intent2=new Intent();
				intent2.setAction(Intent.ACTION_SENDTO);
				intent2.setData(Uri.parse("smsto:"+ editStr2));//the phone number of you send message
				intent2.putExtra("sms_body", "hello android");//the content of the message which you want to send
				startActivity(intent2);
			default:
				break;
			}		
		}
    	
    }
}