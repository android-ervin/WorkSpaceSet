package com.zz.Button;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;


public class Button extends Activity 
{
	
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
//      Button btn1=(Button)findViewById(R.id.btn1);
                                     
        findViewById(R.id.btn1).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) 
			{
				Toast.makeText(Button.this, "you touch me",Toast.LENGTH_LONG).show();
				
			}
		});//创建了一个“匿名内部类”的方法。所有为context参数都是这个activity本身
       /*OnClickListener为一个接口，接口里面有一个onClick抽象方法*/
        
        findViewById(R.id.button1).setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				new AlertDialog.Builder(Button.this).setTitle("title").setMessage("this is a dialog").setPositiveButton(
						"sure",new DialogInterface.OnClickListener()
						{
							
							public void onClick(DialogInterface dialog, int which)
							{
								// TODO Auto-generated method stub
								Toast.makeText(Button.this, "dialog is closed", Toast.LENGTH_SHORT).show();
							}
						}).show();
				
			}
		});
       
        
    }
    
    /*给Btn添加监听事件的第二种方法*/
    public void myOnclick(View view)
    {
    	Toast.makeText(Button.this, "you click me", Toast.LENGTH_LONG).show();
    	Log.i("日志", "log.text");
    	
    }
}
