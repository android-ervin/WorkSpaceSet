package ervin.android.socket;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ServerSocketActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn1=null;
	private Button btn2=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        btn1=(Button)findViewById(R.id.button1);
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				new TcpThread().start();
			}
		});
        btn2=(Button)findViewById(R.id.button2);
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				new Thread(new UdpThread()).start();
			}
		});
    }
    private class TcpThread extends Thread
    {
		@Override
		public void run()
		{
			ServerSocket serverSocket=null;
			try
			{
				//创建一个ServerSocket对象，并让其在1234端口监听
				serverSocket=new ServerSocket(1234);
				//调用ServerSocket的accept方法，接受客户端发送的请求，建立连接，是一个阻塞方法，有响应才返回
				Socket socket=serverSocket.accept();
				//从客户端请求中获得输入流
				InputStream inputStream=socket.getInputStream();
				//根据输入流将数据读取出来
				byte buffer[]=new byte[1024*4];//一次读四个字节的数据到buffer中
				int temp=0;
				while((temp=inputStream.read(buffer))!=-1)
				{
					System.out.println(new String(buffer,0,temp));
				}
				
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
    }
    private class UdpThread implements Runnable
    {
		public void run()
		{
			try
			{
				DatagramSocket socket=new DatagramSocket(1234);
				byte data[] =new byte[1024];
				DatagramPacket packet=new DatagramPacket(data, data.length);
				socket.receive(packet);
				String result=new String(packet.getData(),packet.getOffset(),packet.getLength());
				System.out.println("result--->" + result);
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
    	
    }
}