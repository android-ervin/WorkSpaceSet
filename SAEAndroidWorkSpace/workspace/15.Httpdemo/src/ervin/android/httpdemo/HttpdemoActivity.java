package ervin.android.httpdemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class HttpdemoActivity extends Activity {
    /** Called when the activity is first created. */
	private EditText editName=null;
	private EditText editAge=null;
	private Button btn1;
	private Button btn2;
	private Button btn3;
	private String path="http://10.0.2.2:81/Android.aspx";
	private HttpResponse httpResponse;
	private HttpEntity httpEntity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        editName=(EditText) findViewById(R.id.editText1);
        editAge=(EditText) findViewById(R.id.editText2);
        
        btn1=(Button)findViewById(R.id.button1);
        /*使用Get方式发送数据*/
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				String name=editName.getText().toString();
				String age=editAge.getText().toString();
				String getpath=path+"?"+"name="+name+"&age="+age;
				/*从网络上获得一个流的一般步骤*/
				HttpGet hGet=new HttpGet(getpath);
				HttpClient hClient=new DefaultHttpClient();
				InputStream inputStream=null;
				try
				{
					httpResponse=hClient.execute(hGet);
					httpEntity=httpResponse.getEntity();
					inputStream=httpEntity.getContent();
					
					BufferedReader bReader=new BufferedReader(new InputStreamReader(inputStream));
					String result="";
					String line="";
					while((line=bReader.readLine())!=null)
						result+=line;
					System.out.println(result);
					inputStream.close();
				} catch (ClientProtocolException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
        
        btn2=(Button)findViewById(R.id.button2);
        /*使用Post方式发送数据*/
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				String name=editName.getText().toString();
				String age=editAge.getText().toString();
				
				NameValuePair nameValuePair1=new BasicNameValuePair("sex", name);
				NameValuePair nameValuePair2=new BasicNameValuePair("adr", age);//名称必须和服务器端的相同
				List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>();
				nameValuePairs.add(nameValuePair1);
				nameValuePairs.add(nameValuePair2);//
				try
				{
					httpEntity=new UrlEncodedFormEntity(nameValuePairs);//
					HttpPost hPost=new HttpPost(path);
					hPost.setEntity(httpEntity);
					HttpClient hClient=new DefaultHttpClient();
					InputStream inputStream=null;
					try
					{
						httpResponse=hClient.execute(hPost);
						httpEntity=httpResponse.getEntity();
						inputStream=httpEntity.getContent();
						
						BufferedReader bReader=new BufferedReader(new InputStreamReader(inputStream));
						String result="";
						String line="";
						while((line=bReader.readLine())!=null)
							result+=line;
						System.out.println(result);
					} catch (ClientProtocolException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e)
					{
						// TODO Auto-generated catch block
						
						e.printStackTrace();
					}
					inputStream.close();
				} catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
        
        btn3=(Button)findViewById(R.id.button3);
        btn3.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				String name=editName.getText().toString();
				String age=editAge.getText().toString();
				String getpath=path+"?"+"name="+name+"&age="+age;
				
				URL myUrl=null;
				try
				{
					myUrl=new URL(getpath);
				} catch (MalformedURLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpURLConnection conn;
				try
				{
					conn = (HttpURLConnection) myUrl  
					.openConnection();
					conn.setDoInput(true);  
					conn.connect();  
					InputStream is = conn.getInputStream(); //连接网络后直接得到一个流对象，比第一种方法简便
					BufferedReader bReader=new BufferedReader(new InputStreamReader(is));
					String result="";
					String line="";
					while((line=bReader.readLine())!=null)
						result+=line;
					System.out.println(result);
					is.close();
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
				
			}
		});
    }  
}