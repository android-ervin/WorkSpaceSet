package com.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

public class Helloworld extends Activity{
    /** Called when the activity is first created. */
	private Button btn;
	private Button btn1;
	private EditText txt;
    private String TAG="ErvinActivity";
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my);
        btn = (Button)findViewById(R.id.Button01);
        btn1 = (Button)findViewById(R.id.Button02);
        txt = (EditText)findViewById(R.id.EditText01);
        btn.setOnClickListener(new MyListener());
        btn1.setOnClickListener(new MyPop());
        
        Log.i(TAG,"onCreat");
    }
    class MyListener implements Button.OnClickListener {
		public void onClick(View v) {
			txt.setText("hello");
		}
    }
    
    class MyPop implements Button.OnClickListener {

		public void onClick(View v) {
			txt.setText("Android");
			
		}
		
    	
    }

	/*Activity  lifecycle*/
	protected void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i(TAG, "onDestroy");
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.i(TAG,"onPause");
		this.getPreferences(MODE_PRIVATE).edit().putString("name", txt.getText().toString());
	}

	@Override
	protected void onRestart()
	{
		// TODO Auto-generated method stub
		super.onRestart();
		Log.i(TAG,"onRestart");
	}

	@Override
	protected void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();
		Log.i(TAG, "onResume");
		txt.setText(this.getPreferences(MODE_PRIVATE).getString("name"," "));
	}

	@Override
	protected void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		Log.i(TAG,"onStart");
	}

	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
		super.onStop();
		Log.i(TAG, "onStop");
	}
    
}
