package com.test;


import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Activity1 extends Activity {
	
	private String TAG="activity1";
	EditText Et1=null;
	EditText Et2=null;
	
	 /*Saving activity state*/
	protected void onSaveInstanceState(Bundle outState)
	{
		// TODO Auto-generated method stub
    	Log.v(TAG,"onSavestate");
    	Et1=(EditText)findViewById(R.id.editText1);
        Et2=(EditText)findViewById(R.id.editText2);
    	outState.putString("username", Et1.getText().toString());
    	outState.putString("password", Et2.getText().toString());
		super.onSaveInstanceState(outState);
	}
    public void onCreate(Bundle savedInstanceState) {
    	
    	if(savedInstanceState!=null)//为什么没有被执行
        {
        	Log.v(TAG,"reshow");
        	Et1.setText(savedInstanceState.getString("username"));
        	Et2.setText(savedInstanceState.getString("password"));
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity1);
        
        Button btn1 = (Button)findViewById(R.id.button1);
        Et1=(EditText)findViewById(R.id.editText1);
        Et2=(EditText)findViewById(R.id.editText2);
        btn1.setOnClickListener(new Button.OnClickListener(){
        	public void onClick(View v){
        		Intent intent = new Intent();
        		intent.setClass(Activity1.this,Activity2.class);
        		
        		intent.putExtra("username", Et1.getText().toString());
        		intent.putExtra("Password", Et2.getText().toString());
        		
        		startActivity(intent);
        		//Activity1.this.finish();
        		
        	
        	}
        });	
        
    	
        
    }

    

	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		Log.v(TAG,"onRestorstate");
		super.onRestoreInstanceState(savedInstanceState);
	}


	public void onStart(){
    	super.onStart();
    	Log.v(TAG, "onStart");
    }
    
    public void onPause(){
    	super.onPause();
    	Log.v(TAG, "onPause");
    }
    
    public void onResume(){
    	super.onResume();
    	Log.v(TAG, "onResume");
    }
    
    public void onStop(){
    	super.onStop();
    	Log.v(TAG, "onStop");
    }
    
    public void onRestart(){
    	super.onRestart();
    	Log.v(TAG, "onRestart");
    }
    
    public void onDestroy(){
    	super.onDestroy();
    	Log.v(TAG, "onDestroy");    	
    }
          
}