package ervin.android.service;

import ervin.android.service.MyserviceBinder.myBinder;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ServiceActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
        
        Button btn1=(Button)findViewById(R.id.button1);
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				System.out.println("当前主线程ID："+Thread.currentThread().getId());
				Intent intent=new Intent(ServiceActivity.this,Myservice.class);
				startService(intent);
				System.out.println("Service开启");
				
			}
		});
        
        Button btn2=(Button)findViewById(R.id.button2);
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				//MyServiceIntent ms=new MyServiceIntent("myserviceintent");
				System.out.println("当前主线程ID："+Thread.currentThread().getId());
				System.out.println("当前Client进程ID:"+android.os.Process.myPid());
				Intent intent=new Intent(ServiceActivity.this,MyServiceIntent.class);
				startService(intent);
			}
		});
        
        Button btn3=(Button)findViewById(R.id.button3);
        btn3.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				Intent intent=new Intent(ServiceActivity.this,MyserviceBinder.class);
				bindService(intent, conn, BIND_AUTO_CREATE);
				System.out.println("当前Client线程ID:"+Thread.currentThread().getId());
				System.out.println("当前Client进程ID:"+android.os.Process.myPid());
			}
		});        
    }
    myServiceconnect conn=new myServiceconnect();
    
    class myServiceconnect implements ServiceConnection
    {

		public void onServiceConnected(ComponentName arg0, IBinder ibinder)
		{
			// TODO Auto-generated method stub
			 myBinder mBinder=(myBinder)ibinder;
			 mBinder.getdata();
			
		}

		public void onServiceDisconnected(ComponentName arg0)
		{
			// TODO Auto-generated method stub
			
		}
    	
    }
    
}