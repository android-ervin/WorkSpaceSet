package ervin.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class MyserviceBinder extends Service
{

	@Override
	//Ibinder是一个接口
	public IBinder onBind(Intent intent)
	{
		myBinder mb=new myBinder();
		System.out.println("当前Service线程ID:"+Thread.currentThread().getId());
		System.out.println("当前Service进程ID:"+android.os.Process.myPid());
		return mb;//返回的是一个Binder类(子类)的对象
	}
	//该类继承自Binder类，Binder类实现了Ibinder接口，所以该类也就实现了该Ibinder接口
	public class myBinder extends Binder//myBinder是Binder的子类，Binder又可以看做是Ibinder的子类，myBinder就是Ibinder的子类了。
	{
		public void getdata()
		{
			try
			{
				System.out.println("binder transfer the data");
				System.out.println("当前Service进程ID:"+android.os.Process.myPid());
				Thread.sleep(10000);//模拟耗时操作
			} catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
}
