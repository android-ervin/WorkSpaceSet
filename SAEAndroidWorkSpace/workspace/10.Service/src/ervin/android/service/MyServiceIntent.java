package ervin.android.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.IntentService;
import android.content.Intent;

public class MyServiceIntent extends IntentService
{

	public MyServiceIntent()
	{
		super("myServiceintent");
		// TODO Auto-generated constructor stub
	}
//	public MyServiceIntent(String name)
//	{
//		super(name);
//	}

	@Override
	protected void onHandleIntent(Intent arg0)
	{
		// TODO Auto-generated method stub
		//System.out.println("当前Service进程ID:"+android.os.Process.myPid());
		//System.out.println("当前线程ID："+Thread.currentThread().getId());
		/***模拟带参数访问一个本地网页的耗时操作*/
		String basepath="http://10.0.2.2:81/Android.aspx";
		String path=basepath+"?"+"name=ervin"+"&age=13";
		URL url=null;
		InputStream inputStream=null;
		try
		{
			url=new URL(path);
		} catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			HttpURLConnection connection=(HttpURLConnection) url.openConnection();
			inputStream=connection.getInputStream();
			
			BufferedReader bReader=new BufferedReader(new InputStreamReader(inputStream));
			String result="";
			String line="";
			while((line=bReader.readLine())!=null)
				result+=line;
			System.out.println(result);
			inputStream.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
