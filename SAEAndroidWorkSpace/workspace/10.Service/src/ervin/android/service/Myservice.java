package ervin.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class Myservice extends Service
{

	@Override
	public IBinder onBind(Intent arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate()
	{
		System.out.println("onCreate");
		super.onCreate();
	}

	@Override
	public void onDestroy()
	{
		System.out.println("onDestroy");
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		
		try
		{
			System.out.println("onStartCommand");
			System.out.println("MYservice线程："+Thread.currentThread().getId());
			Thread.sleep(10000);//模拟耗时操作，线程延时10s
			stopService(intent);
			System.out.println("Service结束");
		} catch (InterruptedException e)
		{
		
		}
		return START_STICKY;
	}
	

}
