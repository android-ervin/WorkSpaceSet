package ervin.android.bluetooth;

import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class BluetoothActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn1=null;
	private Button btn2=null;
	BluetoothAdapter adapter=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        //获得本地蓝牙设备对象
        adapter=BluetoothAdapter.getDefaultAdapter();
        //注册一个广播。用来接收找到的蓝牙设备
        IntentFilter bluetoothFilter=new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(new Bluetoothcast(), bluetoothFilter);
        
        btn1=(Button)findViewById(R.id.button1);
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				if(adapter!=null)
				{
					System.out.println("本机拥有蓝牙设备");
					//判断蓝牙是否打开，如果没有打开提示用户打开
					if(!adapter.isEnabled())
					{
						Intent intent=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						startActivity(intent);//系统自动调用的activity
					}
					//获得与本地蓝牙设备已经绑定的所有远程蓝牙设备的集合
					Set<BluetoothDevice> devices = adapter.getBondedDevices();
					//将远程蓝牙设备迭代出来
					for (Iterator iterator = devices.iterator(); iterator
							.hasNext();)
					{
						BluetoothDevice bluetoothDevice = (BluetoothDevice) iterator
								.next();
						System.out.println(bluetoothDevice.getName());
					}
				}
				else {
					System.out.println("本机无蓝牙设备");
				}
			}
		});
        
        btn2=(Button)findViewById(R.id.button2);
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				//开始扫描周围的蓝牙设备，发现一个设备系统就会发送一个广播出去
				adapter.startDiscovery();
			}
		});
    }
    
    public class Bluetoothcast extends BroadcastReceiver
    {

		@Override
		public void onReceive(Context context, Intent intent)
		{
			// TODO Auto-generated method stub
			//获得搜到的蓝牙设备对象
			BluetoothDevice device=intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			System.out.println("发现的设备名称是：" + device.getName());
		}
    	
    }
}