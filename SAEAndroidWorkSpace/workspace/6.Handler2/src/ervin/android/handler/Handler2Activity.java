package ervin.android.handler;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;


public class Handler2Activity extends Activity {
    /** Called when the activity is first created. */
	private Handler handler;
	private Handler handler1;
	ProgressBar pBar=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        new myThread().start();
        
		
        
        pBar=(ProgressBar)findViewById(R.id.progressBar1);
        Button btn1=(Button)findViewById(R.id.button1);
        btn1.setOnClickListener(new OnClickListener()
		{
			int i=0;
			public void onClick(View arg0)
			{
				i+=10;
				handler =new myHandler();
				Message msg=new Message();
				//Message msg=handler.obtainMessage();
				msg.arg1=i;
				handler.sendMessage(msg);
			}
		});
        
        Button btn2=(Button)findViewById(R.id.button2);
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				Message msg1=Message.obtain();
				msg1.arg1=10;
				handler1.sendMessage(msg1);
			}
		});
        
        Button btn3=(Button)findViewById(R.id.button3);
        btn3.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				testThread thread=new testThread();
				thread.start();
				
			}
		});
    }
    
    class myHandler extends Handler
    {

		@Override
		public void handleMessage(Message msg)
		{
			// TODO Auto-generated method stub
			System.out.println("当前handlemessage线程ID："+Thread.currentThread().getName());
			pBar.setProgress(msg.arg1);
			super.handleMessage(msg);
		}	
    }
    myHandler handler3=new myHandler();
    class myThread extends Thread
    {
    	@Override
    	public void run()
    	{
    		// TODO Auto-generated method stub
    		System.out.println("当前线程2ID："+Thread.currentThread().getName());
    		Looper.prepare();//创建准备一个Looper
    		handler1=new Handler()//注意这种内部类的写法
			{
//    			int i=0;
				@Override
				public void handleMessage(Message msg)
				{
		
					pBar.setProgress(msg.arg1+pBar.getProgress());
					
//					System.out.println("第"+i+"次执行");
					System.out.println("当前handmessage线程2ID："+Thread.currentThread().getName());
//					super.handleMessage(msg);
					
				}
		
			};
			Looper.loop();
    	
    	}
    }
    
    class testThread extends Thread
    {
    	@Override
    	public void run()
    	{
    		System.out.println("当前线程3名字："+Thread.currentThread().getName());
    		Runnable r =new Runnable()
			{
				
				public void run()//运行在主线程中
				{
					System.out.println("当前线程3名字："+Thread.currentThread().getName());
					pBar.setProgress(10+pBar.getProgress());
				}
			};
			
			handler3.post(r);
    	}
    }
}