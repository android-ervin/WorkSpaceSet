package ervin.android.location;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LocationActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn=null;
	private TextView tvlong=null;
	private TextView tvlat=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        btn=(Button)findViewById(R.id.button1);
        tvlong=(TextView)findViewById(R.id.textView3);
        tvlat=(TextView)findViewById(R.id.textView4);
        
        btn.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				//获得操作位置类的对象
				System.out.println("1----->" + Thread.currentThread().getName());
				//LocationManager locationManager=(LocationManager) getSystemService(LOCATION_SERVICE);
				LocationManager locationManager=(LocationManager) LocationActivity.this.getSystemService(Context.LOCATION_SERVICE);
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new myLocationListener());
			}
		});
    }
    
    private class myLocationListener implements LocationListener
    {

		public void onLocationChanged(Location location)
		{
			// TODO Auto-generated method stub
			tvlong.setText((int) location.getLongitude());
			tvlat.setText((int)location.getLatitude());
			System.out.println("longitude:" + location.getLongitude());
			System.out.println("latitude:" + location.getLatitude());
			System.out.println("2----->" + Thread.currentThread().getName());
			
		}

		public void onProviderDisabled(String arg0)
		{
			// TODO Auto-generated method stub
			
		}

		public void onProviderEnabled(String arg0)
		{
			// TODO Auto-generated method stub
			
		}

		public void onStatusChanged(String arg0, int arg1, Bundle arg2)
		{
			// TODO Auto-generated method stub
			
		}
    	
    }
}