package ervin.android.handler;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class HandlerActivity extends Activity {
    /** Called when the activity is first created. */
    Button btn=null;
    Button btn1=null;
    ProgressBar pgBar=null;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        btn=(Button)findViewById(R.id.button1);
        btn1=(Button)findViewById(R.id.button2);
        pgBar=(ProgressBar)findViewById(R.id.progressBar1);
        btn.setOnClickListener(new btnoncliclistener());
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				Intent intent=new Intent(HandlerActivity.this,ThreadActivity.class);
				startActivity(intent);
				
				
			}
		});
       
    }
    /*第一种方法，使用handler*/
    class btnoncliclistener implements OnClickListener
    {

		public void onClick(View arg0)
		{
			pgBar.setVisibility(View.VISIBLE);
			handler.post(r);
			System.out.println("当前主线程ID："+Thread.currentThread().getId());
		}
	}
    
    myhandler handler=new myhandler();
    runnable r=new runnable();
    
    
    class myhandler extends Handler
    {
		//Subclasses must implement this to receive messages.
		public void handleMessage(Message msg)
		{
			pgBar.setProgress(msg.arg1);//通过获得的消息去设置进度条
			
			/*Causes the Runnable r to be added to the message queue. 
			The runnable will be run on the thread to which this handler is attached.
			其并没有去创建一个新的线程，而只是去执行run方法*/
			
			/*解决不能从消息队列中移除Runnable对象的bug。handler中的Post和sendmessage在源码中都是调用的
			 * sendMessageDelayed这个方法（查看源码）*/
			if(msg.arg1<=100)
				handler.post(r);//再次执行runnable中的run方法
			else
				handler.removeCallbacks(r);	
		}
		
    }
    class runnable implements Runnable
    {
    	int i=0;
		public void run()
		{
			i+=10;
			//Message msg=handler.obtainMessage();
			Message msg=new Message();
			msg.obj=i;
			msg.arg1=i;
						
			try 
			{
				System.out.println("当前Runnable线程ID："+Thread.currentThread().getId());
				Toast.makeText(HandlerActivity.this, Thread.currentThread().getName(), Toast.LENGTH_LONG).show();
				Thread.sleep(1000);
			} catch (Exception e)
			{
				
			}
			handler.sendMessage(msg);//将消息加入到当前线程的消息队列中。此时主线程的looper会一直维护管理调用消息队列
			if(i==100)
				handler.removeCallbacks(r);
		}
    	
    }
    
}