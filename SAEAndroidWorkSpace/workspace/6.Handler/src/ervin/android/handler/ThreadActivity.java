package ervin.android.handler;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class ThreadActivity extends Activity
{

	Button btn=null;
	ProgressBar pgBar=null;
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);
		
		btn=(Button)findViewById(R.id.button3);
		pgBar=(ProgressBar)findViewById(R.id.progressBar2);
		
		btn.setOnClickListener(new OnClickListener()
		{
			/*不能在UI线程外的线程去操作UI*/
			public void onClick(View arg0)
			{
				pgBar.setVisibility(View.VISIBLE);
				myhandler.post(r);
				new Thread(new Runnable()
				{
					
					public void run()
					{
						
						System.out.println("4,当前线程："+Thread.currentThread().getId());
						
					}
				}).start(); //开启新线程
				System.out.println("1,当前线程："+Thread.currentThread().getId());
				myhandler2.post(r2);
//				new Thread(new Runnable()
//				{
//					int i=0;
//					public void run()
//					{
//						i+=10;
//						Message msg=myhandler.obtainMessage();
//						msg.arg1=i;
//						
//						try
//						{
//							Thread.sleep(1000);
//						} catch (Exception e)
//						{
//							// TODO: handle exception
//						}
//						myhandler.sendMessage(msg);//将消息加入到当前线程的消息队列中。此时主线程的looper会一直维护管理调用消息队列
//						if(i==100)
//						{
//							
//						}
//							//myhandler.removeCallbacks(r);
//					}
//				}).start();
			}
			
			
		});
		
						
	}
	/*1.每一个handler只能在消息队列中接收自己的消息（由消息队列中的what属性开标定到底这条消息是属于哪个handler）
	 * 2.一般情况下，每一个线程都会有一个消息队列，但是不一定每一个消息队列都会有一个Looper
	 * 3.*/
	myHandler myhandler=new myHandler();
	myHandler2 myhandler2=new myHandler2();
	
	myrunnable r=new myrunnable();
	myrunnable2 r2=new myrunnable2();

	
	class myHandler extends Handler
	{
		/*获得消息 与sendmessage对应*/
		public void handleMessage(Message msg)
		{
			pgBar.setProgress(msg.arg1);//通过获得的消息去设置进度条
			if(msg.arg1<=100)
				myhandler.post(r);
			else {
				myhandler.removeCallbacks(r);
				myhandler2.removeCallbacks(r2);
			}
		}
		
	}
	class myHandler2 extends Handler
	{

		@Override
		public void handleMessage(Message msg)
		{
			myhandler2.post(r2);
			Toast.makeText(ThreadActivity.this, msg.getData().getString("ervin"), Toast.LENGTH_SHORT).show();
			
		}
		
	}
	class myrunnable2 implements Runnable
	{
		int j=0;
		public void run()
		{
			j+=1;
			Message msgtest=myhandler2.obtainMessage();
			Bundle bd=new Bundle();
			bd.putString("ervin", "come on ervin");
			msgtest.setData(bd);
			
			try
			{
				System.out.println("3.当前线程："+Thread.currentThread().getId());
				//Thread.sleep(1000);
			} catch (Exception e)
			{
				// TODO: handle exception
			}
			
			
			if(j==10)
				myhandler2.removeCallbacks(r2);
			else {
				myhandler2.sendMessage(msgtest);
			}
		}
		
	}
	
	class myrunnable implements Runnable
	{
		int i=0;
		public void run()
		{
			
			
			
			i+=10;
			Message msg=myhandler.obtainMessage();
			msg.arg1=i;
			/*不能在UI线程外的线程去操作UI*/
//			pgBar.setVisibility(View.VISIBLE);
			try
			{
				//Toast.makeText(ThreadActivity.this, Thread.currentThread().getName(), Toast.LENGTH_LONG).show();
				System.out.println("2.当前线程："+Thread.currentThread().getId());
				Thread.sleep(1000);
			} catch (Exception e)
			{
				// TODO: handle exception
			}
			myhandler.sendMessage(msg);//将消息加入到当前线程的消息队列中。此时主线程的looper会一直维护管理调用消息队列
			
			if(i==100)
			{
				myhandler.removeCallbacks(r);
			}
			
		}
		
	}
	
	
}
