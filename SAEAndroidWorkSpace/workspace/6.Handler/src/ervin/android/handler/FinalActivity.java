package ervin.android.handler;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

public class FinalActivity extends Activity
{

	/*在另一个线程中使用handler，真正去异步处理消息队列中的消息（handler本身是没有开启线程的）
	 * 使用到handlerThread类，该类中会有looper对象去循环处理消息队列中的消息（既可以处理主线程中的消息，也可以处理子线程中的消息）*/
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		System.out.println("1.Activity-->"+Thread.currentThread().getName());
		HandlerThread handlerThread=new HandlerThread("ervin-handlerthread");
		handlerThread.start();
		
		myhandler mh=new myhandler(handlerThread.getLooper());
		Message msg=mh.obtainMessage();
		//msg.sendToTarget();
		mh.sendMessage(msg);
		
	}
	class myhandler extends Handler
	{
		public myhandler(Looper looper)
		{
			super(looper);//调用父类的构造函数
		}
		@Override
		public void handleMessage(Message msg)
		{
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			System.out.println("2.Thread-->"+Thread.currentThread().getName());
		}
		
	}
	
}
