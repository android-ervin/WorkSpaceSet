package ervin.android.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqliteHelper extends SQLiteOpenHelper
{
	private static final int version=1;
	public SqliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
	{
		super(context,name,factory,version);
	}
	public SqliteHelper(Context context, String name, int version)
	{
		this(context,name,null,version);
	}
	public SqliteHelper(Context context, String name)
	{
		this(context, name, version);
	}
	
	//Called when the database is created for the first time
	//This is where the creation of tables and the initial population of the tables should happen.
	public void onCreate(SQLiteDatabase db)
	{
		// TODO Auto-generated method stub
		System.out.println("create a sqlite database");
		//db.execSQL("create table user(id int,name varchar(20))");
		//Execute a single SQL statement that is NOT a SELECT or any other SQL statement that returns data.
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2)
	{
		// TODO Auto-generated method stub
		System.out.println("update a sqlite database");
	}
}