package ervin.android.sqlite;

import android.R.integer;
import android.R.string;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;


public class SqliteActivity extends Activity {
    /** Called when the activity is first created. */
	private static int i=1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
//        SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase");
//        sqh.getReadableDatabase();//将会调用SqliteHelper中的oncreate方法
        
        /*oncreate 当数据库第一次被创建执行时，会调用slqiteopenhelper中的oncreate方法*/
        findViewById(R.id.button1).setOnClickListener(new OnClickListener()
		{  
			
			public void onClick(View arg0)
			{
				SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase");
		        sqh.getReadableDatabase();//将会调用SqliteHelper中的oncreate方法
			}
		});
        
        
        /*databaseupdate 当数据库中的版本号或者其他需要本更新的时候会调用slqiteopenhelper中的onupgrade方法*/
        findViewById(R.id.button4).setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase",2);
				SQLiteDatabase db=sqh.getReadableDatabase();
			}
		});
        
        /*向数据库中的某一张表插入一条数据*/
        findViewById(R.id.button2).setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				ContentValues cValues=new ContentValues();
				cValues.put("id",i++);
				cValues.put("name","ervin");
				
				SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase");
				SQLiteDatabase db=sqh.getReadableDatabase();
				db.insert("user",null,cValues);
				System.out.println("insert to database");
			}
		});
        
        
        /*tableUpdate 更新数据库中的某一张数据表，同时会调用slqiteopenhelper中的onupgrade方法*/
        findViewById(R.id.button5).setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				ContentValues cValues=new ContentValues();
				cValues.put("name", "ervinzhang");
				
				SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase");
				SQLiteDatabase db=sqh.getReadableDatabase();
				db.update("user", cValues, "id=1", null);
				System.out.println("update to database");
			}
		});
        
        /*查询语句，用游标一行一行按查询要求查找输出*/
        findViewById(R.id.button3).setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase");
				SQLiteDatabase db=sqh.getReadableDatabase();
				
				/*Move the cursor to the next row. 
		This method will return false if the cursor is already past the last entry in the result set.
				*/
				Cursor cursor=db.query("user", new String[]{"id","name"}, "id=1", null, null, null, "id");
				while(cursor.moveToNext())
				{
					String name=cursor.getString(cursor.getColumnIndex("name"));
					System.out.println("query-->"+name);
				}
				cursor.close();
			}
		});
        
        /*新建表格*/
        findViewById(R.id.button6).setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase");
				SQLiteDatabase db=sqh.getReadableDatabase();
				String sql="create table user(id int,name varchar(20))";
				if(!tableisexist("user"))
				{
					db.execSQL(sql);
					System.out.println("create a table");
				}
				else {
					System.out.println("already have the table");
				}
				
			}
		});
    }
    
    public boolean tableisexist(String tablename)
    {
    	boolean result=false;
    	if (tablename==null)
    		result=false;
    	try
		{
			Cursor cursor=null;
			SqliteHelper sqh=new SqliteHelper(SqliteActivity.this, "sqldatabase");
			SQLiteDatabase db=sqh.getReadableDatabase();
			
			String sql="select count(*) as c from sqlite_master where type='table' and name='"+tablename+"'";
			cursor=db.rawQuery(sql, null);//返回表的第一行游标which is positioned before the first entry
			if(cursor.moveToNext())
			{
				//int count=cursor.getCount();//Returns the numbers of rows in the cursor
				int count=cursor.getInt(0);//Returns the value of the requested column as an int.
				if(count>0)
					result=true;
			}
			
			
		} catch (Exception e)
		{
			// TODO: handle exception
		}
		return result;
    }
}