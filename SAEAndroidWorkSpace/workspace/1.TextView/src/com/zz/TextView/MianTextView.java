package com.zz.TextView;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle; 
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

public class MianTextView extends Activity 
{
    /* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
       /* TextView tv=new TextView(this); //指向MianTextView那个对象。
        tv.setText("你好");
        setContentView(tv);*/
        
        /*TextView tv=(TextView)findViewById(R.id.tv);
        tv.setText(Html.fromHtml("我要学好安卓系统，<font color=blue>努力工作享受生活</font>，安卓教程"));    //注意格式和方法名
                */
        TextView tView=(TextView)findViewById(R.id.textView1);
        String text="Call me";
        SpannableString span=new SpannableString(text);
        span.setSpan(new ClickableSpan()
		{
			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				String s="13423095237";
				Intent intent=new Intent();
				intent.setAction(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:"+ s));
				startActivity(intent);
			}
		}, 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tView.setText(span);
        //单击链接时凡是有动作要发生时，都要调用MovementMethod方法，如果只是设置属性如背景颜色等，就不需要调用了
        tView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}