package com.nuchina
{
	import com.nuchina.controller.AreaNavUpdataCommand;
	import com.nuchina.controller.CategoryNavUpdataCommand;
	import com.nuchina.controller.ChangeSupervisorMode;
	import com.nuchina.controller.GetAreaListCommand;
	import com.nuchina.controller.GetUrgencyDetail;
	import com.nuchina.controller.LoadArchivesDataCommand;
	import com.nuchina.controller.LoginStateUpdataCommand;
	import com.nuchina.controller.OpenArchivesCommand;
	import com.nuchina.controller.ShowLoginWinCommand;
	import com.nuchina.controller.StartFlagXY;
	import com.nuchina.controller.StartupCommand;
	
	import org.puremvc.as3.patterns.facade.Facade;
	
	public class AppFacade extends Facade
	{
		// Notification name constants
		public static const STARTUP:String 					= "startup";
		public static const ENTERPRISE_LISE_UPDATA:String		= "enterpriseListUpdata";
		public static const ENTERPRISE_AMOUNT_UPDATA:String	= "enterpriseAmountUpdata";
		public static const ENTERPRISE_INFO_UPDATA:String		= "enterpriseInfoUpdata";
		public static const URGENCY_INFO_UPDATA:String		= "urgencyInfoUpdata";
		public static const ENTERPRISE_COORD_UPDATA:String	= "coordUpdata";
		public static const MAP_POSITION_EVENT:String			= "mapPositionEvent";
		//performance
		public static const SUPERVISOR_UPDATA:String			= "SupervisorUpdata";
		public static const PERFORMANCE_UPDATA:String			= "performanceUpdata";
		public static const PATROL_INFO_UPDATA:String			= "patrolInfoUpdata";
		public static const PATROL_EVENT_UPDATA:String		= "patrolEventUpdata";
		public static const TRACK_COORD_UPDATA:String			= "trackCoordUpdata";
		public static const SUPERVISOR_ENTE_UPDATA:String		= "supervisorEnteUpdata";
		/**
		 * 显示执法路线详细信息 
		 */		
		public static const SHOW_PERFOR_INFO:String			= "SHOW_PERFOR_INFO";
		//client
		public static const LOGIN_STATE_UPDATA:String			= "loginStateUpdata";
		public static const GET_COMPLAINT_SUCCESS:String		= "getComplaintSuccess";
		public static const SUBMIT_COMPLAINT_SUCCESS:String	= "submitComplaintSuccess";
		
		
		
		public static const SHOW_ENTERPRISE_INFO:String		= "showEnterpriseINfo";
		
		public static const SHOW_RECORDlIST:String			= "showRecordList";
		
		public static const SHOW_LOGIN_WIN:String				= "showLoginWin";
		
		public static const SHOW_ADV_SEARCH_VIEW:String		= "showADVSearchView";
		
		public static const START_CATEGORY_SEARCH:String		= "startCategorySearch";
		
		public static const OPEN_DOCS_WIN:String 				= "openDocsWin";
		public static const LOAD_ARCHIVES_DATA:String			= "loadArchivesData";
		
		public static const GET_AREA_LIST:String 				= "GET_AREA_LIST";
		
		//mode
		public static const CHANGE_SUPERVISOR_MODE:String		= "CHANGE_SUPERVISOR_MODE";
		
		//urgency
		/**
		 *  当前应急事件
		 */		
		public static const URGENCY_LIST_UPDATA:String		= "URGENCY_LIST_UPDATA";
		/**
		 * 历史应急事件 
		 */		
		public static const HISTORY_LIST_UPDATA:String		= "HISTORY_LIST_UPDATA";
		/**
		 * 显示应急信息窗口 
		 */		
		public static const SHOW_URGENCY_INFO:String			= "SHOW_URGENCY_INFO";
		/**
		 * 显示应急详细内容窗口 
		 */		
		public static const SHOW_URGENCY_WIN:String			= "SHOW_URGENCY_WIN";
		/**
		 * 获取应急详情信息 
		 */		
		public static const GET_URGENCY_DETAIL:String			= "GET_URGENCY_DETAIL";
		
		public static const GET_ENTERPRISE_POWER:String		= "getEnterprisePower";
		public static const GET_ENTERPRISE_X_Y:String			= "getEnterpriseXY";
		public static const START_FLAG_X_Y:String				= "startFlagXY";
		public static const SAVE_FLAG_X_Y:String				= "saveFlagXY";
		
		public static const AREA_NAV_UPDATA:String			= "areNavUpdata";
		public static const CATEGORY_NAV_UPDATA:String		= "categoryNavUpdata";
		
		public static const QUERY_MAP_X_Y:String 				= "queryMapXY";
		
		public static const GET_NOTE:String					= "getNote";
		/**
		 * Singleton ApplicationFacade Factory Method
		 */
		public static function getInstance() : AppFacade 
		{
			if ( instance == null ) instance = new AppFacade( );
			return instance as AppFacade;
		}
		
		/**
		 * Register Commands with the Controller 
		 */
		override protected function initializeController( ) : void 
		{
			super.initializeController(); 
			registerCommand( STARTUP, StartupCommand );
			registerCommand(LOGIN_STATE_UPDATA, LoginStateUpdataCommand);
			registerCommand(SHOW_LOGIN_WIN, ShowLoginWinCommand);
			registerCommand(OPEN_DOCS_WIN, OpenArchivesCommand);
			registerCommand(LOAD_ARCHIVES_DATA, LoadArchivesDataCommand);
			registerCommand(CHANGE_SUPERVISOR_MODE, ChangeSupervisorMode);
			registerCommand(GET_AREA_LIST, GetAreaListCommand);			
			registerCommand(GET_URGENCY_DETAIL, GetUrgencyDetail);
			registerCommand(AREA_NAV_UPDATA, AreaNavUpdataCommand);
			registerCommand(CATEGORY_NAV_UPDATA, CategoryNavUpdataCommand);
			registerCommand(AppFacade.START_FLAG_X_Y, StartFlagXY);
		}
		
		public function startup( app:Main ):void
		{
			sendNotification( STARTUP, app );
		}
	}
}