package com.nuchina.model
{
	import flash.utils.Dictionary;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-8-20
	 */
	public class CategoryDataProxy extends Proxy
	{
		public static const NAME:String = "CategoryDataProxy";
		
		protected var _dic:Dictionary = new Dictionary();
		public function CategoryDataProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		public function addList(key:String, value:Array):void
		{
			_dic[key] = value;
		}
		
		public function getList(key:String):Array
		{
			return _dic[key];
		}
		
		public function hasList(key:String):Boolean
		{
			return _dic[key];
		}
	}
}