package com.nuchina.model
{
	/**
	 * @author Ykk
	 * 2012-5-23
	 */
	public class AppConfig
	{
		public static var server:String = "{server.name}";
		public static var port:String = "{server.port}";
		public static var virtualPath:String = "";
		public static var mapBaseUrl:String = "";
		public static var wsdlUrl:String = "";
		
	}
}