package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.ResultEvent;

	/**
	 * @author Ykk
	 * 2012-6-26
	 */
	public class PopUpWindowsProxy extends AbstractDataProxy
	{
		public static const NAME:String = "PopUpWindowsProxy";
		public function PopUpWindowsProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		private var _dynamicLeve:ArrayCollection;

		public function get dynamicLeve():ArrayCollection
		{
			return _dynamicLeve;
		}

		public function set dynamicLeve(value:ArrayCollection):void
		{
			_dynamicLeve = value;
		}

		
		/**
		 * 
		 * 
		 * 	获取餐饮服务食品安全监督量化动态等级A
			用    途：	查询餐饮服务食品安全监督量化动态等级
			输入参数：	“getDynamicLeve” | usertype|username|QYBHID|password |ssqbh
			参数说明：	方法字符串|用户类型|企业编号ID| 密码|所属区编号
			返 回 值：	企业餐饮服务食品安全监督量化动态等级表
			
			
			
			获取餐饮服务食品安全监督量化动态等级B
			用    途：	查询餐饮服务食品安全监督量化动态等级
			输入参数：	“getDynamicLeve” |usertype|QYBHID|password |ssqbh
			参数说明：	方法字符串|用户类型|企业编号ID| 密码|所属区编号
			返 回 值：	企业餐饮服务食品安全监督量化动态等级表
			@param args
		 */		
		public function getDynamicLeve(...args):void
		{
			var params:String = getParamsCode.apply(null, args);
			sendWebService("ProcessData", getDynamicLeveResult, fault, getParamsCode("getDynamicLeve", params));
		}
		
		private function getDynamicLeveResult(evt:ResultEvent):void
		{
			var jsonObj:Object = JSON.decode(evt.result.toString());
			if(jsonObj.success==1)
			{
				if(jsonObj.data)
					dynamicLeve = new ArrayCollection(jsonObj.data);
			}
			else if(jsonObj.success==0)
			{
				Alert.show("获取列表失败!", "提示");
			}
			else 
			{
				Alert.show('获取数据失败！',"提示");
			}
		}
	}
}