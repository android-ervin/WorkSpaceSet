package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-6-21
	 */
	public class CategorySearchParam implements IKeyword
	{
		private var _keyword:String;
		public function CategorySearchParam()
		{
		}
		
		public function get keyword():String
		{
			return _keyword;
		}
		
		public function set keyword(value:String):void
		{
			_keyword = value;
		}
	}
}