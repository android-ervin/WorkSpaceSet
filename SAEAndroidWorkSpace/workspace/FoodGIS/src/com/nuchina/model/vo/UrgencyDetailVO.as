package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-8-1
	 */
	public class UrgencyDetailVO
	{
		public var index:int;
		public var id:String;
		public function UrgencyDetailVO(id:String, index:int)
		{
			this.id = id;
			this.index = index;
		}
	}
}