package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-7-9
		 * 	根据执法人编号查询绩效成功
		 输入参数：	“getPerformance” | username|password|ID|type
		 参数说明：	方法字符串|用户名|密码|执法编号|（月度“0”，季度“1”，年度“2”）
		 返 回 值：	执法人员绩效内容
		 格	式	：	{data:[{ valuation：良好
		 Coverage：覆盖率
		 Accuracy：准确率
		 Punish：  处罚率
		 Timely：	及时率
		 }
		 ,{…}
		 }		 
	 */
	[Bindable]
	public class PerformanceVo
	{		
		private var _valuation:String;

		public function get valuation():String
		{
			return _valuation;
		}

		public function set valuation(value:String):void
		{
			_valuation = value;
		}

		private var _coverage:String;

		public function get coverage():String
		{
			return _coverage;
		}

		public function set coverage(value:String):void
		{
			_coverage = value;
		}

		private var _accuracy:String;

		public function get accuracy():String
		{
			return _accuracy;
		}

		public function set accuracy(value:String):void
		{
			_accuracy = value;
		}

		private var _punish:String;

		public function get punish():String
		{
			return _punish;
		}

		public function set punish(value:String):void
		{
			_punish = value;
		}

		private var _timely:String;

		public function get timely():String
		{
			return _timely;
		}

		public function set timely(value:String):void
		{
			_timely = value;
		}

		
		public function PerformanceVo(valuation:String="", coverage:String="", accuracy:String="", punish:String="", timely:String="")
		{			
			this.valuation = valuation;
			this.coverage = coverage;
			this.accuracy = accuracy;
			this.punish = punish;
			this.timely = timely;
		}
	}
}