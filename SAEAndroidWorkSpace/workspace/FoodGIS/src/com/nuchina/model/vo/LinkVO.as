package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-8-18
	 */
	public class LinkVO
	{
		public var label:String;
		public var code:String;
		public function LinkVO(label:String, data:String)
		{
			this.label = label;
			this.code = data;
		}
	}
}