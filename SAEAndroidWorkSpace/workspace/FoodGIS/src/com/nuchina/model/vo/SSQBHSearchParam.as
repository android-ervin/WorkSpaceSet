package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-8-6
	 */
	public class SSQBHSearchParam implements IKeyword
	{
		private var _keyword:String;
		public function SSQBHSearchParam()
		{
		}
		
		public function get keyword():String
		{
			return _keyword;
		}
		
		public function set keyword(value:String):void
		{
			_keyword = value;	
		}
	}
}