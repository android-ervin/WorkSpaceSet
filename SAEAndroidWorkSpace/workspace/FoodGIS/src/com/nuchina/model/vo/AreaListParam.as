package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-7-28
	 */
	public class AreaListParam
	{
		public var areaCode:String;
		public var callback:Function;
		public function AreaListParam(areaCode:String, callback:Function)
		{
			this.areaCode = areaCode;
			this.callback = callback;
		}
	}
}