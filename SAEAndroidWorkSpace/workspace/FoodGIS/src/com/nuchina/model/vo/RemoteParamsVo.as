package com.nuchina.model.vo
{
	public class RemoteParamsVo
	{
		public function RemoteParamsVo(destination:String=null, source:String=null, endpoint:String=null)
		{
			this.destination = destination;
			this.source = source;
			this.endpoint = endpoint;
		}
		public var source:String;
		public var destination:String;
		public var endpoint:String;
	}
}