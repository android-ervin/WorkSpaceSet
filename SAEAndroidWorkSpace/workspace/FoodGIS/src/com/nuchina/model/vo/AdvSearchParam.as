package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-6-21
	 */
	public class AdvSearchParam implements IKeyword, IAdvValue
	{
		protected var _keyword:String;
		protected var _foodType:String;
		protected var _allowField:String;
		protected var _safeLevel:String;
		protected var _areaCode:String;
		public function AdvSearchParam()
		{
		}
		
		public function get keyword():String
		{
			return _keyword;
		}
		
		public function set keyword(value:String):void
		{
			_keyword = value;
		}
		
		public function get allowField():String
		{
			return _allowField;
		}
		
		public function set allowField(value:String):void
		{
			_allowField = value;
		}
		
		public function get foodType():String
		{
			return _foodType;
		}
		
		public function set foodType(value:String):void
		{
			_foodType = value;
		}
		
		public function get safeLevel():String
		{
			return _safeLevel;
		}
		
		public function set safeLevel(value:String):void
		{
			_safeLevel = value;
		}
		
		public function get areaCode():String
		{
			return _areaCode;
		}
		
		public function set areaCode(value:String):void
		{
			_areaCode = value;
		}
		
		
	}
}