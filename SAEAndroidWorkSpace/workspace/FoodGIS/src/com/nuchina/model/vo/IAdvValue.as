package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-6-21
	 */
	public interface IAdvValue
	{
		function get foodType():String;
		function set foodType(value:String):void;
		function get allowField():String;
		function set allowField(value:String):void;
		function get safeLevel():String;
		function set safeLevel(value:String):void;
		function get areaCode():String;
		function set areaCode(value:String):void;
	}
}