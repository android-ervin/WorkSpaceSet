package com.nuchina.model.vo
{
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayList;

	/**
	 * @author Ykk
	 * 2012-6-20
	 */
	[Bindable]
	public class NavVo
	{
		private var _rootName:String;
		private var _enableField:String;
		private var _labelField:String;
		private var _codeLength:int;
		private var _codeField:String;
		private var _data:Dictionary;
		public function NavVo()
		{
		}

		/**
		 * 是否可互动字段 
		 */
		public function get enableField():String
		{
			return _enableField;
		}

		/**
		 * @private
		 */
		public function set enableField(value:String):void
		{
			_enableField = value;
		}

		/**
		 * 根节点名称 
		 */
		public function get rootName():String
		{
			return _rootName;
		}

		/**
		 * @private
		 */
		public function set rootName(value:String):void
		{
			_rootName = value;
		}

		/**
		 * 标签名字段 
		 */
		public function get labelField():String
		{
			return _labelField;
		}

		/**
		 * @private
		 */
		public function set labelField(value:String):void
		{
			_labelField = value;
		}

		/**
		 * 导航code长度 
		 */
		public function get codeLength():int
		{
			return _codeLength;
		}

		/**
		 * @private
		 */
		public function set codeLength(value:int):void
		{
			_codeLength = value;
		}

		/**
		 * 导航code字段 
		 */
		public function get codeField():String
		{
			return _codeField;
		}

		/**
		 * @private
		 */
		public function set codeField(value:String):void
		{
			_codeField = value;
		}

		/**
		 * 导航数据
		 */
		public function get data():Dictionary
		{
			return _data;
		}

		/**
		 * @private
		 */
		public function set data(value:Dictionary):void
		{
			_data = value;
		}


	}
}