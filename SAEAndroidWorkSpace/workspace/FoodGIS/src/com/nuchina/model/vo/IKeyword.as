package com.nuchina.model.vo
{
	/**
	 * @author Ykk
	 * 2012-6-21
	 */
	public interface IKeyword
	{
		function get keyword():String;
		function set keyword(value:String):void;
	}
}