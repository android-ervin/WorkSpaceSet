package com.nuchina.model
{
	import flash.utils.Dictionary;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-7-28
	 */
	public class AreaDataProxy extends Proxy implements IProxy
	{
		public static const NAME:String = "AreaDataProxy";
		
		protected var _dic:Dictionary = new Dictionary();
		public function AreaDataProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		public function addList(key:String, value:Array):void
		{
			_dic[key] = value;
		}
		
		public function getList(key:String):Array
		{
			return _dic[key];
		}
		
		public function hasList(key:String):Boolean
		{
			return _dic[key];
		}
	}
}