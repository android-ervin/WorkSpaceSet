package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.business.IService;
	import com.nuchina.model.business.RemoteObjectService;
	import com.nuchina.model.business.ServiceFactory;
	import com.nuchina.model.vo.RemoteParamsVo;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	import mx.controls.Alert;
	import mx.managers.CursorManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class AppSettingProxy extends Proxy
	{
		public static const NAME:String = "AppSettingProxy";
		private var _settingUrl:String = "assets/setting.xml";
		private var _callback:Function;
		private var _server:String = "{server.name}";

		public function get server():String
		{
			return _server;
		}

		public function set server(value:String):void
		{
			_server = value;
		}

		private var _port:String = "{server.port}";

		public function get port():String
		{
			return _port;
		}

		public function set port(value:String):void
		{
			_port = value;
		}

		private var _virtualPath:String = "";

		public function get virtualPath():String
		{
			return _virtualPath;
		}

		public function set virtualPath(value:String):void
		{
			_virtualPath = value;
		}


		public function get domain():String
		{
			var url:String = "http://"+server;
			if(port)
				url += ":"+port;
			if(virtualPath)
				url += "/"+virtualPath;
			return url;
		}

		private var _mapBaseUrl:String = "";

		public function get mapBaseUrl():String
		{
			return _mapBaseUrl;
		}

		public function set mapBaseUrl(value:String):void
		{
			_mapBaseUrl = value;
		}

		private var _wsdlUrl:String = "";

		public function get wsdlUrl():String
		{
			return _wsdlUrl;
		}

		public function set wsdlUrl(value:String):void
		{
			_wsdlUrl = value;
		}
		
		
		private var _remoteXML:XML;

		public function get remoteXML():XML
		{
			return _remoteXML;
		}

		public function set remoteXML(value:XML):void
		{
			_remoteXML = value;
		}

		public var amfServer:IService;
		
		public var webServer:IService;
		
		public function AppSettingProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		public function getSetting(callback:Function=null):void
		{
			_callback = callback;
			var urlloder:URLLoader = new URLLoader();
			urlloder.dataFormat = URLLoaderDataFormat.TEXT;
			urlloder.load(new URLRequest(_settingUrl));
			urlloder.addEventListener(Event.COMPLETE, onInitSetting);
			urlloder.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
		}
		
		private function onInitSetting(evt:Event):void
		{
			var urlloder:URLLoader = evt.currentTarget as URLLoader;
			urlloder.removeEventListener(Event.COMPLETE, onInitSetting);
			urlloder.removeEventListener(IOErrorEvent.IO_ERROR, onIoError);
			var xml:XML = new XML(URLLoader(evt.currentTarget).data);
			server = xml.server;
			port = xml.port;
			virtualPath = xml.virtualPath;
			mapBaseUrl = xml.mapBaseUrl;
			wsdlUrl = xml.webServer.@wsdl;
			_remoteXML = new XML(xml.remoteServer);
			if(_callback!=null)
				_callback();
		}
		
		private function onIoError(evt:IOErrorEvent):void
		{
			trace("无法加载 "+_settingUrl+" 配置文件");
		}
	}
}