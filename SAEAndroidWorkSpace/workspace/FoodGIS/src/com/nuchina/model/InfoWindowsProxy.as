package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.ResultEvent;

	/**
	 * @author Ykk
	 * 2012-6-26
	 */
	public class InfoWindowsProxy extends AbstractDataProxy
	{
		public static const NAME:String = "InfoWindowsProxy";
		public function InfoWindowsProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		private var _currentEInfo:Object;
		
		public function get currentEInfo():Object
		{
			return _currentEInfo;
		}
		
		public function set currentEInfo(value:Object):void
		{
			_currentEInfo = value;
			sendNotification(AppFacade.ENTERPRISE_INFO_UPDATA);
		}
		private var _alertLightDic:Dictionary;

		private var _currentUrgencyInfo:Object;

		/**
		 * 当前紧急信息 
		 */
		public function get currentUrgencyInfo():Object
		{
			return _currentUrgencyInfo;
		}

		/**
		 * @private
		 */
		public function set currentUrgencyInfo(value:Object):void
		{
			_currentUrgencyInfo = value;
			sendNotification(AppFacade.URGENCY_INFO_UPDATA);
		}

		
		/**
		 * 警报灯属性字典 
		 */
		public function get alertLightDic():Dictionary
		{
			return _alertLightDic;
		}

		/**
		 * @private
		 */
		public function set alertLightDic(value:Dictionary):void
		{
			_alertLightDic = value;
		}
		
		private var _patrolInfo:ArrayCollection;
		
		/**
		 * 当前路径 
		 * 			{blnr:"检查笔录内容",yjnr:"监督意见内容"},
		 {blnr:"检查笔录内容",yjnr:"监督意见内容"},
		 {blnr:"检查笔录内容",yjnr:"监督意见内容"},
		 {blnr:"检查笔录内容",yjnr:"监督意见内容"},
		 {blnr:"检查笔录内容",yjnr:"监督意见内容"}
		 */
		public function get patrolInfo():ArrayCollection
		{
			return _patrolInfo;
		}
		
		/**
		 * @private
		 */
		public function set patrolInfo(value:ArrayCollection):void
		{
			_patrolInfo = value;
			sendNotification(AppFacade.PATROL_INFO_UPDATA);
		}
		
		/**
		 * 
		 * @param id
		 * enterpriseName.text=_enterprisInfo.qymc;
		 topAddressTxt.text=_enterprisInfo.lxdz;
		 addressTxt.text=_enterprisInfo.lxdz;
		 allowFieldTxt.text=_enterprisInfo.xkfw;
		 gradeTxt.text=_enterprisInfo.grade;
		 yearGradeTxt.text=_enterprisInfo.yeargrade;
		 permitIDTxt.text=_enterprisInfo.xkzbh;
		 areaTxt.text=_enterprisInfo.ctmj;
		 numWorkerTxt.text=_enterprisInfo.cyrs;
		 numCustomerTxt.text=_enterprisInfo.jcrs;
		 ownerTxt.text=_enterprisInfo.qyfr;
		 policeTxt.text=_enterprisInfo.zfry;
		 */		
		public function getEInfoByID(id:String):void
		{
			sendWebService("ProcessData", getEInfoByIDResult, fault, getParamsCode("getEInfoByID", id));
		}
		
		private function getEInfoByIDResult(evt:ResultEvent):void
		{
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					if(obj.data is Array && obj.data.length > 0)
					currentEInfo = obj.data[0];
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		/**
		 * 	获取紧急处置弹窗信息
			用    途：	获取紧急处置弹窗信息
			输入参数：	“getUrgencyInfo” |ID
			参数说明：	方法字符串|紧急处置信息ID 
			返 回 值：	紧急信息内容
			 
		 * @param id
		 * 
		 */		
		public function getUrgencyInfo(id:String, index:int=0):void
		{
			sendWebService("ProcessData", getUrgencyInfoResult, fault, getParamsCode("getYjdetail", id, index));
		}
		
		private function getUrgencyInfoResult(evt:ResultEvent):void
		{
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					if(obj.data is Array && obj.data.length > 0)
						currentUrgencyInfo = obj.data[0];
					else
						currentUrgencyInfo = null;
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		
		
		/**
		 *	根据BSH来获得该特殊点的详细信息（检查笔录和监督意见）
		 输入参数：	“getTrackInfo” |BSH
		 参数说明：	方法字符串|标识号
		 返 回 值：	检查笔录内容，监督意见内容
		 格式: {data:[{blnr:111,yjnr:11},{blnr:111,yjnr:11}]} 
		 * @param timeStr
		 * 
		 */		
		public function getPatrolInfo(bsh:String):void
		{
			sendWebService("ProcessData", getPatrolInfoResult, fault, getParamsCode("getTrackInfo", bsh));
		}
		
		private function getPatrolInfoResult(evt:ResultEvent):void
		{			
			if (evt.result) 
			{
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					patrolInfo = new ArrayCollection(obj.data);
					if(patrolInfo && patrolInfo.length > 0)
						patrolInfoTime = patrolInfo.getItemAt(0).jckssj;
					else
						patrolInfoTime = "";
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}		
		
		public var patrolInfoTime:String;
	}
}