package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.business.IService;
	import com.nuchina.model.user.EnterpriseUserVO;
	import com.nuchina.model.user.LeaderUserVO;
	import com.nuchina.model.user.SuperviseUserVO;
	import com.nuchina.model.user.UserVO;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-6-24
	 */
	public class ClientStateProxy extends AbstractDataProxy
	{
		public static const NAME:String = "ClientStateProxy";
		
		/**
		 * 执法人员 
		 */
		public static const USER_TYPE_A:String = "A";
		/**
		 * 企业
		 */		
		public static const USER_TYPE_B:String = "B";
		/**
		 * 从业人员 
		 */		
		public static const USER_TYPE_C:String = "C";
		/**
		 * 社会公众 
		 */		
		public static const USER_TYPE_D:String = "D";
		/**
		 * 国家级领导干部
		 */		
		public static const USER_TYPE_E:String = "E";
		/**
		 * 省级领导干部
		 */		
		public static const USER_TYPE_F:String = "F";
		/**
		 * 市级领导干部
		 */		
		public static const USER_TYPE_G:String = "G";
		
		public function ClientStateProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		private var _mgrAreaList:ArrayCollection;
		[Bindable]
		public function get mgrAreaList():ArrayCollection
		{
			return _mgrAreaList;
		}
		
		public function set mgrAreaList(value:ArrayCollection):void
		{
			_mgrAreaList = value;
			for (var i:int = 1; i < value.length; i++) 
			{
				var item:Object = value[i];
				if(item.ssqmc)
					item.ssqmc = String(item.ssqmc).replace(/食品药品监督管理局/g, "");
			}
			
		}
		private var _leaderLevel:ArrayCollection = new ArrayCollection([{label:"省"},{label:"市"},{label:"区/县"}]);

		[Bindable]
		public function get leaderLevel():ArrayCollection
		{
			return _leaderLevel;
		}

		public function set leaderLevel(value:ArrayCollection):void
		{
			_leaderLevel = value;
		}

		
		private var _area:String;

		public function get area():String
		{
			return _area;
		}

		public function set area(value:String):void
		{
			_area = value;
		}

		
		public var tabTag:ArrayList = new ArrayList([{resourceid:"2", resourcename:"特色菜"},{resourceid:"6", resourcename:"举报投诉"}]);
		
		public var list1:ArrayCollection;
		public var list2:ArrayCollection;
		public var list3:ArrayCollection;
		public var list4:ArrayCollection;
		
		public var userVO:UserVO;
		
		public function checkLoginState():void
		{
			sendRemoteService("checkLoginState", checkLoginStateResult, fault);
		}
		
		private function checkLoginStateResult(evt:ResultEvent):void
		{
			var jsonObj:Object=JSON.decode(evt.result.toString());
			if(jsonObj.success == 1)
			{				
				pareseData(jsonObj);
			}
			sendNotification(AppFacade.LOGIN_STATE_UPDATA);
		}
		
		public function getManagerAreaList():void
		{
			sendWebService("Getssqbh", getManagerAreaListResult, fault);
		}
		
		private function getManagerAreaListResult(evt:ResultEvent):void
		{
			if(evt.result){
				var data:Object = JSON.decode(evt.result.toString());
				mgrAreaList = new ArrayCollection(data.data);
			}else{
				mgrAreaList = new ArrayCollection();
			}
		}
		
		public function login(name:String, psw:String, userType:String, area:String=""):void
		{
			sendRemoteService("login", loginResult, fault, name, psw, userType, area);
		}
		
		private function loginResult(evt:ResultEvent):void
		{
			var jsonObj:Object=JSON.decode(evt.result.toString());
			if(jsonObj.success==0)
			{
				Alert.show('调用用户验证异常！',"提示");
			}
			else if(jsonObj.success==-1)
			{
				Alert.show('用户或密码错误！',"提示");
			}
			else if(jsonObj.success==1)
			{		
				pareseData(jsonObj);			
				sendNotification(AppFacade.LOGIN_STATE_UPDATA);
			}
		}
		
		public function loginOut():void
		{
			sendRemoteService("loginOut", loginResult, fault);
		}
		
		private function loginOutResult(evt:ResultEvent):void
		{
			var jsonObj:Object=JSON.decode(evt.result.toString());
			if(jsonObj.success == 1)
			{				
				pareseData(jsonObj);				
				sendNotification(AppFacade.LOGIN_STATE_UPDATA);
			}
			else
			{
				Alert.show('loginOut方法出错！',"提示");
			}
		}
		
		private function pareseData(data:Object):void
		{
			if(data)
			{
				tabTag = new ArrayList();
				list1 = new ArrayCollection();
				list2 = new ArrayCollection();
				list3 = new ArrayCollection();
				list4 = new ArrayCollection();
				for each(var obj:Object in data.data)
				{
					if(obj.resourcetype == "1" && obj.flag == "1")
					{
						tabTag.source.push(obj);
					}
					else if(obj.resourcetype == "2")
					{
						switch(obj.resourceurl)
						{
							case "1":
								list1.addItem(obj);
								break;
							case "2":
								list2.addItem(obj);
								break;
							case "3":
								list3.addItem(obj);
								break;
							case "4":
								list4.addItem(obj);
								break;
						}
					}
				}					
			}
			var user:UserVO;
			switch(String(data.userType).toLowerCase())
			{
				case "a":
					user = new SuperviseUserVO();
					user.type = USER_TYPE_A;
					SuperviseUserVO(user).ssqbh = data.area;
					break;
				case "b":
					user = new EnterpriseUserVO();
					user.type = USER_TYPE_B;
					break;
				case "c":
					user = new UserVO();
					user.type = USER_TYPE_C;
					break;
				case "e":
					user = new LeaderUserVO();
					user.type = USER_TYPE_E;
					break;
				case "f":
					user = new LeaderUserVO();
					user.type = USER_TYPE_F;
					break;
				case "g":
					user = new LeaderUserVO();
					user.type = USER_TYPE_G;
					break;
				default:
					user = new UserVO();
					user.type = USER_TYPE_D;
					break;
			}
			user.id = data.userName;
			user.psw = data.password;
			this.userVO = user;
		}
	}
}