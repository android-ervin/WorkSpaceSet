package com.nuchina.model.user
{
	/**
	 * @author Ykk
	 * 2012-8-6
	 */
	public class EnterpriseUserVO extends UserVO
	{
		/**
		 * 经度 
		 */		
		public var jd:String;
		/**
		 * 纬度 
		 */		
		public var wd:String;
		/**
		 *  企业名称
		 */		
		public var qymc:String;
		/**
		 * 所属区 
		 */		
		public var ssqbh:String;
		/**
		 * 联系地址 
		 */		
		public var lxdz:String;
		public function EnterpriseUserVO()
		{
			super();
		}
	}
}