package com.nuchina.model.user
{
	/**
	 * @author Ykk
	 * 2012-8-6
	 */
	public class SuperviseUserVO extends UserVO
	{
		/**
		 * 所属区编号 
		 */		
		public var ssqbh:String;
		/**
		 * 执法人员工作证号——在登录的时候显示
		 */		
		public var zfrygzzh:String;
		/**
		 * 行政区编号 
		 */		
		public var areacode:String;
		public function SuperviseUserVO()
		{
			super();
		}
	}
}