package com.nuchina.model.user
{
	/**
	 * @author Ykk
	 * 2012-8-6
	 */
	public class UserVO
	{
		/**
		 * 登陆ID 
		 */		
		public var id:String;
		/**
		 * 登陆密码 
		 */		
		public var psw:String;
		/**
		 * 用户类型 
		 */		
		public var type:String;
		public function UserVO()
		{
		}
	}
}