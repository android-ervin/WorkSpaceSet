package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.business.IService;
	import com.nuchina.model.vo.AdvSearchParam;
	import com.nuchina.model.vo.AreaSearchParam;
	import com.nuchina.model.vo.CategorySearchParam;
	import com.nuchina.model.vo.DrawSearchParam;
	import com.nuchina.model.vo.IAdvValue;
	import com.nuchina.model.vo.IKeyword;
	import com.nuchina.model.vo.NavVo;
	import com.nuchina.model.vo.SSQBHSearchParam;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.messaging.messages.RemotingMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-6-20
	 */
	public class PanelViewProxy extends AbstractDataProxy
	{
		public static const NAME:String = "PanelViewProxy";

		public var enterpriseUserItem:Object;
		
		private var _enterpriseAmount:int;

		public function get enterpriseAmount():int
		{
			return _enterpriseAmount;
		}

		public function set enterpriseAmount(value:int):void
		{
			if(_enterpriseAmount==value)
				return;
			_enterpriseAmount = value;
			sendNotification(AppFacade.ENTERPRISE_AMOUNT_UPDATA);
		}

		private var _numOnePage:int = 8;
		/**
		 *  企业列表ITEM个数
		 * @return 
		 * 
		 */
		public function get numOnePage():int
		{
			return _numOnePage;
		}

		public function set numOnePage(value:int):void
		{
			_numOnePage = value;
		}

		private var _searchParam:IKeyword;

		public function get searchParam():IKeyword
		{
			return _searchParam;
		}

		public function set searchParam(value:IKeyword):void
		{
			_searchParam = value;
		}

		
		private var _enterpriseList:ArrayCollection;
		/**
		 *  
		 * @return 企业列表
		 * 
		 */		
		public function get enterpriseList():ArrayCollection
		{
			return _enterpriseList;
		}
		
		public function set enterpriseList(value:ArrayCollection):void
		{
			_enterpriseList = value;
			sendNotification(AppFacade.ENTERPRISE_LISE_UPDATA);
		}
				
		public function PanelViewProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		/**
		 *	按照行政区获取企业基本信息列表（分页）
		 用    途：	按照行政区获取企业基本信息列表（如按照江西省获取企业信息列表）
		 输入参数：	“getEInfoByArea” | areacode |page|length
		 参数说明：	方法字符串|用户类型|用户iD|密码|所属区编号|行政区代码（如36）|第几页|需要获取的记录数
		 返 回 值：	经度，纬度，是否有预警，企业编号ID，企业名称，企业地址 
		 * 
		 */		
		public function getEInfoByArea(areaCode:String, page:int=1, length:int=5):void
		{
			sendWebService("ProcessData", getEInfoResult, fault, getParamsCode("getEInfoByArea", areaCode, page, length));
		}
		
		/**
		 *	按照分类获取企业基本信息列表（分页）
		 用    途：	按照分类获取企业基本信息列表
		 输入参数：	“getEInfoByCategory”| category |page|length
		 参数说明：	方法字符串|用户类型|用户iD|密码|所属区编号|企业分类ID |第几页|需要获取的记录数
		 返 回 值：	经度，纬度，是否有预警，企业编号ID，企业名称，企业地址 
		 * 
		 */		
		public function getEInfoByCategory(categoryCode:String, page:int=1, length:int=5):void
		{
			sendWebService("ProcessData", getEInfoResult, fault, getParamsCode("getEInfoByCategory", categoryCode, page, length));
		}
		
		
		/**
		 *	按照关键字获取企业基本信息列表（分页）
		 用    途：	按照用户自定义关键字获取企业基本信息列表
		 输入参数：	“getEInfoByKeyword” | keyword|page|length
		 参数说明：	方法字符串|用户类型|用户iD|密码|所属区编号|用户自定义关键字|第几页|需要获取的记录数
		 返 回 值：	经度，纬度，是否有预警，企业编号ID，企业名称，企业地址 
		 * 
		 */		
		public function getEInfoByKeyword(keyword:String, page:int=1, length:int=5):void
		{
			sendWebService("ProcessData", getEInfoResult, fault, getParamsCode("getEInfoByKeyword", keyword, page, length));
		}
		
		/**
		 *	按照高级搜索获取企业基本信息列表（分页）
		 用    途：	按照高级搜索界面设置的参数搜索获取企业基本信息列表
		 输入参数：
		 “getEInfoByAdv”| areacode|foodtype|allowfield|safeleve|page|length
		 参数说明：	方法字符串|用户类型|用户Id|密码|所属区|行政区域|类型风味|许可范围|安全等级
		 返 回 值：	经度，纬度，是否有预警，企业编号ID，企业名称，企业地址 
		 * 
		 */		
		public function getEInfoByAdv(param:IKeyword, page:int=1, length:int=5):void
		{
			var advParam:AdvSearchParam = param as AdvSearchParam;
			sendWebService("ProcessData", getEInfoResult, fault, 
				getParamsCode("getEInfoByAdv", advParam.areaCode, advParam.foodType, advParam.allowField, advParam.safeLevel, page, length));
		}
		
		public function getEInfoBySSQBH(ssqbh:String, page:int = 1, length:int=5):void
		{
			sendWebService("ProcessData", getEInfoResult, fault, getParamsCode("getQybyssq", ssqbh, page, length));
		}
		
		public function getEInfoList(param:IKeyword, pageIndex:int, length:int):void
		{
			if(param is AreaSearchParam)
			{
				getEInfoByArea(param.keyword, pageIndex, length);
			}
			else if(param is CategorySearchParam)
			{
				getEInfoByCategory(param.keyword, pageIndex, length);
			}
			else if(param is AdvSearchParam)
			{
				getEInfoByAdv(param, pageIndex, length);
			}
			else if(param is SSQBHSearchParam)
			{
				getEInfoBySSQBH(param.keyword, pageIndex, length);
			}
			else if(param is DrawSearchParam)
			{
				enterpriseList = new ArrayCollection(DrawSearchParam(param).list.slice((pageIndex-1)*length, (pageIndex-1)*length+length));
				enterpriseAmount = DrawSearchParam(param).list.length;
			}
		}
			
		
		private function getEInfoResult(evt:ResultEvent):void
		{
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					enterpriseList = new ArrayCollection(obj.data);
					enterpriseAmount = obj.amout;
				}
			}
			else
			{
				enterpriseList = new ArrayCollection();
				enterpriseAmount = 0;
				trace("result is '' or null");
			}
		}		

	}
}