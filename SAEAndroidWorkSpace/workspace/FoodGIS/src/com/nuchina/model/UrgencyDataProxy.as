package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-7-28
	 */
	public class UrgencyDataProxy extends AbstractDataProxy implements IProxy
	{
		public static const NAME:String = "UrgencyDataProxy";
		public function UrgencyDataProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		public var currentID:String;
		
		private var _urgencyList:ArrayCollection;

		public function get urgencyList():ArrayCollection
		{
			return _urgencyList;
		}

		public function set urgencyList(value:ArrayCollection):void
		{
			_urgencyList = value;			
			sendNotification(AppFacade.URGENCY_LIST_UPDATA);
		}

		/**
		 * 	根据Areacode获得应急列表（新）
			输入参数："getYjlist"|areacode
			参数说明：方法字符串|行政代码
			返回值：qybhid,qymc,jd,wd,zfry,zbbsh,jckssj,lxdz等企业相关信息
 
		 * @param areaCode
		 * 
		 */		
		public function getYjlist(areaCode:String):void
		{
			sendWebService("ProcessData", getYjlistResultHandler, fault, getParamsCode("getYjlist", areaCode));
		}
		
		private function getYjlistResultHandler(evt:ResultEvent):void
		{
			if (evt.result) 
			{
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					urgencyList = new ArrayCollection(obj.data);
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		
		private var _urgencyHistoryList:ArrayCollection;

		public function get urgencyHistoryList():ArrayCollection
		{
			return _urgencyHistoryList;
		}

		public function set urgencyHistoryList(value:ArrayCollection):void
		{
			_urgencyHistoryList = value;
			sendNotification(AppFacade.HISTORY_LIST_UPDATA);
		}
		/**
		 * 	根据时间段查询应急历史列表（新）
			输入参数："getYjhistory"|areacode|time1|time2
			参数说明：方法字符串|行政代码
			返回值：qybhid,qymc,jd,wd,zfry,zbbsh,jckssj,lxdz等企业相关信息
			
			历史应急事件详细信息接口同接口getYjdetail（str）

 
		 * @param areaCode
		 * @param startTime
		 * @param endTime
		 * 
		 */
		public function getYjhistory(areaCode:String, startTime:String, endTime:String):void
		{
			sendWebService("ProcessData", getYjhistoryResultHandler, fault, getParamsCode("getYjhistory", areaCode, startTime, endTime));
		}
		
		private function getYjhistoryResultHandler(evt:ResultEvent):void
		{
			if (evt.result) 
			{
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					urgencyHistoryList = new ArrayCollection(obj.data);
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		
	}
}