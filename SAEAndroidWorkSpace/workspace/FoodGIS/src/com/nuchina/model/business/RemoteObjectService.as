package com.nuchina.model.business
{
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	public class RemoteObjectService implements IService
	{
		protected var _remote:RemoteObject;
		public function RemoteObjectService()
		{
			_remote = new RemoteObject();
		}
		
		public function get destination():String
		{
			return _remote.destination;
		}

		public function set destination(value:String):void
		{
			_remote.destination = value;
		}

		public function get source():String
		{
			return _remote.source;
		}

		public function set source(value:String):void
		{
			_remote.source = value;
		}
		public function get endpoint():String
		{
			return _remote.endpoint;
		}
		
		public function set endpoint(url:String):void
		{
			_remote.endpoint = url;
		}
		
		public function send(method:String, ...params):AsyncToken
		{
			var op:AbstractOperation = _remote.getOperation(method);
			var token:AsyncToken;
			if(params && params.length > 0)
				token = op.send.apply(op, params);
			else
				token = op.send();
			return token;
		}
	}
}