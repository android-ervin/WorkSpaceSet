package com.nuchina.model.business
{
	import mx.rpc.AsyncToken;

	public interface IService
	{
		function send(method:String, ...params):AsyncToken;
	}
}