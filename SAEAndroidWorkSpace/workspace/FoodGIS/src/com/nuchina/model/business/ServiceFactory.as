package com.nuchina.model.business
{
	import flash.utils.Dictionary;

	public class ServiceFactory
	{
		public static const ROMOTE_OBJECT:String = "remoteObject";
		public static const WEB_SERVICE:String = "webService";
		private static var _services:Dictionary = new Dictionary(true);
		public function ServiceFactory()
		{
		}
		
		public static function getService(type:String, params:Object=null):IService
		{
			var s:IService;
			var key:String;
			switch(type)
			{
				case ROMOTE_OBJECT:
					key = params.destination + params.source;
					if(_services[key])
					{
						s = _services[key];
					}
					else
					{
						s = new RemoteObjectService();
						(s as RemoteObjectService).destination = params.destination;
						(s as RemoteObjectService).source = params.source;
						(s as RemoteObjectService).endpoint = params.endpoint
						_services[key] = s;
					}
					break;
				case WEB_SERVICE:
					key = params.wsdl;
					if(_services[key])
					{
						s = _services[key];
					}
					else
					{
						s = new WebServiceEx();
						(s as WebServiceEx).wsdl = params.wsdl;
						(s as WebServiceEx).loadWSDL();
						_services[key] = s;
					}
					break;
			}
			return s;
		}
	}
}