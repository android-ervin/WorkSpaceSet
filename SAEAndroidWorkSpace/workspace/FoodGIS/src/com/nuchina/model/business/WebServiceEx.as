package com.nuchina.model.business
{
	import mx.rpc.AbstractOperation;
	import mx.rpc.AsyncToken;
	import mx.rpc.soap.WebService;
	
	/**
	 * @author Ykk
	 * 2012-6-11
	 */
	public class WebServiceEx implements IService
	{
		private var _ws:WebService;
		public function WebServiceEx()
		{
			_ws = new WebService();
		}
		
		public function send(method:String, ...params):AsyncToken
		{
			var op:AbstractOperation = _ws.getOperation(method);
			var token:AsyncToken;
			if(params && params.length > 0)
				token = op.send.apply(op, params);
			else
				token = op.send();
			return token;
		}
		
		public function loadWSDL(url:String=null):void
		{
			_ws.loadWSDL(url);
		}
		
		public function get wsdl():String
		{
			return _ws.wsdl;
		}
		
		public function set wsdl(value:String):void
		{
			_ws.wsdl = value;
		}

	}
}