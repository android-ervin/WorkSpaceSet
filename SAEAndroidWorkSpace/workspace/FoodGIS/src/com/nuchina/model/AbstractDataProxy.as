package com.nuchina.model
{
	import com.nuchina.model.business.IService;
	
	import mx.messaging.messages.RemotingMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-6-25
	 */
	public class AbstractDataProxy extends Proxy
	{
		protected var _remoteService:IService;
		
		public function get remoteService():IService
		{
			return _remoteService;
		}
		
		public function set remoteService(value:IService):void
		{
			_remoteService = value;
		}
		
		protected var _webService:IService;
		
		public function get webService():IService
		{
			return _webService;
		}
		
		public function set webService(value:IService):void
		{
			_webService = value;
		}
		
		public function AbstractDataProxy(proxyName:String=null, data:Object=null)
		{
			super(proxyName, data);
		}
		
		protected function sendWebService(method:String, result:Function, fault:Function=null, ...params):void
		{
			params.unshift(method);
			new ServiceHelper(_webService, params, result, fault).start();
		}
		
		protected function sendRemoteService(method:String, result:Function, fault:Function=null, ...params):void
		{
			params.unshift(method);
			new ServiceHelper(_remoteService, params, result, fault).start();
		}
		
		protected function fault(evt:FaultEvent):void
		{
			if(evt.token.message is RemotingMessage)
				trace("call method: "+RemotingMessage(evt.token.message).operation+" is fault.");
			else
				trace(evt.toString());
		}
		
		protected function getParamsCode(...argms):String
		{
			var code:String = "";
			if(argms && argms.length > 0){
				code += argms[0];
				for(var i:int = 1; i < argms.length; i++)
				{
					code += "|"+argms[i];
				}
			}
			return code;
		}
		
	}
	
}
import com.nuchina.model.business.IService;

import mx.rpc.AsyncToken;
import mx.rpc.Responder;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

class ServiceHelper{
	private var _retryTime:int;

	public function get retryTime():int
	{
		return _retryTime;
	}

	public function set retryTime(value:int):void
	{
		_retryTime = value;
	}

	private var _iService:IService;

	public function get iService():IService
	{
		return _iService;
	}

	public function set iService(value:IService):void
	{
		_iService = value;
	}

	private var _params:Array;

	public function get params():Array
	{
		return _params;
	}

	public function set params(value:Array):void
	{
		_params = value;
	}

	private var _result:Function;

	public function get result():Function
	{
		return _result;
	}

	public function set result(value:Function):void
	{
		_result = value;
	}

	private var _fault:Function;

	public function get fault():Function
	{
		return _fault;
	}

	public function set fault(value:Function):void
	{
		_fault = value;
	}
	
	private var _isDestroy:Boolean;

	public function get isDestroy():Boolean
	{
		return _isDestroy;
	}

	public function set isDestroy(value:Boolean):void
	{
		_isDestroy = value;
	}


	public function ServiceHelper(service:IService, params:Array, result:Function, falut:Function=null, retryTime:int=3)
	{
		_iService = service;
		_params = params;
		_result = result;
		_fault = falut;
		_retryTime = retryTime;
	}
	
	public function start():void
	{
		if(_iService){
			var token:AsyncToken = _iService.send.apply(null, params);
			token.addResponder(new Responder(resultHandler, falutHandler));
		}
	}
	
	protected function resultHandler(evt:ResultEvent):void
	{
		if(_isDestroy)
			return;
		if(_result!=null)
			_result(evt);
		destroy();
		
	}
	
	protected function falutHandler(evt:FaultEvent):void
	{
		if(_isDestroy)
			return;
		if(_retryTime > 0){
			_retryTime--;
			var token:AsyncToken = _iService.send.apply(null, params);
			token.addResponder(new Responder(resultHandler, falutHandler));
			trace("retry service:", params.toString());
		}else if(_fault !=null){
			_fault(evt);
			destroy();
		}
	}
	
	public function end():void
	{
		destroy();
	}
	
	public function destroy():void
	{
		_iService = null;
		_params = null;
		_result = null;
		_fault = null;
		_retryTime = 0;
		_isDestroy = true;
	}
}