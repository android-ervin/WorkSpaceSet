package com.nuchina.model
{
	
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-8-13
	 */
	[Bindable]
	public class AppModel extends Proxy implements IProxy
	{
		public static const NAME:String = "AppModel";
		public function AppModel(data:Object=null)
		{
			super(NAME, data);
		}
		
		/**
		 * 是否是编辑状态
		 */		
		public var isFlagState:Boolean;
		/**
		 * 企业坐标点 
		 */		
		public var point:Point;
		/**
		 * 执法人员列表 
		 */		
		public var enterpriseList:ArrayCollection;
	}
}