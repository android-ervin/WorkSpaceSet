package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.adobe.utils.StringUtil;
	import com.nuchina.AppFacade;
	import com.nuchina.controller.HttpUtil;
	import com.nuchina.model.business.IService;
	import com.nuchina.model.business.ServiceFactory;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	

	[Bindable]
	public class MapDataProxy extends AbstractDataProxy
	{
		public static const NAME:String = "MapDataProxy";
		
		public function MapDataProxy ( data:Object = null ) 
		{
			super ( NAME, data );
		}
		
		private var _allEnterpriseCoord:Array;
		/**
		 *  all enterprise coord
		 * @return 
		 * 
		 */		
		public function get allEnterpriseCoord():Array
		{
			return _allEnterpriseCoord;
		}

		public function set allEnterpriseCoord(value:Array):void
		{
			_allEnterpriseCoord = value;
		}

		private var _showECoord:Array;

		public function get showECoord():Array
		{
			return _showECoord;
		}

		public function set showECoord(value:Array):void
		{
			_showECoord = value;
			sendNotification(AppFacade.ENTERPRISE_COORD_UPDATA);
		}

		/**
		 * 	按照行政区获取企业坐标信息列表
			用    途：	按照行政区获取企业坐标信息列表（如按照东湖区获取企业坐标信息列表）
			输入参数：	“getCoordByArea” | areacode
			参数说明：	方法字符串|用户类型|用户iD|密码|所属区编号|行政区代码
			返 回 值：	经度，纬度，是否有预警，企业编号ID  
		 * 
		 */	
		public function getCoordByArea(areaCode:String):void
		{
			sendWebService("ProcessData", getCoordByAreaResult, fault, getParamsCode("getCoordByArea", areaCode));
		}
		
		private function getCoordByAreaResult(evt:ResultEvent):void
		{
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					showECoord = obj.data;
					allEnterpriseCoord = obj.data;
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		/**
		 * 	按照行政区获取企业坐标信息列表
		 用    途：	按照行政区获取企业坐标信息列表（如按照东湖区获取企业坐标信息列表）
		 输入参数：	“getCoordByArea” | areacode
		 参数说明：	方法字符串|用户类型|用户iD|密码|所属区编号|行政区代码
		 返 回 值：	经度，纬度，是否有预警，企业编号ID  
		 * 
		 */	
		public function getCoordByType(storeType:String):void
		{
			sendWebService("ProcessData", getCoordByTypeResult, fault, getParamsCode("getCoordByType", storeType));
		}
		
		private function getCoordByTypeResult(evt:ResultEvent):void
		{
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					showECoord = obj.data;
					allEnterpriseCoord = obj.data;
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		
		public function getComplaint(eId:String, callback:Function=null):void
		{
			var token:AsyncToken = _remoteService.send("getComplaint", eId);
			if(callback!=null)
				token.addResponder(new Responder(callback, fault));
			else
				token.addResponder(new Responder(getComplaintResult, fault));
		}
		
		private function getComplaintResult(evt:ResultEvent):void
		{
			var jsonObj:Object = JSON.decode(evt.result.toString());
			if(jsonObj.success==1)
			{
				sendNotification(AppFacade.GET_COMPLAINT_SUCCESS, jsonObj.data);
			}
			else if(jsonObj.success==0)
			{
				Alert.show("获取列表失败!", "提示");
			}
			else 
			{
				Alert.show('获取数据失败！',"提示");
			}
		}
		
		public function submitComplaint(data:Object):void
		{
			var token:AsyncToken = _remoteService.send("AddYHTS", data.userName, data.mobile, data.unit, data.title, data.content, "", data.eId);
			token.addResponder(new Responder(submitComplaintResult, fault));
		}
		
		public function submitComplaintResult(evt:ResultEvent):void
		{
			var jsonObj:Object = JSON.decode(evt.result.toString());
			if(jsonObj.success==1)
			{
				Alert.show("提交成功!", "提示");
				sendNotification(AppFacade.SUBMIT_COMPLAINT_SUCCESS);
			}
			else if(jsonObj.success==0)
			{
				Alert.show("获取列表失败!", "提示");
			}
			else 
			{
				Alert.show('获取数据失败！',"提示");
			}
		}
		
	}
}