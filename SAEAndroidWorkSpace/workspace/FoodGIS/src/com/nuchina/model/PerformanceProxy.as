package com.nuchina.model
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.vo.NavVo;
	import com.nuchina.model.vo.PerformanceVo;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	/**
	 * @author Ykk
	 * 2012-7-4
	 */
	public class PerformanceProxy extends AbstractDataProxy
	{
		public static const NAME:String = "PerformanceProxy";
		public function PerformanceProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		private var _specialAreaNavVo:NavVo;
		
		public function get specialAreaNavVo():NavVo
		{
			return _specialAreaNavVo;
		}
		
		public function set specialAreaNavVo(value:NavVo):void
		{
			_specialAreaNavVo = value;
		}	
		private var _supervisors:ArrayCollection;

		/**
		 * 执法人员列表 
		 * 			{name:"张三", id:"1"},
					{name:"李四", id:"2"},
					{name:"麻子", id:"3"}
		 */
		public function get supervisors():ArrayCollection
		{
			return _supervisors;	
		}

		/**
		 * @private
		 */
		public function set supervisors(value:ArrayCollection):void
		{
			_supervisors = value;
			sendNotification(AppFacade.SUPERVISOR_UPDATA);
		}
		private var _currentPerformance:PerformanceVo = new PerformanceVo();

		/**
		 * 当前绩效成绩 
		 */
		public function get currentPerformance():PerformanceVo
		{
			return _currentPerformance;
		}

		/**
		 * @private
		 */
		public function set currentPerformance(value:PerformanceVo):void
		{
			_currentPerformance = value;
			sendNotification(AppFacade.PERFORMANCE_UPDATA);
		}
		private var _trackCoord:Array;

		/**
		 * 轨迹坐标 
		 * 			{ jd:"116.23933690078388", wd:"28.386837390557156"},
					{ jd:"115.97631897553255", wd:"28.650692142941162"},
					{ jd:"115.90997410185242", wd:"28.664801958901231"}
		 */
		public function get trackCoord():Array
		{
			return _trackCoord;
		}

		/**
		 * @private
		 */
		public function set trackCoord(value:Array):void
		{
			_trackCoord = value;
			sendNotification(AppFacade.TRACK_COORD_UPDATA);
		}

		
		private var _patrolEvents:ArrayCollection;

		/**
		 * 路径数据 
		 * 			{qymc:"企业名称", jd:"116.23933690078388", wd:"28.386837390557156", BSH:1},
					{qymc:"企业名称", jd:"115.97631897553255", wd:"28.650692142941162", BSH:2},
					{qymc:"企业名称", jd:"115.90997410185242", wd:"28.664801958901231", BSH:3}
		 */
		public function get patrolEvents():ArrayCollection
		{
			return _patrolEvents;
		}

		/**
		 * @private
		 */
		public function set patrolEvents(value:ArrayCollection):void
		{
			_patrolEvents = value;
			sendNotification(AppFacade.PATROL_EVENT_UPDATA);
		}

		/**
		 * 	根据用户类型获取执法人员列表
			用    途：	根据用户类型获取执法人员列表
			输入参数：	“getSuperVisor” |areacode
			参数说明：	方法字符串|用户类型 
			返 回 值：	执法人员列表
		 * 
		 */		
		public function getSuperVisor(code:String=""):void
		{
			sendWebService("ProcessData", getSuperVisorResult, fault, getParamsCode("getSuperVisor", code));
		}
		
		private function getSuperVisorResult(evt:ResultEvent):void
		{
			
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					supervisors = new ArrayCollection(obj.data);
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		/**
		 * 	根据执法人编号查询绩效成功
			输入参数：	“getPerformance” | username|password|ID|type
			参数说明：	方法字符串|用户名|密码|执法编号|（月度“0”，季度“1”，年度“2”）
			返 回 值：	执法人员绩效内容
			格	式	：	{data:[{ valuation：良好
			Coverage：覆盖率
			Accuracy：准确率
			Punish：  处罚率
			Timely：	及时率
			}
			,{…}
			}
 
		 * @param code
		 * 
		 */		
		public function getPerformance(id:String, time:String, type:String):void
		{
			sendWebService("ProcessData", getPerformanceResult, fault, getParamsCode("getPerformance", id, time, type));
		}
		
		private function getPerformanceResult(evt:ResultEvent):void
		{
			
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					if(obj.data is Array && (obj.data as Array).length > 0){
						var item:Object = obj.data[0];
						if(item)
							currentPerformance = new PerformanceVo(item.khresult, item.fgl, item.zql, item.cfl, item.jsl);
						else
							currentPerformance = new PerformanceVo();
					}else{
						currentPerformance = new PerformanceVo();
					}
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		
		/**
		 *  根据执法人员姓名和时间获得该执法人员该时间内普通点坐标
			输入参数：	“getTrack” |name|time
			参数说明：	方法字符串|执法人员名字|时间 (2012-7-9)
			返 回 值：	坐标点（执法人员轨迹）
			格	式	：	{data:[{jd:111,wd:11},{jd:111,wd:11}]}
		 * @param name
		 * @param timeStr
		 * 
		 */		
		public function getTrackCoord(name:String, timeStr:String):void
		{
			sendWebService("ProcessData", getTrackCoordResult, fault, getParamsCode("getTrack", name, timeStr));
		}
		
		private function getTrackCoordResult(evt:ResultEvent):void
		{
			
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					trackCoord = obj.data;
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		/**
		 *		根据执法人员姓名和时间获得该执法人员该时间内特殊点坐标
				输入参数：	“getTrackTs” |name|time
				参数说明：	方法字符串|执法人员名字|时间 
				返 回 值：	酒店名称，酒店JD，酒店WD，标识号（BSH）
				格	式	：	{data:[{qymc:企业名称, jd: 酒店JD,WD：酒店WD,BSH:标识号},{{qymc:企业名称, jd: 酒店JD,WD：酒店WD,BSH:标识号}]

		 * @param timeStr
		 * 
		 */		
		public function getPatrolEvent(name:String, timeStr:String):void
		{
			sendWebService("ProcessData", getPatrolEventResult, fault, getParamsCode("getTrackTs", name, timeStr));
		}
		
		private function getPatrolEventResult(evt:ResultEvent):void
		{
			
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					patrolEvents = new ArrayCollection(obj.data);
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
		
		private var _enterpriseList:ArrayCollection;

		public function get enterpriseList():ArrayCollection
		{
			return _enterpriseList;
		}

		public function set enterpriseList(value:ArrayCollection):void
		{
			_enterpriseList = value;
			sendNotification(AppFacade.SUPERVISOR_ENTE_UPDATA);
		}

		/**
		 *  根据执法人员编号查询该执法人员所辖酒店
			输入参数："getSuperHotel"|id|（传执法人员编号）
			参数说明：方法字符串| 执法人员编号ID 
			返回值： 酒店基本信息

		 * @param id
		 * 
		 */		
		public function getSuperHotel(id:String):void
		{
			sendWebService("ProcessData", getSuperHotelResult, fault, getParamsCode("getSuperHotel", id));
		}
		
		private function getSuperHotelResult(evt:ResultEvent):void
		{
			if (evt.result) 
			{				
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success == 1)
				{
					enterpriseList = new ArrayCollection(obj.data);
				}
			}
			else
			{
				trace("result is '' or null");
			}
		}
	}
}