package com.nuchina.event
{
	import flash.events.Event;
	
	/**
	 * @author Ykk
	 * 2012-7-26
	 */
	public class AreaSelectEvent extends Event
	{
		public static const AREA_SELECTED_CHANGE:String = "areaSelectedChange";
		private var _areaCode:String;

		public function get areaCode():String
		{
			return _areaCode;
		}

		public function set areaCode(value:String):void
		{
			_areaCode = value;
		}

		public function AreaSelectEvent(type:String, areaCode:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.areaCode = areaCode;
		}
		
		override public function formatToString(className:String, ...parameters):String
		{
			return super.formatToString(className, parameters);
		}
		
		override public function toString():String
		{
			return super.toString();
		}
		
		
	}
}