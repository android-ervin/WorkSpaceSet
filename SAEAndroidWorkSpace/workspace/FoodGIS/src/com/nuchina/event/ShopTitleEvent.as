package com.nuchina.event
{
	import flash.events.Event;

	public class ShopTitleEvent extends Event
	{
		
		public var Shopx:Number;//店铺x坐标
		public var Shopy:Number;//店铺y坐标
		public var Shopname:String;
		public var Shopaddr:String;
		public var Shoptel:String;
		public static const ShopTitleClick:String = "shoptitleclick";
		public function ShopTitleEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ShopTitleEvent(type, bubbles, cancelable);
		}
		override public function toString():String
		{
			return formatToString("ShopTitleEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}