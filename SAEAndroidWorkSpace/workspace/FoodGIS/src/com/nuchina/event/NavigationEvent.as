package com.nuchina.event
{
	import com.nuchina.model.vo.LinkVO;
	
	import flash.events.Event;
	
	/**
	 * @author Ykk
	 * 2012-8-18
	 */
	public class NavigationEvent extends Event
	{
		public static const NAV_CLICK_EVENT:String = "navClickEvent";
		public var linkVO:LinkVO;
		public function NavigationEvent(type:String, linkVO:LinkVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.linkVO = linkVO;
			super(type, bubbles, cancelable);
		}
	}
}