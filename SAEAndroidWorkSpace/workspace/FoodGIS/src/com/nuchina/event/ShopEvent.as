package com.nuchina.event
{
	import flash.events.Event;

	public class ShopEvent extends Event
	{
		
		public var ShopId:String;
		public static const BtnClick:String = "mybtnclick";
		public function ShopEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		
	}
}