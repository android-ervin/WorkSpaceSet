package com.nuchina.event
{
	import flash.events.Event;

	public class LinkClickEvent extends Event
	{
		
		public var data:Object;
		public static const ITEM_CLICK_EVENT:String = "itemClickEvent";
		public function LinkClickEvent(type:String, data:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.data = data;
			super(type, bubbles, cancelable);
		}
		
		
	}
}