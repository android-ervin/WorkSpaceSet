package com.nuchina.view
{
	import com.nuchina.AppFacade;
	import com.nuchina.event.LinkClickEvent;
	import com.nuchina.event.ShopEvent;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.model.PanelViewProxy;
	import com.nuchina.model.PerformanceProxy;
	import com.nuchina.model.UrgencyDataProxy;
	import com.nuchina.model.vo.AreaSearchParam;
	import com.nuchina.model.vo.CategorySearchParam;
	import com.nuchina.view.components.SearchResultItemRender;
	import com.nuchina.view.components.ShopList;
	
	import flash.debugger.enterDebugger;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import mx.events.IndexChangedEvent;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	import spark.components.NavigatorContent;
	import spark.events.IndexChangeEvent;

	public class PanelViewMediator extends Mediator
	{
		public static const NAME:String="PanelViewMediator";
		
		private var _foodProxy:MapDataProxy;
		private var _panelProxy:PanelViewProxy;
		private var _performanceProxy:PerformanceProxy;
		private var _urgency:UrgencyDataProxy;
		public function PanelViewMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
			_foodProxy=facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
			_panelProxy=facade.retrieveProxy(PanelViewProxy.NAME) as PanelViewProxy;
			_performanceProxy = facade.retrieveProxy(PerformanceProxy.NAME) as PerformanceProxy;
			_urgency = facade.retrieveProxy(UrgencyDataProxy.NAME) as UrgencyDataProxy;
			view.pageBar.addEventListener(ShopEvent.BtnClick, onChangePage);
			view.resultList.addEventListener(IndexChangeEvent.CHANGE, onChangeItem);
			view.areaNav.addEventListener(LinkClickEvent.ITEM_CLICK_EVENT, onLinkClickHander);
			view.categoryNav.addEventListener(LinkClickEvent.ITEM_CLICK_EVENT, onLinkClickHander);
			view.tabView.addEventListener(IndexChangedEvent.CHANGE, panelTab_changeHandler);
		}

		override public function handleNotification(notification:INotification):void
		{
			switch (notification.getName())
			{
				case AppFacade.ENTERPRISE_LISE_UPDATA:
					view.resultList.dataProvider=_panelProxy.enterpriseList;
					view.resultList.cursorManager.removeBusyCursor();
					break;
				case AppFacade.ENTERPRISE_AMOUNT_UPDATA:
					if (_panelProxy.enterpriseAmount > 0)
					{
						view.pageBar.pageSize=_panelProxy.numOnePage;
						view.pageBar.dataBind(_panelProxy.enterpriseAmount);
						view.pageBar.visible=true;
					}
					else
					{
						view.pageBar.visible=false;
					}
					break;
				case AppFacade.SUPERVISOR_UPDATA:
					view.supervisorList.dataProvider = _performanceProxy.supervisors;
					break;
				case AppFacade.PERFORMANCE_UPDATA:
					view.performance = _performanceProxy.currentPerformance;
					break;
				case AppFacade.PATROL_EVENT_UPDATA:
					view.patrolEventList.dataProvider = _performanceProxy.patrolEvents;
					break;
				
				case AppFacade.HISTORY_LIST_UPDATA:
					view.historyList.dataProvider = _urgency.urgencyHistoryList;
					break;
				case AppFacade.URGENCY_LIST_UPDATA:
					view.urgencyList.dataProvider = _urgency.urgencyList;
					break;
				case AppFacade.SUPERVISOR_ENTE_UPDATA:
					if(view.searchKey.text == "")
					{
						view.enterpriseList.dataProvider = _performanceProxy.enterpriseList;						
					}
					else
					{
						var arr:ArrayCollection = new ArrayCollection();
						for each (var item:Object in _performanceProxy.enterpriseList) 
						{
							if(String(item.qymc).search(view.searchKey.text) > -1)
							{
								arr.addItem(item);
							}
						}
						view.enterpriseList.dataProvider = arr;
					}
					break;
			}
		}

		override public function listNotificationInterests():Array
		{
			return [
				AppFacade.ENTERPRISE_LISE_UPDATA, 
				AppFacade.ENTERPRISE_AMOUNT_UPDATA, 
				AppFacade.SUPERVISOR_UPDATA, 
				AppFacade.PERFORMANCE_UPDATA,
				AppFacade.PATROL_EVENT_UPDATA,
				AppFacade.HISTORY_LIST_UPDATA,
				AppFacade.URGENCY_LIST_UPDATA,
				AppFacade.SUPERVISOR_ENTE_UPDATA
			];
		}

		public function changePannelTab(index:int, id:String=null):void
		{
			view.tabView.selectedIndex = index;
		}
		
		public function changeUserType(type:String):void
		{
			switch (type)
			{
				case ClientStateProxy.USER_TYPE_A:
					if (!view.tabView.contains(view.performanceView))
						view.tabView.addElement(view.performanceView);
					if (!view.tabView.contains(view.urgencyView))
						view.tabView.addElement(view.urgencyView);
					view.supervisorArea.visible = false;
					view.urgencyArea.visible = false;
					break;
				case ClientStateProxy.USER_TYPE_E:
				case ClientStateProxy.USER_TYPE_F:
				case ClientStateProxy.USER_TYPE_G:
					if (!view.tabView.contains(view.performanceView))
						view.tabView.addElement(view.performanceView);
					if (!view.tabView.contains(view.urgencyView))
						view.tabView.addElement(view.urgencyView);
					break;
				case ClientStateProxy.USER_TYPE_B:
					if (view.tabView.contains(view.performanceView))
						view.tabView.removeElement(view.performanceView);
					if (!view.tabView.contains(view.urgencyView))
						view.tabView.addElement(view.urgencyView);
					break;
				case ClientStateProxy.USER_TYPE_C:
				case ClientStateProxy.USER_TYPE_D:
					if (view.tabView.contains(view.performanceView))
						view.tabView.removeElement(view.performanceView);
					if (view.tabView.contains(view.urgencyView))
						view.tabView.removeElement(view.urgencyView);
					break;
			}
//			var srir:ClassFactory=new ClassFactory(SearchResultItemRender);
//			srir.properties={btns: _fdata.tabTag};
//			view.resultList.itemRenderer=srir;
		}

		public function get view():PanelView
		{
			return viewComponent as PanelView;
		}

		private function onChangePage(evt:ShopEvent):void
		{
			if (_panelProxy.searchParam)
			{
				_panelProxy.getEInfoList(_panelProxy.searchParam, int(evt.ShopId), _panelProxy.numOnePage);
			}
		}

		private function onChangeItem(evt:IndexChangeEvent):void
		{
			sendNotification(AppFacade.SHOW_ENTERPRISE_INFO, _panelProxy.enterpriseList.source[evt.newIndex]);
			view.resultList.selectedIndex = -1;
		}

		private function onLinkClickHander(evt:LinkClickEvent):void
		{
			var data:Object=evt.data;
			if (evt.currentTarget == view.areaNav)
			{
				if(data != "-1"){
					var areaParam:AreaSearchParam=new AreaSearchParam();
					areaParam.keyword=data["areacode"];
					_panelProxy.searchParam=areaParam;
					_panelProxy.getEInfoList(areaParam, 1, _panelProxy.numOnePage);
					sendNotification(AppFacade.START_CATEGORY_SEARCH, data["areaname"]);					
				}
				sendNotification(AppFacade.MAP_POSITION_EVENT, data);
			}
			else if (evt.currentTarget == view.categoryNav &&data != "-1")
			{
				var categoryParam:CategorySearchParam=new CategorySearchParam();
				categoryParam.keyword=data["categoryname"];
				_panelProxy.searchParam=categoryParam;
				_panelProxy.getEInfoList(categoryParam, 1, _panelProxy.numOnePage);
				sendNotification(AppFacade.START_CATEGORY_SEARCH, data["categoryname"]);
			}
		}
		
		protected function panelTab_changeHandler(event:IndexChangedEvent):void
		{
			var nc:NavigatorContent = event.relatedObject as NavigatorContent;
			if(nc && nc == view.performanceView)
			{
				
			}
			else if(nc && nc == view.urgencyView)
			{
				
			}
		}
		
	}
}
