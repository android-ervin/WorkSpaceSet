/*
 PureMVC AS3 Demo - Flex CafeTownsend
 Copyright (c) 2007-08 Michael Ramirez <michael.ramirez@puremvc.org>
 Parts Copyright (c) 2005-07 Adobe Systems, Inc. 
 Your reuse is governed by the Creative Commons Attribution 3.0 License
 */
package com.nuchina.view
{
    import com.nuchina.AppFacade;
    import com.nuchina.controller.HttpUtil;
    import com.nuchina.model.ClientStateProxy;
    import com.nuchina.model.MapDataProxy;
    import com.nuchina.model.PanelViewProxy;
    import com.nuchina.model.user.EnterpriseUserVO;
    import com.nuchina.model.user.LeaderUserVO;
    import com.nuchina.model.user.SuperviseUserVO;
    import com.nuchina.model.user.UserVO;
    
    import flash.events.MouseEvent;
    
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    
    import org.puremvc.as3.interfaces.*;
    import org.puremvc.as3.patterns.mediator.Mediator;
    
    public class FoodGISMediator extends Mediator implements IMediator
    {
        public static const NAME:String = "FoodGISMediator";
        public function FoodGISMediator( viewComponent:Object ) 
        {
            super( NAME, viewComponent );			 
			_client = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			_fdata = facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
			_panelProxy = facade.retrieveProxy(PanelViewProxy.NAME) as PanelViewProxy;
			view.searchBtn.addEventListener(MouseEvent.CLICK, onSearchClick);
			view.searchTxt.addEventListener(FlexEvent.ENTER, onEnterSearch);
			view.advSearchBtn.addEventListener(MouseEvent.CLICK, onAdvSearchClikcHandler);
        }
       
        override public function listNotificationInterests():Array 
        {
            
            return [
				AppFacade.START_CATEGORY_SEARCH,
				AppFacade.SHOW_ADV_SEARCH_VIEW
					];
        }

        override public function handleNotification( note:INotification ):void 
        {
            switch ( note.getName() ) 
			{   
				case AppFacade.START_CATEGORY_SEARCH:
					view.searchTxt.text = note.getBody() as String;
					break;
				case AppFacade.SHOW_ADV_SEARCH_VIEW:
					showSearchView();
					break;		
            }
        }
		
		public function changeLoginState():void
		{			
			if(_client.userVO is SuperviseUserVO || _client.userVO is EnterpriseUserVO || _client.userVO is LeaderUserVO){
				if(_simpleLogin && _simpleLogin.parent){
					PopUpManager.removePopUp(_simpleLogin);
					_simpleLogin = null;						
				}
				view.loginInBtn.removeEventListener(MouseEvent.CLICK, onLogin);
				view.currentState = "loginIn";
				if(_client.userVO is SuperviseUserVO)
				{
					view.nameTxt.text = SuperviseUserVO(_client.userVO).zfrygzzh;
				}
				else if(_client.userVO is EnterpriseUserVO)
				{
					view.nameTxt.text = EnterpriseUserVO(_client.userVO).qymc;
				}
				else if(_client.userVO is LeaderUserVO)
				{
					view.nameTxt.text = _client.userVO.id
				}
				view.loginOutBtn.addEventListener(MouseEvent.CLICK, onLoginOut);
			}else{
				if(view.loginOutBtn)
					view.loginOutBtn.removeEventListener(MouseEvent.CLICK, onLoginOut);
				view.currentState = "loginOut";
				view.loginInBtn.addEventListener(MouseEvent.CLICK, onLogin);
			}
		}
		
		public function showSearchView():void
		{
			if(_advSearchView==null){
				_advSearchView = new AdvSearchView();
			}
			PopUpManager.addPopUp(_advSearchView, view);
			PopUpManager.centerPopUp(_advSearchView);
		}
		
		public function showLoginWin():void
		{
			_simpleLogin ||= new SimpleLogin();
			PopUpManager.addPopUp(_simpleLogin, view, true);
			PopUpManager.centerPopUp(_simpleLogin);
		}
		
		private function onLogin(evt:MouseEvent):void
		{
			sendNotification(AppFacade.SHOW_LOGIN_WIN);
		}
		
		private function onLoginOut(evt:MouseEvent):void
		{
			_client.loginOut();
		}
		
		private function onSearchClick(evt:MouseEvent):void
		{
			if(view.searchTxt.text != "")
			{
				_panelProxy.getEInfoByKeyword(view.searchTxt.text);				
			}
		}
		
		private function onEnterSearch(evt:FlexEvent):void
		{
			if(view.searchTxt.text != "")
			{
				_panelProxy.getEInfoByKeyword(view.searchTxt.text);				
			}
		}
		
		private function onAdvSearchClikcHandler(evt:MouseEvent):void
		{
//			HttpUtil.openNewPage("search.html", "_blank");
			sendNotification(AppFacade.SHOW_ADV_SEARCH_VIEW);
		}
		
        public function get view():FoodGIS
		{
            return viewComponent as FoodGIS;
        }

		private var _fdata:MapDataProxy;
		private var _client:ClientStateProxy;
		private var _panelProxy:PanelViewProxy;
		private var _simpleLogin:SimpleLogin;
		private var _advSearchView:AdvSearchView;
    }
}
