package com.nuchina.view
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.model.MapDataProxy;
	
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.events.ValidationResultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class LoginMediator extends Mediator
	{
		public static const NAME:String = "LoginMediator";
		public function LoginMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
		}
		
		override public function onRegister():void
		{
			// TODO Auto Generated method stub
			super.onRegister();
			login.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		private function onCreationComplete(evt:FlexEvent):void
		{
			login.btn_login.addEventListener(MouseEvent.CLICK, onLogin);
			
		}
		
		// 登陆
		private function onLogin(evt:MouseEvent):void
		{		
			var fdata:MapDataProxy = facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
			if (this.validateLogin())
			{ 
				fdata.login(login.loginName.text, login.loginPwd.text);
			}
		}
		
		// 验证
		private function validateLogin():Boolean
		{
			var loginValidators:Array=[login.validator_loginName, login.validator_loginPwd];
			
			var re:ValidationResultEvent; // 验证返回的结果
			
			for (var i:int=0; i < loginValidators.length; i++)
			{
				
				re=loginValidators[i].validate();
				
				if (re.results != null)
				{
					loginValidators[i].source.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OVER));
					return false;
				}
			}
			return true;
		}
		
		protected function get login():Login
		{
			return viewComponent as Login;
		}
	}
}