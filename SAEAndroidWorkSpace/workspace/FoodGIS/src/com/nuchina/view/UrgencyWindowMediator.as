package com.nuchina.view
{
	import com.nuchina.AppFacade;
	import com.nuchina.model.UrgencyDataProxy;
	import com.nuchina.model.vo.UrgencyDetailVO;
	
	import flash.display.DisplayObject;
	
	import mx.controls.Button;
	import mx.managers.PopUpManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	/**
	 * @author Ykk
	 * 2012-7-31
	 */
	public class UrgencyWindowMediator extends Mediator implements IMediator
	{
		public static const NAME:String = "UrgencyWindowMediator";
		
		private var _win:UrgencyWindow;
		public function UrgencyWindowMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
		}
		
		public function get urgencyWin():UrgencyWindow
		{
			return _win;
		}
		
		override public function handleNotification(notification:INotification):void
		{
			switch(notification.getName())
			{
				case AppFacade.SHOW_URGENCY_WIN:
					showWin();
					break;
			}
		}
		
		override public function listNotificationInterests():Array
		{
			return [
				AppFacade.SHOW_URGENCY_WIN
			];
		}
		
		protected function get view():DisplayObject
		{
			return getViewComponent() as DisplayObject;
		}
		
		private function showWin():void
		{
			_win ||= new UrgencyWindow();
			PopUpManager.addPopUp(_win, view, true);
			PopUpManager.centerPopUp(_win);			
			var urgency:UrgencyDataProxy = AppFacade.getInstance().retrieveProxy(UrgencyDataProxy.NAME) as UrgencyDataProxy;
			AppFacade.getInstance().sendNotification(AppFacade.GET_URGENCY_DETAIL, new UrgencyDetailVO(urgency.currentID, 1));
		}
	}
}