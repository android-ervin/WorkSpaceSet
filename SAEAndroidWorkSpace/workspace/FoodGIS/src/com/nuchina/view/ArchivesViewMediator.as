package com.nuchina.view
{
	import com.nuchina.AppFacade;
	import com.nuchina.model.PopUpWindowsProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	/**
	 * @author Ykk
	 * 2012-6-26
	 */
	public class ArchivesViewMediator extends Mediator
	{
		public static const NAME:String = "ArchivesViewMediator";
		private var _popup:PopUpWindowsProxy;
		public function ArchivesViewMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
			_popup = facade.retrieveProxy(PopUpWindowsProxy.NAME) as PopUpWindowsProxy;
		}
		
		public function get view():ArchivesWindow
		{
			return getViewComponent() as ArchivesWindow;
		}
		
		public function changeCatalogue(index:int):void
		{
			if(index <= 0)
				view.catalogue.selectedIndex = 0;
			else if(index >= 3)
				view.catalogue.selectedIndex = 3;
			else
				view.catalogue.selectedIndex = index;
		}
	}
}