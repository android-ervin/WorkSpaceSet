package com.nuchina.view
{
	import com.esri.ags.FeatureSet;
	import com.esri.ags.Graphic;
	import com.esri.ags.Map;
	import com.esri.ags.SpatialReference;
	import com.esri.ags.events.DrawEvent;
	import com.esri.ags.geometry.Extent;
	import com.esri.ags.geometry.Geometry;
	import com.esri.ags.geometry.MapPoint;
	import com.esri.ags.layers.GraphicsLayer;
	import com.esri.ags.symbols.PictureMarkerSymbol;
	import com.esri.ags.symbols.SimpleFillSymbol;
	import com.esri.ags.tasks.QueryTask;
	import com.esri.ags.tasks.supportClasses.Query;
	import com.esri.ags.tools.DrawTool;
	import com.esri.ags.utils.GraphicUtil;
	import com.esri.ags.utils.WebMercatorUtil;
	import com.nuchina.AppFacade;
	import com.nuchina.controller.GoogleMapLayer;
	import com.nuchina.event.LinkClickEvent;
	import com.nuchina.event.ShopEvent;
	import com.nuchina.model.AppConfig;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.view.components.FilesList;
	import com.nuchina.view.components.LinkItemRender;
	import com.nuchina.view.components.InfoWindowMap;
	import com.nuchina.view.components.RecordList;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.controls.LinkButton;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AsyncResponder;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	import spark.events.IndexChangeEvent;

	public class ArcGISMapMediator extends Mediator
	{
		public static const NAME:String="ArcGISMapMediator";

		public function ArcGISMapMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
			_fdata=facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;		
			view.selectBtnBar.addEventListener(ItemClickEvent.ITEM_CLICK, onDrawToolClickHandler);
			view.drawTool.addEventListener(DrawEvent.DRAW_END, myDrawTool_drawEndHandler);
			view.infoWin.addEventListener(InfoWindowMap.OPEN_FILES_LIST_EVENT, openFilesList);
			view.infoWin.addEventListener(InfoWindowMap.COMPLAINT_EVENT, onOpenComplaintWin);
			searchCounty_onMap("36");
		}

		override public function handleNotification(notification:INotification):void
		{
			switch (notification.getName())
			{
				//企业数据更新
				case AppFacade.ENTERPRISE_UPDATA:
					createMapIcon();
					break;
				//显示企业信息弹窗
				case AppFacade.SHOW_ENTERPRISE_INFO:
					var ei:Object=notification.getBody();
					view.infoWin.enterpriseInfo=ei;
					var mapPoint:MapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(ei.jd, ei.wd)) as MapPoint;
					view.arcgisMap.infoWindow.show(mapPoint);
					var point:Point=view.arcgisMap.toScreen(mapPoint);
					point.y-=view.infoWin.height * 0.5;
					view.arcgisMap.centerAt(view.arcgisMap.toMap(point));
					break;
				//区域定位
				case AppFacade.MAP_POSITION_EVENT:
					var data:Object=notification.getBody();
//					mapPosition(data.x, data.y, data.level);
					searchCounty_onMap(data.areacode);
					break;
				//提交投诉成功
				case AppFacade.SUBMIT_COMPLAINT_SUCCESS:					
					_fdata.getComplaint(view.infoWin.enterpriseInfo.bh);
					break;
				//获取投诉列表成功
				case AppFacade.GET_COMPLAINT_SUCCESS:
					var info:Object = view.infoWin.enterpriseInfo;
					info.complaintData = new ArrayList(notification.getBody() as Array);
					view.infoWin.enterpriseInfo = info;
					break;
			}
		}

		override public function listNotificationInterests():Array
		{
			return [
				AppFacade.SHOW_ENTERPRISE_INFO, 
				AppFacade.SUBMIT_COMPLAINT_SUCCESS, 
				AppFacade.GET_COMPLAINT_SUCCESS, 
				AppFacade.MAP_POSITION_EVENT];
		}
		
		public function updataTabTag():void
		{
			view.infoWin.tabTag = _fdata.tabTag;
		}
		
		protected function get view():ArcGISMap
		{
			return viewComponent as ArcGISMap;
		}
		
		//根据编号查询行政区域
		protected function searchCounty_onMap(code:String, level:int=-1):void
		{
			view.areaLayer.clear();
			var queryTask:QueryTask;
			var query:Query = new Query();
			if(code.length == 2)
			{
				queryTask= new QueryTask(AppConfig.CityURL);
				query.where = " GBPROV="+code;
			}
			else if(code.length == 4 || code.length == 6)
			{
				queryTask= new QueryTask(AppConfig.CountryURL);
				query.where = " GBCNTY="+code+" or GBCITY="+code;
			}
			queryTask.useAMF = false;
			
			query.outSpatialReference = view.arcgisMap.spatialReference;
			query.returnGeometry = true;
			query.outFields = ['GBPROV','GBCITY','GBCNTY','CNAME'];
			queryTask.execute(query, new AsyncResponder(onResult, onFault));
			
			function onResult(featureSet:FeatureSet, token:Object = null):void
			{
				
				for each (var myGraphic:Graphic in featureSet.features)
				{			
					myGraphic.addEventListener(MouseEvent.MOUSE_OVER,myGraphic_mouseOverHandler);
					myGraphic.addEventListener(MouseEvent.ROLL_OUT,myGraphic_mouseOutHandler);
					myGraphic.addEventListener(MouseEvent.CLICK,myGraphic_mouseClickHandler);
					myGraphic.symbol = view.symbolOut;	
					view.areaLayer.add(myGraphic);													
				}
				var graphicProvider:ArrayCollection = view.areaLayer.graphicProvider as ArrayCollection;
				var graphicsExtent:Extent = GraphicUtil.getGraphicsExtent(graphicProvider.toArray());					
				if (graphicsExtent)
				{
					view.arcgisMap.extent = graphicsExtent;
					if (!view.arcgisMap.extent.contains(graphicsExtent))
					{
						view.arcgisMap.level--;
					}
				}
			}
			function onFault(info:Object, token:Object = null):void
			{
				Alert.show(info.toString(),"提示");
			}
		}
		
		protected function myGraphic_mouseOverHandler(event:MouseEvent):void
		{
			var myGraphic:Graphic=event.target as Graphic;
			myGraphic.symbol = view.symbolOver;
		}
		protected function myGraphic_mouseOutHandler(event:MouseEvent):void
		{
			var myGraphic:Graphic=event.target as Graphic;
			myGraphic.symbol = view.symbolOut;
		}
		
		protected function myGraphic_mouseClickHandler(event:MouseEvent):void
		{
			var myGraphic:Graphic=event.target as Graphic;
			searchCounty_onMap(myGraphic.attributes.GBCNTY);
			
		}

		private function createMapIcon():void
		{
			if (_fdata)
			{
				view.arcgisMap.infoWindow.hide();
				view.gLayer.clear();
				for each (var item:Object in _fdata.allEnterprise.source)
				{
					var g:Graphic=new Graphic();
					g.geometry = WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd));
					g.attributes = item;
					g.addEventListener(MouseEvent.CLICK, clickHandler);
					view.gLayer.add(g);
				}
			}
		}

		private function mapPosition(x:Number, y:Number, level:int):void
		{
			var mapPoint:MapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(x, y)) as MapPoint;
			view.arcgisMap.centerAt(mapPoint);
			if (view.arcgisMap.level != level)
				view.arcgisMap.level=level;
		}

		private function clickHandler(event:MouseEvent):void
		{
			var graphic:Graphic=event.currentTarget as Graphic;
			if (graphic)
			{
				view.infoWin.enterpriseInfo=graphic.attributes;
				var mapPoint:MapPoint=MapPoint(graphic.geometry);
				view.arcgisMap.infoWindow.show(mapPoint);
				var point:Point=view.arcgisMap.toScreen(mapPoint);
				point.y-=view.infoWin.height * 0.5;
				view.arcgisMap.centerAt(view.arcgisMap.toMap(point));
			}
		}


		private function myDrawTool_drawEndHandler(event:DrawEvent):void
		{
			view.drawTool.deactivate();
			view.selectBtnBar.selectedIndex=-1;
			var geometry:Geometry=event.graphic.geometry;
			view.gLayer.clear();
			var selectedEnterprise:ArrayCollection=new ArrayCollection();
			var p:Geometry;
			for (var i:int=0; i < _fdata.enterpriseData.length; i++)
			{
				p=WebMercatorUtil.geographicToWebMercator(new MapPoint(_fdata.enterpriseData[i].jd, _fdata.enterpriseData[i].wd)) as MapPoint;
				if (geometry.extent.contains(p))
				{
					selectedEnterprise.addItem(_fdata.enterpriseData[i]);
				}
			}
			_fdata.allEnterprise=selectedEnterprise;
		}

		private function onDrawToolClickHandler(evt:ItemClickEvent):void
		{
			if (view.selectBtnBar.selectedIndex < 0)
			{
				view.drawTool.deactivate();
			}
			else
			{
				switch (evt.item.tip)
				{
					case "POLYGON":
					{
						view.drawTool.activate(DrawTool.POLYGON);
						break;
					}
					case "FREEHAND_POLYGON":
					{
						view.drawTool.activate(DrawTool.FREEHAND_POLYGON);
						break;
					}
					case "EXTENT":
					{
						view.drawTool.activate(DrawTool.EXTENT);
						break;
					}
					case "CIRCLE":
					{
						view.drawTool.activate(DrawTool.CIRCLE);
						break;
					}
					case "ELLIPSE":
					{
						view.drawTool.activate(DrawTool.ELLIPSE);
						break;
					}
				}
			}
		}

		private function openFilesList(evt:Event):void
		{
			var target:InfoWindowMap = evt.currentTarget as InfoWindowMap;
			if (target)
			{
				if (_filesList == null)
				{
					_filesList=new FilesList();
					_filesList.addEventListener(LinkClickEvent.ITEM_CLICK_EVENT, onFilesListClick);
				}
				_filesList.filesList=_fdata.filesList;
				var ei:Object=target.enterpriseInfo;
				_filesList.title=ei.qymc;
				_filesList.eId = ei.bh;
				PopUpManager.addPopUp(_filesList, view, true);
				PopUpManager.centerPopUp(_filesList);					
			}
		}

		private function onOpenComplaintWin(evt:Event):void
		{
			var target:InfoWindowMap=evt.currentTarget as InfoWindowMap;
			if (target)
			{
				_complaint ||= new ComplaintView();
				_complaint.eId = target.enterpriseInfo.bh;
				PopUpManager.addPopUp(_complaint, view, true);
				PopUpManager.centerPopUp(_complaint);
			}
		}
		
		private function onGetComplaintList(evt:Event):void
		{
			var target:InfoWindowMap=evt.currentTarget as InfoWindowMap;
			if (target)
			{				
				var ei:Object=target.enterpriseInfo;
				_fdata.getComplaint(ei.bh);
			}
		}
		
		private function onFilesListClick(evt:LinkClickEvent):void
		{
			if (evt.data)
				sendNotification(AppFacade.SHOW_RECORDlIST, evt.data);
		}

		private var _filesList:FilesList;

		private var _fdata:MapDataProxy;
		private var _complaint:ComplaintView;
	}
}
