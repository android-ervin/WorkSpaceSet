package com.nuchina.view
{
	import mx.collections.IList;

	/**
	 * @author Ykk
	 * 2012-7-29
	 */
	public class AreaUpdataParam
	{
		public var list:IList;
		public var requireSelected:Boolean;
		public function AreaUpdataParam()
		{
		}
	}
}