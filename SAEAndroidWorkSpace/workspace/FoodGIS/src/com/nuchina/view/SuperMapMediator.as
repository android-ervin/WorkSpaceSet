package com.nuchina.view
{
	import com.esri.ags.geometry.MapPoint;
	import com.esri.ags.utils.WebMercatorUtil;
	import com.nuchina.AppFacade;
	import com.nuchina.event.LinkClickEvent;
	import com.nuchina.model.AppModel;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.InfoWindowsProxy;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.model.PanelViewProxy;
	import com.nuchina.model.PerformanceProxy;
	import com.nuchina.model.UrgencyDataProxy;
	import com.nuchina.model.user.EnterpriseUserVO;
	import com.nuchina.model.user.SuperviseUserVO;
	import com.nuchina.model.vo.AreaSearchParam;
	import com.nuchina.model.vo.DrawSearchParam;
	import com.nuchina.model.vo.LinkVO;
	import com.nuchina.view.components.FilesList;
	import com.nuchina.view.components.InfoWindowMap;
	import com.supermap.web.actions.DrawCircle;
	import com.supermap.web.actions.DrawPolygon;
	import com.supermap.web.actions.DrawRectangle;
	import com.supermap.web.actions.Pan;
	import com.supermap.web.core.Feature;
	import com.supermap.web.core.Point2D;
	import com.supermap.web.core.Rectangle2D;
	import com.supermap.web.core.geometry.GeoLine;
	import com.supermap.web.core.geometry.GeoPoint;
	import com.supermap.web.core.geometry.GeoRegion;
	import com.supermap.web.core.geometry.Geometry;
	import com.supermap.web.core.styles.CompositeStyle;
	import com.supermap.web.core.styles.PictureMarkerStyle;
	import com.supermap.web.core.styles.PredefinedFillStyle;
	import com.supermap.web.core.styles.PredefinedMarkerStyle;
	import com.supermap.web.events.DrawEvent;
	import com.supermap.web.events.MapEvent;
	import com.supermap.web.events.ZoomEvent;
	import com.supermap.web.iServerJava6R.FilterParameter;
	import com.supermap.web.iServerJava6R.Recordset;
	import com.supermap.web.iServerJava6R.queryServices.QueryBySQLParameters;
	import com.supermap.web.iServerJava6R.queryServices.QueryBySQLService;
	import com.supermap.web.iServerJava6R.queryServices.QueryResult;
	import com.supermap.web.utils.GeoUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.controls.SWFLoader;
	import mx.events.ItemClickEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AsyncResponder;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	/**
	 * @author Ykk
	 * 2012-5-23
	 */
	public class SuperMapMediator extends Mediator
	{
		public static const NAME:String="SuperMapMediator";
		public static const MAP_LAYER_PROVINCES:String="Province@Shape";
		public static const MAP_LAYER_CITY:String="chinaCity@Shape";
		public static const MAP_LAYER_COUNTRY:String="chinacountry@Shape";
		public static const AREA_PROVINCES:int=0;
		public static const AREA_CITY:int=1;
		public static const AREA_COUNTRY:int=2;
		public static const INFOWIN_ENTERPRISE:int=0;
		public static const INFOWIN_URGERCY:int=1;
		public static const INFOWIN_PATROL_EVENT:int=2;
		private var _currentAreaType:int;
		private var _currentMapLevel:int;
		/**
		 * key：地图服务图层,区域类型
		 */
		private var _featureCache:Dictionary=new Dictionary();
		/**
		 * key：区域类型,level
		 */
		private var _levelDic:Dictionary=new Dictionary();
		
		private var _currentCode:String = null;
		
		
		private var _client:ClientStateProxy;
		private var _mapData:MapDataProxy;
		private var _info:InfoWindowsProxy;
		private var _performance:PerformanceProxy;
		private var _complaint:ComplaintView;
		
		private var _drawCircle:DrawCircle;
		private var _drawRect:DrawRectangle;
		private var _drawPolygon:DrawPolygon;
		private var _panelProxy:PanelViewProxy;

		public function SuperMapMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
			_client=facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			_mapData=facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
			_info = facade.retrieveProxy(InfoWindowsProxy.NAME) as InfoWindowsProxy;
			_performance = facade.retrieveProxy(PerformanceProxy.NAME) as PerformanceProxy;
			_panelProxy = facade.retrieveProxy(PanelViewProxy.NAME) as PanelViewProxy;
			view.selectBtnBar.addEventListener(ItemClickEvent.ITEM_CLICK, onDrawToolClickHandler);
//			view.mapLayer.url = AppConfig.mapBaseUrl;
//			view.overViewLayer.url = AppConfig.mapBaseUrl;
			view.enterpriseInfoWin.addEventListener(InfoWindowMap.COMPLAINT_EVENT, onOpenComplaintWin);
			view.map.addEventListener(ZoomEvent.ZOOM_END, onZoomEnd);
			view.map.addEventListener(MouseEvent.CLICK, onMapClickHandler);
			view.addEventListener(MouseEvent.MOUSE_MOVE, onEnter)
		}

		private function onEnter(evt:MouseEvent):void
		{
			/*var p:Point = view.globalToLocal(new Point(evt.stageX, evt.stageY));
			var p2d:Point2D = view.map.screenToMap(p);
			view.coord.text = p2d.toString();
			var mapP:MapPoint = new MapPoint(p2d.x, p2d.y);
			mapP = WebMercatorUtil.webMercatorToGeographic(mapP) as MapPoint;
			view.jwd.text = mapP.toString();*/
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			if (view.map.loaded)
				onMapLoaded(null);
			else
				view.map.addEventListener(MapEvent.LOAD, onMapLoaded);
		}

		override public function handleNotification(notification:INotification):void
		{
			switch (notification.getName())
			{
				case AppFacade.ENTERPRISE_LISE_UPDATA:
					createHightlightMark();
					break;
				//企业数据更新
				case AppFacade.ENTERPRISE_COORD_UPDATA:
					createEnterpriseMark();
					break;
				//显示企业信息弹窗
				case AppFacade.SHOW_ENTERPRISE_INFO:
					showInfoWin(notification.getBody());
					break;
				case AppFacade.SHOW_PERFOR_INFO:
					showInfoWin(notification.getBody(), INFOWIN_PATROL_EVENT);
					break;
				case AppFacade.SHOW_URGENCY_INFO:
					showInfoWin(notification.getBody(), INFOWIN_URGERCY);
					break;
				//区域定位
				case AppFacade.MAP_POSITION_EVENT:
					var linkVO:LinkVO=notification.getBody() as LinkVO;
					if(linkVO)
					{
						if (linkVO.code.length == 2)
						{
							getAreaFeature(linkVO.code, MAP_LAYER_CITY);
						}
						else if (linkVO.code.length == 4)
						{
							getAreaFeature(linkVO.code, MAP_LAYER_COUNTRY);
						}
						else if (linkVO.code.length == 6)
						{
							getAreaFeature(linkVO.code, MAP_LAYER_COUNTRY);
						}
					}
					break;
				//提交投诉成功
				case AppFacade.SUBMIT_COMPLAINT_SUCCESS:
					_mapData.getComplaint(view.enterpriseInfoWin.enterpriseInfo.bh);
					break;
				//获取投诉列表成功
				case AppFacade.GET_COMPLAINT_SUCCESS:
					var info:Object=view.enterpriseInfoWin.enterpriseInfo;
					info.complaintData=new ArrayList(notification.getBody() as Array);
					view.enterpriseInfoWin.enterpriseInfo=info;
					break;
				case AppFacade.URGENCY_LIST_UPDATA:
				case AppFacade.HISTORY_LIST_UPDATA:
					createAlertMark();
					break;
				case AppFacade.ENTERPRISE_INFO_UPDATA:
					view.enterpriseInfoWin.currentState = view.enterpriseInfoWin.prevState;
					view.enterpriseInfoWin.enterpriseInfo = _info.currentEInfo;
					if(_client.userVO is SuperviseUserVO)
					{
						view.enterpriseInfoWin.modifyBtn.visible = false;
						if(_info.currentEInfo.ssqbh == SuperviseUserVO(_client.userVO).ssqbh)
							view.enterpriseInfoWin.archivesBtn.enabled = true;
						else
							view.enterpriseInfoWin.archivesBtn.enabled = false;
					}
					else if(_client.userVO is EnterpriseUserVO)
					{
						if(_info.currentEInfo.bh == _client.userVO.id){
							view.enterpriseInfoWin.archivesBtn.enabled = true;
							view.enterpriseInfoWin.modifyBtn.visible = true;
						}
						else{
							view.enterpriseInfoWin.archivesBtn.enabled = false;
							view.enterpriseInfoWin.modifyBtn.visible = false;
						}
					}
					else
					{
						view.enterpriseInfoWin.modifyBtn.visible = false;
						view.enterpriseInfoWin.archivesBtn.enabled = false;
					}
					break;
				case AppFacade.URGENCY_INFO_UPDATA:
					view.urgencyInfoWin.currentState = "normal";
					view.urgencyInfoWin.setData(_info.currentUrgencyInfo);
					break;
				case AppFacade.TRACK_COORD_UPDATA:
					createTrack();
					break;
				case AppFacade.PATROL_EVENT_UPDATA:
					createPatrolEventMark();
					break;
				case AppFacade.PATROL_INFO_UPDATA:
					view.patrolInfoWin.currentState = "normal";
					view.patrolInfoWin.eventDG.dataProvider = _info.patrolInfo;
					view.patrolInfoWin.time.text = _info.patrolInfoTime;
					break;
			}
		}

		override public function listNotificationInterests():Array
		{
			return [
				AppFacade.ENTERPRISE_COORD_UPDATA, 
				AppFacade.PATROL_EVENT_UPDATA, 
				AppFacade.SHOW_ENTERPRISE_INFO, 
				AppFacade.SHOW_PERFOR_INFO, 
				AppFacade.SHOW_URGENCY_INFO, 
				AppFacade.SUBMIT_COMPLAINT_SUCCESS, 
				AppFacade.GET_COMPLAINT_SUCCESS, 
				AppFacade.MAP_POSITION_EVENT,
				AppFacade.ENTERPRISE_INFO_UPDATA,
				AppFacade.ENTERPRISE_LISE_UPDATA,
				AppFacade.URGENCY_INFO_UPDATA,
				AppFacade.TRACK_COORD_UPDATA,
				AppFacade.PATROL_INFO_UPDATA,
				AppFacade.URGENCY_LIST_UPDATA,
				AppFacade.HISTORY_LIST_UPDATA
			];
		}

		public function updataTabTag():void
		{
			view.enterpriseInfoWin.tabTag=_client.tabTag;
		}

		public function get view():SuperMap
		{
			return viewComponent as SuperMap;
		}

		//根据编号查询行政区域
		public function getAreaFeature(code:String, mapLayerType:String=MAP_LAYER_PROVINCES):void
		{
			if(_currentCode == code)
				return;
			_currentCode = code;
			var areaType:int=getAreaTypeByCode(code);
			var key:String=mapLayerType + "," + areaType + "," + code;
			if (_featureCache[key])
			{
				changeFeatureByKey(key, areaType);
			}
			else
			{
				var queryBySQLParam:QueryBySQLParameters=new QueryBySQLParameters();
				var filter:FilterParameter=new FilterParameter();
				if (mapLayerType == MAP_LAYER_PROVINCES)
				{
					filter.fields=["BOU2_4M_ID","NAME"];
					filter.name=MAP_LAYER_PROVINCES;
					if(code == "-1")
					{
						filter.attributeFilter="smid>0";
					}
					else if (code.length == 2)
					{
						filter.attributeFilter="BOU2_4M_ID=" + code;
					}
				}
				else if (mapLayerType == MAP_LAYER_CITY)
				{
					filter.fields=["GBCITY","CNAME"];
					filter.name=MAP_LAYER_CITY;
					if (code.length == 2)
					{
						filter.attributeFilter="GBPROV=" + code;
					}
					else if (code.length == 4)
					{
						filter.attributeFilter="GBCITY=" + code;
					}
					else if (code.length == 6)
					{
						filter.attributeFilter="GBCNTY=" + code;
					}
				}
				else if (mapLayerType == MAP_LAYER_COUNTRY)
				{
					filter.fields=["GBCNTY","CNAME"];
					filter.name=MAP_LAYER_COUNTRY;
					if (code.length == 2)
					{
						filter.attributeFilter="GBPROV=" + code;
					}
					else if (code.length == 4)
					{
						filter.attributeFilter="GBCITY=" + code;
					}
					else if (code.length == 6)
					{
						filter.attributeFilter="GBCNTY=" + code;
					}
				}
				queryBySQLParam.filterParameters=[filter];
				queryBySQLParam.returnContent=true;
				view.areaLayer.enabled=false;
				/* 执行 SQL 查询 */
				var setting:AppSettingProxy=facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
				var queryByDistanceService:QueryBySQLService=new QueryBySQLService(setting.mapBaseUrl);
				queryByDistanceService.processAsync(queryBySQLParam, new AsyncResponder(function(queryResult:QueryResult, mark:Object=null):void
				{
					cleanAreaFeature();
					if (queryResult.recordsets == null || queryResult.recordsets.length == 0)
					{
						//Alert.show("查询结果为空");
						return;
					}
					var recordSets:Array=queryResult.recordsets;
					if (recordSets.length != 0)
					{
						for each (var recordSet:Recordset in recordSets)
						{
							if (!recordSet.features || recordSet.features.length == 0)
							{
								//Alert.show("当前图层查询结果为空");
								return;
							}
							for each (var feature:Feature in recordSet.features)
							{
								view.areaLayer.addFeature(feature);
								feature.style=view.outStyle;
								if (mapLayerType == MAP_LAYER_PROVINCES)
									feature.toolTip = feature.attributes["NAME"];
								else
									feature.toolTip = feature.attributes["CNAME"];
								feature.addEventListener(MouseEvent.CLICK, onAreaClickHandler);
								feature.addEventListener(MouseEvent.MOUSE_OVER, onAreaOverHandler);
								feature.addEventListener(MouseEvent.MOUSE_OUT, onAreaOutHandler);
							}
							_currentAreaType=areaType;
							_featureCache[key]=recordSet.features;
							view.map.viewBounds=view.areaLayer.bounds;
							var key2:String=areaType + "," + view.map.level;
							for (var k:String in _levelDic)
							{
								var type:String=k.split(",")[0];
								if (type == areaType)
								{
									delete _levelDic[k];
									break;
								}
							}
							_levelDic[key2]=key;
							view.map.zoomToLevel(view.map.level, view.areaLayer.bounds.center);
						}
					}
					view.areaLayer.enabled=true;
				}, function(object:Object, mark:Object=null):void
				{
					_currentCode = null;
					//Alert.show("与服务端交互失败", "抱歉");
				}, null));
			}
		}

		private function changeFeatureByKey(key:String, areaType:int):void
		{
			_currentAreaType=areaType;
			var fs:Array=_featureCache[key];
			cleanAreaFeature();
			view.areaLayer.features=fs.concat();
			view.map.viewBounds=view.areaLayer.bounds;
			view.map.zoomToLevel(view.map.level, view.areaLayer.bounds.center);
		}

		private function onZoomEnd(evt:ZoomEvent):void
		{
			var key:String;
			var targetAreaType:int;
			if (evt.level < _currentMapLevel)
			{
				targetAreaType=_currentAreaType - 1;
			}
			else
			{
				targetAreaType=_currentAreaType + 1;
			}
			key=targetAreaType + "," + evt.level;
			_currentMapLevel=evt.level;
			if (_levelDic[key])
			{
				changeFeatureByKey(_levelDic[key], targetAreaType);
			}
//			if (view.map.level < 6)
//			{
//				view.enterpriseLayer.style=new PictureMarkerStyle("assets/img/e_03.png");
//			}
//			else if (view.map.level < 12)
//			{
//				view.enterpriseLayer.style=new PictureMarkerStyle("assets/img/e_02.png");
//			}
//			else
//			{
//				view.enterpriseLayer.style=new PictureMarkerStyle("assets/img/e_01.png");
//			}
			if(view.map.level >= 12)
				view.featureVisible = true;
			else
				view.featureVisible = false;
		}

		private function onMapLoaded(evt:MapEvent):void
		{
			if (evt != null)
				view.map.removeEventListener(MapEvent.LOAD, onMapLoaded);
			view.map.infoWindow.setStyle("backgroundColor", "#F6F9E8");
			view.map.infoWindow.setStyle("infoPlacement", "top");
			view.map.infoWindow.setStyle("borderColor", "#006737");
			view.map.infoWindow.closeButton = view.closeBtn;
			//				map.viewBounds = new Rectangle2D(6757255.550132199, 1929770.9736388698, 16159091.0045962, 7472936.460333269);
			//				map.restrictedBounds = new Rectangle2D(6757255.550132199, 1929770.9736388698, 16159091.0045962, 7472936.460333269);
			//				map.minResolution = 1.1955052;
			//				map.maxResolution = 9793.5785984;
			//				map.resolutions = [9793.5785984, 4896.7892992, 2448.3946496, 1224.1973248, 612.0986624, 306.0493312, 
			//					153.0246656, 76.5123328, 38.2561664, 19.1280832, 9.5640416, 4.7820208, 2.3910104, 1.1955052];

			//				map.restrictedBounds = new Rectangle2D(65.13162273209477, 11.09687605932275, 145.90279933312266, 58.19359888624564);
			//				map.viewBounds = new Rectangle2D(65.13162273209477, 11.09687605932275, 145.90279933312266, 58.19359888624564);
			//				map.minScale =  4e-8;
			//				map.maxScale = 6.4e-7;
			view.map.viewBounds=new Rectangle2D(5141839.980514627, -302435.51376914047, 18461106.874338627, 7649950.308131659);
			_currentMapLevel=view.map.level;
		}

		private function onMapClickHandler(evt:MouseEvent):void
		{
			var appModel:AppModel = facade.retrieveProxy(AppModel.NAME) as AppModel;
			if(appModel.isFlagState)
			{
				view.hightLightLayer.clear();
				var f:Feature = new Feature();
				var p:Point = view.globalToLocal(new Point(evt.stageX, evt.stageY));
				var p2d:Point2D = view.map.screenToMap(p);
				f.geometry = new GeoPoint(p2d.x, p2d.y);
				f.style = new PictureMarkerStyle("assets/img/marks/mark.png");
				view.hightLightLayer.addFeature(f);
				var mapP:MapPoint = new MapPoint(p2d.x, p2d.y);
				mapP = WebMercatorUtil.webMercatorToGeographic(mapP) as MapPoint;
				appModel.point = new Point(mapP.x, mapP.y);
			}
		}
		
		private function createEnterpriseMark():void
		{
			if (_mapData)
			{
				view.map.infoWindow.hide();
				view.enterpriseLayer.clear();
				for each (var item:Object in _mapData.showECoord)
				{
					var f:Feature=new Feature();
					var mapPoint:MapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;
					f.geometry=new GeoPoint(mapPoint.x, mapPoint.y);
					f.attributes=item;
					switch(item.savelevel)
					{
						case "0":							
							f.style = new PredefinedMarkerStyle(PredefinedMarkerStyle.SYMBOL_CIRCLE, 6, 0x22B14C);
							break;
						case "1":
							f.style = new PredefinedMarkerStyle(PredefinedMarkerStyle.SYMBOL_CIRCLE, 6, 0xFFF648);
							break;
						case "2":
							f.style = new PredefinedMarkerStyle(PredefinedMarkerStyle.SYMBOL_CIRCLE, 6, 0xED1C24);
							break;
						default:
							f.style = new PredefinedMarkerStyle(PredefinedMarkerStyle.SYMBOL_CIRCLE, 6, 0xED1C24);
							break;
					}
					f.addEventListener(MouseEvent.CLICK, onFeatureClickHandler);
					view.enterpriseLayer.addFeature(f);
				}
			}
		}
		
		private function createHightlightMark():void
		{
			if (_panelProxy.enterpriseList)
			{
				view.map.infoWindow.hide();
				view.hightLightLayer.clear();
				var i:int = 0;
				for each (var item:Object in _panelProxy.enterpriseList)
				{
					var f:Feature=new Feature();
					var mapPoint:MapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;
					f.geometry=new GeoPoint(mapPoint.x, mapPoint.y);
					f.attributes=item;
					f.addEventListener(MouseEvent.CLICK, onFeatureClickHandler);
					var source:String
					switch(item.savelevel)
					{
						case "0":							
							source = "assets/img/marks/mark_green_"+i+".png";
							break;
						case "1":
							source = "assets/img/marks/mark_yellow_"+i+".png";
							break;
						case "2":
							source = "assets/img/marks/mark_red_"+i+".png";
							break;
						default:
							source = "assets/img/marks/mark_red_"+i+".png";
							break;
					}
					f.style = new PictureMarkerStyle(source);
					view.hightLightLayer.addFeature(f);
					i++;
				}
			}
		}
		
		private function createAlertMark():void
		{
			var urgency:UrgencyDataProxy = facade.retrieveProxy(UrgencyDataProxy.NAME) as UrgencyDataProxy;
			if (urgency)
			{
				view.map.infoWindow.hide();
				view.alertLayer.clear();
				_info.alertLightDic = new Dictionary();
				var swf:SWFLoader;
				var item:Object;
				var mapPoint:MapPoint;
				if(urgency.urgencyList){
					for each (item in urgency.urgencyList)
					{
						swf = new SWFLoader();
						swf.source = "assets/swf/alert.swf";
						mapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;					
						view.alertLayer.addComponent(swf, new Rectangle2D(mapPoint.x, mapPoint.y, mapPoint.x, mapPoint.y));
						swf.addEventListener(MouseEvent.CLICK, onAlertLightClick);
						_info.alertLightDic[swf] = item;
					}				
				}
				if(urgency.urgencyHistoryList){
					for each (item in urgency.urgencyHistoryList)
					{
						swf = new SWFLoader();
						swf.source = "assets/swf/alert.swf";
						mapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;					
						view.alertLayer.addComponent(swf, new Rectangle2D(mapPoint.x, mapPoint.y, mapPoint.x, mapPoint.y));
						swf.addEventListener(MouseEvent.CLICK, onAlertLightClick);
						_info.alertLightDic[swf] = item;
					}			
				}
			}
		}
		
		private function createTrack():void
		{
			view.trackLayer.clear();
			if (_performance && _performance.trackCoord.length >= 2)
			{
				var arr:Array = [];
				var i:int = 0;
				for each (var item:Object in _performance.trackCoord)
				{
					var mapPoint:MapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;
					arr.push(new Point2D(mapPoint.x + 579.11576041, mapPoint.y - 435.985543474));
					i++;
//					if(i>20)
//						break;
//					trace(item.jd, item.wd, mapPoint.toString());
				}
				var geoLine:GeoLine = new GeoLine();
				geoLine.addPart(arr);
				var lineFeature:Feature = new Feature(geoLine, view.myLineStyle);
				view.trackLayer.addFeatureAt(lineFeature, 0);
			}
		}

		private function createPatrolEventMark():void
		{
			if (_performance)
			{
				view.patrolEventLayer.clear();
				for each (var item:Object in _performance.patrolEvents)
				{
					var f:Feature=new Feature();
					var mapPoint:MapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;
					f.geometry=new GeoPoint(mapPoint.x, mapPoint.y);
					f.attributes=item;
					f.addEventListener(MouseEvent.CLICK, onPatrolEventClickHandler);
					view.patrolEventLayer.addFeature(f);
				}
			}
		}
		
		private function onFeatureClickHandler(evt:MouseEvent):void
		{
			var f:Feature=evt.currentTarget as Feature;
			if (f)
			{
				showInfoWin(f.attributes);
			}
		}

		private function onPatrolEventClickHandler(evt:MouseEvent):void
		{
			var f:Feature=evt.currentTarget as Feature;
			if (f)
			{
				showInfoWin(f.attributes, INFOWIN_PATROL_EVENT);
			}
		}
		
		private function showInfoWin(item:Object, type:int=INFOWIN_ENTERPRISE):void
		{
			if(item){
				switch(type)
				{
					case INFOWIN_ENTERPRISE:
						view.map.infoWindow.content=view.enterpriseInfoWin;
						view.enterpriseInfoWin.currentState = "loading";
						_info.getEInfoByID(item.bh);
						break;
					case INFOWIN_URGERCY:
						view.map.infoWindow.content=view.urgencyInfoWin;
						view.urgencyInfoWin.currentState = "loading";
						_info.getUrgencyInfo(item.zbbsh);
						var urgency:UrgencyDataProxy = facade.retrieveProxy(UrgencyDataProxy.NAME) as UrgencyDataProxy;
						urgency.currentID = item.zbbsh;
						break;
					case INFOWIN_PATROL_EVENT:
						view.map.infoWindow.content=view.patrolInfoWin;
						view.patrolInfoWin.currentState = "loading";
						view.patrolInfoWin.enterpriseName.text = item.qymc;
						_info.getPatrolInfo(item.bsh);
						break;						
				}				
				if(view.map.level <13)
				{
					view.map.zoomToLevel(13);
				}
				view.areaLayer.clear();
				var mapPoint:MapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;
				var point2D:Point2D=new Point2D(mapPoint.x, mapPoint.y);
				view.map.infoWindow.show(point2D);
				var point:Point=view.map.mapToScreen(point2D);
				point.y-=140;
				point2D = view.map.screenToMap(point);
				view.map.panTo(point2D);
			}
		}
		
		private function onAlertLightClick(evt:MouseEvent):void
		{
			var swf:SWFLoader = evt.currentTarget as SWFLoader;
			var item:Object = _info.alertLightDic[swf];
			showInfoWin(item, INFOWIN_URGERCY);
		}
		
		private function drawEndHandler(event:DrawEvent):void
		{
			view.selectBtnBar.selectedIndex=-1;
			view.map.action=new Pan(view.map);
			var geo:GeoRegion=event.feature.geometry as GeoRegion;
			if (geo)
			{
				view.enterpriseLayer.clear();
				var mapPoint:MapPoint;
				var point:GeoPoint;
				var selectedEnterprise:Array=new Array();
				for each(var item:Object in _mapData.allEnterpriseCoord)
				{
					mapPoint=WebMercatorUtil.geographicToWebMercator(new MapPoint(item.jd, item.wd)) as MapPoint;
					point=new GeoPoint(mapPoint.x, mapPoint.y);
					if (geo.contains(point))
					{
						selectedEnterprise.push(item);
					}
				}
				_mapData.showECoord=selectedEnterprise;
				var param:DrawSearchParam = new DrawSearchParam();
				param.list = selectedEnterprise;
				_panelProxy.searchParam = param;
				_panelProxy.getEInfoList(param, 1, _panelProxy.numOnePage);
			}
		}

		private function onDrawToolClickHandler(evt:ItemClickEvent):void
		{
			if (view.selectBtnBar.selectedIndex < 0)
			{
				view.map.action=new Pan(view.map);
			}
			else
			{
				switch (evt.item.tip)
				{
					case "多边形":
						if (!_drawPolygon)
						{
							_drawPolygon=new DrawPolygon(view.map);
							_drawPolygon.addEventListener(DrawEvent.DRAW_END, drawEndHandler);
						}
						view.map.action=_drawPolygon;
						break;
					case "矩形":
						if (!_drawRect)
						{
							_drawRect=new DrawRectangle(view.map);
							_drawRect.addEventListener(DrawEvent.DRAW_END, drawEndHandler);
						}
						view.map.action=_drawRect;
						break;
					case "圆形":
						if (!_drawCircle)
						{
							_drawCircle=new DrawCircle(view.map);
							_drawCircle.addEventListener(DrawEvent.DRAW_END, drawEndHandler);
						}
						view.map.action=_drawCircle;
						break;
				}
			}
		}

		private function onOpenComplaintWin(evt:Event):void
		{
			var target:InfoWindowMap=evt.currentTarget as InfoWindowMap;
			if (target)
			{
				_complaint||=new ComplaintView();
				_complaint.eId=target.enterpriseInfo.bh;
				PopUpManager.addPopUp(_complaint, view, true);
				PopUpManager.centerPopUp(_complaint);
			}
		}

		private function onFilesListClick(evt:LinkClickEvent):void
		{
			if (evt.data)
				sendNotification(AppFacade.SHOW_RECORDlIST, evt.data);
		}

		private function cleanAreaFeature():void
		{
			while (view.areaLayer.numFeatures > 0)
			{
				view.areaLayer.removeFeatureAt(0);
//				f.removeEventListener(MouseEvent.CLICK, onAreaClickHandler);
//				f.removeEventListener(MouseEvent.MOUSE_OVER, onAreaOverHandler);
//				f.removeEventListener(MouseEvent.MOUSE_OUT, onAreaOutHandler);				
			}
		}

		private function onAreaClickHandler(evt:MouseEvent):void
		{
			var code:String;
			var name:String;
			var f:Feature=evt.currentTarget as Feature;
			if (f.attributes["BOU2_4M_ID"])
			{
				code=f.attributes["BOU2_4M_ID"];
				name = f.attributes["NAME"];
				code=code.substr(0, 2);
				getAreaFeature(code, MAP_LAYER_CITY);
			}
			else if (f.attributes.hasOwnProperty("GBCITY"))
			{
				code=f.attributes["GBCITY"];
				name = f.attributes["CNAME"];
				getAreaFeature(code, MAP_LAYER_COUNTRY);
			}
			else
			{
				code=f.attributes["GBCNTY"];
				name = f.attributes["CNAME"];
				view.map.viewBounds=f.geometry.bounds;
				cleanAreaFeature();
				_currentAreaType=AREA_COUNTRY;
			}
			if(name){
				var areaParam:AreaSearchParam=new AreaSearchParam();
				areaParam.keyword=code;
				_panelProxy.searchParam=areaParam;
				_panelProxy.getEInfoList(areaParam, 1, _panelProxy.numOnePage);
				var mapP:MapDataProxy = AppFacade.getInstance().retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
				mapP.getCoordByArea(code);
				sendNotification(AppFacade.START_CATEGORY_SEARCH, name);					
			}
		}

		private function onAreaOverHandler(evt:MouseEvent):void
		{
			var f:Feature=evt.currentTarget as Feature;
			f.style=view.overStyle;

		}

		private function onAreaOutHandler(evt:MouseEvent):void
		{
			var f:Feature=evt.currentTarget as Feature;
			f.style=view.outStyle;
		}

		private function getAreaTypeByCode(code:String):int
		{
			var type:int;
			switch (code.length)
			{
				case 2:
					type=AREA_PROVINCES;
					break;
				case 4:
					type=AREA_CITY;
					break;
				case 6:
					type=AREA_COUNTRY;
					break;
			}
			return type;
		}

	}
}
