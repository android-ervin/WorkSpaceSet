package com.nuchina.view
{
	import com.nuchina.AppFacade;
	
	import mx.events.FlexEvent;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class AppMediator extends Mediator
	{
		public static const NAME:String = "ApplicationMediator";
		public function AppMediator( viewComponent:Object ) 
		{
			super( NAME, viewComponent );				
		}
		
		override public function listNotificationInterests():Array 
		{
			
			return [
					AppFacade.LOGIN_STATE_UPDATA
					];
		}
		
		override public function handleNotification( note:INotification ):void 
		{
			switch ( note.getName() ) 
			{      
				case AppFacade.LOGIN_STATE_UPDATA:
					break;
			}
		}
		
		public function get main():Main
		{
			return viewComponent as Main
		}
		
	}
}
