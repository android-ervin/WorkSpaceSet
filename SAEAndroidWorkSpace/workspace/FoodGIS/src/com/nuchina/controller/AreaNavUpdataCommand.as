package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.event.NavigationEvent;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.AreaDataProxy;
	import com.nuchina.model.vo.LinkVO;
	import com.nuchina.view.PanelViewMediator;
	import com.nuchina.view.components.NavigationComponent;
	
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-8-18
	 */
	public class AreaNavUpdataCommand extends ResponderCommand
	{
		private var _mode:AreaDataProxy;
		private var _setting:AppSettingProxy;
		private var _panelView:PanelViewMediator;
		private var _code:String;
		private var _link:LinkVO;
		/**
		 * 0(市、区、县)， 1（镇），2（街） 
		 */		
		private var areaType:int = 0;
		override public function execute(notification:INotification):void
		{
			_mode = facade.retrieveProxy(AreaDataProxy.NAME) as AreaDataProxy;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			_panelView = facade.retrieveMediator(PanelViewMediator.NAME) as PanelViewMediator;
			_link  = notification.getBody() as LinkVO;
			_code = _link.code;
			if(_code)
			{
				if(_code.length == 6)
				{
					areaType = 1;
				}
				else if(_code.length > 6 && _code.split(",").length == 2)
				{
					areaType = 2;
				}
				else if(_code.length < 6)
				{
					areaType = 0;
				}
				else
				{
					return;
				}
				
				if(_mode.hasList(_code))
				{
					_panelView.view.areaNav.updata(_link, objToLinkVO(_mode.getList(_code), areaType));
				}
				else
				{
					getData();
				}
				
			}
		}
		
		override public function fault(evt:Object):void
		{
			if(_currentTime < _reloadTime)
			{
				getData();
				_currentTime++;
			}
		}
		
		override protected function getData():void
		{
			if(_currentTime < _reloadTime)
			{
				var token:AsyncToken;
				if(areaType == 0)
				{
					token = _setting.webServer.send("ProcessData", "getAreaname|"+_code);
				}
				else if(areaType == 1)
				{
					token = _setting.webServer.send("ProcessData", "getTownName|"+_code);
				}
				else if(areaType == 2)
				{
					token = _setting.webServer.send("ProcessData", "getStreetName|"+_code.split(",")[1]);
				}
				token.addResponder(this);
				_currentTime++;
			}
		}
		
		override public function result(evt:Object):void
		{
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result);
				_mode.addList(_code, obj.data);
				_panelView.view.areaNav.updata(_link, objToLinkVO(_mode.getList(_code), areaType));
			}
		}
		
		private function objToLinkVO(arr:Array, type:int):Array
		{
			var t:Array = [];
			for each(var item:Object in arr)
			{
				if(type == 0)
					t.push(new LinkVO(item["areaname"], item["areacode"]));
				else if(type == 1)
					t.push(new LinkVO(item["zhenname"], _code+","+item["autobh"]));
				else if(type == 2)
					t.push(new LinkVO(item["jiename"], _code+","+item["autobh"]));
			}				
			return t;
		}
	}
}