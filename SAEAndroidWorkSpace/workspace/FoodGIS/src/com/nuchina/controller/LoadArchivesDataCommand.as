package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.InfoWindowsProxy;
	import com.nuchina.model.PopUpWindowsProxy;
	import com.nuchina.view.ArchivesViewMediator;
	import com.nuchina.view.ArchivesWindow;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-6-26
	 */
	public class LoadArchivesDataCommand extends ResponderCommand
	{
		private var _item:Object;
		private var archives:ArchivesViewMediator;
		override public function execute(notification:INotification):void
		{
			_item = notification.getBody();
			getData();
		}		
		
		override protected function getData():void
		{
			if(_currentTime < _reloadTime)
			{
				var info:InfoWindowsProxy = facade.retrieveProxy(InfoWindowsProxy.NAME) as InfoWindowsProxy;
				var _setting:AppSettingProxy = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
				var _client:ClientStateProxy = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
				archives = facade.retrieveMediator(ArchivesViewMediator.NAME) as ArchivesViewMediator;
				var token:AsyncToken;
				switch(_item.showtype)
				{
					case "word":
					case "image":
						archives.view.currentState = "word";
						token = _setting.amfServer.send("getArchivesByType", _item.resourceid, info.currentEInfo.bh);
						token.addResponder(this);
						break;
					case "getDynamicLeve":
						archives.view.currentState = "table";
						if(_client.userVO.type == ClientStateProxy.USER_TYPE_A)
						{//client.userVO.type, client.userVO.id, info.currentEInfo.bh, client.userVO.psw, client.area);
							token = _setting.webServer.send("ProcessData", 
								getParamsCode("getDynamicLeve", _client.userVO.type, _client.userVO.id, 
									info.currentEInfo.bh, _client.userVO.psw, _client.area));
							token.addResponder(this);
						}
						else if(_client.userVO.type == ClientStateProxy.USER_TYPE_B)
						{
							token = _setting.webServer.send("ProcessData", 
								getParamsCode("getDynamicLeve", _client.userVO.type, _client.userVO.id, 
									info.currentEInfo.bh, _client.userVO.psw, info.currentEInfo.ssqbh));
							token.addResponder(this);
						}
						break;
					case "geYearLeve":
						archives.view.currentState = "table";
						if(_client.userVO.type == ClientStateProxy.USER_TYPE_A)
						{//client.userVO.type, client.userVO.id, info.currentEInfo.bh, client.userVO.psw, client.area);
							token = _setting.webServer.send("ProcessData", 
								getParamsCode("geYearLeve", _client.userVO.type, _client.userVO.id, 
									info.currentEInfo.bh, _client.userVO.psw, _client.area));
							token.addResponder(this);
						}
						else if(_client.userVO.type == ClientStateProxy.USER_TYPE_B)
						{
							token = _setting.webServer.send("ProcessData", 
								getParamsCode("geYearLeve",  _client.userVO.type, _client.userVO.id, 
									_client.userVO.psw, info.currentEInfo.ssqbh));
							token.addResponder(this);
						}
						break;
					case "getCheckList":
					case "getDiaryInCheck":
					case "getSuggestInCheck":
					case "getDiaryInActivity":
					case "getSuggestInActivity":
						archives.view.currentState = "complex";
						if(_client.userVO.type == ClientStateProxy.USER_TYPE_A)
						{//client.userVO.type, client.userVO.id, info.currentEInfo.bh, client.userVO.psw, client.area);
							token = _setting.webServer.send("ProcessData", 
								getParamsCode("getCheckList", info.currentEInfo.bh));
							token.addResponder(this);
						}
						else if(_client.userVO.type == ClientStateProxy.USER_TYPE_B)
						{
							token = _setting.webServer.send("ProcessData", 
								getParamsCode("getCheckList",  info.currentEInfo.bh));
							token.addResponder(this);
						}
						break;
					case "getAccident":
						archives.view.currentState = "table";
						token = _setting.webServer.send("ProcessData", 
							getParamsCode("getAccident", info.currentEInfo.bh));
						token.addResponder(this);
						break;
				}
				_currentTime++;
			}
		}
		
		override public function fault(evt:Object):void
		{
			getData();
		}
		
		override public function result(evt:Object):void
		{
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result);
				if(obj.success == 1)
				{
					switch(_item.showtype)
					{
						case "word":
						case "image":
							archives.view.wordList.dataProvider = new ArrayCollection(obj.data);
							break;
						case "getDynamicLeve":
							archives.view.table.dataProvider = new ArrayCollection(obj.data);
							break;
						case "geYearLeve":
							archives.view.table.dataProvider = new ArrayCollection(obj.data);
							break;
						case "getCheckList":
						case "getDiaryInCheck":
						case "getSuggestInCheck":
						case "getDiaryInActivity":
						case "getSuggestInActivity":
							archives.view.complexList.dataProvider = new ArrayCollection(obj.data);
							if(archives.view.complexList.dataProvider.length > 0)
							{
								var item:Object = archives.view.complexList.dataProvider.getItemAt(0);
								if(item)
								{
									if(!facade.hasCommand(AppFacade.GET_NOTE))
										facade.registerCommand(AppFacade.GET_NOTE, GetNoteCommand);
									sendNotification(AppFacade.GET_NOTE, item);
								}
							}
							break;
						case "getAccident":
							archives.view.table.dataProvider = new ArrayCollection(obj.data);
							break;
					}
				}
			}
			else
			{
				archives.view.table.dataProvider = new ArrayCollection();
				archives.view.wordList.dataProvider = new ArrayCollection();
				archives.view.complexList.dataProvider = new ArrayCollection();
			}
		}
	}
}