package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.AreaDataProxy;
	import com.nuchina.model.vo.AreaListParam;
	
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-9-26
	 */
	public class GetTownNameCommand extends ResponderCommand
	{
		private var _param:AreaListParam;
		private var _setting:AppSettingProxy;
		private var _reloadTime:int = 3;
		private var _currentTime:int = 0;
		override public function fault(evt:Object):void
		{
			getData();
		}
		
		override protected function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken = _setting.webServer.send("ProcessData", "getTownName|"+_param.areaCode);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}			
		}
		
		override public function result(evt:Object):void
		{
			var obj:Object = JSON.decode(evt.result.toString());
			if(obj.success==1)
			{
				var arr:Array = obj.data as Array;
				_param.callback(arr);
				_param = null;
				_setting = null;
			}
			else
			{
				getData();
			}
		}
		
		override public function execute(notification:INotification):void
		{
			_param = notification.getBody() as AreaListParam;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			getData();
		}
		
		
	}
}