package com.nuchina.controller
{
	import com.supermap.web.mapping.TiledCachedLayer;
	import com.supermap.web.core.*;
	import com.supermap.web.mapping.*;
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	
	import mx.events.*;
	/**
	 * @author Ykk
	 * 2012-8-29
	 */
	public class TiledGoogleMapLayer extends TiledCachedLayer
	{
		public function TiledGoogleMapLayer()
		{
			this.bounds = new Rectangle2D(-2.00375e+007, -2.00375e+007, 2.00375e+007, 2.00375e+007);
			this.CRS = new CoordinateReferenceSystem(102113);
			this.buildTileInfo();
			setLoaded(true);				
		}
		
		override protected function getTileURL(row:int, col:int, level:int) : URLRequest
		{
			var url:String="http://mt"+(col%4)+".google.cn/vt/v=w2.114&hl=zh-CN&gl=cn&" +
				"x=" + col + "&" +
				"y=" + row + "&" +
				"z=" + level+ "&s=";
			
			return new URLRequest(url);
		}
		
		private function buildTileInfo() : void
		{
			this.tileSize = 256;
			this.origin = new Point2D(-2.00375e+007, 2.00375e+007);
			this.resolutions = [78271.5, 39135.8, 19567.9,
				9783.94, 4891.97, 2445.98, 1222.99,  
				611.496, 305.748, 152.874, 76.437, 
				38.2185, 19.1093, 9.55463, 4.77731,
				2.38866, 1.19433];
			/*this.resolutions = [78271.5, 39135.8, 19567.9,
			9783.94, 4891.97, 2445.98, 1222.99,  
			611.496, 305.748, 152.874, 76.437, 
			38.2185, 19.1093, 9.55463, 4.77731,
			2.38866, 1.19433, 0.597164, 0.298582,
			0.149291, 0.074646, 0.037323, 0.018661];*/
		}
		
		
	}
}