package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.vo.UrgencyDetailVO;
	import com.nuchina.view.UrgencyWindowMediator;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-8-1
	 */
	public class GetUrgencyDetail extends SimpleCommand implements ICommand, IResponder
	{
		private var _setting:AppSettingProxy;
		private var _reloadTime:int = 3;
		private var _currentTime:int = 0;
		private var _param:UrgencyDetailVO;
		private var _view:UrgencyWindowMediator;
		override public function execute(notification:INotification):void
		{
			_param = notification.getBody() as UrgencyDetailVO;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			_view = facade.retrieveMediator(UrgencyWindowMediator.NAME) as UrgencyWindowMediator;
			getData();
		}
		
		
		public function result(evt:Object):void
		{
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj)
				{
					if(obj.success==1)
					{
						_view.urgencyWin.currentState = "normal";
						_view.urgencyWin.btns.selectedIndex = _param.index-1;
						var arr:Array = obj.data as Array;
						_view.urgencyWin.setData(arr[0], _param.index);
					}
					else
					{
						getData();
					}
				}
			}
			else
			{
				trace("getYjdetail result is null or ''");
			}
		}
		
		public function fault(evt:Object):void
		{
			getData();
		}
		
		private function getData():void
		{
			_view.urgencyWin.currentState = "loading";
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken = _setting.webServer.send("ProcessData", "getYjdetail|" + _param.id + "|" + _param.index);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}			
		}
		
	}
}