package com.nuchina.controller
{
	import com.nuchina.view.FoodGISMediator;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-5-17
	 */
	public class ShowLoginWinCommand extends SimpleCommand
	{
		
		override public function execute(notification:INotification):void
		{
			var foodGIS:FoodGISMediator = facade.retrieveMediator(FoodGISMediator.NAME) as FoodGISMediator;
			foodGIS.showLoginWin();
		}
		
	}
}