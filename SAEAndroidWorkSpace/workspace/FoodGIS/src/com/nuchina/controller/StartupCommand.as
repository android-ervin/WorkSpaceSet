package com.nuchina.controller
{
	import org.puremvc.as3.patterns.command.AsyncMacroCommand;
	import org.puremvc.as3.patterns.command.MacroCommand;
	
	public class StartupCommand extends AsyncMacroCommand
	{		
		override protected function initializeAsyncMacroCommand():void
		{
			addSubCommand( PowerCheckCommand );
			addSubCommand( ModelPrepCommand );
			addSubCommand( ViewPrepCommand );
			addSubCommand(GetInitDataCommand);
		}
		
	}
}
