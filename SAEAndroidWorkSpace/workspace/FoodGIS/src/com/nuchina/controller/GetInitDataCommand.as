package com.nuchina.controller
{
	import com.nuchina.AppFacade;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.model.PanelViewProxy;
	import com.nuchina.model.vo.AreaSearchParam;
	import com.nuchina.model.vo.LinkVO;
	import com.nuchina.view.PanelViewMediator;
	import com.nuchina.view.SuperMapMediator;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-6-24
	 */
	public class GetInitDataCommand extends SimpleCommand
	{	
		override public function execute(notification:INotification):void
		{
			var foodProxy:MapDataProxy = facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
			var client:ClientStateProxy = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			client.checkLoginState();
			var panelView:PanelViewMediator = facade.retrieveMediator(PanelViewMediator.NAME) as PanelViewMediator;
			panelView.view.categoryNav.init(new LinkVO("分类搜索", ""));
			panelView.view.areaNav.init(new LinkVO("江西", "36"));
		}
	}
}