package com.nuchina.controller
{
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.user.SuperviseUserVO;
	import com.nuchina.view.PanelViewMediator;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-7-27
	 */
	public class ChangeSupervisorMode extends SimpleCommand
	{
		
		override public function execute(notification:INotification):void
		{
			var client:ClientStateProxy = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			if(client.userVO is SuperviseUserVO)
			{
				var panel:PanelViewMediator = facade.retrieveMediator(PanelViewMediator.NAME) as PanelViewMediator;
				panel.changePannelTab(1, notification.getBody().toString());
			}
		}
		
	}
}