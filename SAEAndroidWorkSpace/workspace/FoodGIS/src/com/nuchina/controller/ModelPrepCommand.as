package com.nuchina.controller
{
	import com.nuchina.model.AppModel;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.AreaDataProxy;
	import com.nuchina.model.CategoryDataProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.InfoWindowsProxy;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.model.PanelViewProxy;
	import com.nuchina.model.PerformanceProxy;
	import com.nuchina.model.PopUpWindowsProxy;
	import com.nuchina.model.UrgencyDataProxy;
	import com.nuchina.model.business.IService;
	import com.nuchina.model.business.ServiceFactory;
	import com.nuchina.model.vo.RemoteParamsVo;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class ModelPrepCommand extends AsyncCommand
	{
		private var _setting:AppSettingProxy;
		override public function execute( note:INotification ) :void    
		{			
			_setting = new AppSettingProxy();
			facade.registerProxy(_setting);
			_setting.getSetting(initModel);
		}
		
		private function initModel():void
		{
			var rParams:RemoteParamsVo = new RemoteParamsVo(_setting.remoteXML.@destination, _setting.remoteXML.@source);
			//			params.endpoint = "http://{server.name}:{server.port}/Gateway.aspx";
			rParams.endpoint = _setting.domain + _setting.remoteXML.@endpoint;
			var wsParams:Object = {wsdl: _setting.wsdlUrl};
			var remoteService:IService = ServiceFactory.getService(ServiceFactory.ROMOTE_OBJECT, rParams);
			var webService:IService = ServiceFactory.getService(ServiceFactory.WEB_SERVICE, wsParams);
			_setting.amfServer = remoteService;
			_setting.webServer = webService;
			facade.registerProxy(new AreaDataProxy());
			facade.registerProxy(new CategoryDataProxy());
			//地图数据
			var mapData:MapDataProxy = new MapDataProxy();
			mapData.webService = webService;
			mapData.remoteService = remoteService;
			facade.registerProxy(mapData);
			//左侧面板数据
			var panelData:PanelViewProxy = new PanelViewProxy();
			panelData.webService = webService;
			panelData.remoteService = remoteService;
			facade.registerProxy(panelData);
			//登陆状态
			var client:ClientStateProxy = new ClientStateProxy();
			client.webService = webService;
			client.remoteService = remoteService;
			facade.registerProxy(client);
			//info弹窗数据
			var info:InfoWindowsProxy = new InfoWindowsProxy();
			info.webService = webService;
			info.remoteService = remoteService;
			facade.registerProxy(info);
			//弹窗
			var popup:PopUpWindowsProxy = new PopUpWindowsProxy();
			popup.webService = webService;
			popup.remoteService = remoteService;
			facade.registerProxy(popup);
			//绩效面板数据
			var per:PerformanceProxy = new PerformanceProxy();
			per.webService = webService;
			per.remoteService = remoteService;
			facade.registerProxy(per);
			//应急
			var urgency:UrgencyDataProxy = new UrgencyDataProxy();
			urgency.webService = webService;
			urgency.remoteService = remoteService;
			facade.registerProxy(urgency);
			var appModel:AppModel = new AppModel();
			facade.registerProxy(appModel);
			commandComplete();
		}
	}
}