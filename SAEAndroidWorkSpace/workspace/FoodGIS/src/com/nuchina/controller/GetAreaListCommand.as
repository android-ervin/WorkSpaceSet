package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.AreaDataProxy;
	import com.nuchina.model.vo.AreaListParam;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-7-28
	 */
	public class GetAreaListCommand extends SimpleCommand implements ICommand, IResponder
	{
		private var _param:AreaListParam;
		private var _areaList:AreaDataProxy;
		private var _setting:AppSettingProxy;
		private var _reloadTime:int = 3;
		private var _currentTime:int = 0;
		override public function execute(notification:INotification):void
		{
			_areaList = facade.retrieveProxy(AreaDataProxy.NAME) as AreaDataProxy;
			_param = notification.getBody() as AreaListParam;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			if(_areaList.hasList(_param.areaCode))
			{
				_param.callback(_areaList.getList(_param.areaCode));
			}
			else
			{
				getData();
			}
		}
		
		public function result(evt:Object):void
		{
			var obj:Object = JSON.decode(evt.result.toString());
			if(obj.success==1)
			{
				var arr:Array = obj.data as Array;
				_areaList.addList(_param.areaCode, arr);
				_param.callback(arr);
				_areaList = null;
				_param = null;
				_setting = null;
			}
			else
			{
				getData();
			}
		}
		
		public function fault(evt:Object):void
		{
			getData();
		}
		
		private function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken = _setting.webServer.send("ProcessData", "getAreaname|"+_param.areaCode);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}			
		}
	}
}