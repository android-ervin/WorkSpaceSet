package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.model.PanelViewProxy;
	import com.nuchina.model.PopUpWindowsProxy;
	import com.nuchina.model.user.EnterpriseUserVO;
	import com.nuchina.model.user.SuperviseUserVO;
	import com.nuchina.model.user.UserVO;
	import com.nuchina.model.vo.SSQBHSearchParam;
	import com.nuchina.view.ArchivesViewMediator;
	import com.nuchina.view.FoodGISMediator;
	import com.nuchina.view.PanelViewMediator;
	import com.nuchina.view.SuperMapMediator;
	
	import mx.controls.Alert;
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-5-16
	 */
	public class LoginStateUpdataCommand extends SimpleCommand implements IResponder
	{
		
		private var _setting:AppSettingProxy;
		private var _reloadTime:int = 3;
		private var _currentTime:int = 0;
		private var _client:ClientStateProxy;
		override public function execute(notification:INotification):void
		{
			_client = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			var food:MapDataProxy = facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
			var panel:PanelViewMediator = facade.retrieveMediator(PanelViewMediator.NAME) as PanelViewMediator;
			if(!_client.userVO){
				_client.userVO = new UserVO();
				_client.userVO.type = ClientStateProxy.USER_TYPE_D; 
			}
			switch (_client.userVO.type)
			{
				case ClientStateProxy.USER_TYPE_A:
					if (!panel.view.tabView.contains(panel.view.performanceView))
						panel.view.tabView.addElement(panel.view.performanceView);
					if (!panel.view.tabView.contains(panel.view.urgencyView))
						panel.view.tabView.addElement(panel.view.urgencyView);
					panel.view.areaVisible = false;
					break;
				case ClientStateProxy.USER_TYPE_E:
				case ClientStateProxy.USER_TYPE_F:
				case ClientStateProxy.USER_TYPE_G:
					if (!panel.view.tabView.contains(panel.view.performanceView))
						panel.view.tabView.addElement(panel.view.performanceView);
					if (!panel.view.tabView.contains(panel.view.urgencyView))
						panel.view.tabView.addElement(panel.view.urgencyView);
					panel.view.areaVisible = true;
					break;
				case ClientStateProxy.USER_TYPE_B:
					if (panel.view.tabView.contains(panel.view.performanceView))
						panel.view.tabView.removeElement(panel.view.performanceView);
					if (panel.view.tabView.contains(panel.view.urgencyView))
						panel.view.tabView.removeElement(panel.view.urgencyView);
					break;
				case ClientStateProxy.USER_TYPE_C:
				case ClientStateProxy.USER_TYPE_D:
					if (panel.view.tabView.contains(panel.view.performanceView))
						panel.view.tabView.removeElement(panel.view.performanceView);
					if (panel.view.tabView.contains(panel.view.urgencyView))
						panel.view.tabView.removeElement(panel.view.urgencyView);
					break;
			}
			//			var srir:ClassFactory=new ClassFactory(SearchResultItemRender);
			//			srir.properties={btns: _fdata.tabTag};
			//			view.resultList.itemRenderer=srir;
			var map:SuperMapMediator = facade.retrieveMediator(SuperMapMediator.NAME) as SuperMapMediator;
			map.updataTabTag();
			var archives:ArchivesViewMediator = facade.retrieveMediator(ArchivesViewMediator.NAME) as ArchivesViewMediator;
			archives.view.navList1 = _client.list1;
			archives.view.navList2 = _client.list2;
			archives.view.navList3 = _client.list3;
			archives.view.navList4 = _client.list4;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			var supermap:SuperMapMediator;
			if(_client.userVO is SuperviseUserVO)
			{
				supermap = facade.retrieveMediator(SuperMapMediator.NAME) as SuperMapMediator;
				supermap.getAreaFeature("36", SuperMapMediator.MAP_LAYER_CITY);
				getData();
			}
			else if(_client.userVO is EnterpriseUserVO)
			{
				facade.registerCommand(AppFacade.GET_ENTERPRISE_X_Y, GetEnterpriseXYCommand);
				sendNotification(AppFacade.GET_ENTERPRISE_X_Y);
			}
			else
			{				
				supermap = facade.retrieveMediator(SuperMapMediator.NAME) as SuperMapMediator;
				supermap.getAreaFeature("36", SuperMapMediator.MAP_LAYER_CITY);
				var fgis:FoodGISMediator = facade.retrieveMediator(FoodGISMediator.NAME) as FoodGISMediator;
				fgis.changeLoginState();
				supermap.view.map.infoWindow.hide();
				supermap.view.trackLayer.clear();
				supermap.view.patrolEventLayer.clear();
				supermap.view.alertLayer.clear();
			}
		}
		
		
		public function result(evt:Object):void
		{
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success==1)
				{
					var item:Object = obj.data[0];
					SuperviseUserVO(_client.userVO).ssqbh = item.ssqbh;
					SuperviseUserVO(_client.userVO).zfrygzzh = item.zfrygzzh;
					SuperviseUserVO(_client.userVO).areacode = item.areacode;
					var panelProxy:PanelViewProxy = facade.retrieveProxy(PanelViewProxy.NAME) as PanelViewProxy;
					var param:SSQBHSearchParam=new SSQBHSearchParam();
					param.keyword = item.ssqbh;
					panelProxy.searchParam=param;
					panelProxy.getEInfoList(param, 1, panelProxy.numOnePage);
					var fgis:FoodGISMediator = facade.retrieveMediator(FoodGISMediator.NAME) as FoodGISMediator;
					fgis.changeLoginState();					
					var supermap:SuperMapMediator = facade.retrieveMediator(SuperMapMediator.NAME) as SuperMapMediator;
					supermap.getAreaFeature("36", SuperMapMediator.MAP_LAYER_CITY);
				}
				else
				{
					getData();
				}
			}
		}
		
		public function fault(evt:Object):void
		{
			getData();
		}
		
		private function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken
				token = _setting.webServer.send("ProcessData", "getZfryArea|"+_client.userVO.id+"|"+SuperviseUserVO(_client.userVO).ssqbh);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}			
		}
	}
}