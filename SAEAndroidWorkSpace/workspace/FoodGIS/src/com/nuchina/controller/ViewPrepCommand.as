package com.nuchina.controller
{
	import com.nuchina.AppFacade;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.view.AppMediator;
	import com.nuchina.view.ArcGISMapMediator;
	import com.nuchina.view.ArchivesViewMediator;
	import com.nuchina.view.ArchivesWindow;
	import com.nuchina.view.FoodGIS;
	import com.nuchina.view.FoodGISMediator;
	import com.nuchina.view.Login;
	import com.nuchina.view.LoginMediator;
	import com.nuchina.view.PanelViewMediator;
	import com.nuchina.view.SuperMap;
	import com.nuchina.view.SuperMapMediator;
	import com.nuchina.view.UrgencyWindowMediator;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class ViewPrepCommand extends SimpleCommand
	{
		override public function execute( note:INotification ) :void    
		{
			var client:ClientStateProxy = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			var food:MapDataProxy = facade.retrieveProxy(MapDataProxy.NAME) as MapDataProxy;
			var appMed:AppMediator = new AppMediator(note.getBody());
			facade.registerMediator(appMed);
			var foodMed:FoodGISMediator = new FoodGISMediator(appMed.main.foodGISView);
			facade.registerMediator(foodMed);
			var mapMed:SuperMapMediator = new SuperMapMediator(foodMed.view.superMap);
			facade.registerMediator(mapMed);
			var panelMed:PanelViewMediator = new PanelViewMediator(foodMed.view.panelView);
			facade.registerMediator(panelMed);
			var archives:ArchivesViewMediator = new ArchivesViewMediator(new ArchivesWindow());
			facade.registerMediator(archives);
			var urgencyWin:UrgencyWindowMediator = new UrgencyWindowMediator(foodMed.view);
			facade.registerMediator(urgencyWin);
		}
	}
}