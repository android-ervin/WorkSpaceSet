package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-8-13
	 */
	public class GetEnterprisePowerCommand extends ResponderCommand
	{
		private var _setting:AppSettingProxy;
		private var _client:ClientStateProxy;
		
		override public function execute(notification:INotification):void
		{
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			_client = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			getData();
		}
		
		override public function result(evt:Object):void
		{
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success==1)
				{
					facade.removeCommand(AppFacade.GET_ENTERPRISE_POWER);
					var item:Object = obj.data[0];
					if(item.gisflag == "0")
					{
						
					}
					else if(item.gisflag == "1")
					{
						sendNotification(AppFacade.START_FLAG_X_Y);
					}
				}
				else
				{
					getData();
				}
			}
		}
		
		override public function fault(evt:Object):void
		{
			getData();
		}
		
		override protected function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken
				token = _setting.webServer.send("ProcessData", "getQyBz|"+_client.userVO.id);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}
		}
		
	}
}