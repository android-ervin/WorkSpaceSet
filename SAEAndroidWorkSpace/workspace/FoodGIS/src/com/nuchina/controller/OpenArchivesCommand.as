package com.nuchina.controller
{
	import com.nuchina.model.InfoWindowsProxy;
	import com.nuchina.model.MapDataProxy;
	import com.nuchina.model.PopUpWindowsProxy;
	import com.nuchina.view.AppMediator;
	import com.nuchina.view.ArchivesViewMediator;
	
	import mx.managers.PopUpManager;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-6-25
	 */
	public class OpenArchivesCommand extends SimpleCommand
	{
		
		override public function execute(notification:INotification):void
		{
			var app:AppMediator = facade.retrieveMediator(AppMediator.NAME) as AppMediator;
			var archives:ArchivesViewMediator = facade.retrieveMediator(ArchivesViewMediator.NAME) as ArchivesViewMediator;
			PopUpManager.addPopUp(archives.view, app.main, true);
			PopUpManager.centerPopUp(archives.view);
			if(notification.getBody())
			{
				archives.changeCatalogue(notification.getBody() as int);
			}
		}
		
	}
}