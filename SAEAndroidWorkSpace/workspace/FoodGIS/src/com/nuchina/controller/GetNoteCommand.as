package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.view.ArchivesViewMediator;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-8-31
	 */
	public class GetNoteCommand extends ResponderCommand
	{
		
		private var _id:String;
		override protected function getData():void
		{
			var _setting:AppSettingProxy = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			var token:AsyncToken = _setting.webServer.send("ProcessData", 
				getParamsCode("getNoteByBsh", _id));
			token.addResponder(this);
		}
		
		override public function result(evt:Object):void
		{
			var avm:ArchivesViewMediator = facade.retrieveMediator(ArchivesViewMediator.NAME) as ArchivesViewMediator;
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result);
				if(obj.success == 1)
				{
					avm.view.table.dataProvider = new ArrayCollection(obj.data);
				}
				else
				{
					avm.view.table.dataProvider = new ArrayCollection();
				}
			}
			else
			{
				avm.view.table.dataProvider = new ArrayCollection();
			}
		}
		
		override public function execute(notification:INotification):void
		{
			var item:Object = notification.getBody();
			_id = item["bsh"];
			getData();
		}
		
	}
}