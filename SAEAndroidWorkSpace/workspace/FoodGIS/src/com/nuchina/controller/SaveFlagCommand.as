package com.nuchina.controller
{
	import com.nuchina.model.AppModel;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.PanelViewProxy;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-8-14
	 */
	public class SaveFlagCommand extends ResponderCommand
	{
		private var _setting:AppSettingProxy;
		private var _client:ClientStateProxy;
		private var _appModel:AppModel;
		override public function fault(evt:Object):void
		{
			getData();
		}
		
		override protected function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken
				token = _setting.webServer.send("ProcessData", 
					"EditQySite|"+_appModel.point.x+"|"+_appModel.point.y+"|"+_client.userVO.id);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}			
		}
		
		override public function result(evt:Object):void
		{
			var appModel:AppModel = facade.retrieveProxy(AppModel.NAME) as AppModel;
			var panelData:PanelViewProxy = facade.retrieveProxy(PanelViewProxy.NAME) as PanelViewProxy;
			panelData.enterpriseUserItem.jd = appModel.point.x;
			panelData.enterpriseUserItem.wd = appModel.point.y;
			panelData.enterpriseList = new ArrayCollection([panelData.enterpriseUserItem]);
			panelData.enterpriseAmount = 0;
		}
		
		override public function execute(notification:INotification):void
		{
			_appModel = facade.retrieveProxy(AppModel.NAME) as AppModel;
			_appModel.isFlagState = false;
			if(notification.getBody())
			{
				_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
				_client = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
				if(_appModel.point)
				{
					getData();
				}
				else
				{
					Alert.show("请点击地图来定位企业的坐标！", "提示");
				}
			}else
			{
				var panelData:PanelViewProxy = facade.retrieveProxy(PanelViewProxy.NAME) as PanelViewProxy;
				panelData.enterpriseList = new ArrayCollection([panelData.enterpriseUserItem]);
				panelData.enterpriseAmount = 0;
			}
		}
		
	}
}