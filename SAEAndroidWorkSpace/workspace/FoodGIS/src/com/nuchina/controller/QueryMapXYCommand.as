package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.model.AppModel;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.view.PanelViewMediator;
	import com.nuchina.view.SuperMapMediator;
	import com.supermap.web.core.Feature;
	import com.supermap.web.iServerJava6R.FilterParameter;
	import com.supermap.web.iServerJava6R.Recordset;
	import com.supermap.web.iServerJava6R.queryServices.QueryBySQLParameters;
	import com.supermap.web.iServerJava6R.queryServices.QueryBySQLService;
	import com.supermap.web.iServerJava6R.queryServices.QueryResult;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AsyncResponder;
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-8-20
	 */
	public class QueryMapXYCommand extends ResponderCommand
	{
		private var _model:AppModel;
		private var _setting:AppSettingProxy;
		private var _code:String;
		override public function execute(notification:INotification):void
		{
			_code = notification.getBody().toString();
			_model = facade.retrieveProxy(AppModel.NAME) as AppModel;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			getData();
			
		}
		
		override protected function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var queryBySQLParam:QueryBySQLParameters=new QueryBySQLParameters();
				var filter:FilterParameter=new FilterParameter();
				if (_code && _code.length == 4)
				{
					filter.fields=["GBCITY","CNAME"];
					filter.name=SuperMapMediator.MAP_LAYER_CITY;
					filter.attributeFilter="GBCITY=" + _code;
				}
				else if (_code && _code.length == 6)
				{
					filter.fields=["GBCNTY","CNAME"];
					filter.name=SuperMapMediator.MAP_LAYER_COUNTRY;
					filter.attributeFilter="GBCNTY=" + _code;
				}
				queryBySQLParam.filterParameters=[filter];
				queryBySQLParam.returnContent=true;
				/* 执行 SQL 查询 */
				var setting:AppSettingProxy=facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
				var queryByDistanceService:QueryBySQLService=new QueryBySQLService(setting.mapBaseUrl);
				queryByDistanceService.processAsync(queryBySQLParam, new AsyncResponder(function(queryResult:QueryResult, mark:Object=null):void
				{
					if (queryResult.recordsets == null || queryResult.recordsets.length == 0)
					{
						return;
					}
					var recordSets:Array=queryResult.recordsets;
					if (recordSets.length != 0)
					{
						for each (var recordSet:Recordset in recordSets)
						{
							if (!recordSet.features || recordSet.features.length == 0)
							{
								return;
							}
							for each (var feature:Feature in recordSet.features)
							{
								var supermap:SuperMapMediator = facade.retrieveMediator(SuperMapMediator.NAME) as SuperMapMediator;
								supermap.view.map.zoomToLevel(13, feature.geometry.center);
							}
						}
					}
				}, function(object:Object, mark:Object=null):void
				{
					getData();
				}, null));
			}else{
				trace("load GetAreaName is fault");
			}
		}
		
	}
}