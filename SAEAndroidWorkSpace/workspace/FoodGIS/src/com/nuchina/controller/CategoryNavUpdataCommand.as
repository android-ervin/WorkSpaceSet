package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.event.NavigationEvent;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.AreaDataProxy;
	import com.nuchina.model.CategoryDataProxy;
	import com.nuchina.model.vo.LinkVO;
	import com.nuchina.view.PanelViewMediator;
	import com.nuchina.view.components.NavigationComponent;
	
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-8-19
	 */
	public class CategoryNavUpdataCommand extends ResponderCommand
	{
		private var _mode:CategoryDataProxy;
		private var _setting:AppSettingProxy;
		private var _panelView:PanelViewMediator;
		private var _code:String;
		private var _link:LinkVO;
		override public function execute(notification:INotification):void
		{
			_mode = facade.retrieveProxy(CategoryDataProxy.NAME) as CategoryDataProxy;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			_panelView = facade.retrieveMediator(PanelViewMediator.NAME) as PanelViewMediator;
			_link  = notification.getBody() as LinkVO;
			_code = _link.code;
			if(_mode.hasList(_code) && _mode.getList(_code).length > 0)
			{
				var arr:Array = [];
				for each(var item:Object in _mode.getList(_code))
				{
					arr.push(new LinkVO(item["categoryname"], item["categoryid"]));
				}
				_panelView.view.categoryNav.updata(_link, arr);
			}
			else
			{
				getData();
			}
		}
		
		override public function fault(evt:Object):void
		{
			if(_currentTime < _reloadTime)
			{
				getData();
				_currentTime++;
			}
		}
		
		override protected function getData():void
		{
			if(_currentTime < _reloadTime)
			{
				var token:AsyncToken = _setting.amfServer.send("getCategory", _code);
				token.addResponder(this);
				_currentTime++;
			}
		}
		
		override public function result(evt:Object):void
		{
			if(evt.result)
			{
				var data:Object = JSON.decode(evt.result);
				if(data.data && data.data.length > 0)
				{
					var arr:Array = [];
					for each(var item:Object in data.data)
					{
						arr.push(new LinkVO(item["categoryname"], item["categoryid"]));
					}
					_panelView.view.categoryNav.updata(_link, arr);
				}
				_mode.addList(_code, data.data);
			}
		}
		
	}
}