package com.nuchina.controller
{
	import com.nuchina.model.AppSettingProxy;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	import org.puremvc.as3.interfaces.ICommand;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * @author Ykk
	 * 2012-8-13
	 */
	public class ResponderCommand extends SimpleCommand implements ICommand, IResponder
	{
		protected var _reloadTime:int = 3;
		protected var _currentTime:int = 0;
		public function ResponderCommand()
		{
			
		}
		
		
		public function result(evt:Object):void
		{
			
		}
		
		public function fault(evt:Object):void
		{
			
		}
		
		protected function getData():void
		{
			
		}
		
		
		protected function getParamsCode(...argms):String
		{
			var code:String = "";
			if(argms && argms.length > 0){
				code += argms[0];
				for(var i:int = 1; i < argms.length; i++)
				{
					code += "|"+argms[i];
				}
			}
			return code;
		}
		
	}
}