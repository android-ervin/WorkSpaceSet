package com.nuchina.controller
{
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	public class HttpUtil
	{
		// 发送ajax请求
		public static function send(url:String,callBack:Function,params:Object=null,method:String="post",resultFormat:String="object"):void
		{
			var hs:HTTPService = new HTTPService();
			hs.url = url;
			
			// 添加回调函数
			hs.addEventListener(ResultEvent.RESULT,callBack);
			
			hs.method = method;
			
			
			/**
			 * 返回数据的格式resultFormat有几种类型，object、array、xml、flashvars、text和e4x，默认的设置为object。 
				RESULT_FORMAT_ARRAY : String = "array" 
				[] 结果格式“array”与“object”相似，但是其返回的值始终为数组；这样，如果从结果格式“object”返回的结果尚不是数组，则将把该项目添加为一个新数组的第一个项目。 
				RESULT_FORMAT_E4X : String = "e4x" 
				[] 结果格式“e4x”指定返回的值是一个 XML 实例，此实例可以使用 ECMAScript for XML (E4X) 表达式访问。 
				RESULT_FORMAT_FLASHVARS : String = "flashvars" 
				[] 结果格式“flashvars”指定返回的值是包含由 & 符号分隔的名称=值对的文本，该文本被分析为 ActionScript 对象。 
				RESULT_FORMAT_OBJECT : String = "object" 
				[] 结果格式“object”指定返回的值是 XML，但按照 ActionScript 对象树分析。 
				RESULT_FORMAT_TEXT : String = "text" 
				[] 结果格式“text”指定 结果文本应为未经处理的字符串。 
				RESULT_FORMAT_XML : String = "xml" 
				[] 结果格式“xml”指定结果应作为指向父 flash.xml.XMLDocument 的第一个子项的 flash.xml.XMLNode 实例返回。 
			*/
			
			hs.resultFormat = resultFormat;
			
			hs.send(params);
		}
		//打开新窗口
		public static function openNewWin(href:String, hrefName:String = "_blank"):void
		{
			ExternalInterface.call("window.open",href,hrefName);
		}
		/**
		 * 
		 * @param href 地址
		 * @param hrefName 跳转类型 "_blank" "_self"
		 * 
		 */		
		// 打开新页面
		public static function openNewPage(href:String, hrefName:String = "_self"):void
		{
			//跳转新页面：
			var strUrl:URLRequest = new URLRequest(href);
			//这个地址是Flex生成的html文件的地址
			navigateToURL(strUrl, hrefName);
		}

	}
}