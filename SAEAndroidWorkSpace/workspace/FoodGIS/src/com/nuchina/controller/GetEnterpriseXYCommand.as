package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.model.PanelViewProxy;
	import com.nuchina.model.user.EnterpriseUserVO;
	import com.nuchina.view.FoodGISMediator;
	import com.nuchina.view.PanelViewMediator;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;

	/**
	 * @author Ykk
	 * 2012-8-13
	 */
	public class GetEnterpriseXYCommand extends ResponderCommand
	{
		private var _setting:AppSettingProxy;
		private var _client:ClientStateProxy;
		override public function execute(notification:INotification):void
		{
			_client = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			getData();
		}
		
		
		override public function result(evt:Object):void
		{
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result.toString());
				if(obj.success==1)
				{
					facade.removeCommand(AppFacade.GET_ENTERPRISE_X_Y);
					var item:Object = obj.data[0];
					EnterpriseUserVO(_client.userVO).jd = item.jd;
					EnterpriseUserVO(_client.userVO).wd = item.wd;
					EnterpriseUserVO(_client.userVO).lxdz = item.lxdz;
					EnterpriseUserVO(_client.userVO).qymc = item.qymc;
					EnterpriseUserVO(_client.userVO).ssqbh = item.ssqbh;				
					var fgis:FoodGISMediator = facade.retrieveMediator(FoodGISMediator.NAME) as FoodGISMediator;
					fgis.changeLoginState();
					var panel:PanelViewProxy = facade.retrieveProxy(PanelViewProxy.NAME) as PanelViewProxy;
					panel.enterpriseUserItem = item;
					panel.enterpriseList = new ArrayCollection([item]);
					panel.enterpriseAmount = 0;
					if(item.jd==""|| item.wd=="")
					{
						facade.registerCommand(AppFacade.GET_ENTERPRISE_POWER, GetEnterprisePowerCommand);
						sendNotification(AppFacade.GET_ENTERPRISE_POWER);
					}
					else
					{
						sendNotification(AppFacade.SHOW_ENTERPRISE_INFO, item);		
					}
				}
				else
				{
					getData();
				}
			}
		}
		
		override public function fault(evt:Object):void
		{
			getData();
		}
		
		override protected function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken
				facade.registerCommand(AppFacade.GET_ENTERPRISE_POWER, GetEnterprisePowerCommand);
				token = _setting.webServer.send("ProcessData", "getQySite|"+_client.userVO.id);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}			
		}
	}
}