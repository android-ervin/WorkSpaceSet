package com.nuchina.controller
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import net.nuchina.core.AuthorityManager;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.AsyncCommand;
	
	/**
	 * @author Ykk
	 * 2012-9-25
	 */
	public class PowerCheckCommand extends AsyncCommand
	{
		public static const NAME:String = "AppSettingProxy";
		private var _settingUrl:String = "assets/setting.xml";
		private var _callback:Function;
		private var _server:String = "{server.name}";
		
		public function get server():String
		{
			return _server;
		}
		
		public function set server(value:String):void
		{
			_server = value;
		}
		
		private var _port:String = "{server.port}";
		
		public function get port():String
		{
			return _port;
		}
		
		public function set port(value:String):void
		{
			_port = value;
		}
		
		private var _virtualPath:String = "";
		
		public function get virtualPath():String
		{
			return _virtualPath;
		}
		
		public function set virtualPath(value:String):void
		{
			_virtualPath = value;
		}
		
		
		public function get domain():String
		{
			var url:String = "http://"+server;
			if(port)
				url += ":"+port;
			if(virtualPath)
				url += "/"+virtualPath;
			return url;
		}
		
		private var _mapBaseUrl:String = "";
		
		public function get mapBaseUrl():String
		{
			return _mapBaseUrl;
		}
		
		public function set mapBaseUrl(value:String):void
		{
			_mapBaseUrl = value;
		}
		
		private var _wsdlUrl:String = "";
		
		public function get wsdlUrl():String
		{
			return _wsdlUrl;
		}
		
		public function set wsdlUrl(value:String):void
		{
			_wsdlUrl = value;
		}
		
		
		private var _remoteXML:XML;
		
		public function get remoteXML():XML
		{
			return _remoteXML;
		}
		
		public function set remoteXML(value:XML):void
		{
			_remoteXML = value;
		}

		private function onInitSetting(evt:Event):void
		{
			var urlloder:URLLoader = evt.currentTarget as URLLoader;
			urlloder.removeEventListener(Event.COMPLETE, onInitSetting);
			var xml:XML = new XML(URLLoader(evt.currentTarget).data);
			server = xml.server;
			port = xml.port;
			virtualPath = xml.virtualPath;
			mapBaseUrl = xml.mapBaseUrl;
			wsdlUrl = xml.webServer.@wsdl;
			_remoteXML = new XML(xml.remoteServer);
			if(_callback!=null)
				_callback();
		}
		
		private var _main:Main;
		override public function execute(notification:INotification):void
		{
			_main = notification.getBody() as Main;
			var mgr:AuthorityManager = new AuthorityManager(_main, authorityCallBack, _main.version);
			mgr.start();
		}
		
		private function authorityCallBack():void
		{
			commandComplete();
		}
		
	}
}