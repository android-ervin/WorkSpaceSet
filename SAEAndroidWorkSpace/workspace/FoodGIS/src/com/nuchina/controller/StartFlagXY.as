package com.nuchina.controller
{
	import com.adobe.serialization.json.JSON;
	import com.nuchina.AppFacade;
	import com.nuchina.model.AppModel;
	import com.nuchina.model.AppSettingProxy;
	import com.nuchina.model.ClientStateProxy;
	import com.nuchina.view.AppMediator;
	import com.nuchina.view.SuperMap;
	import com.nuchina.view.SuperMapMediator;
	import com.nuchina.view.components.FlagOkWindow;
	
	import mx.managers.PopUpManager;
	import mx.rpc.AsyncToken;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;

	/**
	 * @author Ykk
	 * 2012-8-13
	 */
	public class StartFlagXY extends ResponderCommand
	{
		private var _setting:AppSettingProxy;
		private var _client:ClientStateProxy;
		override public function execute(notification:INotification):void
		{
			_setting = facade.retrieveProxy(AppSettingProxy.NAME) as AppSettingProxy;
			_client = facade.retrieveProxy(ClientStateProxy.NAME) as ClientStateProxy;
			var appModel:AppModel = facade.retrieveProxy(AppModel.NAME) as AppModel;
			appModel.isFlagState = true;
			var supermap:SuperMapMediator = facade.retrieveMediator(SuperMapMediator.NAME) as SuperMapMediator;
			supermap.view.map.infoWindow.hide();
			var win:FlagOkWindow = new FlagOkWindow();
			win.x = 320;
			win.y = 106;
			PopUpManager.addPopUp(win, supermap.view);
			getData();
			
			
			//调试
//			facade.registerCommand(AppFacade.QUERY_MAP_X_Y, QueryMapXYCommand);
//			sendNotification(AppFacade.QUERY_MAP_X_Y, "3601");
		}
		
		override public function fault(evt:Object):void
		{
			getData();
		}
		
		override protected function getData():void
		{
			_currentTime++;
			if(_currentTime < _reloadTime){
				var token:AsyncToken;
				token = _setting.webServer.send("ProcessData", "getQyAreacode|"+_client.userVO.id);
				token.addResponder(this);
			}else{
				trace("load GetAreaName is fault");
			}
		}
		
		override public function result(evt:Object):void
		{
			if(evt.result)
			{
				var obj:Object = JSON.decode(evt.result);
				if(obj.success == 1)
				{
					var item:Object = obj.data[0];
					facade.registerCommand(AppFacade.QUERY_MAP_X_Y, QueryMapXYCommand);
					sendNotification(AppFacade.QUERY_MAP_X_Y, "3601");
				}
			}
		}
		
		
	}
}