package ervin.android.broadcastreceiver;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Broadcast2Activity extends Activity {
    /** Called when the activity is first created. */
	private static final String ACTION_SMS="android.provider.Telephony.SMS_RECEIVED";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button btn1=(Button)findViewById(R.id.button1);
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
//				Intent intent=new Intent();
//				intent.setAction(ACTION_SMS);
//				Broadcast2Activity.this.sendBroadcast(intent);
				
				Receiver receiver=new Receiver();
				IntentFilter filter =new IntentFilter();
				filter.addAction(ACTION_SMS);
				Broadcast2Activity.this.registerReceiver(receiver, filter);
				
			}
		});
    }
}