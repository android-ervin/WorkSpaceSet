package ervin.android.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Entity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class HttpActivity extends Activity {
    /** Called when the activity is first created. */
	
	Button btn1;
	ImageView image;
	private ProgressDialog dialog=null;
	/*访问本地服务器时需要，android把自己作为了localhost，所以在IIS上部署的服务要变ip为10.0.2.2*/
	String path="http://pics.htwed.com/2014/02/11/52f97e395286c.jpg!l";//"http://10.0.2.2:80/pin.png";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dialog=new ProgressDialog(this);
        dialog.setTitle("提示");
        dialog.setMessage("正在下载，请稍后...");
        dialog.setCancelable(false);//不显示
        btn1=(Button)findViewById(R.id.button1);
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				new AnotherThread().start();
				//new myThread().start();
				dialog.show();
				//image.setImageResource(R.drawable.icon);
				
			}
		});
    }
    
    Handler handler=new Handler(){

		@Override
		public void handleMessage(Message msg)
		{
			Bitmap bm=(Bitmap) msg.obj;
			
			image.setImageBitmap(bm);
			
			if(msg.what==1)
			{
				dialog.dismiss();
			}
			System.out.println("获得的字符串-->"+msg.obj);
			
		}
    	
    };
    
    class myThread extends Thread
    {

		@Override
		public void run()
		{
			// TODO Auto-generated method stub
			HttpGet httpGet=new HttpGet(path);//获得一个请求request对象
			HttpClient hClient=new DefaultHttpClient();//获得客户端对象（DefaultHttpClient向上转型为HttpClient对象）
			/*DefaultHttpClient继承自抽象类abstractHttpclient，而abstractHttpclient又实现了HttpClient接口
			 *于是接口HttpClient的对象hclient就是由其子类去实例化的。（这句话有错误）
			 *1、接口不能被实例化
			 *2、接口对象可以指向它的实现类对象（就像上面的列子）hClent指向了一个实现它的DefaultHttpClient对象
			 *设计工厂模式，将生成对象的代码封装在工厂类中，可以有效减少代码量*/
			try
			{
				HttpResponse httpResponse=hClient.execute(httpGet);//客户端发送请求，返回一个httpresponse对象,服务器获得响应
				
				HttpEntity httpEntity=httpResponse.getEntity();//通过服务器的响应获得返回的内容。可是两个都是接口，怎么能被实例化呢？
//				if(httpResponse.getStatusLine().getStatusCode()==200)
//				{
//					data1=EntityUtils.toByteArray(httpEntity);
//				}
				InputStream inputstream=httpEntity.getContent();
				//BufferedReader readr=new BufferedReader(new InputStreamReader(inputstream));//java中的字节转字符流操作
				Bitmap bm=BitmapFactory.decodeStream(inputstream);
				Message msg=handler.obtainMessage();
//				while((data=readr.readLine())!=null)
//				{
//					msg.obj=data;
//				}
				inputstream.close();
				msg.obj=bm;
				msg.what=1;
				handler.sendMessage(msg);
				
			}  catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			super.run();
		}
    	
    }
    class AnotherThread extends Thread
    {
    	@Override
    	public void run()
    	{
    		HttpClient hClient=new DefaultHttpClient();
    		HttpGet hGet=new HttpGet(path);
    		HttpResponse httpResponse=null;
    		try
			{
    			/*
				URL myFileUrl=new URL(path);
				HttpURLConnection conn=(HttpURLConnection) myFileUrl.openConnection();
				conn.setDoInput(true);
				conn.connect();
				InputStream is=conn.getInputStream();
				
				Message msg=handler.obtainMessage();
				msg.obj=is;
				is.close();
				handler.sendMessage(msg);
				*/
    			httpResponse=hClient.execute(hGet);
    			if(httpResponse.getStatusLine().getStatusCode()==200)
    			{
    				byte[] data=EntityUtils.toByteArray(httpResponse.getEntity());
    				Message msg=Message.obtain();
    				msg.obj=data;
    				msg.what=1;
    				handler.sendMessage(msg);
    			}
    			System.out.println("当前线程"+Thread.currentThread().getName());
    			
			} catch (Exception e)
			{
				// TODO: handle exception
				e.printStackTrace();
				
			}
    		
    	}
    }
}