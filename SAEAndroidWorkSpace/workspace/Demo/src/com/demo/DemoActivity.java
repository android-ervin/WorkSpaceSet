package com.demo;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class DemoActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn1=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        btn1=(Button)findViewById(R.id.btn1);
        btn1.setText(R.string.btn);
 /*       btn1.setOnClickListener(new View.OnClickListener() {
        	         public void onClick(View v) {
        	        	 Intent intent=new Intent();
        	        	 intent.setClass(DemoActivity.this, OtherActivity.class);
        	        	 startActivity(intent);
        	        }
        	    });*/
        
        btn1.setOnClickListener((View.OnClickListener) listener);
    	}
    
    	
    /*添加菜单栏*/
    	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 1, 1, R.string.exit);
		menu.add(1, 2, 2, R.string.about);
		return super.onCreateOptionsMenu(menu);
	}

    	/*选择按钮后的动作*/

		@Override
		public boolean onOptionsItemSelected(MenuItem item)
		{
			if(item.getItemId()==1)
				finish();// TODO Auto-generated method stub
			return super.onOptionsItemSelected(item);
		}
 
		

		private View.OnClickListener listener = new View.OnClickListener()
		{
			
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				 Intent intent=new Intent();
	        	 intent.setClass(DemoActivity.this, OtherActivity.class);
	        	 intent.putExtra("one", 1);
	        	 intent.putExtra("two", 2);
	        	 
	        	 startActivity(intent);
			}
		};
}