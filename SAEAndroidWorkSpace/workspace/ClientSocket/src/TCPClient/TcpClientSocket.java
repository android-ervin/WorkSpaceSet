package TCPClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


public class TcpClientSocket
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		try
		{
			Socket socket=new Socket("192.168.1.103", 1234);
			InputStream inputStream=new FileInputStream("E://scr/from.txt");
			//获得一个输出流，用来向服务器端发送数据
			OutputStream outputStream=socket.getOutputStream();
			byte buffer[]=new byte[1024*4];
			int temp=0;
			//用输入流将数据从txt文件中读取出来并写入到输出流中
			while((temp=inputStream.read(buffer))!=-1)
				outputStream.write(buffer,0,temp);
			outputStream.flush();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
