package ervin.android.remotedata;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class Webservice
{
	private static final String METHOD_NAME = "getListdata";   
    private static final String NAMESPACE = "http://tempuri.org/";   
    private static final String URL = "http://10.0.2.2:80/WebService.asmx";
    private static String SOAP_ACTION="http://tempuri.org/getListdata";
    
    public static String getdata()
    {
    	String result="";
    	SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
    	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        
        envelope.dotNet = true;  
        envelope.setOutputSoapObject(request);  
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);  
        try
		{
			androidHttpTransport.call(SOAP_ACTION, envelope);
			Object ReturnStr = (Object) envelope.getResponse();  
			result=ReturnStr.toString();  
		} catch (HttpResponseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    	return result; 	
    }
}
