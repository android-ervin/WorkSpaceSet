package ervin.android.downloadservice;


import ervin.android.model.MyPrasemodel;
import ervin.android.musicplayer.Const;
import ervin.android.musicplayer.Mp3MusicPlayerActivity;
import ervin.android.musicplayer.R;
import ervin.android.write2sdcard.Write2SDcard;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class DownloadService extends Service
{
	

	@Override
	public IBinder onBind(Intent arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		MyPrasemodel mp3info_item=new MyPrasemodel();
		mp3info_item=(MyPrasemodel)intent.getSerializableExtra("mp3info_item");
		new Thread(new DownloadMp3(mp3info_item)).start();
		//System.out.println("service---->"+mp3info_item);
		return super.onStartCommand(intent, flags, startId);
	}
	
	class DownloadMp3 implements Runnable
	{
		private MyPrasemodel mp3infos;
		public DownloadMp3(MyPrasemodel mp3infos)
		{
			this.mp3infos=mp3infos;
		}
		public void run()
		{
			String url="http://10.0.2.2:81//styles/" + mp3infos.getName();
			Write2SDcard write2sDcard=new Write2SDcard();
			/*InputStream is;
			is = write2sDcard.getInputStreamFromUrl(url);
			write2sDcard.writeInputstream2SD("mp3/", mp3infos.getName(), is);*/
			int result=write2sDcard.downfile(url, "mp3/", mp3infos.getName());
			
			try
			{
				Thread.sleep(1000);//延时是因为，当启动DownloadService和PlayerActivity后，有可能下载服务的广播已经发出来了但是Activity的广播还没注册
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			sendCast(result);
			showNotification(result);
			stopSelf();//下载完成后停止服务
			
		}
		
		
	}
	public void showNotification(int flag)
	{
		NotificationManager nManager=(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Intent intent=new Intent(this,Mp3MusicPlayerActivity.class);
		PendingIntent contentIntent=PendingIntent.getActivity(this, 0, intent, 0);
		Notification notification=null;
		switch (flag)
		{
		case 0:
			notification=new Notification(R.drawable.notify, "The file is exit", System.currentTimeMillis());
			notification.setLatestEventInfo(this, "notification", "the file is exit", contentIntent);
			nManager.notify(Const.FILE_EXIT, notification);
			break;
		case 1:
			notification=new Notification(R.drawable.notify, "The file is ok", System.currentTimeMillis());
			notification.setLatestEventInfo(this, "notification", "the file is ok", contentIntent);
			nManager.notify(Const.FILE_OK, notification);
			break;
		case -1:
			notification=new Notification(R.drawable.notify, "The file is fail", System.currentTimeMillis());
			notification.setLatestEventInfo(this, "notification", "the file is fail", contentIntent);
			nManager.notify(Const.FILE_FAIL, notification);
		default:
			break;
		}
		
	}
	public void sendCast(int result)
	{
		// TODO Auto-generated method stub
		Intent intent=new Intent();
		intent.setAction("ervin.downloadservice.action.broadcast");
		intent.putExtra("result", result);
		this.sendBroadcast(intent);
	}

}
