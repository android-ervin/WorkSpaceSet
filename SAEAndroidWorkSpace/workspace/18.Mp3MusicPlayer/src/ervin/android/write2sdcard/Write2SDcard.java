package ervin.android.write2sdcard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


import ervin.android.model.MyPrasemodel;

import android.os.Environment;

public class Write2SDcard
{
	private String path;
	public  String  getStringPath()
	{
		return path;
	}
	//构造函数获得当前外部存储设备的目录
	public Write2SDcard()
	{
		path=Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator;
		System.out.println("path--->"+path);
	}
	
	/*在SD卡下创建文件
	 * @author Ervin
	 * */
	public File creatSDFile(String filename,String dir) throws IOException
	{
		File file=new File(path+dir+File.separator+filename);
		file.createNewFile();
		System.out.println("file---->"+file);
		return file;
	}
	/*在SD卡下创建文件夹
	 * @author Ervin
	 * */
	public File creatSDDir(String dirname)
	{
		File dir=new File(path+dirname+File.separator);
		dir.mkdir();
		return dir;
	}
	//文件是否存在
	public boolean isexit(String filename,String dirname)
	{
		File file=new File(path+dirname+File.separator+filename);
		return file.exists();
		
	}
	/*将一个inputstream里面的数据写入到SD卡中*/
	public File writeInputstream2SD(String dirname,String filename,InputStream is) throws IOException
	{
		//这里的参数dirname，是文件夹名字
		File file=null;
		OutputStream out=null;//写入流
		try
		{
			creatSDDir(dirname);
			file=creatSDFile(filename,dirname);
			out=new FileOutputStream(file);
			byte buffer[]=new byte[4*1024];
			int temp;
			while((temp=(is.read(buffer)))!=-1)//inputstream将流数据读入到buffer中
			{
				//out.write(buffer);//outputstream将buffer中的内容写入到文件中
				out.write(buffer, 0, temp);
			}
			out.flush();
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.close();
		
		return file;
	}
	
	public InputStream getInputStreamFromUrl(String urlStr)
			throws MalformedURLException,IOException
	{
		URL	url=new URL(urlStr);
		HttpURLConnection con=(HttpURLConnection)url.openConnection();
		InputStream is=con.getInputStream();
		return is;
	}
	/***
	 * 
	 * @param urlstr 网络路径
	 * @param dirname 文件夹名称
	 * @param filename 文件名称
	 * @return 0代表文件已经存在，1代表下载成功，-1代表下载失败
	 */
	public int downfile(String urlstr,String dirname,String filename)
	{
		InputStream is=null;
		if(isexit(filename, dirname))
			return 0;
		else 
		{
			try
			{
				is=getInputStreamFromUrl(urlstr);
				File retrunfile=writeInputstream2SD(dirname, filename, is);
				if(retrunfile==null)
					return -1;
				else {
					return 1;
				}
			}
			 catch (IOException e)
			{
				// TODO Auto-generated catch block
				 return -1;
			}
		}
	}
	
	public String toPath(MyPrasemodel mPrasemodel)
	{
		String path=Environment.getExternalStorageDirectory().getAbsolutePath();
		path+=File.separator+"mp3/"+mPrasemodel.getName();
		return path;
		
	}

}

