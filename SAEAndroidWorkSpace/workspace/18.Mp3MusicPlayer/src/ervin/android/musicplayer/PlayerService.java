package ervin.android.musicplayer;

import ervin.android.model.MyPrasemodel;
import ervin.android.write2sdcard.Write2SDcard;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;

public class PlayerService extends Service
{
	boolean isPlay=false;
	boolean isPause=false;
	boolean isRelease=false;
	static int i=0;
	
	MyPrasemodel mp3Infos=new MyPrasemodel();
	MediaPlayer mPlayer=null;
	@Override
	
	public IBinder onBind(Intent arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		if(i==0)
		{
			mp3Infos=(MyPrasemodel) intent.getSerializableExtra("mp3info_item");
			i++;
		}
		System.out.println("playservice---->" + mp3Infos);
		int flag=intent.getIntExtra("MSG", 0);
		System.out.println("playservice----->" + flag);
		if(mp3Infos!=null)
		{
			switch (flag)
			{
			case Const.MSG_BEGIN:
				mp3Play();
				break;
			case Const.MSG_PAUSE:
				mp3Pause();
				break;
			case Const.MSG_STOP:
				mp3Stop();
				break;
			default:
				break;
			}
		}
		return super.onStartCommand(intent, flags, startId);
	}
	public void mp3Play()
	{
		if(!isPlay)
		{	
			Write2SDcard ws=new Write2SDcard();
			String path=ws.toPath(mp3Infos);
			mPlayer=MediaPlayer.create(this, Uri.parse("file://" + path));
			mPlayer.setLooping(true);
			mPlayer.start();
			isPlay=true;
			isRelease=false;
		}
	}
	
	public void mp3Pause()
	{
		if(mPlayer!=null)
			if(!isRelease)
			{
				if (!isPause)
				{
					mPlayer.pause();
					isPause = true;
					isPlay = false;
				} 
				else
				{
					mPlayer.start();
					isPause = false;
					isPlay = true;
				}
			}
	}
	
	public void mp3Stop()
	{
		if(mPlayer!=null)
		{
			if(isPlay)
			{
				if(!isRelease)
				{
					mPlayer.stop();
					mPlayer.release();
					isRelease=true;
					isPlay=false;
					isPause=false;
				}
			}	
		}
	}
	
}
