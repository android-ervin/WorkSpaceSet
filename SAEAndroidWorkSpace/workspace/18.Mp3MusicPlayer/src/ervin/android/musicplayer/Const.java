package ervin.android.musicplayer;

public interface Const
{
	static final int MSG_BEGIN=1;
	static final int MSG_PAUSE=2;
	static final int MSG_STOP=3;
	
	static final int FILE_EXIT=0;
	static final int FILE_OK=1;
	static final int FILE_FAIL=-1;
	
}
