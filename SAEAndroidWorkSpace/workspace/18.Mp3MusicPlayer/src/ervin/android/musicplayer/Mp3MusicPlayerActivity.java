package ervin.android.musicplayer;


import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import ervin.android.downloadservice.DownloadService;
import ervin.android.httpdownload.HttpDownload;

import ervin.android.listview.ListviewShow;
import ervin.android.model.MyPrasemodel;
import ervin.android.musicplayer.R.string;
import ervin.android.prase.NewXMLPrase;
import ervin.android.prase.SAXPrase;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class Mp3MusicPlayerActivity extends ListActivity {
	private static final int ITEM_UPDATE=1;
	private static final int ITEM_ABOUT=2;
	private static final int ITEM_DATA=3;
	
	String XMLStr="";
	String path="http://10.0.2.2:81//Workers1.xml";
	downloadHandler handler=new downloadHandler();
	List<MyPrasemodel> modelInfos=new ArrayList<MyPrasemodel>();
	private ProgressDialog dialog=null;  
	downloadThread mythread=new downloadThread();
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
    }
    

	@Override
	protected void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		NotificationManager nManager=(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		/*nManager.cancel(0);
		nManager.cancel(1);
		nManager.cancel(-1);*/
		nManager.cancelAll();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0,ITEM_UPDATE,1,R.string.UPDATE);
		menu.add(0,ITEM_ABOUT,2,R.string.ABOUT);
		menu.add(0,ITEM_DATA,3,"远程数据列表");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		
		if(item.getItemId()==ITEM_UPDATE)
		{
			dialog = new ProgressDialog(this);
			dialog.setTitle("提示");
			dialog.setMessage("正在下载，请稍后...");
			dialog.setCancelable(false);// 不显示
			dialog.show();
			new downloadThread().start();
			
		}
		else if(item.getItemId()==ITEM_ABOUT)
		{
			new AlertDialog.Builder(this).setTitle(R.string.alerttitle).setMessage(
					R.string.context).setPositiveButton(R.string.below, new DialogInterface.OnClickListener()
					{
						
						public void onClick(DialogInterface arg0, int arg1)
						{
							// TODO Auto-generated method stub	
						}
					}).show();
		}
		else if(item.getItemId()==ITEM_DATA)
		{
			Intent intent=new Intent(Mp3MusicPlayerActivity.this,ListviewShow.class);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}
	class downloadThread extends Thread
	{
		String result="";
		@Override
		public void run()
		{
			result=downLoadXML(path);
			Message msg=handler.obtainMessage();
			msg.obj=result;
			handler.sendMessage(msg);
			super.run();
		}
		
	}
	class downloadHandler extends Handler
	{
		@Override
		public void handleMessage(Message msg)
		{
			XMLStr=(String) msg.obj;
			System.out.println(XMLStr);
			if(XMLStr!="")
				dialog.dismiss();
			else {
				dialog.setMessage("下载出错！...");
			}
			updateListView();
			super.handleMessage(msg);
		}
		
	}
	private void updateListView()
	{
		if(XMLStr!="")
		{
			modelInfos = Prase(XMLStr);
			SimpleAdapter simpleAdapter=buildAdapter(modelInfos);
			setListAdapter(simpleAdapter);
		}
		else {
			dialog.setMessage("下载出错！...");
			dialog.dismiss();
		}	
	}
	/**
	 * 将按照simpleAdatper的格式将数据从list中取出
	 * @param myPrasemodels
	 * @return 
	 */
	private SimpleAdapter buildAdapter(List<MyPrasemodel> myPrasemodels)
	{
		// 开始处理收到的信息,首先将集合modelInfos中的信息迭代出来存放在一个hashmap中，最后更新界面
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		for (Iterator<MyPrasemodel> iterator = myPrasemodels.iterator(); iterator
				.hasNext();)
		{
			MyPrasemodel mp3modelInfos = (MyPrasemodel) iterator.next();
			HashMap<String, String> mp3Map = new HashMap<String, String>();
			mp3Map.put("mp3_name", mp3modelInfos.getName());
			mp3Map.put("mp3_position", mp3modelInfos.getPosition());
			mp3Map.put("mp3_address", mp3modelInfos.getAddress());
			list.add(mp3Map);
		}
		/*将ListActivity和布局文件联系起来*/
		SimpleAdapter simpleAdapter = new SimpleAdapter(
				Mp3MusicPlayerActivity.this, list, R.layout.mp3infos,
				new String[]
				{ "mp3_name", "mp3_position", "mp3_address" }, new int[]
				{ R.id.mp3_name, R.id.mp3_position, R.id.mp3_address });
		return simpleAdapter;
	}
	private String downLoadXML(String path)
	{
		String result="";
		HttpDownload httpDownload=new HttpDownload();
		result=httpDownload.Downloadfile(path);
		return result;
	}

	private List<MyPrasemodel> Prase(String xmlstr)
	{
		List<MyPrasemodel> infos=new ArrayList<MyPrasemodel>();//生成一个List<MyPrasemodel>泛型对象
		try
		{
			SAXParserFactory factory=SAXParserFactory.newInstance();
			XMLReader reader=factory.newSAXParser().getXMLReader();
			
			NewXMLPrase prase=new NewXMLPrase(infos);//调用构造函数生成一个解析内容处理器对象
			reader.setContentHandler(prase);//为reader对象设置对象处理器
			reader.parse(new InputSource(new StringReader(xmlstr)));//开始解析XML文档，然后所有的信息都会被保存到infos中
			
			for (Iterator<MyPrasemodel> iterator = infos.iterator(); iterator.hasNext();)//生成一个model对象迭代器，用来取出集合中的对象元素
			{
				MyPrasemodel mp3Info = (MyPrasemodel) iterator.next();//将infos中的MyPrasemodel类型对象依次取出来
				System.out.println(mp3Info);
				
			}
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return infos ;
		
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		MyPrasemodel mp3Info_item=modelInfos.get(position);//根据用户点击的position参数来确定用户点击了哪行信息，并将该行信息取出放入对象中
		//System.out.println(mp3Info_item);
		Intent intent=new Intent();
		intent.putExtra("mp3info_item", (Serializable)mp3Info_item);//将参数传入到service中
		intent.setClass(Mp3MusicPlayerActivity.this, DownloadService.class);
		startService(intent);//开启下载歌曲服务
		
		Intent intenttoplay=new Intent(Mp3MusicPlayerActivity.this,PlayerActivity.class);
		startActivity(intenttoplay);//进入歌曲播放页面
		
		Intent intentservice=new Intent(Mp3MusicPlayerActivity.this,PlayerService.class);
		intentservice.putExtra("mp3info_item", (Serializable)mp3Info_item);
		startService(intentservice);//开启歌曲播放服务
		super.onListItemClick(l, v, position, id);
		
	}
	
}