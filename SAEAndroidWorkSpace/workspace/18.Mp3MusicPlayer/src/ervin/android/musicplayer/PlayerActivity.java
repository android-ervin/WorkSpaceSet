package ervin.android.musicplayer;

import ervin.android.model.MyPrasemodel;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class PlayerActivity extends Activity
{
	ImageButton beginButton=null;
	ImageButton pauseButton=null;
	ImageButton stopButton=null;
	MyPrasemodel mp3infos=null;
	
	
	Intent playerIntent;
	ProgressDialog dialog=null;
	Object downloadResult=null;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.player);
		
		//注册广播
		IntentFilter intentFilter=new IntentFilter();
		intentFilter.addAction("ervin.downloadservice.action.broadcast");
		registerReceiver(new MyBroadcastRecevier(), intentFilter);
		
		showMyDialog();
		playerIntent=new Intent(PlayerActivity.this,PlayerService.class);
		
		beginButton=(ImageButton)findViewById(R.id.begin);
		beginButton.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				playerIntent.putExtra("MSG", Const.MSG_BEGIN);
				startService(playerIntent);
			}
		});
		
		pauseButton=(ImageButton)findViewById(R.id.pause);
		pauseButton.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				playerIntent.putExtra("MSG", Const.MSG_PAUSE);
				startService(playerIntent);
			}
		});
		
		stopButton=(ImageButton)findViewById(R.id.stop);
		stopButton.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				playerIntent.putExtra("MSG", Const.MSG_STOP);
				startService(playerIntent);
			}
		});
		
	}
	public class MyBroadcastRecevier extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context arg0, Intent arg1)
		{
			// TODO Auto-generated method stub
			String action=arg1.getAction();
			if(action.equals("ervin.downloadservice.action.broadcast"))
			{
				downloadResult=arg1.getIntExtra("result", 0);
				dialog.dismiss();
				System.out.println("downloadResult------>" + downloadResult);
			}
			PlayerActivity.this.unregisterReceiver(this);
		}
		
	}
	public void showMyDialog()
	{
		dialog = new ProgressDialog(this);
		dialog.setTitle("提示");
		dialog.setMessage("正在下载Mp3文件，请稍后...");
		dialog.setCancelable(false);// 不显示
		dialog.show();
	}
	
}
