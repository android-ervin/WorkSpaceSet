package ervin.android.model;

import java.io.Serializable;

public class MyPrasemodel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3073555264943234803L;
	private String name,sex,position,address;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public String getPosition()
	{
		return position;
	}

	public void setPosition(String position)
	{
		this.position = position;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public MyPrasemodel(String name, String sex, String position, String address)
	{
		super();
		this.name = name;
		this.sex = sex;
		this.position = position;
		this.address = address;
	}

	public MyPrasemodel()
	{
		super();
	}

	@Override
	public String toString()
	{
		return "MyPrasemodel [address=" + address + ", name=" + name
				+ ", position=" + position + ", sex=" + sex + "]";
	}
	
}
