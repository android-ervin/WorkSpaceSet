package ervin.android.prase;

import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import ervin.android.model.MyPrasemodel;

public class NewXMLPrase extends DefaultHandler
{
	private List<MyPrasemodel> Info=null;
	private MyPrasemodel mp3Info=null;
	private String tagName="";

	public NewXMLPrase(List<MyPrasemodel> info)
	{
		super();
		Info = info;
	}

	public List<MyPrasemodel> getInfo()
	{
		return Info;
	}

	public void setInfo(List<MyPrasemodel> info)
	{
		Info = info;
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException
	{
		/*解析XML中的各个字符，并赋值给model的各个成员变量*/
		String temp=new String(ch,start,length);
		if(tagName.equals("name"))
			mp3Info.setName(temp);
		else if(tagName.equals("sex"))
			mp3Info.setSex(temp);
		else if(tagName.equals("position"))
			mp3Info.setPosition(temp);
		else if(tagName.equals("address"))
			mp3Info.setAddress(temp);
	}

	@Override
	public void endDocument() throws SAXException
	{
		// TODO Auto-generated method stub
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		/**
		 * 当读到一个<worker></worker>结束时，把一个完整的<worker></worker>model对象mp3info添加到List中
		 */
		if(qName.equals("worker"))
			Info.add(mp3Info);
		tagName="";
	}

	@Override
	public void startDocument() throws SAXException
	{
		// TODO Auto-generated method stub
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException
	{
		/**
		 * 每当读到一个<worker></worker>开始时，都生成一个mp3info对象
		 */
		tagName=localName;
		if(tagName.equals("worker"))
		{
			mp3Info=new MyPrasemodel();
		}
	}
	
}
