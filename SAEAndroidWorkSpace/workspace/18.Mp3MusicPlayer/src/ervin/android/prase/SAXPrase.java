package ervin.android.prase;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXPrase extends DefaultHandler
{
	String namestr,address,position,sex,Tagname;

	@Override
	public void startDocument() throws SAXException
	{
		System.out.println("begin prase XML document");
	}
	@Override
	public void endDocument() throws SAXException
	{
		System.out.println("finish prase XML document");
	}
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException
	{
		if (Tagname.equals("name"))
			namestr=new String(ch,start,length);
		else if (Tagname.equals("sex")) 
			sex=new String(ch,start,length);
		else if(Tagname.equals("position"))
			position=new String(ch,start,length);
		else if(Tagname.equals("address"))
			address=new String(ch,start,length);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		if(qName=="worker")
			this.printout();
		Tagname="";//当读到</name>类似的标签时，将Tagname清空，不然会读不到内容
	}
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException
	{
		Tagname=localName;
		if(localName=="worker")
		{
			for(int i=0;i<attributes.getLength();i++)
			{
				System.out.println(attributes.getLocalName(i)+"="+attributes.getValue(i));
			}
		}
	}

	private void printout()
	{
		System.out.println("name="+namestr);
		System.out.println("sex="+sex);
		System.out.println("position="+position);
		System.out.println("address="+address);
		System.out.println();
	}
}

