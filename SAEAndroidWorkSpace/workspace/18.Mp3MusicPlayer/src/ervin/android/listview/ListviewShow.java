package ervin.android.listview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ervin.android.musicplayer.R;
import ervin.android.remotedata.Webservice;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ListviewShow extends Activity
{
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remotedata);
        
        ListView lv=(ListView) findViewById(R.id.ListView);
        String data=Webservice.getdata();
        
        List<HashMap<String,String>> list=new ArrayList<HashMap<String,String>>();
        if((data.indexOf("|"))!=-1)
        {
        	String[] resultlist=data.split("="); //���ַ�����=���и�
        	for(int i=0;i<resultlist.length;i++)
        	{
        		HashMap<String, String> hashMap=new HashMap<String, String>();
        		String [] item=resultlist[i].split("\\|");//ת���ַ�
        		hashMap.put("node_name", item[0]);
        		hashMap.put("node_temp", item[1]);
        		hashMap.put("time", item[2]);
        		list.add(hashMap);
        		System.out.println("--->" + item[2].toString());
        	}
        }
        
        //��ListView����adapter
        String[] mFrom=new String[]{"node_name","node_temp","time"};
        int [] mTo=new int[]{R.id.name,R.id.temp,R.id.datetime};
        SimpleAdapter mAdapter=new SimpleAdapter(this, list, R.layout.item, mFrom, mTo);
        lv.setAdapter(mAdapter);
    }
}
