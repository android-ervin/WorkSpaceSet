package ervin.android.httpdownload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpDownload
{
	/**
	 * 根据URL下载网络文件，前提是这个文件当中的内容是文本，返回值就是这个文件当中的内容
	 * @param pathstr
	 * @return
	 */
	public String Downloadfile(String pathstr)
	{
    	URL url =null;
    	BufferedReader bReader=null;
    	StringBuffer sbBuffer=new StringBuffer();
    	String line=null;
		try
		{
			url=new URL(pathstr);
		} catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			//conn.setDoInput(true);  
   		 	//conn.connect();
			InputStream is=conn.getInputStream();
			bReader=new BufferedReader(new InputStreamReader(is));
			while((line=bReader.readLine())!=null)
			{
				sbBuffer.append(line);
			}
			//bReader.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sbBuffer.toString();
	}
}
