package com.ervin.decorater;

/*A 公司的工人装饰者，在这里相当去在Worker至上添加一系列装饰*/
public class AWorker implements Worker
{	public Worker worker;
	public AWorker(Worker worker)
	{
		this.worker=worker;
	}
	public void doSomeWork()
	{
		System.out.println("在这里可以添加装饰了");
		worker.doSomeWork();
	}
}
