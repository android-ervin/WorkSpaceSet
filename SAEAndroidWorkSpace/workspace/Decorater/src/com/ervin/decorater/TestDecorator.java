package com.ervin.decorater;

public class TestDecorator
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		//生成一个A公司的水管工,运用这种装饰者模式的时候就可以在Worker这个基类上装饰任何行为了，有了AWorker这个类就不必
		//去生成一大堆的子类，比如A公司的水管工类，A公司的电工类等等。
		Plumber plumber=new Plumber();
		AWorker aWorker=new AWorker(plumber);
		aWorker.doSomeWork();
		
		Worker workerimp=new WorkerIpm();
		Electricer aWorker2=new Electricer(workerimp);
		aWorker2.doSomeWork();
	}

}
