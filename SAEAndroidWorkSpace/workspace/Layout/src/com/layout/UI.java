package com.layout;

import javax.security.auth.PrivateCredentialPermission;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;

public class UI extends Activity
{
	private ProgressBar Pb1=null;
	private ProgressBar pb2=null;
	private Button btn1=null;
	private int i=0;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ui);
		
		Pb1=(ProgressBar)findViewById(R.id.progressBar1);
		pb2=(ProgressBar)findViewById(R.id.progressBar2);
		btn1=(Button)findViewById(R.id.button1);
		
		btn1.setOnClickListener(listener);
	}
	private OnClickListener listener =new OnClickListener()
	{
		
		public void onClick(View v)
		{
			// TODO Auto-generated method stub
			if(i==0)
			{
				Pb1.setVisibility(View.VISIBLE);
				pb2.setVisibility(View.VISIBLE);
			}
			else if(i<Pb1.getMax()||i>Pb1.getMax())
			{
				
				Pb1.setProgress(i);
				Pb1.setSecondaryProgress(i+10);
				pb2.setProgress(i);
				
			}
			else 
			{
				Pb1.setVisibility(View.GONE);
				pb2.setVisibility(View.GONE);
			}
			i=i+10;
		}
	};
}
