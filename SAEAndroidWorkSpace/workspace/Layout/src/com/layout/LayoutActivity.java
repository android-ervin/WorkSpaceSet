package com.layout;

import javax.security.auth.PrivateCredentialPermission;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class LayoutActivity extends Activity {
    /** Called when the activity is first created. */
    
    private Button btn1=null;
    private Button btn2=null;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.relativelayout);
        
        btn1=(Button)findViewById(R.id.ok);
        btn1.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Toast.makeText(LayoutActivity.this, "click", 0).show();
			}
		});
        
        btn2=(Button)findViewById(R.id.button2);
        btn2.setOnClickListener(listener);
        
        
    }
    private OnClickListener listener =new OnClickListener()
	{
		
		public void onClick(View v)
		{
			// TODO Auto-generated method stub
			Intent intent=new Intent();
			intent.setClass(LayoutActivity.this, UI.class);
			startActivity(intent);
			
		}
	};
}