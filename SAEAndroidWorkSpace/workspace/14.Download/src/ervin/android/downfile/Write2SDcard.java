package ervin.android.downfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.os.Environment;

public class Write2SDcard
{
	private String path;
	public  String  getStringPath()
	{
		return path;
	}
	//构造函数获得当前外部存储设备的目录
	public Write2SDcard()
	{
		path=Environment.getExternalStorageDirectory()+"/";
	}
	
	/*在SD卡下创建文件
	 * @author Ervin
	 * */
	public File creatSDFile(String filename) throws IOException
	{
		File file=new File(path+filename);
		file.createNewFile();
		return file;
	}
	/*在SD卡下创建文件夹
	 * @author Ervin
	 * */
	public File creatSDDir(String dirname)
	{
		File dir=new File(path+dirname);
		dir.mkdir();
		return dir;
	}

	/*将一个inputstream里面的数据写入到SD卡中*/
	public File writeInputstream2SD(String path,String filename,InputStream is) throws IOException
	{
		File file=null;
		OutputStream out=null;//写入流
		try
		{
			creatSDDir(path);
			file=creatSDFile(path+filename);
			out=new FileOutputStream(file);
			byte buffer[]=new byte[4*1024];
			while((is.read(buffer))!=-1)//inputstream将流数据读入到buffer中
			{
				out.write(buffer);//outputstream将buffer中的内容写入到文件中
			}
			out.flush();
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.close();
		
		return file;
	}

}
