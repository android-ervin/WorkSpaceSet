package ervin.android.downfile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DownloadActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn;
	private Button btn2;
	String path="http://10.0.2.2:80//Styles/Site.css";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        btn=(Button)findViewById(R.id.button1);
        btn.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				String string=Downloadfile(path);
				System.out.println("downloadfile"+string);
				
			}
		});
        
        btn2=(Button)findViewById(R.id.button2);
        btn2.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				Download2SD(path);
			}
		});
    }
   
    public String Downloadfile(String pathstr)
	{
    	URL url =null;
    	BufferedReader bReader=null;
    	StringBuffer sbBuffer=new StringBuffer();
    	String line=null;
		try
		{
			url=new URL(pathstr);
		} catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			//conn.setDoInput(true);  
   		 	//conn.connect();
			InputStream is=conn.getInputStream();
			bReader=new BufferedReader(new InputStreamReader(is));
			while((line=bReader.readLine())!=null)
			{
				sbBuffer.append(line);
			}
			//bReader.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sbBuffer.toString();
	}
    
    public void Download2SD(String path)
	{
    	URL url =null;
    	Write2SDcard wDcard=new Write2SDcard();
    	try
		{
			url=new URL(path);
		} catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		try
		{
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			InputStream is=conn.getInputStream();
			wDcard.writeInputstream2SD("downloadfile/", "Site.css", is);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
}
