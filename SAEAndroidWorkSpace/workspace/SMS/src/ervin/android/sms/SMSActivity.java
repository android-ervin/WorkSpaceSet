package ervin.android.sms;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SMSActivity extends Activity {
    /** Called when the activity is first created. */
	String number="";
	String content="";
	EditText mEdit_number=null;
	EditText mEdit_content=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mEdit_number=(EditText)findViewById(R.id.editText1);
        mEdit_content=(EditText)findViewById(R.id.editText2);
        Button btn=(Button)findViewById(R.id.button1);
        btn.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				number=mEdit_number.getText().toString().trim();
				content=mEdit_content.getText().toString().trim();
				if(TextUtils.isEmpty(number)||TextUtils.isEmpty(content))
				{
					Toast.makeText(SMSActivity.this, "请输入手机号码或者短信", Toast.LENGTH_LONG).show();
				}
				else {
					SmsManager smsManager=SmsManager.getDefault();
					smsManager.sendTextMessage(number, null, content, null, null);
				}
			}
		});
    }
}