package evin.andorid.contactresolver;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class ContactResolver {

	public static Cursor getContactDataByResolver(Context context)
	{
		Cursor cursor = null;
		ContentResolver resolver =context.getContentResolver();
		Uri uri = Uri.parse("content://People.CONTENT_URI/");
		cursor = resolver.query(uri, null, null, null, null);
		return cursor;
	}
}
