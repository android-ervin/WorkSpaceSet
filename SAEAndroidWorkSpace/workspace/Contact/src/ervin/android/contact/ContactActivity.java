package ervin.android.contact;

import evin.andorid.contactresolver.ContactResolver;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;

public class ContactActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Cursor cursor = ContactResolver.getContactDataByResolver(this);
        String[] str = cursor.getColumnNames();
        for(int i =0;i<str.length;i++)
        {
        	System.out.println(str[i]);
        }

    }
}