package com.liulian.scan;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;


public class Media_Scan extends Activity {
	
	public static LinkedList<MovieInfo> playList = new LinkedList<MovieInfo>();
	public class MovieInfo{
		String displayName;  
		String path;
	}
	private Uri videoListUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	private Button button_dis = null;

    /** Called when the activity is first created. */
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);	
        setContentView(R.layout.main);
        
        button_dis = (Button) findViewById(R.id.display);
        button_dis.setOnClickListener(new OnClickListener(){
        	
        	public void onClick(View arg0){
				Intent intent = new Intent();
				intent.setClass(Media_Scan.this, Display.class);
				//startActivityForResult can pass parameters
				Media_Scan.this.startActivityForResult(intent, 0);
        	}
        	
        });  


    getVideoFile(playList, new File("/sdcard/"));
    
    if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
    	
    	Cursor cursor = getContentResolver().query(videoListUri, new String[]{"_display_name","_data"}, null, null, null);
        int n = cursor.getCount();
        cursor.moveToFirst();
        LinkedList<MovieInfo> playList2 = new LinkedList<MovieInfo>();
        for(int i = 0 ; i != n ; ++i){
        	MovieInfo mInfo = new MovieInfo();
        	mInfo.displayName = cursor.getString(cursor.getColumnIndex("_display_name"));
        	mInfo.path = cursor.getString(cursor.getColumnIndex("_data"));
        	playList2.add(mInfo);
        	cursor.moveToNext();
        }
        
        if(playList2.size() > playList.size()){
        	playList = playList2;
        }
    }
    
	
    //getVideoFile(playList, new File("/sdcard/"));
    }

	private void getVideoFile(final LinkedList<MovieInfo> list, File file) {
		// TODO Auto-generated method stub
    	file.listFiles(new FileFilter(){

			public boolean accept(File file) {
				// TODO Auto-generated method stub
				String name = file.getName();
				int i = name.indexOf('.');
				if(i != -1){
					name = name.substring(i);
					if(name.equalsIgnoreCase(".mp4")||name.equalsIgnoreCase(".3gp")||name.equalsIgnoreCase(".mp3")||name.equalsIgnoreCase(".wma")){
						
						MovieInfo mi = new MovieInfo();
						mi.displayName = file.getName();
						mi.path = file.getAbsolutePath();
						list.add(mi);
						return true;
					}
				}else if(file.isDirectory()){
					getVideoFile(list, file);
				}
				return false;
			}
    	});

	}
}