package com.liulian.scan;

import java.util.LinkedList;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;

public class Display extends Activity {
	
	private LinkedList<com.liulian.scan.Media_Scan.MovieInfo> mLinkedList;
	private LayoutInflater mInflater;
	View root;
	private EditText urlInput;
	protected Context context = null;   

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        final Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.display);
		
		mLinkedList = Media_Scan.playList;
		
		mInflater = getLayoutInflater();
		ImageButton iButton = (ImageButton) findViewById(R.id.cancel);
		iButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Display.this.finish();
			}
			
		});
		
		ListView myListView = (ListView) findViewById(R.id.list);
		myListView.setAdapter(new BaseAdapter(){

			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return mLinkedList.size();
			}

			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return arg0;
			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return arg0;
			}

			@Override
			public View getView(int arg0, View convertView, ViewGroup arg2) {
				// TODO Auto-generated method stub
				if(convertView==null){
					convertView = mInflater.inflate(R.layout.list, null);
				}
				TextView text = (TextView) convertView.findViewById(R.id.text);
				text.setText(mLinkedList.get(arg0).displayName);
				
				return convertView;   
			}
			
		});

		myListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(this, "The file name is:" + mLinkedList.get(arg2).displayName, Toast.LENGTH_SHORT).show();
				Toast.makeText(context, "The file name is:" + mLinkedList.get(arg2).displayName, Toast.LENGTH_SHORT).show();
				//DisplayToast("The file name is:" + mLinkedList.get(arg2).displayName);
			}
		});
/**
		public void DisplayToast(String str){
			Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
		}
*/		
		urlInput = (EditText) findViewById(R.id.url_input);
		urlInput.setText("http://");
		urlInput.setOnEditorActionListener(new OnEditorActionListener(){

			@Override
			public boolean onEditorAction (TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				
				Log.d("actionId", ""+actionId);
				Editable url = ((EditText)v).getEditableText();
				Intent intent = new Intent();
				intent.putExtra("CHOOSE_URL", url.toString());
				Display.this.setResult(Activity.RESULT_OK, intent);
				Display.this.finish();
								
				return false;
			}
			
		});

		myListView.requestFocus();
	}
}