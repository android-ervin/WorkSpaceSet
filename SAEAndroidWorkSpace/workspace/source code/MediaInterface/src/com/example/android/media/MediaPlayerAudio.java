package com.example.android.media;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MediaPlayerAudio extends Activity {
    private static final String TAG = "MediaPlayerDemo";
    private MediaPlayer mMediaPlayer;
    private static final String MEDIA = "media";
    private static final int LOCAL_AUDIO = 1;
    private static final int RESOURCES_AUDIO = 2;
    private String path;
    private TextView mTextView;
    //MediaPlayerAudio
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mediaplayer02);
        mTextView = (TextView) findViewById(R.id.TextView01);
        Bundle extras = getIntent().getExtras();
        playAudio(extras.getInt(MEDIA));
    }

    private void playAudio(Integer media) {
        try {
            switch (media) {
                case LOCAL_AUDIO:
         
                    path = "/sdcard/song21.mp3";
                    if (path == "") {
    
                        Toast.makeText(MediaPlayerAudio.this,"hasnot setup audio file", Toast.LENGTH_LONG).show();
                    }
                    mMediaPlayer = new MediaPlayer();
                    mMediaPlayer.setDataSource(path);
                    mMediaPlayer.prepare();
                    mMediaPlayer.start();
                    mTextView.setText("LOCAL_AUDIO-playing music...");
                    break;
                    
                case RESOURCES_AUDIO:    
                	mMediaPlayer = MediaPlayer.create(this, R.raw.haoxianghaoxiang);
                    mMediaPlayer.start();
                    mTextView.setText("RESOURCES_AUDIO-playing music...");
                    break;
            }
        } catch (Exception e) {
            Log.e(TAG, "error: " + e.getMessage(), e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }
}

