package ervin.android.asynctask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Entity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
	private Button btn;
	private ImageView imageView;
	private String image_path="http://10.0.2.2:81/Styles/worldcup.jpg";
	ProgressDialog dialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        btn=(Button)findViewById(R.id.button1);
        imageView=(ImageView)findViewById(R.id.imageView1);
        dialog=new ProgressDialog(this);
        dialog.setTitle("Tips");
        dialog.setMessage("Downloading...");
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        
        btn.setOnClickListener(new OnClickListener()
		{
			
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				new myTask().execute(image_path);
			}
		});
    }
    
    public class myTask extends AsyncTask<String, Integer, Bitmap>
    {
    	
		@Override
		protected void onPreExecute()
		{
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog.show();
		}
		/***
		 * 主要执行后台耗时操作的
		 */
		@Override
		protected Bitmap doInBackground(String... arg0)
		{
			// TODO Auto-generated method stub
			Bitmap map =null;
			InputStream inputStream=null;//获得一个输入流将数据从response中读取到字节数据data中
			ByteArrayOutputStream outputStream=new ByteArrayOutputStream(); //获得一个输出流将字节数据data中的数据写到手机内存中，该OutputStream类型可以将OutputStream转换为字节数组
			 
			HttpClient client=new DefaultHttpClient();
			HttpGet hGet=new HttpGet(arg0[0]);
			System.out.println("------>" + Thread.currentThread().getName());//不是主线程
			try
			{
				HttpResponse response=client.execute(hGet);
				if(response.getStatusLine().getStatusCode()==200)
				{
					HttpEntity httpEntity=response.getEntity();//获得反馈内容
					inputStream=httpEntity.getContent();

					//获得下载文件的总长度
					long file_length=httpEntity.getContentLength();
					//每次读取的长度
					int len=0;
					byte[] data=new byte[1024];
					//累计读取的长度
					int total_length=0;
					while((len=inputStream.read(data))!=-1)
					{
						total_length+=len;
						//对应在进度条上的值，累加取得值/总的值*100
						int value=((int) ((total_length / (float) file_length) * 100));
						//将这个进度条刻度值发布到UI线程中
						publishProgress(value);
						//将读入到data字节数组中的数据流写入到手机内存区中
						outputStream.write(data, 0, len);
					}
					//数据写入完毕后，将数据流转换为bitmap
					byte[] result=outputStream.toByteArray();//将输出流转换为字节流
					map=BitmapFactory.decodeByteArray(result, 0, result.length);//将字节流转换为bitmap
//					map=BitmapFactory.decodeStream(inputStream);
					inputStream.close();
				}
//				byte[] data=EntityUtils.toByteArray(httpEntity);
//				map=BitmapFactory.decodeByteArray(data, 0, data.length);
				
			} catch (Exception e)
			{
				// TODO: handle exception
			}
			
			return map;
		}
		
    	@Override
    	protected void onProgressUpdate(Integer... values)
    	{
    		// TODO Auto-generated method stub
    		super.onProgressUpdate(values);
    		dialog.setProgress(values[0]);
    	}
    	
    	@Override
    	protected void onPostExecute(Bitmap result)
    	{
    		// TODO Auto-generated method stub
    		super.onPostExecute(result);
    		dialog.dismiss();
    		imageView.setImageBitmap(result);
    	}
    	
    }
}