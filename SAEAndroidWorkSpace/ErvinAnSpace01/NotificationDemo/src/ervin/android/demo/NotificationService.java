package ervin.android.demo;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.view.accessibility.AccessibilityEvent;

public class NotificationService extends AccessibilityService {

	@Override
	public void onAccessibilityEvent(AccessibilityEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInterrupt() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onServiceConnected() {
		AccessibilityServiceInfo info = new AccessibilityServiceInfo();
	      info.feedbackType = 1;
	          info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
	      info.notificationTimeout = 100; 
	      setServiceInfo(info);	     
	}
}
