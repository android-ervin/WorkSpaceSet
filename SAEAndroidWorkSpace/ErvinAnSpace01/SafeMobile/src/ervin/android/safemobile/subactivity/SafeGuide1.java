package ervin.android.safemobile.subactivity;

import ervin.android.safemobile.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SafeGuide1 extends MyGestureActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.safe_guide1);
	}

	@Override
	public void IntentNext() {
		Intent intent = new Intent(SafeGuide1.this,SafeGuide2.class);
		startActivity(intent);	
		finish();
	}

	@Override
	public void IntentPre() {
		// TODO Auto-generated method stub
		
	}
}
