package ervin.android.safemobile.subactivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import ervin.android.safemobile.R;
import ervin.android.safemobile.service.ScreenOff;
import ervin.android.safemobile.utils.ServiceUtils;

public class TaskSetting extends Activity {

	private CheckBox  cb_showsystem;
	private CheckBox cb_screenoff;
	
	private SharedPreferences sp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.taskmanager_setting);
		
		sp = getSharedPreferences("config", MODE_PRIVATE);
		cb_showsystem = (CheckBox) findViewById(R.id.cb_showsystem);
		cb_showsystem.setChecked(sp.getBoolean("showsystem", false));
		cb_showsystem.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Editor editor = sp.edit();
				editor.putBoolean("showsystem", isChecked);
				editor.commit();
			}
		});
		
		
		cb_screenoff = (CheckBox) findViewById(R.id.cb_screenoff);
		cb_screenoff.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Intent intent = new Intent(TaskSetting.this,ScreenOff.class);
				
				if(isChecked)
				{		
					startService(intent);
				}else
					stopService(intent);
				
			}
		});
				
	}
	
	@Override
	protected void onStart() {
		//Activity可见时，如果服务还在运行的话
		if(ServiceUtils.isServiceRunning(TaskSetting.this, " ervin.android.safemobile.service.ScreenOff"))
		{
			cb_screenoff.setChecked(true);
		}else
			cb_screenoff.setChecked(false);
		super.onStart();
	}
}
