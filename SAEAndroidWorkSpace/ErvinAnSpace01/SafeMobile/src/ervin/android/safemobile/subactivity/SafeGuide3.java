package ervin.android.safemobile.subactivity;

import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import ervin.android.safemobile.R;

public class SafeGuide3 extends MyGestureActivity {

	private EditText et_selected_contact;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.safe_guide3);
		
		et_selected_contact = (EditText) findViewById(R.id.et_selected);
		
		String safenumber = sp.getString("safenumber", "");
		et_selected_contact.setText(safenumber);
	}
	
	
	public void selectContact(View view)
	{
		Intent intent = new Intent(this,SelectContactActivity.class);
		startActivityForResult(intent, 0);
		//startActivity(intent);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(data!=null){
			String number = data.getStringExtra("phone").replace("-", "").replace(" ", "");
			et_selected_contact.setText(number);
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void IntentNext() {
		//下一页之前，判断是否有设置安全号码，有的话保存
		String text = et_selected_contact.getText().toString().trim();
		if(TextUtils.isEmpty(text))
		{
			Toast.makeText(this, "未设置安全号码", 0).show();
			return;
		}
		Editor editor = sp.edit();
		editor.putString("safenumber", "+86"+text);
		editor.commit();
		
		
		Intent intent = new Intent(SafeGuide3.this,SafeGuide4.class);
		startActivity(intent);
		finish();
	}

	@Override
	public void IntentPre() {
		Intent intent = new Intent(SafeGuide3.this,SafeGuide2.class);
		startActivity(intent);
		finish();
	}
}
