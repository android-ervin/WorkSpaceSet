package ervin.android.safemobile.subactivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import ervin.android.safemobile.R;
import ervin.android.safemobile.Safe;

public class SafeGuide4 extends MyGestureActivity {

	private SharedPreferences sp;
	private CheckBox cb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.safe_guide4);
		
		sp = getSharedPreferences("config", MODE_PRIVATE);
		boolean protecting = sp.getBoolean("protecting", false);
		cb = (CheckBox) findViewById(R.id.cb_setupsafenumber);
		
		if(protecting)
		{
			cb.setText("手机防盗已经开启");
			cb.setChecked(true);
		}else
		{
			cb.setText("手机防盗未开启");
			cb.setChecked(false);
		}
		cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					cb.setText("手机防盗已经开启");
				else
					cb.setText("手机防盗未开启");
				Editor editor = sp.edit();
				editor.putBoolean("protecting", isChecked);
				editor.commit();
				
			}
		});
	}

	@Override
	public void IntentNext() {
		Editor editor = sp.edit();
		editor.putBoolean("safeguide", true);
		editor.commit();
		Intent intent = new Intent(SafeGuide4.this,Safe.class);
		startActivity(intent);
		finish();
	}

	@Override
	public void IntentPre() {
		Intent intent = new Intent(SafeGuide4.this,SafeGuide3.class);
		startActivity(intent);	
		finish();
	}
}
