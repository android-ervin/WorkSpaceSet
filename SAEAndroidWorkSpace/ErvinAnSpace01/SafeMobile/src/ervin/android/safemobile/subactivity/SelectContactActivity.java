package ervin.android.safemobile.subactivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import ervin.android.safemobile.R;

public class SelectContactActivity extends Activity{

	private ListView lv_selectcontact;
	private List<Map<String,String>> list ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.safe_selectcontact);
		
		
		lv_selectcontact = (ListView) findViewById(R.id.lv_selectcontact);
		//执行异步查询（联系人）
		list = searchContact();
		lv_selectcontact.setAdapter(new myAdapter());
		
		lv_selectcontact.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				String phone_number = list.get(position).get("phone").toString();
				Intent intent = new Intent();
				intent.putExtra("phone", phone_number);
				setResult(0, intent);	//返回			
				finish();
			}
		});
	}
	/**
	 * 1,先在表raw_contacts中取出联系人id
	 * 2,根据联系人id在data表中找到对应的mimetype与data1
	 * 		其中mimetype_id=5,就是vnd.android.cursor.item/phone_v2时 data1为电话号码
	 * 		当mimetype_id =7，就是vnd.android.cursor.item/name是，data1为联系人姓名
	 */
	private List<Map<String,String>> searchContact() {

		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		
		ContentResolver cr = getContentResolver();
		//存放联系人id
		Uri uri = Uri.parse("content://com.android.contacts/raw_contacts");
		Uri datauri = Uri.parse("content://com.android.contacts/data");
				
		Cursor cursor_id = cr.query(uri, new String[]{"contact_id"}, null, null, null);
		while(cursor_id.moveToNext())
		{			
			String contact_id = cursor_id.getString(0);
			System.out.print("id:"+contact_id);
			//根据id去data表中查找最终想要的数据
			if (contact_id != null) {
			
				//取出一个联系人时，产生一个map对象--->一个联系人信息对应一个map对象
				Map<String,String> map = new HashMap<String, String>();
				
				Cursor cursor_data = cr.query(datauri, new String[] {
						"mimetype", "data1",}, "contact_id=?",
						new String[] { contact_id }, null);
				
				while(cursor_data.moveToNext())
				{				
					//将数据取出来
					String mimetype = cursor_data.getString(0);
					String data1 = cursor_data.getString(1);
				//	System.out.println(mimetype_id+","+data1);
					
					if(mimetype.equals("vnd.android.cursor.item/phone_v2"))
						map.put("phone", data1);
					if(mimetype.equals("vnd.android.cursor.item/name"))
						map.put("name", data1);
//					if(Integer.valueOf(mimetype_id)==5)
//					{
//						map.put("phone", data1);						
//					}
//					if(Integer.valueOf(mimetype_id)==5)
//					{
//						map.put("name", data1);
//					}					
				}
				list.add(map);
				cursor_data.close();
			}
		}
		cursor_id.close();
		
		return list;
	}

	public class myAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(getApplicationContext(), R.layout.safe_selectcontact_item, null);
			
			TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
			TextView tv_number =(TextView) view.findViewById(R.id.tv_number);
			
			Map map = list.get(position);
			String name=(String) map.get("name");
			String number =(String) map.get("phone");
						
			tv_name.setText(name);
			tv_number.setText(number);
			return view;
		}
		
	}
}
