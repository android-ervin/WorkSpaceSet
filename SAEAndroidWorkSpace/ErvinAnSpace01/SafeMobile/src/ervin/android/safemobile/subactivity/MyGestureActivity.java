package ervin.android.safemobile.subactivity;

import ervin.android.safemobile.R;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public abstract class MyGestureActivity extends Activity {

	//定义一个手势识别器
	private GestureDetector mgd;
	protected SharedPreferences sp;  //所有子类都能用
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mgd = new GestureDetector(this, new myGestureListener());
		sp = getSharedPreferences("config", MODE_PRIVATE);
	}
	public class myGestureListener extends SimpleOnGestureListener
	{
		//屏幕滑动时的回调函数
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			//屏蔽斜着滑动屏幕
			if(Math.abs(e1.getRawY()-e2.getRawY())>100)
			{
				return true;
			}
			//屏蔽x轴速度过慢
			if(Math.abs(velocityX) < 200)
			{
				return true;
			}
			
			//第一次点击的x坐标大于第二次点击的x坐标，从右向左滑动，下一页
			if((e1.getRawX()-e2.getRawX()) > 150)
			{
				ShowNext();
				return true;
			}
			if((e2.getRawX()-e1.getRawX()) > 150)
			{
				ShowPre();
				return true;
			}
			return super.onFling(e1, e2, velocityX, velocityY);
		}
		
	}
	
	public void ShowNext()
	{
		//各个子类去跳转页面
		IntentNext();
		overridePendingTransition(R.anim.tran_in, R.anim.tran_out);//下一页
	}
	public void ShowPre()
	{
		IntentPre();
		overridePendingTransition(R.anim.tran_pre_in, R.anim.tran_pre_out);//上一页
	}
	//各个子类去实现自己所要跳转的页面
	public abstract void IntentNext();
	public abstract void IntentPre();
	
	
	/*Called when a touch screen event was not handled by any of the views under it. 
	This is most useful to process touch events that happen outside of your window bounds,
	 where there is no view to receive it.*/
	//被activity接收的event传递给手势识别器
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mgd.onTouchEvent(event);
		return super.onTouchEvent(event);
	}
	
}
