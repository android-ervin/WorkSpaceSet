package ervin.android.safemobile.subactivity;

import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import ervin.android.safemobile.R;
import ervin.android.safemobile.SettingCustomerView;

public class SafeGuide2 extends MyGestureActivity {

	private SettingCustomerView scv_bindsim;
	private String sim_number;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.safe_guide2);
		
		TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		sim_number = tm.getSimSerialNumber();
				
		scv_bindsim = (SettingCustomerView) findViewById(R.id.scv_bindsim);
		
		//开始的时候判断用户原来是否绑定sim卡了
		String sim = sp.getString("simcard", null);
		if(TextUtils.isEmpty(sim))
		{
			scv_bindsim.setSelect(false);
		}else
			scv_bindsim.setSelect(true);
		
		scv_bindsim.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				Editor editor = sp.edit();
				//判断是否选中
				if(scv_bindsim.isCheck())
				{
					scv_bindsim.setSelect(false);
					editor.putString("simcard", null);
				}else{
					scv_bindsim.setSelect(true);
					editor.putString("simcard", sim_number);
				}
				editor.commit();
			}
		});
	}

	@Override
	public void IntentNext() {
		Intent intent = new Intent(SafeGuide2.this,SafeGuide3.class);
		startActivity(intent);	
		finish();
	}

	@Override
	public void IntentPre() {
		Intent intent = new Intent(SafeGuide2.this,SafeGuide1.class);
		startActivity(intent);
		finish();
	}
}
