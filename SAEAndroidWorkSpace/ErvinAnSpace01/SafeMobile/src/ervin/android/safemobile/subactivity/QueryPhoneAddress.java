package ervin.android.safemobile.subactivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.EditText;
import android.widget.TextView;
import ervin.android.safemobile.R;
import ervin.android.safemobile.utils.QueryPhoneAddressUtils;

public class QueryPhoneAddress extends Activity {
	private EditText et_phone;
	private TextView tv_result;
	
	private Vibrator vb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.atools_queryphone);
		
		et_phone = (EditText) findViewById(R.id.et_phone);
		tv_result = (TextView) findViewById(R.id.tv_address);
		//获得系统震动服务
		vb = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
		
		et_phone.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s!=null && s.length()>=7)
				{
					String text = QueryPhoneAddressUtils.QueryAddressByPhone(s.toString());
					tv_result.setText(text);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void query(View view)
	{
		String phoneNum = et_phone.getText().toString().trim();
		if(TextUtils.isEmpty(phoneNum))
		{
			//输入框抖动效果（APIDemo）
			Animation shake =  AnimationUtils.loadAnimation(this, R.anim.shake);
			et_phone.setAnimation(shake);
			//自定义效果，添加插入器
//			shake.setInterpolator(new Interpolator() {
//				
//				//写相应的数学表达式，类似二元一次方程之类的，做出动画效果
//				public float getInterpolation(float input) {
//					// TODO Auto-generated method stub
//					return 0;
//				}
//			});
			
			
			//手机震动效果
			//vb.vibrate(2000);//震动2s
			
			//pattern an array of longs of times for which to turn the vibrator on or off.
			//设置开关时间
			long[] pattern = {200,200,300,300,400,400};
			//-1不重复，非-1充指定位置开始重复
			vb.vibrate(pattern, -1);
			
			return;
		}else
		{
			String address = QueryPhoneAddressUtils.QueryAddressByPhone(phoneNum);
			tv_result.setText(address);
		}
	}
}
