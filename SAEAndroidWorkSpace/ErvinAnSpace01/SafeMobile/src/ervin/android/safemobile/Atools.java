package ervin.android.safemobile;

import java.util.ArrayList;
import java.util.List;

import ervin.android.safemobile.db.Smsinfo;
import ervin.android.safemobile.subactivity.QueryPhoneAddress;
import ervin.android.safemobile.utils.SmsUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Atools extends Activity {

	private List<Smsinfo> Smsinfos;
	private ProgressDialog pd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.atools);
		Smsinfos = new ArrayList<Smsinfo>();
	}
	//获得系统所有短信信息
	public void getSmsInfo()
	{
		ContentResolver smsResolver = getContentResolver();
		Uri uri = Uri.parse("content://sms/");
		Cursor cursor = smsResolver.query(uri, new String[]{"body","address","type","date"}, null, null, null);
		
		while(cursor.moveToNext())
		{
			Smsinfo sms = new Smsinfo();
			sms.setBody(cursor.getString(0));
			sms.setAddress(cursor.getString(1));
			sms.setType(cursor.getString(2));
			sms.setDate(cursor.getString(3));
			Smsinfos.add(sms);
		}
		cursor.close();
	}
	
	public void queryPhone(View view)
	{
		//进入号码归属地查询页面
		Intent intent = new Intent(this,QueryPhoneAddress.class);
		startActivity(intent);		
	}
	
	public void backupSMS(View view)
	{
		getSmsInfo(); //得到系统所有短信
		pd = new ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); //水平条
		pd.setMessage("正在备份短信");
		pd.show();
		if(Smsinfos!=null)
		{	
			new Thread(){
				SmsUtils stools = new SmsUtils(Smsinfos);
				public void run() {
					try {
						stools.useXMLSerilizeBackUp(Atools.this,pd);
						runOnUiThread(new Runnable() {						
							@Override
							public void run() {
								//必须在UI线程中运行
								Toast.makeText(Atools.this, "备份成功", Toast.LENGTH_LONG).show();							
							}
						});
					} catch (Exception e) {
						runOnUiThread(new Runnable() {						
							@Override
							public void run() {
								Toast.makeText(Atools.this, "备份失败", Toast.LENGTH_LONG).show();
							}
						});
						e.printStackTrace();
					} finally
					{
						pd.dismiss();
					}
					super.run();
				}
				
			}.start();			
		}
	}
	
	public void restoreSMS(View view)
	{
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("提示");
		builder.setMessage("恢复短信需要先清空原有的短信，是否确认该操作？");
		
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				dialog.dismiss();
				pd = new ProgressDialog(Atools.this);
				pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); //水平条
				pd.setMessage("正在恢复短信");
				pd.show();
				
				new Thread(){
					public void run() {
						try {
							SmsUtils.restoreSMS(Atools.this,pd);
							runOnUiThread(new Runnable() {						
								@Override
								public void run() {
									//必须在UI线程中运行
									Toast.makeText(Atools.this, "恢复成功", Toast.LENGTH_LONG).show();							
								}
							});
						} catch (Exception e) {
							runOnUiThread(new Runnable() {						
								@Override
								public void run() {
									Toast.makeText(Atools.this, "恢复失败", Toast.LENGTH_LONG).show();
								}
							});
							e.printStackTrace();
						}finally					
						{
							pd.dismiss();
						}
					};
				}.start();
				
			}
		});
		
		builder.setNegativeButton("取消", null);
		AlertDialog dialog = builder.create();
		dialog.show();
		
		
	}
}
