package ervin.android.safemobile.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

public class BootCompleteReceiver extends BroadcastReceiver {

	//监听开机广播，当开机时，检查此时的sim卡和保存在sp中的sim卡是否相同
	private SharedPreferences sp;
	@Override
	public void onReceive(Context context, Intent intent) {
		sp = context.getSharedPreferences("config", context.MODE_PRIVATE);
		//取出保存的sim id
		String sp_sim = sp.getString("simcard", null);
		//取出当前的sim id
		TelephonyManager tm  = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
		String cur_sim = tm.getSimSerialNumber();
		if(!sp_sim.equals(cur_sim))
		{
			//发送短信给安全号码，sim卡已经改变了
			//取出安全号码
			String safenum = sp.getString("safenumber", "");
			if(safenum!="")
			{
				SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(safenum, null, "sim card change !", null, null);
			}
				
		}

	}

}
