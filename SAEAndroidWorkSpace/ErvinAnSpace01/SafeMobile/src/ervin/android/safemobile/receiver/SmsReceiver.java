package ervin.android.safemobile.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import ervin.android.safemobile.R;
import ervin.android.safemobile.service.LocationService;


public class SmsReceiver extends BroadcastReceiver {

	/**
	 * 	监控本机短信广播，当有短信到来时,判断是否是安全号码发过来的短信指令。
	 * 	如果是，将短信截获（将该广播接受者优先级调至最高1000），并且让手机做出相应的操作，定位，报警等；
	 * 	如果不是，正常接收短信
	 */
	
	private SharedPreferences sp;
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		sp = context.getSharedPreferences("config", context.MODE_PRIVATE);
		String safenum = sp.getString("safenumber", "");
		
		//接收短信的官方写法
//		Object[] objs = (Object[]) intent.getExtras().get("pdus"); //获得系统有短信到来时发过来的数据
//		for(Object b : objs)
//		{
//			SmsMessage sms = SmsMessage.createFromPdu((byte[]) b);
//			String sender = sms.getOriginatingAddress(); // 获得发短信人的号码
//			String body = sms.getMessageBody();//获得短信内容
//			
//			if("#*location*#".equals(body)){
//				//获得手机位置地址
//				
//				abortBroadcast(); //截获短信
//			}else if("#*alarm*#".equals(body)){
//				//手机报警
//				
//				abortBroadcast();
//			}else if("#*lockscreen*#".equals(body)){
//				//远程锁屏
//				
//				abortBroadcast();
//			}else if("#*wipedata*#".equals(body)){
//				//清除手机数据
//				
//				abortBroadcast();
//			}
//
//		}
		
		Object[] objs = (Object[]) intent.getExtras().get("pdus");
		for(Object b : objs)
		{
			android.telephony.SmsMessage sms = android.telephony.SmsMessage.createFromPdu((byte[]) b);
			String sender = sms.getOriginatingAddress(); // 获得发短信人的号码
			String body = sms.getMessageBody();//获得短信内容
			
			//判断是否是安全号码发过来的？
			if (sender.equals(safenum)) {
				if ("#*location*#".equals(body)) {
					// 获得手机位置地址(当解析短信是发来的定位命令时，可以开启获取位置的服务)
					Intent intentservice = new Intent(context,LocationService.class);
					context.startService(intentservice);
					//getLocation(context);
					String location =sp.getString("location", "");
					if(TextUtils.isEmpty(location))
					{
						SmsManager.getDefault().sendTextMessage(sender, null, "is getting...", null,null);
					}else
					{
						SmsManager.getDefault().sendTextMessage(sender, null, location, null, null);
						//context.stopService(intentservice);
					}
					abortBroadcast(); // 截获短信
				} else if ("#*alarm*#".equals(body)) {
					// 手机报警
					MediaPlayer mp = MediaPlayer.create(context, R.raw.itl); //create方法中已经创建了多媒体对象，并且prepare()了
					mp.setLooping(true); //循环播放
					mp.setVolume(1.0f, 1.0f);//设置左右声道都是最大音量
					mp.start();			
					abortBroadcast();
				} else if ("#*lockscreen*#".equals(body)) {
					// 远程锁屏(一键锁屏--官方版本)
					
					abortBroadcast();
				} else if ("#*wipedata*#".equals(body)) {
					// 清除手机数据

					abortBroadcast();
				}
			}
		}
			
	}
	private void getLocation(Context context) {
		// 获得系统位置管理服务
		LocationManager lm = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
		//List<String> providers = lm.getAllProviders();//获取所有可用的定位服务，一般有三种，网络，基站，GPS
		//自动选择三个定位方法中最好的方式
		Criteria criteria  = new Criteria();//给provider设置条件
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		String provider = lm.getBestProvider(criteria, true);
		lm.requestLocationUpdates(provider, 1*60*1000, 50, new LocationListener() {
			//当GPS状态改变的时候调用，GPS从开--->关，从关--->开
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub
				
			}
			//定位服务可用时，网络，基站，GPS至少有一个可用时调用
			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}
			//定位服务不可用是调用
			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				
			}
			//位置发生改变时调用
			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				String longitude = "lng:"+location.getLongitude();
				String lattitude = "lat:"+location.getLatitude();
				String accuracy = "acu"+location.getAccuracy();
				
				//将坐标纠偏（省略）
				
				//将最后一次的改变的坐标保存
				Editor editor = sp.edit();
				editor.putString("location", longitude+","+lattitude+","+accuracy);
				editor.commit();						
			}
		});
		
	}

}
