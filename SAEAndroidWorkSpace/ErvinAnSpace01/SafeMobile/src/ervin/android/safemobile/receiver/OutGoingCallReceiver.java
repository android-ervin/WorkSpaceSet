package ervin.android.safemobile.receiver;

import ervin.android.safemobile.utils.CustomToast;
import ervin.android.safemobile.utils.QueryPhoneAddressUtils;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class OutGoingCallReceiver extends BroadcastReceiver {

	private CustomToast CToast;
	private String action = "android.intent.action.NEW_OUTGOING_CALL";
	//监听系统的广播，当有电话呼出时，会发送广播给所有action相同的广播接收者
	@Override
	public void onReceive(Context context, Intent intent) {

		CToast = CustomToast.getInstance();
		System.out.println("有电话打出。。。。。");
		String phoneNum = getResultData(); // 获得拨出的电话号码
		String address = QueryPhoneAddressUtils.QueryAddressByPhone(phoneNum);
		//Toast.makeText(context, address, 1).show();
		//自定义Toast
		CToast.showCustomToast(context,address);

	}

}
