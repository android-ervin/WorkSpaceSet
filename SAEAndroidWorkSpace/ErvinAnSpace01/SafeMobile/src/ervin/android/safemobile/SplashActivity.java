package ervin.android.safemobile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SplashActivity extends ActionBarActivity {

	private String updateurl="";
	protected static final int CONNECT_OK = 0;
	protected static final int CONNECT_ERRO = 1;
	private TextView tv_version;
	
	private SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
        
        tv_version = (TextView) findViewById(R.id.tv_version);
        tv_version.setText("版本为："+getVersion());  
        
        sp = getSharedPreferences("config", MODE_PRIVATE);
        boolean ischeckupdate = sp.getBoolean("isupdate", false);
        //拷贝号码归属地数据库到指定目录
        CoypDB();
        //判断是否联网升级        
        if(ischeckupdate)
        {
        	checkUpdate();
        }else
        {
        	mHandler.postDelayed(new Runnable() {				
				@Override
				public void run() {
					enterHome();					
				}
			}, 2000);
        }
        AlphaAnimation aa = new AlphaAnimation(0.2f, 1.0f);
        aa.setDuration(500);
        findViewById(R.id.splash_layout).startAnimation(aa);
    }
    
	private void CoypDB() {
		//拷贝assets中的数据库到指定目录中
		try {
			InputStream is = getAssets().open("address.db");//将数据写进内存中			
			//在绝对路径下创建一个文件  "data/data/ervin.android.safemobile/files/address.db";
			File file = new File(getFilesDir(),"address.db"); 
			//判断文件是否存在，如果存在就 不copy了
			if (file.exists() && file.length() != 0) 
			{
				
			}else
			{
				// 获得一个输出流，将内存中的数据写入到文件中
				FileOutputStream fos = new FileOutputStream(file);
				// 获得一个中间容器
				byte[] b = new byte[1024];
				int len = 0;
				while ((len = is.read(b)) != -1) {
					// 将1024byte的数据读到内存中，判断没读到末尾就写入到文件中

					// 读出多少就写入多少数据，边读边写(第二个参数是buffer的起始位置，当然从buffer的第0位开始读)
					/**
					 * Parameters: buffer the buffer to be written. byteOffset
					 * the start position in buffer from where to get bytes.
					 * byteCount the number of bytes from buffer to write to
					 * this stream.
					 */
					fos.write(b, 0, len);
				}
				is.close();
				fos.flush();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == CONNECT_OK) {
				String result = (String) msg.obj;
				try {
					JSONObject json = new JSONObject(result);
					String version = (String) json.get("version");
					String desc = (String) json.get("description");
					updateurl = json.getString("apkurl");
					// System.out.println(version+","+desc+","+url);
					if (!getVersion().equals(version)) {
						AlertDialog.Builder builder = new Builder(SplashActivity.this);
						builder.setOnCancelListener(new DialogInterface.OnCancelListener() {							
							@Override
							public void onCancel(DialogInterface dialog) {
								// TODO Auto-generated method stub
								enterHome();
								dialog.dismiss();
							}
						});
						builder.setTitle("提示更新");
						builder.setMessage(desc);
						builder.setPositiveButton("马上升级",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// 下载升级安装包
										installNewApk();
									}
								});
						builder.setNegativeButton("下次再说",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// 进入home页面
										enterHome();
									}
								});
						builder.show();
					}else //没有更新
					{
						//进入home页面
						enterHome();
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (msg.what == CONNECT_ERRO) {				
				try {				
					Thread.sleep(2000);
					Toast.makeText(getApplicationContext(), "联网错误", 0).show();
					enterHome();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			super.handleMessage(msg);
		}
	};
	
    private String getVersion()
    {
    	 PackageManager pm = getPackageManager();
 		try {
 			PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
 			return info.versionName;
 		} catch (NameNotFoundException e) {
 			e.printStackTrace();
 			return "";
 		}	
    }
    
    //联网获取跟新信息
	private void checkUpdate() {
		new Thread(new Runnable() {
			public void run() {
				Message msg = Message.obtain();
				long starttime = System.currentTimeMillis();
				try {

					URL url = new URL("http://10.0.2.2:80/update.htm");// "http://10.0.2.2:80/Workers1.xml"
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setConnectTimeout(4000);
					int code = conn.getResponseCode();
					System.out.println("返回码:" + code);
					if (conn.getResponseCode() == 200) {
						// 联网成功
						InputStream is = conn.getInputStream();
						BufferedReader br = new BufferedReader(
								new InputStreamReader(is));
						String line = "";
						String result = "";
						while ((line = br.readLine()) != null) {
							result += line;
						}
						System.out.println("conncect ok" + result);
						// 使用消息，将得到的字符串返回给主线程
						msg.what = CONNECT_OK;
						msg.obj = result;

					} else {
						msg.what = CONNECT_ERRO;
						// 子线程不能更改UI
					}

				} catch (MalformedURLException e) {
					msg.what = CONNECT_ERRO;
					e.printStackTrace();
				} catch (IOException e) {
					msg.what = CONNECT_ERRO;
					e.printStackTrace();
				} finally {
					long endtime = System.currentTimeMillis();
					if (endtime - starttime < 2000)
						try {
							Thread.sleep(2000 - (endtime - starttime));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					mHandler.sendMessage(msg);
				}

			}
		}).start();
	}
    
    public void enterHome()
    {
    	Intent intent = new Intent(SplashActivity.this,Home.class);
    	startActivity(intent);
    	finish();
    }
    
    public void installNewApk()
    {
    	if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))//表示SD卡已经挂载
    	{
    		//这里用到一个开源的下载框架 afinal(大神michael)
    		FinalHttp fh = new FinalHttp();
    		
    		fh.download(updateurl, Environment.getExternalStorageDirectory().getAbsolutePath()+
    				"/update.apk", new AjaxCallBack<File>() {

						@Override
						public void onFailure(Throwable t, int errorNo,
								String strMsg) {
							Toast.makeText(SplashActivity.this, "下载失败:"+strMsg, 0).show();
							enterHome();
							super.onFailure(t, errorNo, strMsg);
						}

						@Override
						public void onLoading(long count, long current) {
							long l=current / count ;
							
							super.onLoading(count, current);
						}

						@Override
						public void onSuccess(File t) {
							// 开始安装apk,调用系统的Activity
							Toast.makeText(SplashActivity.this, "下载成功，准备安装", 0).show();
							Intent intent = new Intent();
							intent.setAction("android.intent.action.VIEW");
							intent.addCategory("android.intent.category.DEFAULT");
							//MIME映射关系,其中application/vnd.andorid.package-archive对应的就是.apk文件
							intent.setDataAndType(Uri.fromFile(t), "application/vnd.android.package-archive");
							startActivity(intent);
							enterHome();
							super.onSuccess(t);
						}  			
					});
    	}else
    	{
    		Toast.makeText(SplashActivity.this, "SD卡已经拔出，请检查", 0).show();
    		enterHome();
    	}
    }

    
}
