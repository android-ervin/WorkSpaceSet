package ervin.android.safemobile.utils;

import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;

public class ServiceUtils {

	//判断某个服务是否还在运行
	public static boolean isServiceRunning(Context context,String serviceName)
	{
		//获得activity管理器
		ActivityManager am =  (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		//得到正在运行的服务集合
		List<RunningServiceInfo> service = am.getRunningServices(100);
		for(RunningServiceInfo rs : service)
		{
			String servicename = rs.service.getClassName();//得到系统名称
			if(servicename.equals(serviceName))
				return true;
		}
		return false;
	}
}
