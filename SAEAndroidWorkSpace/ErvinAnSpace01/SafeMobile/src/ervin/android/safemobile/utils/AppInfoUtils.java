package ervin.android.safemobile.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import ervin.android.safemobile.AppManager;
import ervin.android.safemobile.db.Appinfo;

public class AppInfoUtils {

	/**
	 * 获取手机所有应用信息
	 * @param context ：上下文
	 * @param handler ：用来往主线程发送消息，更新UI
	 */
	public static List<Appinfo> getAllAppInfo(Context context,Handler handler)
	{
		//TextView tv;
		List<Appinfo> appinfos = new ArrayList<Appinfo>();
		
		PackageManager pm =  context.getPackageManager();
		//Return a List of all packages that are installed on the device.
		List<PackageInfo> packageinfos = pm.getInstalledPackages(0); //获得所有已安装APP的包信息
		for(PackageInfo packageinfo : packageinfos )
		{
			Appinfo appinfo = new Appinfo();
			String packagename = packageinfo.packageName;  //包名
			Drawable icon = packageinfo.applicationInfo.loadIcon(pm);  //应用程序的icon
			String labelname = (String) packageinfo.applicationInfo.loadLabel(pm); //应用程序名称
			int flags = packageinfo.applicationInfo.flags;//点击看源码，表示应用的一些状态
			
			if((flags & packageinfo.applicationInfo.FLAG_SYSTEM)==0)//表示不是系统应用
			{

				appinfo.setSystem(false);//用户程序
			}else{
				appinfo.setSystem(true);//系统应用
			}
			if((flags & packageinfo.applicationInfo.FLAG_EXTERNAL_STORAGE)==0) //表示存放位置
			{
				appinfo.setRom(true);
			}else
				appinfo.setRom(false);
			
			Message msg = handler.obtainMessage();
			msg.obj = labelname;
			handler.sendMessage(msg);
			//tv = AppManager.tv_scaning;
			//tv.setText("当前扫描："+labelname);
			appinfo.setPackagename(packagename);
			appinfo.setIcon(icon);
			appinfo.setLabelname(labelname);
			
			appinfos.add(appinfo);
		}
		return appinfos;
	}
}
