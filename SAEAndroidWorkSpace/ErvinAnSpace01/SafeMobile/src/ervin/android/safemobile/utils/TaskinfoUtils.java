package ervin.android.safemobile.utils;

import java.util.ArrayList;
import java.util.List;

import ervin.android.safemobile.R;
import ervin.android.safemobile.db.Taskinfo;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;

/**
 * 获取进程任务的具体实现类
 * @author acer
 *
 */
public class TaskinfoUtils {

	/**
	 * 获得用户所有进程信息
	 * 包括：packagename,labelname,memosize,icon等。
	 * 1，首先根据am得到包名
	 * 2，根据包名和pm得到包管理列表的其他进程的信息
	 * @param context
	 * @return
	 */
	public static List<Taskinfo> getTaskInfo(Context context)
	{
		List<Taskinfo> taskinfos = new ArrayList<Taskinfo>();
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		PackageManager pm = context.getPackageManager();
		List<RunningAppProcessInfo> appinfo = am.getRunningAppProcesses();
		for(RunningAppProcessInfo info : appinfo)
		{		
			int level = info.importance; // 判断是否为空进程
			Taskinfo taskinfo = new Taskinfo();
			String packagename = info.processName; // 应用程序包名
			taskinfo.setPackagename(packagename);
			int pid = info.pid; // 进程号
			// Return information about the memory usage of one or more
			// processes. All results are in kB.
			android.os.Debug.MemoryInfo[] memo = am
					.getProcessMemoryInfo(new int[] {pid}); // 得到当前pid进程的内存占用信息			
			//long memsize = (memo[0].dalvikPss + memo[0].nativePss) * 1024; // dalvik虚拟机和本地c++一共所占用的内存空间
			long memsize = memo[0].getTotalPrivateDirty()*1024;
			taskinfo.setMemsize(memsize);
			
			try {				
				String lablename = (String) pm.getApplicationInfo(packagename,
						0).loadLabel(pm); //会报异常
				Drawable icon = pm.getApplicationInfo(packagename, 0).loadIcon(
						pm);//会报异常			
				taskinfo.setIcon(icon);
				taskinfo.setLablename(lablename);	
				int flag = pm.getApplicationInfo(packagename, 0).flags;
				if ((flag & pm.getApplicationInfo(packagename, 0).FLAG_SYSTEM) == 0) {// 是用户应用
					taskinfo.setIsuser(true);
				} else {
					// 系统应用
					taskinfo.setIsuser(false);
				}
			} catch (NameNotFoundException e) {
				// 如果找不到对应的label名称
				taskinfo.setLablename(packagename);
				taskinfo.setIcon(context.getResources().getDrawable(R.drawable.icon));
				e.printStackTrace();
			}
			taskinfos.add(taskinfo);
		}
		return taskinfos;
		
	}
}
