package ervin.android.safemobile.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5ForPsw {

	//给密码加密。Md5加密算法
	public static String addSecurity(String psw)
 {
		// 得到一个消息摘要器
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");

			byte[] result = digest.digest(psw.getBytes());
			StringBuffer sb = new StringBuffer();
			// 把每一个byte位与0xff做与运算
			for (byte b : result) {
				int number = b & 0xff;
				String pswstr = Integer.toHexString(number);
				// 当pswstr只有1位十六进制的数时，在该十六进制数前加“0”
				if (pswstr.length() == 1) {
					sb.append("0");
				}
				sb.append(pswstr);
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			
			e.printStackTrace();
			return "";
		}
		
	}
}
