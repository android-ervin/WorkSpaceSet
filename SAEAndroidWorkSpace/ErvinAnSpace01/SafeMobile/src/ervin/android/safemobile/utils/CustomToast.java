package ervin.android.safemobile.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PixelFormat;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.TextView;
import ervin.android.safemobile.R;

public class CustomToast {

	private static View view;
	private WindowManager wm;
	private WindowManager.LayoutParams params;
	public SharedPreferences sp;
	long mHits[] = new long[2]; //点击两次
	private static CustomToast CToast =  new CustomToast();
	private CustomToast()
	{	
	}
	public static CustomToast getInstance()
	{
		return CToast;
	}
	public void showCustomToast(Context context,String text)
	{		
		view = View.inflate(context, R.layout.atools_customtoast, null);	
		TextView tv_address = (TextView) view.findViewById(R.id.tv_toastaddress);
		tv_address.setText(text);
		view.setBackgroundResource(R.drawable.call_locate_green);
		
		sp = context.getSharedPreferences("config", context.MODE_PRIVATE);
		
		//监听view的点击事件，双击view的话居中，用Google经典代码实现
		
		view.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				System.out.println("view is clicked");
				
				System.arraycopy(mHits, 1, mHits, 0, mHits.length-1);//拷贝数组
				mHits[mHits.length-1] = SystemClock.uptimeMillis(); //最后一位设置为当前开机时间
				if(mHits[0]>=(SystemClock.uptimeMillis()-500)) //说明符合双击要求
				{
					System.out.println("双击了");
					params.x = wm.getDefaultDisplay().getWidth()/2-view.getWidth()/2;//水平居中
					wm.updateViewLayout(view, params);
					Editor editor = sp.edit();
					editor.putInt("paramsx", params.x);
					editor.commit();
				}
			}
		});
		//监控view的触摸事件，使自定义Toast能够移动
		/**
		 * 触摸事件和点击事件---》
		 * 	点击事件：点击事件是一系列动作的组合，按下--停留--抬起，这才是一个完整的点击事件（按下后，在别的地方抬起这种就不会响应点击事件）
		 * 	触摸事件：只要触摸到屏幕就会触发
		 */ 
		view.setOnTouchListener(new OnTouchListener() {	
			//记录用户触摸的起始坐标
			int startx;
			int starty;
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: //按下
					startx = (int) event.getRawX();
					starty = (int) event.getRawY();
					break;
				case MotionEvent.ACTION_MOVE:  //移动
					//移动，就相当于设置params参数的params.x和params.y参数来控制view显示的位置
					//获得其移动后的坐标
					int movex = (int) event.getRawX();
					int movey = (int) event.getRawY();				
					//计算与起始点比较，坐标偏移了多少
					int dx = movex - startx;
					int dy = movey - starty;
					//在上一次的基础上移动，并且考虑边界问题params必须在屏幕的尺寸范围内
					params.x+=dx;
					params.y+=dy;
					if(params.x<0)
						params.x =0;
					if(params.y<0)
						params.y = 0;
					if(params.x > (wm.getDefaultDisplay().getWidth()-view.getWidth()))
						params.x = wm.getDefaultDisplay().getWidth()-view.getWidth();
					if(params.y > (wm.getDefaultDisplay().getHeight() - view.getHeight()))
						params.y = wm.getDefaultDisplay().getHeight() - view.getHeight();
					//更新起始坐标
					startx = movex;
					starty = movey;
					//立刻更新view的显示位置
					wm.updateViewLayout(view, params);
					break;
				case MotionEvent.ACTION_UP:   //抬起
					//抬起的时候说明移动结束，记录此时的params坐标
					Editor editor = sp.edit();
					editor.putInt("paramsx", params.x);
					editor.putInt("paramsy", params.y);
					editor.commit();
					break;
				default:
					break;
				}
				return false;//如果返回true表示事件到此处理完毕，view不再响应其它事件，如click等。
			}
		});
		
		
		wm = (WindowManager) context.getSystemService("window");
		//配置Toast显示在窗体的什么位置和一些style(SDK源码)
		params = new WindowManager.LayoutParams();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.format = PixelFormat.TRANSLUCENT;//半透明
        
        params.gravity=Gravity.TOP+Gravity.LEFT; //默认显示在左上角
        //表示Toast在屏幕(100,100)的位置显示
        //读取sp中保存的位置
        params.x = sp.getInt("paramsx", 0);
        params.y = sp.getInt("paramsy", 0);
        
       // params.type = WindowManager.LayoutParams.TYPE_TOAST;  // 这种类型的Toast天生没有点击或者触摸事件
        
        //这种类型的view具有焦点,优先级很好，会影响用户键盘，需要设置权限system_alter_window
        params.type = WindowManager.LayoutParams.TYPE_PRIORITY_PHONE ; 
        //将定义好的view加载到窗体
		wm.addView(view, params);
		
	}
	public void dismissCustomToast()
	{
		if(view!=null)
		{
			wm.removeView(view);
			view = null;
			wm = null;
		}
			
	}
	
}
