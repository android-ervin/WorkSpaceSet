package ervin.android.safemobile.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import ervin.android.safemobile.db.Smsinfo;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Xml;
import android.widget.Toast;


public class SmsUtils {

	private List<Smsinfo> smsinfos = null;
	public SmsUtils(List<Smsinfo> smsinfos)
	{
		this.smsinfos = smsinfos ;
	}
	public void backUp(Context context)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		sb.append("<smss>");
		for (Smsinfo info : smsinfos) {
			sb.append("<sms>");
			sb.append("<address>");
			sb.append(info.getAddress());
			sb.append("</address>");
			
			sb.append("<type>");
			sb.append(info.getType());
			sb.append("</type>");
			
			sb.append("<body>");
			sb.append(info.getBody());
			sb.append("</body>");
			
			sb.append("<date>");
			sb.append(info.getDate());
			sb.append("</date>");
			sb.append("</sms>");
		}
		sb.append("</smss>");
		try {
			File file =new File(Environment.getExternalStorageDirectory(),"backup_one.xml");
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(sb.toString().getBytes());
			fos.close();
			Toast.makeText(context,"备份成功", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(context,"备份失败", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	
	//抛出异常，让调用者去捕获
	/**
	 * 用xml序列化器备份短信
	 * @param context
	 * @param pd  显示进度条
	 * @throws Exception
	 */
	public void useXMLSerilizeBackUp(Context context,ProgressDialog pd) throws Exception {
		XmlSerializer serializer = Xml.newSerializer();// 生成一个xml序列号器
		File file = new File(Environment.getExternalStorageDirectory(),
				"smsbackup.xml");// 文件保存至SD卡
		FileOutputStream fos = new FileOutputStream(file);
		serializer.setOutput(fos, "utf-8");

		serializer.startDocument("utf-8", true);
		serializer.startTag(null, "smss");
		
		//serializer.setProperty("max", smsinfos.size()+"");//加一条总短信条数的属性（方便还原的时候读取）
		serializer.attribute(null, "max", smsinfos.size()+"");
		//获得一共有多少条短信需要备份
		pd.setMax(smsinfos.size());
		int process = 0;
		for (Smsinfo info : smsinfos) {
			//Thread.sleep(500);//为了演示进度条效果
			serializer.startTag(null, "sms");
			//serializer.attribute(null, "id", info.getId() + "");

			serializer.startTag(null, "address");
			serializer.text(info.getAddress() + "");
			serializer.endTag(null, "address");

			serializer.startTag(null, "type");
			serializer.text(info.getType());
			serializer.endTag(null, "type");

			serializer.startTag(null, "body");
			serializer.text(info.getBody());
			serializer.endTag(null, "body");

			serializer.startTag(null, "date");
			serializer.text(info.getDate());
			serializer.endTag(null, "date");

			serializer.endTag(null, "sms");
			//每备份一条，显示进度（pd可以在子线程调用）
			process++;
			pd.setProgress(process);
		}
		serializer.endTag(null, "smss");
		serializer.endDocument();
		fos.close();
	}

	public static void  restoreSMS(Context context,ProgressDialog pd) throws Exception
	{ 
		File file = new File(Environment.getExternalStorageDirectory(),
				"smsbackup.xml");
		InputStream is = new FileInputStream(file);
		
		XmlPullParser xmlPull = Xml.newPullParser(); //得到一个pull解析器，用来解析xml文件
		xmlPull.setInput(is, "UTF-8");  
        int eventType = xmlPull.getEventType();  
        boolean isDone = false; 
        String max =null;
        int process = 0;//解析进度
        
        //恢复短信
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.parse("content://sms/");
        
        //删除原有短信
        cr.delete(uri, null, null);
        ContentValues values = null;        
       
        while(!isDone){
        	String localname = null ; 
        	switch (eventType) {
			case XmlPullParser.START_DOCUMENT:
				System.out.println("开始解析xml");
				break;
			case XmlPullParser.START_TAG:{
				localname = xmlPull.getName();//获得当前节点的名字
				if("smss".equals(localname))
				{
					max = xmlPull.getAttributeValue(null, "max");
					pd.setMax(Integer.parseInt(max));
					System.out.println("一共有："+max+"条短信");
				}
				if("sms".equals(localname))
				{
					System.out.println("开始解析短信");
					values = new ContentValues();
				}
				if("address".equals(localname))
				{
					String address = xmlPull.nextText();
					values.put("address", address);
					System.out.println("address:"+address);
				}
				if("body".equals(localname))
				{
					String body = xmlPull.nextText();
					values.put("body", body);
					System.out.println("body:"+body);
				}
				if("type".equals(localname))
				{
					String type = xmlPull.nextText();
					values.put("type", type);
					System.out.println("type:"+type);
				}
				if("date".equals(localname))
				{
					String date = xmlPull.nextText();
					values.put("date", date);
					System.out.println("date:"+date);
				}
			}
				break;
			case XmlPullParser.END_TAG:
				localname = xmlPull.getName();//获得当前节点的名字
				if("sms".equals(localname))
				{
					process++;
					pd.setProgress(process);
					
					//插入短信
					cr.insert(uri, values);
					System.out.println("解析完一条短信");
				}
				break;
			case XmlPullParser.END_DOCUMENT:
				isDone = true;
				break;
			}
        	eventType = xmlPull.next(); //解析下一个event
        }
	}
}
