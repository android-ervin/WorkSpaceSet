package ervin.android.safemobile.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.net.TrafficStats;
import ervin.android.safemobile.db.Appinfo;

public class TrafficInfoUtils {
	
	public static List<Appinfo> getAppaTrafInfo(Context context)
	{
		List<Appinfo> appinfos = new ArrayList<Appinfo>();
		PackageManager pm = context.getPackageManager();
		//Return a List of all application packages that are installed on the device
		List<ApplicationInfo> applicationInfos = pm.getInstalledApplications(0); // 获得在手机上安装的所有APP应用程序信息
		for(ApplicationInfo ai : applicationInfos)
		{
			Appinfo appinfo = new Appinfo();
			int uid = ai.uid; //得到用户id
			//得到当前UID的APP单独消耗的流量
			long appTx = TrafficStats.getUidRxBytes(uid);
			long appRx = TrafficStats.getUidTxBytes(uid);
			appinfo.setTrafficused(appRx+appTx);
			
			String packagename = ai.packageName;
			try {
				Drawable icon = pm.getApplicationInfo(packagename, 0).loadIcon(pm);
				appinfo.setIcon(icon);
				String labelname = (String) pm.getApplicationInfo(packagename, 0).loadLabel(pm);
				appinfo.setLabelname(labelname);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if((appTx!=-1) && (appRx!=-1))
			{
				appinfos.add(appinfo);
			}
			
		}
		return appinfos;
	}

}
