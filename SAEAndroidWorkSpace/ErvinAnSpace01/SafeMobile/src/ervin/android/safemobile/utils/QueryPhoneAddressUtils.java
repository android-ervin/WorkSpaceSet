package ervin.android.safemobile.utils;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class QueryPhoneAddressUtils {
	
	public static String path = "data/data/ervin.android.safemobile/files/address.db";
	public static String QueryAddressByPhone(String phoneNum)
	{
		String address = "";
		// 对传过来的电话号码做个正则表达式判断
		if (phoneNum.matches("^1[345678]\\d{9}$"))
		{			
			// 数据库的路径 data/data/包名/files/address.db
			SQLiteDatabase sd = SQLiteDatabase.openDatabase(path, null,
					SQLiteDatabase.OPEN_READONLY);

			// select location from data2 where id in (select outkey from data1
			// where id = "1300020")
			// 处理电话号码--取出前七位

			String sql = "select location from data2 where id in (select outkey from data1 where id = ?)";
			Cursor cursor = sd.rawQuery(sql,
					new String[] { phoneNum.substring(0, 7) });
			while (cursor.moveToNext()) {
				address = cursor.getString(0);
			}
		}
		return address;	
	}
}
