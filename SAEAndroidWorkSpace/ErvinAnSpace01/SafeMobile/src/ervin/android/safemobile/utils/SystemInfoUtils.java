package ervin.android.safemobile.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;

public class SystemInfoUtils {

	/**
	 * 获得正在运行的总进程数
	 * @param context
	 * @return
	 */
	public static int getRunningProcessCount(Context context)
	{
		//通过系统服务获得进程管理器
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		//Returns a list of application processes that are running on the device. 
		List<RunningAppProcessInfo> infos = am.getRunningAppProcesses();
		return infos.size();
	}
	/**
	 * 获得当前剩余内存
	 * @param context
	 */
	public static long getAvailMem(Context context)
	{
		//通过系统服务获得进程管理器
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		//获得系统的总体进程的内存信息
		MemoryInfo outInfo = new MemoryInfo();
		am.getMemoryInfo(outInfo);
		
		//获得进程号为pid的进程的内存信息
//		android.os.Debug.MemoryInfo meminfo = new android.os.Debug.MemoryInfo();
//		am.getProcessMemoryInfo(pids)
		return outInfo.availMem;
	}
	
	public static long getTotalMem(Context context)
	{
		// 通过系统服务获得进程管理器
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
//		// 获得系统的总体进程的内存信息
//		MemoryInfo outInfo = new MemoryInfo();
//		am.getMemoryInfo(outInfo);
//		return outInfo.totalMem; //SDK 4.0以后可以用（真机正常）
		
		/**
		 * linux命名行进入查看内存信息
		 * 1,adb shell
		 * 2,cd proc
		 * 3,cat memoinfo
		 * 
		 * linux命令行删除系统应用（root权限）
		 * 1.adb shell
		 * 2，cd system/app  --进入系统APP文件夹
		 * 3，rm browser.apk --一般因为权限read-only会失败
		 * 	3.1，mount -o remount ,rw/system   --将文件重新挂载为可读可写 状态
		 *  3.2，rm -r browser.apk   --删除该系统应用程序的apk就是卸载掉了
		 */
		//为了兼容低级版本，可以用一下操作来获得系统总内存可用内存
		File file = new File("/proc/meminfo");
		StringBuffer sb = new StringBuffer();
		try {
			InputStream is = new FileInputStream(file);
			//读取文件第一行内容就是总内存：MemTotal:         516312 kB
			BufferedReader br = new BufferedReader(new InputStreamReader(is)) ;		
			String memoinfo = br.readLine();
			for(char c : memoinfo.toCharArray())
			{
				if(c>'0' && c<'9')
					sb.append(c);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Long.parseLong(sb.toString())*1024; //byte
	}
}
