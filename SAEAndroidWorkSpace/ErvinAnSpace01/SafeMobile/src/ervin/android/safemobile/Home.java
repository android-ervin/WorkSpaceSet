package ervin.android.safemobile;

import ervin.android.safemobile.utils.Md5ForPsw;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends Activity{

	private GridView gv_list;
	private String[] names={"手机防盗","通讯卫士","软件管理","进程管理","流量统计",
			"手机杀毒","缓存清理","高级工具","设置中心"};
	private static int[] ids = {
		R.drawable.safe,R.drawable.callmsgsafe,R.drawable.app,
		R.drawable.taskmanager,R.drawable.netmanager,R.drawable.trojan,
		R.drawable.sysoptimize,R.drawable.atools,R.drawable.settings		
	};
	private final int SAFE=0;
	private final int CALLMSGSAFE =1;
	private final int APP = 2;
	private final int TASKMANAGER =3;
	private final int NETMANAGER =4;
	private final int TROJAN =5;
	private final int SYSOPTIMIZE =6;
	private final int ATOOLS =7;
	private final int SETTINGS =8;
	
	public SharedPreferences sp ;
	private String psw;
	private String psw2;
	private EditText et_psw;
	private EditText et_psw2;
	private EditText et_enterpsw;
	private Dialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		
		sp = getSharedPreferences("config", MODE_PRIVATE);
		gv_list = (GridView) findViewById(R.id.gv_list);
		gv_list.setAdapter(new myAdapter());
		gv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				System.out.println("你点击了:"+position);
				Intent intent ;
				switch (position) {
				case SETTINGS:
					intent = new Intent(Home.this,Setting.class);
					startActivity(intent);
					break;
				case SAFE:
					String psw = sp.getString("password", null);
					if(TextUtils.isEmpty(psw))
					{
						//没有密码，弹出设置密码对话框
						showSetupPsw();
					}else
					{	//有密码，进入输入密码对话框
						showEnterPsw();
					}
					break;
				case ATOOLS:
					intent = new Intent(Home.this,Atools.class);
					Home.this.startActivity(intent);
					break;
				case CALLMSGSAFE:
					intent = new Intent(Home.this,CallMsgSafe.class);
					Home.this.startActivity(intent);
					break;
				case APP:
					intent = new Intent(Home.this,AppManager.class);
					Home.this.startActivity(intent);
					break;
				case TASKMANAGER:
					intent =  new Intent(Home.this,TaskManager.class);
					Home.this.startActivity(intent);
					break;
				case NETMANAGER:
					intent =  new Intent(Home.this,TrafficManager.class);
					Home.this.startActivity(intent);
					break;
				case SYSOPTIMIZE:
					intent =  new Intent(Home.this,ClearCache.class);
					Home.this.startActivity(intent);
					break;
				default:
					break;
				}
			}

			private void showEnterPsw() {
				AlertDialog.Builder builder = new Builder(Home.this);
				View view = View.inflate(Home.this, R.layout.safe_enter_customdialog, null);
				
				et_enterpsw = (EditText)view.findViewById(R.id.et_psw);
				Button btn_ok = (Button)view.findViewById(R.id.ok);
				Button btn_cancel = (Button)view.findViewById(R.id.cancel);
				
				btn_ok.setOnClickListener(new OnClickListener() {				
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String psw = et_enterpsw.getText().toString().trim();
						String config_psw = sp.getString("password", null);
						if(Md5ForPsw.addSecurity(psw).equals(config_psw))
						{
							//进入页面
							dialog.dismiss();
							Intent intent = new Intent(Home.this,Safe.class);
							startActivity(intent);
						}else
						{
							Toast.makeText(Home.this, "密码输入错误！请重新输入！", 0).show();
							et_enterpsw.setText("");
							return ;
						}
					}
				});
				btn_cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				//为保证在低版本上也能应用
//				AlertDialog alerdialog = builder.create();
//				alerdialog.setView(view, 0, 0, 0, 0);
//				alerdialog.show();
				builder.setView(view);
				dialog = builder.show();

			}

			private void showSetupPsw() {
				AlertDialog.Builder builder = new Builder(Home.this);
				View view = View.inflate(Home.this, R.layout.safe_setup_customdialog, null);
				et_psw = (EditText) view.findViewById(R.id.et_psw);
				et_psw2 = (EditText)view.findViewById(R.id.et_psw2);
				Button btn_ok = (Button)view.findViewById(R.id.ok);
				Button btn_cancel = (Button)view.findViewById(R.id.cancel);
							
				btn_ok.setOnClickListener(new OnClickListener() {					
					@Override
					public void onClick(View v) {
						
						psw = et_psw.getText().toString().trim();
						psw2 = et_psw2.getText().toString().trim();
						
						if(TextUtils.isEmpty(psw) || TextUtils.isEmpty(psw2))
						{
							Toast.makeText(Home.this, "密码不能为空！", 0).show();
						}else
						{
							if(psw.equals(psw2))
							{
								Editor editor = sp.edit();
								editor.putString("password", Md5ForPsw.addSecurity(psw));
								editor.commit();
								Toast.makeText(getApplicationContext(), "密码设置成功！", 0).show();
								dialog.dismiss();
								//进入safe页面
								Intent intent = new Intent(Home.this,Safe.class);
								startActivity(intent);
							}else
								Toast.makeText(getApplicationContext(), "两次密码输入不一致，请重新输入！", 0).show();
						}
						
					}
				});
				btn_cancel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				builder.setView(view);
				dialog = builder.show();
			}
		});
	}
	
	public class myAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return names.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(Home.this, R.layout.home_item, null);
			
			ImageView iv_item = (ImageView) view.findViewById(R.id.iv_item);
			TextView tv_item = (TextView) view.findViewById(R.id.tv_item);
			
			iv_item.setImageResource(ids[position]);
			tv_item.setText(names[position]);
			return view;
		}
		
	}
}
