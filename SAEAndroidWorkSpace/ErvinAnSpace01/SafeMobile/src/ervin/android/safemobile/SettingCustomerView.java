package ervin.android.safemobile;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingCustomerView extends RelativeLayout{

	private TextView tv_title;
	private TextView tv_content;
	private CheckBox cb_select;
	private String namespace = "http://schemas.android.com/apk/res/ervin.android.safemobile";
	
	private String update_on="";
	private String update_off="";

	public void initView(Context context)
	{
		System.out.println("一个参数的构造函数");
		View.inflate(context, R.layout.setting_customview, this);
		RelativeLayout rl = (RelativeLayout) this.findViewById(R.id.rl_view);
		tv_title = (TextView) this.findViewById(R.id.tv_title);
		tv_content = (TextView) this.findViewById(R.id.tv_content);
		cb_select= (CheckBox) this.findViewById(R.id.cb_select);
		
	}
	public SettingCustomerView(Context context, AttributeSet attrs, int defStyle) {
		
		super(context, attrs, defStyle);
		initView(context);
		System.out.println("三个参数的构造函数");
	}
	/**
	 * 当res有属性文件需要关联的时候，会调用这个2个参数的构造函数
	 * @param context
	 * @param attrs
	 */
	public SettingCustomerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
		System.out.println("二个参数的构造函数");
		String title = attrs.getAttributeValue(namespace, "title_name");
		update_on = attrs.getAttributeValue(namespace, "update_on");
		update_off = attrs.getAttributeValue(namespace, "update_off");	
		tv_title.setText(title);
		setDesc(update_off);
	}

	public SettingCustomerView(Context context) {
		super(context);
		
		initView(context);
	}

	//判断cb是否被选中
	public boolean isCheck()
	{
		return cb_select.isChecked();
	}
	//设置cb选中状态
	public void setSelect(boolean flag)
	{
		cb_select.setChecked(flag);
		if(isCheck())
		{			
			setDesc(update_on);
		}else
		{
			setDesc(update_off);
		}		
	}
	//给文本赋值
	public void setDesc(String text)
	{
		tv_content.setText(text);
	}
	
}
