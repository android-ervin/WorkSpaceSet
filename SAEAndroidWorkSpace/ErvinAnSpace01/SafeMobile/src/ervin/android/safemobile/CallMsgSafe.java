package ervin.android.safemobile;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.AlteredCharSequence;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import ervin.android.safemobile.db.BlackListDao;
import ervin.android.safemobile.db.BlackListInfo;

public class CallMsgSafe extends Activity {

	private ListView lv_blacklist;
	private BlackListDao bld;
	private List<BlackListInfo> blacklist;
	
	private EditText et_blacknum ;
	private CheckBox cb_phone;
	private CheckBox cb_sms;
	private callMsgSafeAdapter adapter ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.callmsgsafe);
		
		adapter = new callMsgSafeAdapter();
		bld = new BlackListDao(this);
		
		/**
		 * listview的优化
		 * 如果数据库中有大量的数据，或者数据取自互联网（很多）的时候，下面的操作需要开启一个子线程
		 * 可以让ListView使用分批，分页加载，一次加载20条数据，当拖动到最低部的时候再加载20条数据的方式
		 */	
		blacklist = bld.dbFindAll();		
		lv_blacklist = (ListView) findViewById(R.id.lv_blacklist);		
		lv_blacklist.setAdapter(adapter);
		
		/**
		 * 给ListView设置监听条目点击事件，用来更新黑名单的拦截模式
		 */
		lv_blacklist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				// 更新黑名单
				//Toast.makeText(CallMsgSafe.this, "view被点击了", 0).show();
				AlertDialog.Builder builder = new Builder(CallMsgSafe.this);
				final AlertDialog dialog =builder.create();
				final View addblacknum_view = View.inflate(CallMsgSafe.this, R.layout.callmsgsafe_updateblacknum_customdialog, null);
				et_blacknum = (EditText) addblacknum_view.findViewById(R.id.et_blacknum);
				cb_phone = (CheckBox)addblacknum_view.findViewById(R.id.cb_phone);
				cb_sms = (CheckBox) addblacknum_view.findViewById(R.id.cb_sms);
				Button btn_ok = (Button)addblacknum_view.findViewById(R.id.ok);
				Button btn_cancel = (Button)addblacknum_view.findViewById(R.id.cancel);
				
				et_blacknum.setText(blacklist.get(position).getName());
				et_blacknum.setEnabled(false);
				btn_ok.setOnClickListener(new OnClickListener() {					
					@Override
					public void onClick(View v) {
						
						String mode = "3";
						if(cb_phone.isChecked() && !cb_sms.isChecked())
							mode = "1";
						else if(!cb_phone.isChecked() && cb_sms.isChecked())
							mode = "2";
						else if(cb_phone.isChecked() && cb_sms.isChecked())
							mode="3";
						else if(!cb_phone.isChecked() && !cb_sms.isChecked())
						{
							Toast.makeText(getApplicationContext(), "拦截模式不能为空", 0).show();
							return;
						}
						//更新数据库
						bld.dbUpdate(blacklist.get(position).getName(), mode);
						BlackListInfo info = new BlackListInfo();
						info.setName(blacklist.get(position).getName());
						info.setMode(mode);
						blacklist.set(position, info);
						
						adapter.notifyDataSetChanged();
						dialog.dismiss();
					}
				});
				btn_cancel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
			
				dialog.setView(addblacknum_view, 0, 0, 0, 0);
				dialog.show();
				
			}
		});
	}
	
	private class callMsgSafeAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return blacklist.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		/**
		 * 对listview做优化(标准做法)
		 * 1，重复利用回收的view对象，不要每次都调用view.inflate方法（该方法很消耗资源）
		 * 2，不要重复去findviewbyid
		 * --其中convertView是保存了上一个离开屏幕的item的物理地址，可以拿着它去存放将要出现的item，这样就可以一直循环
		 * 内存中只有刚开始由view.inflate所创建的8个item view对象了。
		 * --当控件刚被创建的时候，找到控件并且保存他们，当需要他们的时候可以直接去引用，而不用再去父控件(view)中
		 * findviewbyId查找了。
		 * 
		 */
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder;		
			View view ;
			if(convertView==null)
			{
				view = View.inflate(CallMsgSafe.this, R.layout.callmsgsafe_blacklist_item, null);
				holder = new ViewHolder();
				holder.tv_number = (TextView) view.findViewById(R.id.tv_number);
				holder.tv_mode = (TextView) view.findViewById(R.id.tv_mode);
				holder.iv_delete = (ImageView) view.findViewById(R.id.iv_delete);
				view.setTag(holder);
			}else
			{
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}
			
			//给iv_delete添加点击事件，如果iv_delete没有添加点击事件，将在view中处理用户的点击事件
			holder.iv_delete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AlertDialog.Builder builder = new Builder(CallMsgSafe.this);
					builder.setTitle("提醒");
					builder.setMessage("是否要删除这条数据？");
					builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {					
						@Override
						public void onClick(DialogInterface dialog, int which) {
							bld.dbDelete(blacklist.get(position).getName()); // 删除指定条目的数据库中的数据
							//更新数据集合balcklist
							blacklist.remove(position); // 对应在ListView中也不会显示了
							adapter.notifyDataSetChanged();//通知ListView更新							
						}
					});
					builder.setNegativeButton("取消", null);
					builder.show();
					
				}
			});
			//给控件设置值
			holder.tv_number.setText(blacklist.get(position).getName().toString());
			String mode = blacklist.get(position).getMode().toString();
			if(mode.equals("1"))
				holder.tv_mode.setText("电话拦截");
			else if(mode.equals("2"))
				holder.tv_mode.setText("短信拦截");
			else if(mode.equals("3"))
				holder.tv_mode.setText("全部拦截");
			return view;
		}
		
	}
	class ViewHolder{
		TextView tv_number;
		TextView tv_mode ;
		ImageView iv_delete;
	}
	
	/**
	 * 按钮点击事件，用来增加黑名单
	 * @param view
	 */
	public void addBlackNum(View view)
	{
		AlertDialog.Builder builder = new Builder(CallMsgSafe.this);
		final AlertDialog dialog =builder.create();
		final View addblacknum_view = View.inflate(CallMsgSafe.this, R.layout.callmsgsafe_addblacknum_customdialog, null);
		et_blacknum = (EditText) addblacknum_view.findViewById(R.id.et_blacknum);
		cb_phone = (CheckBox)addblacknum_view.findViewById(R.id.cb_phone);
		cb_sms = (CheckBox) addblacknum_view.findViewById(R.id.cb_sms);
		Button btn_ok = (Button)addblacknum_view.findViewById(R.id.ok);
		Button btn_cancel = (Button)addblacknum_view.findViewById(R.id.cancel);
		
		
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String number = et_blacknum.getText().toString().trim();
				String mode = "3";
				if(cb_phone.isChecked() && !cb_sms.isChecked())
					mode = "1";
				else if(!cb_phone.isChecked() && cb_sms.isChecked())
					mode = "2";
				else if(cb_phone.isChecked() && cb_sms.isChecked())
					mode="3";
				else if(!cb_phone.isChecked() && !cb_sms.isChecked())
				{
					Toast.makeText(getApplicationContext(), "拦截模式不能为空", 0).show();
					return;
				}
				if(TextUtils.isEmpty(number))
				{
					Toast.makeText(getApplicationContext(), "电话号码不能为空", 0).show();
					//输入框抖动效果（APIDemo）
					Animation shake =  AnimationUtils.loadAnimation(CallMsgSafe.this, R.anim.shake);
					et_blacknum.setAnimation(shake);
					return;
				}else{
					int result = bld.dbAdd(number, mode);
					switch (result) {
					case 1:
						Toast.makeText(getApplicationContext(), "设置成功！", 0).show();
						dialog.dismiss();
						//更新ListView中的数据blacklist
						BlackListInfo info = new BlackListInfo();
						info.setName(number);
						info.setMode(mode);
						blacklist.add(0,info);//在开始的位置添加新插入的num，显示在最上面
						//适配器通知listview数据改变了
						adapter.notifyDataSetChanged();
						break;
					case -1:
						Toast.makeText(getApplicationContext(), "设置失败！", 0).show();
						dialog.dismiss();
						break;
					case 0:
						Toast.makeText(getApplicationContext(), "已经设置过该号码", 0).show();
						dialog.dismiss();
						break;
					default:
						break;
					}
					
				}				
			}
		});	
		btn_cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	
		dialog.setView(addblacknum_view, 0, 0, 0, 0);
		dialog.show();
	}
	
}
