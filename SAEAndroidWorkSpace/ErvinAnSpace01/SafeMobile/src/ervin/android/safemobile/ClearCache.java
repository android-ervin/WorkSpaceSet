package ervin.android.safemobile;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import android.app.Activity;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageStats;
import android.os.Bundle;
import android.os.RemoteException;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ClearCache extends Activity {

	private TextView tv_cache;
	private ProgressBar pb_cache;
	
	private PackageManager pm ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.clearcache);
		
		pb_cache = (ProgressBar) findViewById(R.id.pb_cache);
		tv_cache = (TextView) findViewById(R.id.tv_cache);
		scanCache();
	}

	private void scanCache() {
		pm = getPackageManager();
		new Thread() {
			public void run() {

				List<PackageInfo> packageInfos = pm.getInstalledPackages(0);

				Method getPackageSizeInfoMethod = null;
				Method[] methods = PackageManager.class.getMethods();// 获得所有PackageManager中的方法包括隐藏的方法

				pb_cache.setMax(packageInfos.size());
				int progress = 0;
				for (Method method : methods) {
					if ("getPackageSizeInfo".equals(method.getName())) {
						getPackageSizeInfoMethod = method;
					}
				}
				for (PackageInfo packageInfo : packageInfos) {
					String packagename = packageInfo.packageName;
					/**
					 * receiver:(谁调用该方法) the object on which to call this method
					 * (or null for static methods) args : （该方法的参数） the
					 * arguments to the method
					 */
					try {
						getPackageSizeInfoMethod.invoke(pm,packagename,new myDataObserver());
						Thread.sleep(50);
						// Method method =
						// PackageManager.class.getDeclaredMethod("getPackageSizeInfo",
						// new Class[]{String.class,Object.class}) ;
						// System.out.println("方法为："+method.toString());
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					progress++;
					pb_cache.setProgress(progress);
				}
			};
		}.start();
	}
	class myDataObserver extends IPackageStatsObserver.Stub
	{
		@Override
		public void onGetStatsCompleted(PackageStats pStats,
				boolean succeeded) throws RemoteException {
			// TODO Auto-generated method stub
			long cachesize = pStats.cacheSize;
			long codesize = pStats.codeSize;
			long datasize = pStats.dataSize;
			
			final String packagename = pStats.packageName;
			runOnUiThread(new Runnable() {
				public void run() {
					try {
						tv_cache.setText("正在扫描："+pm.getApplicationInfo(packagename, 0).loadLabel(pm));
						
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
		}
		
	}

}
