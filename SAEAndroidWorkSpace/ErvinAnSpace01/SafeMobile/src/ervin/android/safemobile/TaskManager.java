package ervin.android.safemobile;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import ervin.android.safemobile.db.Taskinfo;
import ervin.android.safemobile.subactivity.TaskSetting;
import ervin.android.safemobile.utils.SystemInfoUtils;
import ervin.android.safemobile.utils.TaskinfoUtils;

public class TaskManager extends Activity {
	
	private TextView tv_process_count;
	private TextView tv_avil_mem;
	private ListView lv_tasklist;
	private LinearLayout ll_load;
	private TextView tv_status;
	private CheckBox cb_status;
	
	private List<Taskinfo> taskinfos;     //总的任务进程列表
	private List<Taskinfo> usertaskinfos; //用户进程列表
	private List<Taskinfo> systaskinfos;  //系统进程列表
	private Taskinfo taskinfo_item;   //被点击的条目
	
	private TaskAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.taskmanager);
		
		tv_process_count = (TextView) findViewById(R.id.tv_process_count);
		tv_avil_mem = (TextView) findViewById(R.id.tv_avil_mem);
		lv_tasklist = (ListView) findViewById(R.id.lv_tasklist);
		ll_load = (LinearLayout) findViewById(R.id.ll_load);
		tv_status = (TextView) findViewById(R.id.tv_status);
		cb_status = (CheckBox) findViewById(R.id.cb_status);
		
		
		
		//lv_tasklist.setAdapter(adapter);
		fillData();
		//点击事件
		lv_tasklist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(position==0 || position == usertaskinfos.size()+1)
				{
					return;
				}
				if(position <= usertaskinfos.size())
				{//点击了用户进程
					int newposition = position-1;
					taskinfo_item = usertaskinfos.get(newposition);
				}else
				{
					int newposition = position - usertaskinfos.size()-2;
					taskinfo_item = systaskinfos.get(newposition);
				}

				HolderView holder = (HolderView) view.getTag();//也可以用findviewbyid
				if (taskinfo_item.isIschecked()) {
					taskinfo_item.setIschecked(false);
					holder.cb_status.setChecked(false);
				} else {
					taskinfo_item.setIschecked(true);
					holder.cb_status.setChecked(true);
				}				
			}
		});
		
		//滑动事件
		lv_tasklist.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}		
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (usertaskinfos != null && systaskinfos != null) { //会报空指针异常
					if (firstVisibleItem < usertaskinfos.size()+1) {
						tv_status.setText(" 用户进程数：" + usertaskinfos.size()
								+ "个");
					} else
						tv_status.setText(" 系统进程数：" + systaskinfos.size() + "个");
				}
			}
		});
	}

	private void setTitle() {
		int processcount = SystemInfoUtils.getRunningProcessCount(this);
		tv_process_count.setText("正在运行的应用程序个数："+processcount+"个");
		long avialmem = SystemInfoUtils.getAvailMem(this);
		long totalmem = SystemInfoUtils.getTotalMem(this);	
		tv_avil_mem.setText("剩余内存/总内存: "+Formatter.formatFileSize(this, avialmem)+"/"+
				Formatter.formatFileSize(this, totalmem));
	}

	private void fillData() {
		ll_load.setVisibility(View.VISIBLE);
		new Thread(){
			public void run() {
				taskinfos = TaskinfoUtils.getTaskInfo(getApplicationContext());
				usertaskinfos = new ArrayList<Taskinfo>();
				systaskinfos = new ArrayList<Taskinfo>();
				
				for(Taskinfo taskinfo :taskinfos)
				{
					if(taskinfo.isIsuser())
					{//用户进程
						usertaskinfos.add(taskinfo);
					}else{
						systaskinfos.add(taskinfo);
					}	
				}
				runOnUiThread(new  Runnable() {
					public void run() {
						if(adapter==null)
						{
							adapter = new TaskAdapter();
							lv_tasklist.setAdapter(adapter);
						}else
							adapter.notifyDataSetChanged();					
						ll_load.setVisibility(View.INVISIBLE);
						setTitle();
					}
				});
			};
		}.start();	
	}

	class TaskAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// 是否显示系统进程
			SharedPreferences sp = getSharedPreferences("config", MODE_PRIVATE);
			if(sp.getBoolean("showsystem", false)){
				return taskinfos.size()+2;
			}else
				return usertaskinfos.size()+1;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(position==0)
			{
				TextView tv = new TextView(getApplicationContext());
				tv.setText(" 用户进程数："+usertaskinfos.size()+"个");
				tv.setBackgroundColor(Color.GRAY);
				tv.setTextColor(Color.WHITE);
				tv.setTextSize(16);
				return tv;
			}
			if(position == usertaskinfos.size()+1)
			{
				TextView tv = new TextView(getApplicationContext());
				tv.setText(" 系统进程数："+systaskinfos.size()+"个");
				tv.setBackgroundColor(Color.GRAY);
				tv.setTextColor(Color.WHITE);
				tv.setTextSize(16);
				return tv;
			}
			if(position <= usertaskinfos.size())
			{//显示用户进程
				int newposition = position-1;
				taskinfo_item = usertaskinfos.get(newposition);
			}else
			{
				int newposition = position - usertaskinfos.size()-2;
				taskinfo_item = systaskinfos.get(newposition);
			}
			View view;
			HolderView holder;
			if(convertView!=null && convertView instanceof RelativeLayout)
			{
				view = convertView;
				holder = (HolderView) view.getTag();
			}else
			{
				view = View.inflate(getApplicationContext(), R.layout.taskmanager_taskinfo_item, null);
				holder = new HolderView();
				holder.tv_lablename = (TextView) view.findViewById(R.id.tv_task_lablename);
				holder.tv_memsize = (TextView) view.findViewById(R.id.tv_task_memo);
				holder.iv_icon = (ImageView) view.findViewById(R.id.iv_icon);
				holder.cb_status = (CheckBox) view.findViewById(R.id.cb_status);
				view.setTag(holder);
			}
			
			holder.tv_lablename.setText(taskinfo_item.getLablename());
			String memo = Formatter.formatFileSize(getApplicationContext(), taskinfo_item.getMemsize());
			holder.tv_memsize.setText("占用内存："+memo);
			holder.iv_icon.setImageDrawable(taskinfo_item.getIcon());
			holder.cb_status.setChecked(taskinfo_item.isIschecked());
			return view;
		}
		
	}
	class HolderView 
	{
		private TextView tv_lablename;
		private TextView tv_memsize;
		private ImageView iv_icon;
		private CheckBox cb_status;
	}
	
	/**
	 * 按钮的点击事件
	 */
	
	public void selectAll(View view)
	{
		for(Taskinfo taskinfo : taskinfos)
		{
			taskinfo.setIschecked(true);
		}
		adapter.notifyDataSetChanged();
	}
	
	public void selectOp(View view)
	{
		for(Taskinfo taskinfo:taskinfos)
		{
			if(taskinfo.isIschecked())
				taskinfo.setIschecked(false);
			else
				taskinfo.setIschecked(true);
		}
		adapter.notifyDataSetChanged();
	}
	public void clean(View view)
	{
		ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		int count = 0;
		long savemem =0;
		for(Taskinfo taskinfo : taskinfos)
		{
			if(taskinfo.isIschecked())
			{
				count++;
				am.killBackgroundProcesses(taskinfo.getPackagename());//kill进程
				savemem += taskinfo.getMemsize();
//				//更新界面
//				if(taskinfo.isIsuser())
//				{
//					usertaskinfos.remove(taskinfo);
//				}else
//				{
//					systaskinfos.remove(taskinfo);
//				}	
				
			}				
		}
		
		fillData();
		Toast.makeText(this, "杀死了"+count+"个进程，释放了"+Formatter.formatFileSize(this, savemem)
				+"的内存", 1).show();
			
	}
	
	public void setting(View view){
		Intent intent = new Intent(this,TaskSetting.class);
		startActivityForResult(intent, 0);//返回时，刷新listview界面
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		adapter.notifyDataSetChanged();
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public class ScreenOnReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent) {
			// 解锁屏幕后，更新ListView
			System.out.println("屏幕解锁了。。");
			//fillData();
			//adapter.notifyDataSetChanged();
		}
		
	}
}
