package ervin.android.safemobile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.text.format.Formatter;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import ervin.android.safemobile.db.Appinfo;
import ervin.android.safemobile.utils.AppInfoUtils;
import ervin.android.safemobile.utils.DensityUtils;

public class AppManager extends Activity implements OnClickListener {

	private TextView tv_sd_avilable;
	private TextView tv_phone_avilabel;
	private LinearLayout ll_load;
	private ListView lv_applist;
	public static TextView tv_scaning; // 未实现的功能
	StautsHandler handler;
	private TextView tv_status;  //用户程序和系统程序固定显示标签

	private List<Appinfo> appinfos; // 整个应用程序信息
	private List<Appinfo> userappinfos; // 用户应用程序信息
	private List<Appinfo> systemappinfos; // 系统应用程序信息
	private ApplistAdapter adapter;
	
	private Appinfo appinfo_item;  //listview中点击的条目
	private PopupWindow pp ;//弹出的popwindow
	
	private LinearLayout ll_open;
	private LinearLayout ll_uninstall;
	private LinearLayout ll_share;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appmanager);

		tv_sd_avilable = (TextView) findViewById(R.id.tv_sd_avilable);
		tv_phone_avilabel = (TextView) findViewById(R.id.tv_phone_avilable);
		ll_load = (LinearLayout) findViewById(R.id.ll_load);
		lv_applist = (ListView) findViewById(R.id.lv_applist);
		tv_scaning = (TextView) findViewById(R.id.tv_scaning);
		tv_status = (TextView) findViewById(R.id.tv_status);
		handler = new StautsHandler();

		String sd_text = getAvialableSpace(Environment
				.getExternalStorageDirectory().getAbsolutePath());
		String phone_text = getAvialableSpace(Environment.getDataDirectory()
				.getAbsolutePath());

		tv_sd_avilable.setText("SD卡可用空间: " + sd_text);
		tv_phone_avilabel.setText("手机可用空间：" + phone_text);

		
		fillData();
		/**
		 *  给listview添加拖动事件
		 */
		lv_applist.setOnScrollListener(new OnScrollListener() {

			// 正在被拖动
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				//System.out.println("list正在被拖动。。。");
			}

			// 已经被拖动了
			// firstVisibleItem： the index of the first visible
			// cell(第一个可见的item在ListView中的位置)
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				//System.out.println("list已经被拖动了。。。");
				popDismiss();
				if (userappinfos != null && systemappinfos != null) {
					if (firstVisibleItem > userappinfos.size()) // 表示已经拖动到了系统程序了
					{
						tv_status.setText("  系统应用：" + systemappinfos.size()
								+ "个");
					} else
						tv_status.setText("  用户应用：" + userappinfos.size() + "个");
				}
			}
		});
		
		/**
		 * 给listview添加条目点击事件，弹出popwindow
		 */
		lv_applist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(position==0||position==userappinfos.size()+1)//表示点击了标签
				{
					return;
				}else if(position<=userappinfos.size()) //表示点击的是用户程序的条目
				{
					int newposition = position - 1;//当前ListView的位置减去标签占用的一个位置
					appinfo_item = userappinfos.get(newposition);
					//System.out.println("点击了用户"+appinfo.getLabelname());
					//显示pupwindow
				}else  //表示点击了系统用户的条目
				{
					int newposition = position-1-userappinfos.size()-1;
					appinfo_item = systemappinfos.get(newposition);
					//System.out.println("点击了系统"+appinfo.getLabelname());
				}
				
				popDismiss();
				View contentView = View.inflate(getApplicationContext(), R.layout.appmanager_appinfo_dialog, null);
				//设置popup宽高包裹内容
				pp = new PopupWindow(contentView, ViewGroup.LayoutParams.WRAP_CONTENT, -2);		
				//popup如果要想显示动画必须指定背景（规定）
				pp.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				//当前点击的条目view在窗体的位置（和屏幕位置的区别）
				int[] location = new int[2];
				view.getLocationInWindow(location);
				//程序中的指定宽高单位都是sp（像素），如果要保证在各种分辨率下效果相同，则要把sp转化为dip
				int px = 60;
				int dip = DensityUtils.px2dip(getApplicationContext(), px);
				//根据点击的条目view的位置来设置popup显示在窗口的位置
				pp.showAtLocation(parent, Gravity.TOP|Gravity.LEFT, location[0]+dip, location[1]-15);
				//设置动画
				ScaleAnimation sa = new ScaleAnimation(0.3f, 1.0f, 0.3f, 1.0f, Animation.RELATIVE_TO_SELF, 0,
						Animation.RELATIVE_TO_SELF, 0.5f);
				sa.setDuration(300);
				AlphaAnimation aa = new AlphaAnimation(0.1f, 1.0f);
				aa.setDuration(300);
				AnimationSet set = new AnimationSet(false);//false if each animation should use its own interpolator.
				set.addAnimation(sa);
				set.addAnimation(aa);
				contentView.startAnimation(set);
				
				//找到三个view并且设置点击事件
				ll_open = (LinearLayout) contentView.findViewById(R.id.ll_open);
				ll_uninstall = (LinearLayout) contentView.findViewById(R.id.ll_uninstall);
				ll_share = (LinearLayout) contentView.findViewById(R.id.ll_share);
				ll_open.setOnClickListener(AppManager.this);
				ll_uninstall.setOnClickListener(AppManager.this);
				ll_share.setOnClickListener(AppManager.this);
			}

			
		});
	}
	class StautsHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			String name = (String) msg.obj;
			tv_scaning.setText("当前扫描："+name);
			super.handleMessage(msg);
		}
		
	}

	private void fillData() {
		ll_load.setVisibility(View.VISIBLE);
		new Thread() {
			public void run() {
				appinfos = AppInfoUtils.getAllAppInfo(AppManager.this,handler); // 耗时操作，开线程
				userappinfos = new ArrayList<Appinfo>();
				systemappinfos = new ArrayList<Appinfo>();
				for (Appinfo appinfo : appinfos) {
					if (appinfo.isSystem())
						systemappinfos.add(appinfo);
					else
						userappinfos.add(appinfo);
				}
				// 通知listview数据适配器 UI操作
				runOnUiThread(new Runnable() {
					public void run() {
						if (adapter == null) {						
							adapter = new ApplistAdapter();
							lv_applist.setAdapter(adapter);//数据第一次加载时需要设置适配器
						}else{
							adapter.notifyDataSetChanged();//数据更新时，通知就行，不要再加载适配器了
						}
						ll_load.setVisibility(View.INVISIBLE);
					}
				});
			};
		}.start();
	}

	public class ApplistAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			// return appinfos.size();
			return userappinfos.size() + systemappinfos.size() + 2;// 总条数加上两个标签数
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Appinfo appinfo;
			if (position == 0) {
				// 添加一个标签（用户应用数量）
				TextView tv_user = new TextView(getApplicationContext());
				tv_user.setText("  用户应用：" + userappinfos.size() + "个");
				tv_user.setTextColor(Color.WHITE);
				tv_user.setBackgroundColor(Color.GRAY);
				tv_user.setTextSize(16);
				return tv_user;
			}
			if (position == userappinfos.size() + 1) {
				// 添加系统应用的标签
				TextView tv_sys = new TextView(getApplicationContext());
				tv_sys.setText("  系统应用：" + systemappinfos.size() + "个");
				tv_sys.setTextColor(Color.WHITE);
				tv_sys.setBackgroundColor(Color.GRAY);
				tv_sys.setTextSize(16);
				return tv_sys;
			}
			if (position <= userappinfos.size()) // 装载用户应用
			{
				int userposition = position - 1;
				appinfo = userappinfos.get(userposition);
			} else // 装载系统应用
			{
				// 系统应用第一个元素的位置应该是当前listview的位置-两个标签-用户应用的个数
				int sysposition = position - userappinfos.size() - 2;
				appinfo = systemappinfos.get(sysposition);
			}
			
			/**
			 * 优化ListView
			 */
			View view;
			ViewHolder holder;
			if (convertView != null && convertView instanceof RelativeLayout) { // 判断convertView是否为空，并且是否可以被下一个元素复用
				view = convertView;
				holder = (ViewHolder) view.getTag();
			} else {
				view = View.inflate(AppManager.this,
						R.layout.appmanager_appinfo_item, null);
				holder = new ViewHolder();
				holder.image_icon = (ImageView) view.findViewById(R.id.iv_icon);
				holder.tv_lablename = (TextView) view
						.findViewById(R.id.tv_lablename);
				holder.tv_localname = (TextView) view
						.findViewById(R.id.tv_localname);

				view.setTag(holder);
			}

			// 绑定数据
			// Appinfo appinfo = appinfos.get(position);
			holder.image_icon.setImageDrawable(appinfo.getIcon());
			holder.tv_lablename.setText(appinfo.getLabelname());
			if (appinfo.isRom()) {
				holder.tv_localname.setText("手机内存");
			} else
				holder.tv_localname.setText("外部存储");
			return view;
		}
	}

	public class ViewHolder {
		private ImageView image_icon;
		private TextView tv_lablename;
		private TextView tv_localname;
	}

	/**
	 * 根据路径获得可用空间
	 * 
	 * @param path
	 */
	private String getAvialableSpace(String path) {
		StatFs staf = new StatFs(path);
		int totalBlock = staf.getBlockCount();// The total number of blocks on
												// the file system
		int availBlock = staf.getAvailableBlocks();// The number of blocks that
													// are free on the file
													// system and available to
													// applications
		int blockSize = staf.getBlockSize();// The size, in bytes, of a block on
											// the file system每一个block的大小

		long totalspace = totalBlock * blockSize;
		long freespace = availBlock * blockSize;

		System.out.println("可用" + Formatter.formatFileSize(this, freespace));
		System.out.println("一共" + Formatter.formatFileSize(this, totalspace));

		return Formatter.formatFileSize(this, freespace) + "/"
				+ Formatter.formatFileSize(this, totalspace);
		// return size2string(freespace)+"/"+size2string(totalspace);
	}

	// 格式化字符串
	private String size2string(long size) {
		DecimalFormat df = new DecimalFormat("0.00");
		String mysize = "";
		if (size > 1024 * 1024 * 1024) {
			mysize = df.format(size / 1024f / 1024f / 1024f) + "G";
		} else if (size > 1024 * 1024) {
			mysize = df.format(size / 1024f / 1024f) + "M";
		} else if (size > 1024) {
			mysize = df.format(size / 1024f) + "K";
		} else {
			mysize = size + "B";
		}
		return mysize;
	}
	
	@Override
	protected void onDestroy() {
		popDismiss();
		super.onDestroy();
	}
	
	private void popDismiss() {
		if(pp!=null&&pp.isShowing())
		{
			pp.dismiss();
			pp = null;
		}
	}

	@Override
	public void onClick(View v) {
		popDismiss();
		switch (v.getId()) {
		case R.id.ll_open:
			openActivity();
			break;
		case R.id.ll_uninstall:
			if(appinfo_item.isSystem())
			{
				Toast.makeText(this, "系统应用，不能卸载", 0).show();
				return;
			}
			uninstallApp();
			break;
		case R.id.ll_share:
			shareApp();
			break;
		}
	}

	private void shareApp() {
		Intent intent = new Intent();
		intent.setAction("android.intent.action.SEND");
		intent.addCategory("android.intent.category.DEFAULT");
		intent.setType("text/plain");
		intent.putExtra(intent.EXTRA_TEXT, "我正在用一款："+appinfo_item.getLabelname()+" 的应用，很好用，推荐你也来用！");
		startActivity(intent);
		
	}

	//卸载APP
	private void uninstallApp() {
		// splash界面中有安装应用程序的
		Intent intent = new Intent();
		intent.setAction("android.intent.action.VIEW");
		intent.setAction("android.intent.action.DELETE");
		intent.addCategory("android.intent.category.DEFAULT");
		intent.setData(Uri.parse("package:"+appinfo_item.getPackagename()));
		startActivityForResult(intent, 0);
		//startActivity(intent);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 卸载完成后回到ListView界面，通知界面更新
			//因为数据已经保存在了appinfos集合里面了，要更新ListView只能是重新获取appinfos了
//		adapter.notifyDataSetChanged();
//		lv_applist.invalidate();
		
		fillData(); //重新加载数据
		
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void openActivity() {
		PackageManager pm = getPackageManager();
//		Intent intent = new Intent();
//		//开启每个app中的启动Activity
//		intent.setAction("android.intent.action.MAIN");
//		intent.addCategory("android.intent.category.LAUNCHER");
//		//查询出手机中所有有启动界面的activity
//		List<ResolveInfo> infos = pm.queryIntentActivities(intent, PackageManager.GET_INTENT_FILTERS);
		
		Intent intent = pm.getLaunchIntentForPackage(appinfo_item.getPackagename());//根据包名找到启动的应用程序Activity
		if(intent!=null) //有些程序没有启动页面Intent为空
		{
			startActivity(intent);
		}else
			Toast.makeText(this, "不能启动该应用", 0).show();
	}
}
