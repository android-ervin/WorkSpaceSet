package ervin.android.safemobile;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;
import ervin.android.safemobile.subactivity.SafeGuide1;

public class Safe extends Activity{

	
	private SharedPreferences sp;
	private TextView tv_safenum;
	private ToggleButton tb_isprotect;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
		sp = getSharedPreferences("config", MODE_PRIVATE);
					
		Boolean safeguide = sp.getBoolean("safeguide", false);
		if(safeguide)
		{
			//已经用过引导页面了，直接进入safe页面
			setContentView(R.layout.safe);
			
			tv_safenum = (TextView) findViewById(R.id.tv_safenum);
			tb_isprotect = (ToggleButton) findViewById(R.id.tb_isprotect);
			//是否设置了安全号码
			String safenum = sp.getString("safenumber", "");
			if(TextUtils.isEmpty(safenum))
			{
				tv_safenum.setText("");
				tb_isprotect.setChecked(false);
				tb_isprotect.setEnabled(false);
			}
			else{
				tv_safenum.setText(safenum);
				tb_isprotect.setChecked(true);
				tb_isprotect.setEnabled(false);
			}
		}else{
			//进入引导页面
			Intent intent = new Intent(Safe.this,SafeGuide1.class);
			startActivity(intent);
			finish();
		}

	}
	
	//重新进入引导页面
	public void enterGuide(View view)
	{
		Intent intent = new Intent(Safe.this,SafeGuide1.class);
		startActivity(intent);
		finish();
	}
}
