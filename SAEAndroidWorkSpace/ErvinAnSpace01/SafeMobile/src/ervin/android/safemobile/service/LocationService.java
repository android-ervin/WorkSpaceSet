package ervin.android.safemobile.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class LocationService extends Service {

	private SharedPreferences sp;
	private LocationManager lm ;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		sp = getSharedPreferences("config", MODE_PRIVATE);
		// 获得系统位置管理服务
		lm = (LocationManager)getSystemService(LOCATION_SERVICE);
		super.onCreate();
	}
	//服务被onCreat()（只被调用一次）后调用该方法
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		//List<String> providers = lm.getAllProviders();//获取所有可用的定位服务，一般有三种，网络，基站，GPS
		//自动选择三个定位方法中最好的方式
		Criteria criteria  = new Criteria();//给provider设置条件
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		String provider = lm.getBestProvider(criteria, true);
		lm.requestLocationUpdates(provider, 1*60*1000, 50, new LocationListener() {
			//当GPS状态改变的时候调用，GPS从开--->关，从关--->开
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "GPS状态改变了", 1).show();
			}
			//定位服务可用时，网络，基站，GPS至少有一个可用时调用
			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}
			//定位服务不可用是调用
			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				
			}
			//位置发生改变时调用
			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				String longitude = "lng:"+location.getLongitude();
				String lattitude = "lat:"+location.getLatitude();
				String accuracy = "acu"+location.getAccuracy();
				
				//将坐标纠偏（省略）
				
				//将最后一次的改变的坐标保存
				Editor editor = sp.edit();
				editor.putString("location", longitude+","+lattitude+","+accuracy);
				editor.commit();	
			}
		});
		return super.onStartCommand(intent, flags, startId);
	}
	@Override
	public void onDestroy() {
		//取消监听
		
		super.onDestroy();
	}
	
}
