package ervin.android.safemobile.service;

import ervin.android.safemobile.receiver.OutGoingCallReceiver;
import ervin.android.safemobile.utils.CustomToast;
import ervin.android.safemobile.utils.QueryPhoneAddressUtils;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class AddressService extends Service{

	private TelephonyManager tm ;
	private myPhoneListener listener;
	
	private OutGoingCallReceiver ogcReceiver;
	//自定义Toast
	private CustomToast CToast ; 
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		
		CToast = CustomToast.getInstance();
		// 得到系统电话管理服务 
		tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		listener = new myPhoneListener();
		//在服务启动的时候，开启来电监听
		tm.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
		
		/**
		 * 使接收系统去电的广播器与号码归属地服务有相同的生命周期
		 */
		//在服务开启的时候，注册去电广播接收者，当有系统有电话呼出时，去接收系统所发出的广播
		ogcReceiver = new OutGoingCallReceiver();
		IntentFilter filter = new IntentFilter();//设置接收过滤器，接收指定的广播	
		filter.addAction("android.intent.action.NEW_OUTGOING_CALL");
		this.registerReceiver(ogcReceiver, filter);
		super.onCreate();
	}
	private class myPhoneListener extends PhoneStateListener
	{
		//当呼叫状态发生改变的时候回调该方法
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			switch(state)
			{
				case TelephonyManager.CALL_STATE_RINGING:  //响铃
				{
					String address = QueryPhoneAddressUtils.QueryAddressByPhone(incomingNumber);
					//Toast.makeText(getApplicationContext(), address, 1).show();
					//使用自定义Toast
					CToast.showCustomToast(getApplicationContext(),address);
					break;
				}
				case TelephonyManager.CALL_STATE_OFFHOOK: //接通
				{
//					if(CToast!=null)
//						CToast.dismissCustomToast();
					break;
				}
				case TelephonyManager.CALL_STATE_IDLE: //当挂断，空闲的时候
				{
					CToast.dismissCustomToast();
					break;
				}
			}
			super.onCallStateChanged(state, incomingNumber);
		}		
	}
	
	@Override
	public void onDestroy() {
		//取消监听
		tm.listen(listener, PhoneStateListener.LISTEN_NONE);
		listener = null;
		
		//注销广播接收器
		this.unregisterReceiver(ogcReceiver);
		ogcReceiver = null;
		super.onDestroy();
		
		CToast = null;
		//CToast.dismissCustomToast();
	}
}
