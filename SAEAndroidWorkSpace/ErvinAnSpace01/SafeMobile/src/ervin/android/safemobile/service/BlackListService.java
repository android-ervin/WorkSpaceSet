package ervin.android.safemobile.service;

import java.lang.reflect.Method;

import com.android.internal.telephony.ITelephony;

import ervin.android.safemobile.db.BlackListDao;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CallLog;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;

public class BlackListService extends Service {

	/**
	 * 在服务中开启或者关闭 来电，来短信的广播，这样会使服务和广播具有相同的生命周期，便于设置
	 */
	private BlackListDao bld ;
	private BlackListSMS smsReceiver;
	
	private TelephonyManager tm; //电话管理器---》Ibinder
	private PhoneListen listener;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	private class BlackListSMS extends BroadcastReceiver
	{
		/**
		 * 当接收到系统广播 短信到来时调用该方法
		 */
		@Override
		public void onReceive(Context context, Intent intent) {
			//获得短信体的标准代码
			Object objs[] = (Object[]) intent.getExtras().get("pdus");
			for(Object obj : objs)
			{
				SmsMessage sms = SmsMessage.createFromPdu((byte[]) obj); //获得短信了
				String sender = sms.getOriginatingAddress();//获得发件号码
				//判断是否在黑名单中，模式是2或者3
				String mode = bld.dbFindMode(sender);
				if("2".equals(mode)||"3".equals(mode))
				{
					this.abortBroadcast();  //拦截短信
				}
			}
		}
		
	}
	private class PhoneListen extends PhoneStateListener
	{
		//电话状态发生改变时调用
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			switch (state) {
			case TelephonyManager.CALL_STATE_RINGING: //响铃
				// 判断是否是黑名单电话
				String mode = bld.dbFindMode(incomingNumber);
				if("1".equals(mode)||"3".equals(mode))
				{
					/*拦截电话(要用到android os包下的aidl和SDK源码的一些类如contextimpl，getsystemservice-->是Ibinder的代理，
					还有一些java反射机制,根据对象找到类)*/
					endCall(); //调用了另一个进程中的服务去挂掉电话
					//电话挂断后，但是通话记录数据库中会保存这次通话记录，所以有删除通话的需求
					//当有电话到来时，定义一个内容观察者，观察通话记录数据库的变化，当有变化时就删除该记录
					 //CallLog.CONTENT_URI;
					Uri uri = Uri.parse("content://call_log/calls");
					getContentResolver().registerContentObserver(uri, true, new CallLogOberver(incomingNumber,new Handler()) {
					}); //注册一个内容观察者，去观察数据库变化
				}
				break;
			}
			
			super.onCallStateChanged(state, incomingNumber);
		}
		
	}
	
	private class CallLogOberver extends ContentObserver
	{
		String incomingNumber;
		public CallLogOberver(String incomingNumber,Handler handler) {
			super(handler);
			this.incomingNumber = incomingNumber;
		}
		//当被观察的数据有变化时，回调该方法
		@Override
		public void onChange(boolean selfChange) {
			deleteCallLog(incomingNumber);
			getContentResolver().unregisterContentObserver(this); //取消注册
			super.onChange(selfChange);
		}
		
	}

	
	@Override
	public void onCreate() {
		smsReceiver = new BlackListSMS();
		bld = new BlackListDao(this);
		
		listener = new PhoneListen();
		tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		tm.listen(listener,PhoneStateListener.LISTEN_CALL_STATE);//开启监听电话
		
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.provider.Telephony.SMS_RECEIVED"); //接收系统短信的action
		filter.setPriority(1000); //设置优先级
		registerReceiver(smsReceiver, filter);
		super.onCreate();
	}
	
	public void deleteCallLog(String incomingNumber) {
		Uri uri = Uri.parse("content://call_log/calls");
		ContentResolver cr = getContentResolver(); //获得内容解析者，用来解析另一个程序中的暴露的数据
		cr.delete(uri, "number=?", new String[]{incomingNumber}); //根据电话号码删除calllog数据库中的该条记录
	}

	@SuppressWarnings("unchecked")
	public void endCall() {
		try {
			Class serviceManage = BlackListService.class.getClassLoader().loadClass("android.os.ServiceManager");
			//获取该类中的方法
			Method method = serviceManage.getDeclaredMethod("getService", String.class);
			//调用该方法,返回服务真正的对象Ibinder,源码中获得ibinder对象是:
			//IBinder binder = ServiceManager.getService(TELEPHONY_SERVICE);
			IBinder binder = (IBinder) method.invoke(null, TELEPHONY_SERVICE);
			//调用远程服务aidl
			ITelephony.Stub.asInterface(binder).endCall();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onDestroy() {
		if(smsReceiver!=null){
			this.unregisterReceiver(smsReceiver);
			smsReceiver = null ;
			
			tm.listen(listener, PhoneStateListener.LISTEN_NONE); //取消电话监听
		}
		super.onDestroy();
	}
}
