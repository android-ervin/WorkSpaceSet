package ervin.android.safemobile.service;

import java.util.List;

import ervin.android.safemobile.TaskManager;
import ervin.android.safemobile.TaskManager.ScreenOnReceiver;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class ScreenOff extends Service {

	private ScreenOffReceiver receiver;
	private TaskManager tm;
	private TaskManager.ScreenOnReceiver receiveron;
	private ActivityManager am;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		
		am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		//注册开屏广播
		tm = new TaskManager();
		receiveron = tm.new ScreenOnReceiver();
		registerReceiver(receiveron, new IntentFilter(Intent.ACTION_SCREEN_ON));
		//注册关闭屏幕广播
		receiver = new ScreenOffReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		registerReceiver(receiver, filter);
		super.onCreate();
	}
	
	@Override
	public void onDestroy() {

		unregisterReceiver(receiver);
		unregisterReceiver(receiveron);
		receiver = null;
		receiveron =null;
		super.onDestroy();
	}
	
	class ScreenOffReceiver extends BroadcastReceiver
	{	//系统锁屏时，接收到的广播
		@Override
		public void onReceive(Context context, Intent intent) {
			// 接收到广播后，清理进程
			List<RunningAppProcessInfo> infos = am.getRunningAppProcesses();
			for(RunningAppProcessInfo info :infos)
			{
				am.killBackgroundProcesses(info.processName);
			}
		}
	}
}
