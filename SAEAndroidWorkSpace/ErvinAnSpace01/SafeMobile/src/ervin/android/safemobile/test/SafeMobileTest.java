package ervin.android.safemobile.test;

import java.util.Random;

import android.test.AndroidTestCase;
import ervin.android.safemobile.db.BlackListDBOpenHelper;
import ervin.android.safemobile.db.BlackListDao;

public class SafeMobileTest extends AndroidTestCase {

	
	public void testCreateDb()
	{
		BlackListDBOpenHelper blHelper = new BlackListDBOpenHelper(getContext());
		blHelper.getReadableDatabase();
	}
	public void testAdd()
	{		
		BlackListDao bld = new BlackListDao(getContext());
		bld.dbAdd("15970422814", "1");
		bld.dbAdd("15870422814", "2");
		bld.dbAdd("13670422814", "3");
		Random r = new Random();
		for(int i =0;i<20;i++)
		{
			bld.dbAdd("1360000000"+i,(String.valueOf(r.nextInt(3)+1)));
		}
	}
	public void testFind()
	{
		BlackListDao bld = new BlackListDao(getContext());
		boolean result= bld.dbFind("15870422814");
		assertEquals(true, result);
	}
	public void testUpdate()
	{
		BlackListDao bld = new BlackListDao(getContext());
		boolean result = bld.dbUpdate("15970422814", "3");
		assertEquals(true, result);
	}
	public void testDelete()
	{
		BlackListDao bld = new BlackListDao(getContext());
		int i = bld.dbDelete("15870422814");
		assertEquals(1, i);
	}
}

