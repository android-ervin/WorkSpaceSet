package ervin.android.safemobile;

import java.text.DecimalFormat;
import java.util.List;

import android.app.Activity;
import android.net.TrafficStats;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import ervin.android.safemobile.db.Appinfo;
import ervin.android.safemobile.utils.TrafficInfoUtils;

public class TrafficManager extends Activity {

	private TextView tv_total;
	private TextView tv_3g;
	private LinearLayout ll_load;
	
	private ListView lv_trafficlist;
	private List<Appinfo> appinfos ;
	private Appinfo appinfo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trafficmanager);
		
		ll_load = (LinearLayout) findViewById(R.id.ll_load);
		tv_total = (TextView) findViewById(R.id.tv_total_traffic);
		tv_3g = (TextView) findViewById(R.id.tv_3g_traffic);
		long Tx = TrafficStats.getTotalTxBytes(); //总上传流量
		long Rx = TrafficStats.getTotalRxBytes(); //下载流量
		long mTx = TrafficStats.getMobileTxBytes();  //2G/3G网络上传流量
		long mRx = TrafficStats.getMobileRxBytes();  
		tv_total.setText("总共消耗流量为："+size2string(Tx+Rx));
		tv_3g.setText("消耗2G/3G流量："+size2string(mTx+mRx));
		
		lv_trafficlist = (ListView) findViewById(R.id.lv_trafficlist);	
		fillData();				
	}
	
	private void fillData() {
		ll_load.setVisibility(View.VISIBLE);
		new Thread(){
			public void run() {
				appinfos = TrafficInfoUtils.getAppaTrafInfo(TrafficManager.this);
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						lv_trafficlist.setAdapter(new myAdapter());
						ll_load.setVisibility(View.INVISIBLE);
					}
				});
			};
		}.start();		
	}

	class myAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return appinfos.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;
			ViewHolder holder;
			if(convertView!=null)
			{
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}else
			{
				//view  = View.inflate(getApplicationContext(), R.layout.trafficmanager_appinfo_item, null);
				view = getLayoutInflater().inflate(R.layout.trafficmanager_appinfo_item, parent, false);
				holder = new ViewHolder();
				holder.icon = (ImageView) view.findViewById(R.id.iv_icon);
				holder.tv_lablename = (TextView) view.findViewById(R.id.tv_lablename);
				holder.tv_traffic = (TextView) view.findViewById(R.id.tv_traffic);
				view.setTag(holder);
			}
			appinfo = appinfos.get(position);		
			holder.icon.setImageDrawable(appinfo.getIcon());
			holder.tv_lablename.setText(appinfo.getLabelname());
			holder.tv_traffic.setText("消耗了"+size2string(appinfo.getTrafficused()));
			return view;
		}
		
	}
	class ViewHolder 
	{
		ImageView icon;
		TextView tv_lablename;
		TextView tv_traffic;
	}
	
	// 格式化字符串
		private String size2string(long size) {
			DecimalFormat df = new DecimalFormat("0.00");
			String mysize = "";
			if (size > 1024 * 1024 * 1024) {
				mysize = df.format(size / 1024f / 1024f / 1024f) + "G";
			} else if (size > 1024 * 1024) {
				mysize = df.format(size / 1024f / 1024f) + "M";
			} else if (size > 1024) {
				mysize = df.format(size / 1024f) + "K";
			} else {
				mysize = size + "B";
			}
			return mysize;
		}
}
