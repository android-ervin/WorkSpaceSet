package ervin.android.safemobile;

import ervin.android.safemobile.service.AddressService;
import ervin.android.safemobile.service.BlackListService;
import ervin.android.safemobile.utils.ServiceUtils;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class Setting extends Activity{

	//设置是否自动更新
	private SettingCustomerView scv_update;
	private SharedPreferences sp;
	//设置显示来电归属地
	private SettingCustomerView scv_showaddress ;
	//设置是否开启黑名单拦截
	private SettingCustomerView scv_blacklist;
	
	@Override
	protected void onResume() {
		//获得焦点的时候判断一次（解决bug）
		boolean isrunnig_blacklist = ServiceUtils.isServiceRunning(this,
				"ervin.android.safemobile.service.BlackListService");		
		scv_blacklist.setSelect(isrunnig_blacklist);
		super.onResume();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		
		sp= getSharedPreferences("config", MODE_PRIVATE);
		scv_update = (SettingCustomerView) findViewById(R.id.scv_update);
		if(sp!=null)
		{
			boolean update = sp.getBoolean("isupdate", false);
			scv_update.setSelect(update);
		}
		scv_update.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Editor editor =	sp.edit();
				if(scv_update.isCheck())
				{	//表示关闭更新
					scv_update.setSelect(false);
					editor.putBoolean("isupdate", false);
				}else{
					//表示打开更新
					scv_update.setSelect(true);
					editor.putBoolean("isupdate", true);
				}
				editor.commit();
			}
		});
		
		//设置是否显示来电归属地
		scv_showaddress = (SettingCustomerView) findViewById(R.id.scv_showaddress);
			//判断该服务是否还在运行
		boolean isrunnig_showaddress = ServiceUtils.isServiceRunning(this, "ervin.android.safemobile.service.AddressService");
		if(isrunnig_showaddress)
		{
			scv_showaddress.setSelect(true);
		}else
			scv_showaddress.setSelect(false);
		scv_showaddress.setOnClickListener(new OnClickListener() {
		Intent service = new Intent(Setting.this,AddressService.class);
			@Override
			public void onClick(View v) {
				if(scv_showaddress.isCheck())
				{
					scv_showaddress.setSelect(false);
					stopService(service);
				}else{
					scv_showaddress.setSelect(true);
					startService(service);
				}
				
			}
		});
		
		//设置是否开启黑名单拦截
		scv_blacklist = (SettingCustomerView) findViewById(R.id.scv_blacklsit);
		// 判断该服务是否还在运行
		boolean isrunnig_blacklist = ServiceUtils.isServiceRunning(this,
				"ervin.android.safemobile.service.BlackListService");
		if (isrunnig_blacklist) {
			scv_blacklist.setSelect(true);
		} else
			scv_blacklist.setSelect(false);
		scv_blacklist.setOnClickListener(new OnClickListener() {
			Intent service = new Intent(Setting.this, BlackListService.class);

			@Override
			public void onClick(View v) {
				if (scv_blacklist.isCheck()) {
					scv_blacklist.setSelect(false);
					stopService(service);
				} else {
					scv_blacklist.setSelect(true);
					startService(service);
				}

			}
		});
	}
}
