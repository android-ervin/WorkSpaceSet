package ervin.android.safemobile.db;

public class Smsinfo {
	private int id;
	private String address;
	private String body;
	private String type;
	private String date;
	
	public Smsinfo(){}
	
	public Smsinfo(int id, String address, String body, String type, String date) {
		super();
		this.id = id;
		this.address = address;
		this.body = body;
		this.type = type;
		this.date = date;
	}
	
	public Smsinfo(String address, String body, String type, String date) {
		super();
		this.address = address;
		this.body = body;
		this.type = type;
		this.date = date;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
