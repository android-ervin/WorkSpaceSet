package ervin.android.safemobile.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class BlackListDBOpenHelper extends SQLiteOpenHelper {

	/**
	 * 数据库创建的方法，当调用getWritableDatabase()或者getReadableDatabase()方法时
	 * @param context
	 */
	public BlackListDBOpenHelper(Context context) {
		super(context, "BlackList.db", null, 1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Called when the database is created for the first time
	 * 创建表
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "create table blacklist (_id integer primary key autoincrement,number varchar(20),mode varchar(2))";
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
