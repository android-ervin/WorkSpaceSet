package ervin.android.safemobile.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * 黑名单增删改查业务类，实现增删改查的具体操作
 * @author acer
 */
public class BlackListDao {
	private BlackListDBOpenHelper blHelper;
	public BlackListDao(Context context)
	{
		blHelper = new BlackListDBOpenHelper(context);
	}
	
	
	/**
	 * 根据电话号码查找记录
	 * @param number
	 * @return 有记录返回true，否则false
	 */
	public boolean dbFind(String number)
	{
		SQLiteDatabase db = blHelper.getReadableDatabase();
		String sql = "select * from blacklist where number=?";
		Cursor cursor = db.rawQuery(sql, new String[]{number});
		while(cursor.moveToNext())
		{
			return true;
		}
		return false;
	}
	/**
	 * 根据电话号码查找拦截模式
	 * @param number
	 * @return 返回拦截模式。mode=1：电话拦截，2：短信拦截，3：都拦截，null:没有该号码
	 */
	public String dbFindMode(String number)
	{
		String result = null;
		SQLiteDatabase db = blHelper.getReadableDatabase();
		String sql = "select mode from blacklist where number=?";
		Cursor cursor = db.rawQuery(sql, new String[]{number});
		while(cursor.moveToNext())
		{
			result = cursor.getString(0);
			return result;
		}
		return result;
	}
	/**
	 * 查询所有的黑名单
	 * @return
	 */
	public List<BlackListInfo> dbFindAll()
	{
		List<BlackListInfo> result = new ArrayList<BlackListInfo>();
		SQLiteDatabase db = blHelper.getReadableDatabase();
		String sql = "select * from blacklist order by _id desc";
		Cursor cursor = db.rawQuery(sql, null);
		while(cursor.moveToNext())
		{
			BlackListInfo blinfo = new BlackListInfo();
			blinfo.setName(cursor.getString(cursor.getColumnIndex("number")));
			blinfo.setMode(cursor.getString(cursor.getColumnIndex("mode")));
			result.add(blinfo);
		}
		cursor.close();
		db.close();
		return result;
	}
	
	/**
	 * 插入一条黑名单记录
	 * @param number
	 * @param mode mode=1：电话拦截，2：短信拦截，3：都拦截
	 * @return 返回1表示插入成功，-1插入失败，0表示数据库已经存在该号码
	 */
	public int dbAdd(String number,String mode)
	{
		SQLiteDatabase db = blHelper.getWritableDatabase();	
		//当插入的号码在数据库中存在的话，不能插入
		boolean isexit = dbFind(number);
		if(isexit)
			return 0;
		// The keys should be the column names and the values the column values
		ContentValues values = new ContentValues();
		values.put("number", number);
		values.put("mode", mode);
		long raw_id = db.insert("blacklist", null, values);
		db.close();
		if(raw_id!=-1)
			return 1;
		else
			return -1;
	}
	
	/**
	 * 根据电话号码修改拦截模式
	 * @param number 要修改的电话号码
	 * @param mode 新的拦截模式
	 * @return true表示修改成功
	 */
	public boolean dbUpdate(String number,String mode)
	{
		SQLiteDatabase db = blHelper.getWritableDatabase();	
		// The keys should be the column names and the values the column values
		ContentValues values = new ContentValues();
		values.put("mode", mode);
		int i = db.update("blacklist", values, "number=?",  new String[]{number});
		db.close();
		if(i!=0)
			return true;
		else
			return false;
	}
	
	/**
	 * 删除一条黑名单数据
	 * @param number
	 * @return 返回受影响行数
	 */
	public int dbDelete(String number)
	{
		SQLiteDatabase db = blHelper.getWritableDatabase();
		int i = db.delete("blacklist", "number=?", new String[]{number});
		db.close();
		return i;	
	}
}
