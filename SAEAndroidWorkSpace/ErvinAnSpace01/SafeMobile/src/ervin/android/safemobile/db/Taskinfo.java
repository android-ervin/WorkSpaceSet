package ervin.android.safemobile.db;

import android.graphics.drawable.Drawable;

public class Taskinfo {

	private Drawable icon;
	private String lablename ;
	private String packagename;
	private boolean isuser;
	private long memsize; //占用内存大小
	private boolean ischecked ;
	
	public Drawable getIcon() {
		return icon;
	}
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}
	public String getLablename() {
		return lablename;
	}
	public void setLablename(String lablename) {
		this.lablename = lablename;
	}
	public String getPackagename() {
		return packagename;
	}
	public void setPackagename(String packagename) {
		this.packagename = packagename;
	}
	public boolean isIsuser() {
		return isuser;
	}
	public void setIsuser(boolean isuser) {
		this.isuser = isuser;
	}
	public long getMemsize() {
		return memsize;
	}
	public void setMemsize(long memsize) {
		this.memsize = memsize;
	}
	public boolean isIschecked() {
		return ischecked;
	}
	public void setIschecked(boolean ischecked) {
		this.ischecked = ischecked;
	}
	
	
	
	
}
