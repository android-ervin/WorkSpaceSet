package ervin.android.safemobile.db;

import android.graphics.drawable.Drawable;

public class Appinfo {
	private Drawable icon;
	private String labelname;
	private String packagename;
	private boolean isRom; //flase表示在SD卡中，true表示在手机内存中
	private boolean isSystem;//flase表示是用户程序，true表示是系统程序
	
	private long trafficused; //消耗流量
	
	public long getTrafficused() {
		return trafficused;
	}
	public void setTrafficused(long trafficused) {
		this.trafficused = trafficused;
	}
	public Drawable getIcon() {
		return icon;
	}
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}
	public String getLabelname() {
		return labelname;
	}
	public void setLabelname(String labelname) {
		this.labelname = labelname;
	}
	public String getPackagename() {
		return packagename;
	}
	public void setPackagename(String packagename) {
		this.packagename = packagename;
	}
	public boolean isRom() {
		return isRom;
	}
	public void setRom(boolean isRom) {
		this.isRom = isRom;
	}
	public boolean isSystem() {
		return isSystem;
	}
	public void setSystem(boolean isSystem) {
		this.isSystem = isSystem;
	}
	
	
	
	
}
