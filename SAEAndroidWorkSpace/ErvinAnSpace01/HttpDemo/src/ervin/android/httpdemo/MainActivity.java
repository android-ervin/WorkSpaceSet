package ervin.android.httpdemo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	private String url = "http://10.0.2.2:80/Workers1.xml";
	private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

        tv = (TextView)findViewById(R.id.textView1);
    }

    public void connect(View view)
    {

		new Thread(new Runnable() {
			public void run() {
				URL myurl;
				try {
					myurl = new URL(url);
					HttpURLConnection conn = (HttpURLConnection) myurl
							.openConnection();
					conn.setRequestMethod("GET");
					conn.setConnectTimeout(5000);
					int code = conn.getResponseCode();
					System.out.println("返回码：" + code);
					if (code == 200) {
						// 联网成功
						InputStream is = conn.getInputStream();// 从外面获得一个流进入到程序里面来
						// 将输入流转换成字符串
						BufferedReader br = new BufferedReader(
								new InputStreamReader(is));
						String line = "";
						String result = "";
						while ((line = br.readLine()) != null) {
							result += line;
						}
						System.out.println("返回值：" + result);
						tv.setText(result);
						is.close();
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}

}
