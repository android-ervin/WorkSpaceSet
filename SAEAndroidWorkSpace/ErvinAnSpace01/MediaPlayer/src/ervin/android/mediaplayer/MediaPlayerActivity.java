package ervin.android.mediaplayer;

import java.io.IOException;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MediaPlayerActivity extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
	private EditText et ; 
	private MediaPlayer mplayer;
	private boolean isplay = false;
	private boolean ispause = false ;
	private boolean isrelese = false;
	private SurfaceView sv ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        et = (EditText) findViewById(R.id.et);
        Button btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        Button btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
        Button btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(this);
        
        sv = (SurfaceView) findViewById(R.id.sv);
        sv.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
       
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn1:
			System.out.print("按钮id"+v.getId());
			play();
			break;
		case R.id.btn2:
			pause();
			break;
		case R.id.btn3:
			stop();
			break;
		}
		
		
	}
	private void stop() {
		if(mplayer !=null)
		{
			if(isplay)
			{
				if(!isrelese)
				{
					mplayer.stop();
					mplayer.release();
					mplayer = null;
					
					isplay = false;
					ispause = false ;
					isrelese =true ;
				}
			}
		}
		
	}
	/**
	 * 暂停播放
	 */
	private void pause() {
		if(mplayer!=null)
			if(!isrelese)
			{
				if (!ispause)
				{
					mplayer.pause();
					ispause = true;
					isplay = false;
				} 
				else
				{
					mplayer.start();
					ispause = false;
					isplay = true;
				}
			}
			
	}
	/**
	 * 开始播放
	 */
	private void play() {
		
			try {
				if (!isplay) {
					System.out.print("musicplayer is playing");
					String path = et.getText().toString().trim();
					mplayer = new MediaPlayer();
					mplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
					mplayer.setDisplay(sv.getHolder()); //播放视频
					mplayer.setDataSource(path);
					mplayer.prepare();
					mplayer.start();
					
					isplay = true;
					isrelese = false ;
					ispause = false ;

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	@Override
	protected void onDestroy() {
		mplayer.stop();
		mplayer.release();
		mplayer = null ;
		super.onDestroy();
	}
}