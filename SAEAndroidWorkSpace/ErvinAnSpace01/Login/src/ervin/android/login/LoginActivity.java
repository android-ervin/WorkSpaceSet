package ervin.android.login;

import ervin.android.listview.ListViewActivity;
import ervin.android.operatedatabase.OperatorSqliteDatabase;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
    /** Called when the activity is first created. */
	private EditText et_name = null;
	private EditText et_psw = null ;
	private Button btn_login = null ;
	private Button btn_logon =null ;
	private CheckBox check =null ;
	
	OperatorSqliteDatabase operator = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        et_name =(EditText)findViewById(R.id.user);
        et_psw =(EditText)findViewById(R.id.psw);
        check = (CheckBox)findViewById(R.id.check1);
        operator = new OperatorSqliteDatabase(this);
        
        
        SharedPreferences sp_get = this.getSharedPreferences("config", MODE_PRIVATE);
        check.setChecked(sp_get.getBoolean("ischecked", true));
        if(sp_get.getBoolean("ischecked", true))
        {
        	et_name.setText(sp_get.getString("username", "")) ;
        	et_psw.setText(sp_get.getString("password", ""));
        }
        
        btn_login =(Button)findViewById(R.id.btn1);
        btn_logon = (Button)findViewById(R.id.btn2);
        btn_login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String username = et_name.getText().toString().trim();
				String password = et_psw.getText().toString().trim();
				boolean ischecked = check.isChecked();
				if(check.isChecked())
				{
					SharedPreferences sp_set = getSharedPreferences("config", MODE_PRIVATE);
					Editor editor = sp_set.edit();
					editor.putString("username", username);
					editor.putString("password", password);
					editor.putBoolean("ischecked", ischecked);
					editor.commit();
				}else
				{
					SharedPreferences sp_set = getSharedPreferences("config", MODE_PRIVATE);
					Editor editor = sp_set.edit();
					editor.putString("username", "");
					editor.putString("password", "");
					editor.putBoolean("ischecked", ischecked);
					editor.commit();
				}
				if(password.equals(operator.getPswByUsername(username)))
				{
					Toast.makeText(LoginActivity.this, "��¼�ɹ���", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(LoginActivity.this,ListViewActivity.class);
					startActivity(intent);
				}else{
					Toast.makeText(LoginActivity.this, "��¼ʧ�ܣ�", Toast.LENGTH_LONG).show();
				}
			}
		});
    }
    
    public void Logon(View view)
    {
    	String username = et_name.getText().toString().trim();
		String password = et_psw.getText().toString().trim();
    	if(TextUtils.isEmpty(username)||TextUtils.isEmpty(password))
    	{
    		Toast.makeText(this, "�û��������벻��Ϊ�գ�ע��ʧ��", Toast.LENGTH_LONG).show();
    	}else{
    		operator.insertData(username, password);
    		Toast.makeText(this, "ע��ɹ�", Toast.LENGTH_LONG).show();
    	}
    }
    
}