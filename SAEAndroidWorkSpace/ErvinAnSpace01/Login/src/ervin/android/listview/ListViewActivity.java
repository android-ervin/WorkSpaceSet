package ervin.android.listview;

import java.util.List;

import ervin.android.login.R;
import ervin.android.model.Person;
import ervin.android.operatedatabase.OperatorSqliteDatabase;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListViewActivity extends Activity {
	
	private List<Person> persons = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview);
		
		OperatorSqliteDatabase operator =new OperatorSqliteDatabase(this);
		persons = operator.queryAllData();
		
		ListView lv = (ListView)findViewById(R.id.lv);
		lv.setAdapter(new myAdapter());
	}
	public class myAdapter extends BaseAdapter
	{
		/**
		 * 返回集合的长度
		 */
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return persons.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			Person person = persons.get(position);//通过位置获得person对象
//			TextView tv = new TextView(getApplicationContext());
//			tv.setTextColor(Color.RED);
//			tv.setBackgroundColor(Color.WHITE);
//			tv.setTextSize(10);
//			tv.setText(person.toString());
			View view = View.inflate(ListViewActivity.this, R.layout.listview_item, null);
			TextView tv_id = (TextView) view.findViewById(R.id.tv_id);
			tv_id.setText("id:" + person.getId());
			
			TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
			tv_name.setText("name：" + person.getUsername());
			
			TextView tv_number = (TextView)view.findViewById(R.id.tv_number);
			tv_number.setText("phone：" + person.getPassword());
			return view;
		}
		
	}
	
}
