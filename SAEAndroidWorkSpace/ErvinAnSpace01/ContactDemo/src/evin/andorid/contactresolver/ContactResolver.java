package evin.andorid.contactresolver;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class ContactResolver {

	public static Cursor getContactDataByResolver(Context context,Uri uri)
	{
		//Cursor cursor = null;
		ContentResolver resolver =context.getContentResolver();
		Cursor cursor = resolver.query(uri, null, null, null, null);
		return cursor;
	}
}
