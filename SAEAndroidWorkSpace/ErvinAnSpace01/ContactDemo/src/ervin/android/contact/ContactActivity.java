package ervin.android.contact;

import evin.andorid.contactresolver.ContactResolver;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class ContactActivity extends Activity {
	Uri uri=null;
	//Cursor cursor =null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
    }
    public void click(View view)
    {
    	//查询raw_contacts表里的id数据
        uri = Uri.parse("content://com.android.contacts/raw_contacts");
        Cursor cursor = ContactResolver.getContactDataByResolver(this,uri);
        if(cursor!=null)
        {
        	while(cursor.moveToNext())
        	{
        		String id = cursor.getString(cursor.getColumnIndex("contact_id"));
        		System.out.println(id);
        		//根据ID来查找  data表和mimetype表在系统中是同一张视图表，可以一起查询名字，号码等内容
        		uri = Uri.parse("content://com.android.contacts/data");
        		ContentResolver resolver =this.getContentResolver();
        		Cursor cursor_data = resolver.query(uri, null, "raw_contact_id=?", new String[]{id}, null);
        		//cursor = ContactResolver.getContactDataByResolver(this, uri);
        		if(cursor_data!=null)
        		{
        			while(cursor_data.moveToNext())
        			{
        				String data1 = cursor_data.getString(cursor_data.getColumnIndex("data1"));
        				String mime_id = cursor_data.getString(cursor_data.getColumnIndex("mimetype"));
        				System.out.println("data1 = "+ data1);
        				System.out.println("mimetype_id = " + mime_id);      				
        			}
        			cursor_data.close();
        		}
        		//cursor.close();
        		System.out.println("-----------------");
        	}
        	cursor.close();
        }
    }
}