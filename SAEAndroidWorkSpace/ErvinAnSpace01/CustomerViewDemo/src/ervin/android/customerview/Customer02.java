package ervin.android.customerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class Customer02 extends View{

	public Paint mpaint;
	public String mText ="drawtest";
	public Customer02(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mpaint =  new Paint();
		TypedArray type = context.obtainStyledAttributes(attrs,R.styleable.customer02);
		
		int color = type.getColor(R.styleable.customer02_textcolor, Color.GREEN);
		float textsize =type.getDimension(R.styleable.customer02_fontsize, 35);
		String text = (String) type.getText(R.styleable.customer02_text);
		
		mpaint.setColor(color);
		mpaint.setTextSize(textsize);
		// 为了保持以后使用该属性一致性,返回一个绑定资源结束的信号给资源  
		type.recycle();
	}
	@Override
	protected void onDraw(Canvas canvas) {
		mpaint.setStyle(Style.FILL);  
        canvas.drawText(mText, 10, 60, mpaint); 
		super.onDraw(canvas);
	}
	
	
}
