package com.heima.news;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.Window;

import com.heima.news.fragment.HomeFragment;
import com.heima.news.fragment.MenuFragment2;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

public class MainActivity extends SlidingFragmentActivity{
    private SlidingMenu sm;
	private MenuFragment2 menuFragment;
	/**
     * 1 寰楀埌婊戝姩鑿滃崟
     * 2 璁剧疆婊戝姩鑿滃崟鏄湪宸﹁竟鍑烘潵杩樻槸鍙宠竟鍑烘潵
     * 3 璁剧疆婊戝姩鑿滃崟鍑烘潵涔嬪悗锛屽唴瀹归〉锛屾樉绀虹殑鍓╀綑瀹藉害
     * 4 璁剧疆婊戝姩鑿滃崟鐨勯槾褰�璁剧疆闃村奖锛岄槾褰遍渶瑕佸湪寮�鐨勬椂鍊欙紝鐗瑰埆鏆楋紝鎱㈡參鐨勫彉娣�
     * 5 璁剧疆闃村奖鐨勫搴�
     * 6 璁剧疆婊戝姩鑿滃崟鐨勮寖鍥�
     */

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setBehindContentView(R.layout.menu);
		setContentView(R.layout.content);
		
		
//		Fragment fragment1 = new Fragment1();
//		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment1).commit();
		
		
		sm = getSlidingMenu();
		//2 璁剧疆婊戝姩鑿滃崟鏄湪宸﹁竟鍑烘潵杩樻槸鍙宠竟鍑烘潵
		//鍙傛暟鍙互璁剧疆宸﹁竟LEFT锛屼篃鍙互璁剧疆鍙宠竟RIGHT 锛岃繕鑳借缃乏鍙矻EFT_RIGHT
		sm.setMode(SlidingMenu.LEFT);
		//3 璁剧疆婊戝姩鑿滃崟鍑烘潵涔嬪悗锛屽唴瀹归〉锛屾樉绀虹殑鍓╀綑瀹藉害
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		//4 璁剧疆婊戝姩鑿滃崟鐨勯槾褰�璁剧疆闃村奖锛岄槾褰遍渶瑕佸湪寮�鐨勬椂鍊欙紝鐗瑰埆鏆楋紝鎱㈡參鐨勫彉娣�
		sm.setShadowDrawable(R.drawable.shadow);
		//5 璁剧疆闃村奖鐨勫搴�
		sm.setShadowWidth(R.dimen.shadow_width);
		//6 璁剧疆婊戝姩鑿滃崟鐨勮寖鍥�
		// 绗竴涓弬鏁�SlidingMenu.TOUCHMODE_FULLSCREEN 鍙互鍏ㄥ睆婊戝姩
		// 绗簩涓弬鏁�SlidingMenu.TOUCHMODE_MARGIN 鍙兘鍦ㄨ竟娌挎粦鍔�
		// 绗笁涓弬鏁�SlidingMenu.TOUCHMODE_NONE 涓嶈兘婊戝姩
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		
		menuFragment = new MenuFragment2();
		//鑾峰彇fragment鐨勭鐞嗚�
		getSupportFragmentManager()
		//寮�惎浜嬬墿
		.beginTransaction()
		//鏇挎崲
		.replace(R.id.menu_frame, menuFragment, "Menu")
		//鎻愪氦
		.commit();
		
		HomeFragment homeFragment = new HomeFragment();
		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, homeFragment, "Home").commit();
		/**
		 * 涓嬮潰鐨勪唬鐮佹槸鍙宠竟渚ф粦
		 */
//		sm.setSecondaryMenu(R.layout.right_menu);
//		sm.setSecondaryShadowDrawable(R.drawable.shadowright);
//		RightMenuFragment rightMenuFragment = new RightMenuFragment();
//		getSupportFragmentManager().beginTransaction().replace(R.id.right_menu_frame, rightMenuFragment).commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * 鑾峰彇鑿滃崟
	 */
	public MenuFragment2 getMenuFragment2(){
		 menuFragment = (MenuFragment2) getSupportFragmentManager().findFragmentByTag("Menu");
		return menuFragment;
	}
    /**
     *鏂规硶D
     *鍥炶皟
     */
	public void switchFragment(Fragment f){
		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, f).commit();
		//鑷姩鍒囨崲
		sm.toggle();
	}
}
