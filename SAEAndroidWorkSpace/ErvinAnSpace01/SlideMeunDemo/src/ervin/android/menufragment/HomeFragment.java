package ervin.android.menufragment;

import java.util.ArrayList;
import java.util.List;

import com.example.slidemeundemo.R;

import ervin.android.viewpage.AffairsPage;
import ervin.android.viewpage.BasePage;
import ervin.android.viewpage.HomePage;
import ervin.android.viewpage.NewsCenterPage;
import ervin.android.viewpage.ServicePage;
import ervin.android.viewpage.SettingPage;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

/**
 * 测试代码
 * @author acer
 *
 */
public class HomeFragment extends Fragment {

	private FrameLayout fl_container ;
	private ViewPager mPager;
	private RadioGroup mrg;
	private View view;
	
	private List<BasePage> mlistpage = new ArrayList<BasePage>();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.frag_home2, null);	
		
		fl_container = (FrameLayout) view.findViewById(R.id.container);	
		mrg = (RadioGroup) view.findViewById(R.id.main_radio);
		//加载一个viewpager布局
		mPager = new ViewPager(getActivity());
		TextView tv = new TextView(getActivity());
		tv.setText("frag_home");
		fl_container.addView(mPager);
		fl_container.addView(tv);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		initListData();
		super.onActivityCreated(savedInstanceState);
		
	}

	private void initListData() {
		
		mlistpage.add(new HomePage(getActivity()));
		mlistpage.add(new NewsCenterPage(getActivity()));
		mlistpage.add(new ServicePage(getActivity()));
		mlistpage.add(new AffairsPage(getActivity()));
		mlistpage.add(new SettingPage(getActivity()));
		System.out.println("list加载数据完毕");
	}

	
}
