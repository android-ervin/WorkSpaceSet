package ervin.android.menufragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.slidemeundemo.MainActivity;
import com.example.slidemeundemo.R;

import ervin.android.contentfragment.FragMent1;
import ervin.android.contentfragment.FragMent2;
import ervin.android.contentfragment.FragMent3;
import ervin.android.contentfragment.FragMent4;
import ervin.android.contentfragment.FragMent5;

public class MenuFramgment extends Fragment {

	private View view;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		
		//给ListView绑定数据
		ListView menu_list = (ListView) view.findViewById(R.id.menu_list);
		menu_list.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, initData()));
		
		menu_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Fragment fm =null;
				switch (position) {
				case 0:
					fm = new FragMent1();
					break;

				case 1:
					fm = new FragMent2();
					break;
				case 2:
					fm = new FragMent3();
					break;
				case 3:
					fm = new FragMent4();
					break;
				case 4:
					fm = new FragMent5();
					break;
				}
				//怎么让Activity能接收到fm对象，从而去更新内容页面
				//1.回调函数
				//2。广播
				changeContent(fm);
			}
		});
		
		super.onActivityCreated(savedInstanceState);
	}
	

	protected void changeContent(Fragment fm) {
		if(fm!=null)
		{
			((MainActivity)getActivity()).showFragmentContent(fm);
		}
	}


	private List<String> initData() {
		List<String> list = new ArrayList<String>();
		list.add("fragment1");
		list.add("fragment2");
		list.add("fragment3");
		list.add("fragment4");
		list.add("fragment5");
		
		return list;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//获得view对象
		view = inflater.inflate(R.layout.menu_list, null);
		return view;
	}

}
