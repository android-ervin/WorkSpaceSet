package ervin.android.menufragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.example.slidemeundemo.MainActivity;
import com.example.slidemeundemo.R;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import ervin.android.viewpage.AffairsPage;
import ervin.android.viewpage.BasePage;
import ervin.android.viewpage.HomePage;
import ervin.android.viewpage.NewsCenterPage;
import ervin.android.viewpage.ServicePage;
import ervin.android.viewpage.SettingPage;

public class HomeBarFragment extends Fragment {

	private FrameLayout fl_container ;
	private ViewPager mPager;
	private RadioGroup mrg;
	private View view;
	
	private SlidingMenu sm;
	private List<BasePage> mlistpage;
	//第一次默认显示home
	private int checkid = R.id.rb_function;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		System.out.println("Fragment---->onCreate");
		//初始化sm
		sm = ((MainActivity)getActivity()).getSlidingMenu();
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		System.out.println("Fragment---->onCreateView");
		//初始化view
		view = inflater.inflate(R.layout.frag_home2, null);	
		fl_container = (FrameLayout) view.findViewById(R.id.container);	
		mrg = (RadioGroup) view.findViewById(R.id.main_radio);
		//加载一个viewpager布局
		mPager = new ViewPager(getActivity());
		TextView tv = new TextView(getActivity());
		tv.setText("frag_home");
		fl_container.addView(mPager);
		//fl_container.addView(tv);
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		System.out.println("Fragment---->onActivityCreated");
		
		initListData();
		// 给viewpager加载数据
		mPager.setAdapter(new HomeViewPagerAdapter());
		//给viewpager添加滑动事件(ViewPager默认就可以实现滑动，这里是为了处理滑动菜单)
		mPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				if(position==0)
				{
					sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
				}else{
					sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
				}
				//BasePage page = mlistpage.get(position);				
				
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//给radiogroup添加点击事件
		mrg.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rb_function:
					mPager.setCurrentItem(0, false);
					System.out.println("点击了0");
					checkid=0;
					break;
				case R.id.rb_news_center:
					mPager.setCurrentItem(1, false);
					checkid=1;
					break;
				case R.id.rb_smart_service:
					mPager.setCurrentItem(2, false);
					checkid=2;
					break;
				case R.id.rb_gov_affairs:
					mPager.setCurrentItem(3, false);
					checkid=3;
					break;
				case R.id.rb_setting:
					mPager.setCurrentItem(4, false);
					checkid=4;
					break;
				}
				
			}
		});
		mrg.check(checkid);
		super.onActivityCreated(savedInstanceState);
	}

	private void initListData() {
		mlistpage = new ArrayList<BasePage>();
		mlistpage.add(new HomePage(getActivity()));
		mlistpage.add(new NewsCenterPage(getActivity()));
		mlistpage.add(new ServicePage(getActivity()));
		mlistpage.add(new AffairsPage(getActivity()));
		mlistpage.add(new SettingPage(getActivity()));	
		
	}

	class HomeViewPagerAdapter extends PagerAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mlistpage.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			System.out.println("isViewFromObject---"+arg0==arg1);
			return arg0==arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			System.out.println("销毁destoryItem---"+position);
			((ViewPager)container).removeView(mlistpage.get(position).getView());
			
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			System.out.println("加载instantiateItem---"+position);
			((ViewPager)container).addView(mlistpage.get(position).getView(), 0);
			return mlistpage.get(position).getView();
		}
		
		
	}
}
