package ervin.android.viewpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class ServicePage extends BasePage{

	public ServicePage(Context context) {
		super(context);
	}

	@Override
	public View initView(LayoutInflater inflater) {
		TextView tv = new TextView(context);
		tv.setText("ServicePage");
		return tv;
	}

}
