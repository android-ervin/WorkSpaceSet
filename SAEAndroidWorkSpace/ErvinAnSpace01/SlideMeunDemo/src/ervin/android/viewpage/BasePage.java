package ervin.android.viewpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public abstract class BasePage {

	private View view;
	public Context context;
	public BasePage(Context context){
		this.context = context;
		LayoutInflater inflater = (LayoutInflater)context.getSystemService
			      (Context.LAYOUT_INFLATER_SERVICE);
		view = initView(inflater);
	}

	public View getView()
	{
		return view;
	}
	/**
	 * 使用布局加载器加载各自的布局
	 * @param inflater
	 */
	public abstract View initView(LayoutInflater inflater);
}
