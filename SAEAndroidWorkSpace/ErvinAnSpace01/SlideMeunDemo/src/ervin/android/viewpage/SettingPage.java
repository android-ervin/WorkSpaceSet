package ervin.android.viewpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class SettingPage extends BasePage{

	public SettingPage(Context context) {
		super(context);
	}

	@Override
	public View initView(LayoutInflater inflater) {
		TextView tv = new TextView(context);
		tv.setText("SettingPage");
		return tv;
	}

}
