package ervin.android.viewpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class AffairsPage extends BasePage{

	public AffairsPage(Context context) {
		super(context);
	}

	@Override
	public View initView(LayoutInflater inflater) {
		TextView tv = new TextView(context);
		tv.setText("AffairsPage");
		return tv;
	}

}
