package com.example.slidemeundemo;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Window;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingActivity;

import ervin.android.menufragment.HomeBarFragment;
import ervin.android.menufragment.HomeFragment;
import ervin.android.menufragment.MenuFramgment;

public class MainActivity extends SlidingActivity {
	
	private SlidingMenu sm;
	private MenuFramgment mfragment;
	private HomeBarFragment hbfragment;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//设置无标题
		
		setBehindContentView(R.layout.menu);
		setContentView(R.layout.content); //加载帧布局，方便下面的fragment替换
		//得到滑动菜单
		sm = getSlidingMenu();
		sm.setMode(SlidingMenu.LEFT); //从坐标滑出
		sm.setBehindWidthRes(R.dimen.slidingmenu_offset);//sm.setBehindWidth(30);//设置菜单宽度  30px
		sm.setShadowDrawable(R.drawable.shadow); //设置阴影渐变颜色
		sm.setShadowWidthRes(R.dimen.shadow_width); //设置菜单和内容页交界处阴影的宽度
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);//设置触摸模式，屏幕上都可以滑动
		
		//侧滑菜单
		mfragment = new MenuFramgment();
		getFragmentManager().beginTransaction().replace(R.id.menu_frame, mfragment, "Menu").commit();
		
		//设置底部bar
//		HomeFragment hfragment = new HomeFragment();
//		getFragmentManager().beginTransaction().replace(R.id.content_frame, hfragment, "homebar").commit();
		hbfragment = new HomeBarFragment();
		getFragmentManager().beginTransaction().replace(R.id.content_frame, hbfragment, "homebar").commit();
			
	}
	public void showFragmentContent(Fragment fm)
	{
		getFragmentManager().beginTransaction().replace(R.id.content_frame, fm, "content").commit();
		//getFragmentManager().beginTransaction().replace(R.id.content_frame, hbfragment, "homebar").commit();
		sm.toggle(); //自动合上menu
	}
}
