package ervin.java.test;

public class Demo1 {

	public static int i=10;
	public static void main(String[] args) {
		new myThread1(i).start();
		//new myThread2(i).start();
		new myThread3(i).start();
		

	}

}
class myThread1 extends Thread
{
	int i=0;
	public myThread1(int i)
	{
		this.i=i;
	}
	@Override
	public void run() {

		synchronized (this) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
		super.run();
	}
}
class myThread2 extends Thread
{
	int i=0;
	public myThread2(int i)
	{
		this.i=i;
	}
	@Override
	public void run() {

		synchronized (this) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i--;
		}
		super.run();
	}
}
class myThread3 extends Thread
{
	int i=0;
	public myThread3(int i)
	{
		this.i=i;
	}
	@Override
	public void run() {

		synchronized (this) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(i);
		}
		super.run();
	}
}