package ervin.android.httpclient;

import ervin.android.httpclienthelper.HttpClientHelper;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class HttpClientActivity extends Activity {
    EditText et_name = null;
    EditText et_age = null;
    TextView tv = null;
    String path ="http://10.0.2.2:81/Android.aspx";
    MyHandler myHandler = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        et_name =(EditText) findViewById(R.id.et_name);
        et_age =(EditText) findViewById(R.id.et_age);
        tv =(TextView) findViewById(R.id.tv);
        myHandler = new MyHandler();
    }
    public class MyHandler extends Handler
    {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			String text = (String) msg.obj;
			tv.setText(text);
		}
    }
    
    public void Httpget(View view)
    {
    	new Thread(new Runnable() {
			public void run() {
				String name = et_name.getText().toString().trim();
				String age = et_age.getText().toString().trim();
				String getpath=path+"?"+"name="+name+"&age="+age;
				HttpClientHelper helper = new HttpClientHelper();
				String content = helper.get(getpath);
				Message msg = myHandler.obtainMessage();
				msg.obj = content;
				msg.what = 1;
				myHandler.sendMessage(msg);
			}
		}).start();
    }
    
    
    public void Httppost(View view)
    {
    	new Thread(new Runnable() {
			public void run() {
				String name = et_name.getText().toString().trim();
				String age = et_age.getText().toString().trim();
				
				HttpClientHelper helper = new HttpClientHelper();
				String content = helper.post(path,name,age);
				Message msg = myHandler.obtainMessage();
				msg.obj = content;
				msg.what = 2;
				myHandler.sendMessage(msg);
			}
		}).start();
    }
}