package ervin.android.backupsms;

import java.util.ArrayList;
import java.util.List;

import ervin.android.smsmodel.Smsinfo;
import ervin.android.smsresolver.SmsResolver;
import ervin.android.xmlserializable.SerializeUtils;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;

public class BackupSMSActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    public void click(View view)
    {
    	List<Smsinfo> smsinfos =new ArrayList<Smsinfo>();
    	Cursor cursor = SmsResolver.getSmsData(this);
    	while(cursor.moveToNext())
    	{
    		String address =cursor.getString(0);
    		String type = cursor.getString(1);
    		String body = cursor.getString(2);
    		String date = cursor.getString(3);
    		Smsinfo sms =new Smsinfo(address, body, type, date);
    		smsinfos.add(sms);
    	}
    	
    	SerializeUtils xml =new SerializeUtils(smsinfos);
    	xml.backUp(this);
    }
    
    public void start(View view)
    {
    	List<Smsinfo> smsinfos =new ArrayList<Smsinfo>();
    	Cursor cursor = SmsResolver.getSmsData(this);
    	while(cursor.moveToNext())
    	{
    		String address =cursor.getString(0);
    		String type = cursor.getString(1);
    		String body = cursor.getString(2);
    		String date = cursor.getString(3);
    		Smsinfo sms =new Smsinfo(address, body, type, date);
    		smsinfos.add(sms);
    	}
    	
    	SerializeUtils xml =new SerializeUtils(smsinfos);
    	xml.useXMLSerilizeBackUp(this);
    }
}