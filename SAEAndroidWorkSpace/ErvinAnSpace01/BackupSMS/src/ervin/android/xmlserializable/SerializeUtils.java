package ervin.android.xmlserializable;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.os.Environment;
import android.util.Xml;
import android.widget.Toast;

import ervin.android.smsmodel.Smsinfo;

public class SerializeUtils {

	private List<Smsinfo> smsinfos = null;
	public SerializeUtils(List<Smsinfo> smsinfos)
	{
		this.smsinfos = smsinfos ;
	}
	public void backUp(Context context)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		sb.append("<smss>");
		for (Smsinfo info : smsinfos) {
			sb.append("<sms>");
			sb.append("<address>");
			sb.append(info.getAddress());
			sb.append("</address>");
			
			sb.append("<type>");
			sb.append(info.getType());
			sb.append("</type>");
			
			sb.append("<body>");
			sb.append(info.getBody());
			sb.append("</body>");
			
			sb.append("<date>");
			sb.append(info.getDate());
			sb.append("</date>");
			sb.append("</sms>");
		}
		sb.append("</smss>");
		try {
			File file =new File(Environment.getExternalStorageDirectory(),"backup_one.xml");
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(sb.toString().getBytes());
			fos.close();
			Toast.makeText(context,"备份成功", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(context,"备份失败", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	
	public void useXMLSerilizeBackUp(Context context)
	{
		try {
			XmlSerializer serializer =  Xml.newSerializer();
			File file =new File(Environment.getExternalStorageDirectory(),"backup_two.xml");//文件保存至SD卡
			FileOutputStream fos = new FileOutputStream(file);
			serializer.setOutput(fos, "utf-8");
			serializer.startDocument("utf-8", true);
			serializer.startTag(null, "smss");
			for(Smsinfo info : smsinfos)
			{
				serializer.startTag(null, "sms");
				serializer.attribute(null, "id", info.getId()+"");
				
				serializer.startTag(null, "address");
				serializer.text(info.getAddress()+"");
				serializer.endTag(null, "address");
				
				serializer.startTag(null, "type");
				serializer.text(info.getType());
				serializer.endTag(null, "type");
				
				serializer.startTag(null, "body");
				serializer.text(info.getBody());
				serializer.endTag(null, "body");
				
				serializer.startTag(null, "date");
				serializer.text(info.getDate());
				serializer.endTag(null, "date");
				
				serializer.endTag(null, "sms");
			}
			serializer.endTag(null, "smss");
			serializer.endDocument();
			fos.close();
			Toast.makeText(context,"备份成功", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(context,"备份失败", Toast.LENGTH_LONG).show();
		} 
	}
}
