package ervin.android.smsresolver;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class SmsResolver {

	public static Cursor getSmsData(Context context)
	{
		Uri uri = Uri.parse("content://sms/");
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, new String[]{"address","type","body","date"}, null, null, null);
		return cursor;
	}
}
