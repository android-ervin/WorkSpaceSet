package ervin.android.demo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class myView extends View {

	//屏幕宽高
	int winWidth;
	int winHeight;
	private WindowManager wm;
	private Context mcontext;
	private final static int SPACE = 30; //30像素
	//矩形宽高
	int recsize;
	public myView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mcontext = context;
		wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		winHeight = wm.getDefaultDisplay().getHeight();
		winWidth = wm.getDefaultDisplay().getWidth();
	}

	
	@Override
	protected void onDraw(Canvas canvas) {
		recsize = (winWidth-4*SPACE)/3;
		//第一个矩形的位置
		int left = DensityUtils.px2dip(mcontext, SPACE);
		int top = DensityUtils.px2dip(mcontext,(float)((0.5*winWidth)-((3/2)*recsize))/2);
		int right = DensityUtils.px2dip(mcontext, left+recsize);
		int bottom = DensityUtils.px2dip(mcontext, top+recsize);
		int index = DensityUtils.px2dip(mcontext, recsize);		
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		for(int i=0;i<=2;i++)
		{
			for(int j=0;j<=2;j++)
			{
				canvas.drawRect((j+1)*left+j*index, (i+1)*top+i*index, (j+1)*right,(i+1)*bottom, paint);
			}
		}
		super.onDraw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			System.out.println("用户按下了");
			break;
		case MotionEvent.ACTION_MOVE:
			System.out.println("用户拖动了");
			break;
		case MotionEvent.ACTION_UP:
			System.out.println("用户抬起了");
			break;
		default:
			break;
		}
		return true;
	}

	
}
