package ervin.android.demo;

import android.content.Context;

public class DensityUtils {
	/**
	 * 将dip转换为px
	 * @param context
	 * @param dpvalue
	 * @return
	 */
	public static int dip2px(Context context,float dpvalue)
	{
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int)(dpvalue * scale + 0.5f);
	}
	
	/**
	 * 将px转换成dip
	 * @param context
	 * @param pxvalue
	 * @return
	 */
	public static int px2dip(Context context,float pxvalue)
	{
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int)(pxvalue * scale + 0.5f);
	}

}
