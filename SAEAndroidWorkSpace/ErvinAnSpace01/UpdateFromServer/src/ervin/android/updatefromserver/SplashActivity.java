package ervin.android.updatefromserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SplashActivity extends Activity {

	private String updateurl="";
	protected static final int CONNECT_OK = 0;
	protected static final int CONNECT_ERRO = 1;
	private TextView tv_version;
	private RelativeLayout splash_layout;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
        
        tv_version = (TextView) findViewById(R.id.tv_version);
        tv_version.setText("版本为："+getVersion());  
       
        checkUpdate();
        AlphaAnimation aa = new AlphaAnimation(0.2f, 1.0f);
        aa.setDuration(500);
        findViewById(R.id.splash_layout).startAnimation(aa);
    }
    
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == CONNECT_OK) {
				String result = (String) msg.obj;
				try {
					JSONObject json = new JSONObject(result);
					String version = (String) json.get("version");
					String desc = (String) json.get("description");
					updateurl = json.getString("apkurl");
					// System.out.println(version+","+desc+","+url);
					if (!getVersion().equals(version)) {
						AlertDialog.Builder builder = new Builder(SplashActivity.this);
						builder.setOnCancelListener(new DialogInterface.OnCancelListener() {							
							@Override
							public void onCancel(DialogInterface dialog) {
								// TODO Auto-generated method stub
								enterHome();
								dialog.dismiss();
							}
						});
						builder.setTitle("提示更新");
						builder.setMessage(desc);
						builder.setPositiveButton("马上升级",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// 下载升级安装包
										installNewApk();
									}
								});
						builder.setNegativeButton("下次再说",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// 进入home页面
										enterHome();
									}
								});
						builder.show();
					}else //没有更新
					{
						//进入home页面
						enterHome();
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (msg.what == CONNECT_ERRO) {
				Toast.makeText(SplashActivity.this, "联网错误", 0).show();
			}
			super.handleMessage(msg);
		}
	};
    private String getVersion()
    {
    	 PackageManager pm = getPackageManager();
 		try {
 			PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
 			return info.versionName;
 		} catch (NameNotFoundException e) {
 			e.printStackTrace();
 			return "";
 		}	
    }
    
    //联网获取跟新信息
    private void checkUpdate()
    {
    	new Thread(
    		new Runnable() {
				public void run() {
					try {
						long starttime = System.currentTimeMillis();
						Message msg = Message.obtain();
						URL url = new URL("http://10.0.2.2:80/update.htm");//"http://10.0.2.2:80/Workers1.xml"
						HttpURLConnection conn =  (HttpURLConnection) url.openConnection();
						conn.setRequestMethod("GET");
						conn.setConnectTimeout(4000);
						int code = conn.getResponseCode();
						System.out.println("返回码:"+code);
						if(conn.getResponseCode()==200)
						{
							//联网成功
							InputStream is = conn.getInputStream();
							BufferedReader br = new BufferedReader(new InputStreamReader(is));
							String line="";
							String result="";
							while((line = br.readLine())!=null)
							{
								result+= line;
							}
							System.out.println("conncect ok"+result);
							//使用消息，将得到的字符串返回给主线程							
							msg.what = CONNECT_OK;
							msg.obj = result;							
							
						}else{
							msg.what = CONNECT_ERRO;
							//子线程不能更改UI
						}
						long endtime = System.currentTimeMillis();
						if(endtime - starttime<2000)
							Thread.sleep(2000-(endtime-starttime));
						mHandler.sendMessage(msg);

					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
				}
			}).start();
    }
    
    public void enterHome()
    {
    	Intent intent = new Intent(SplashActivity.this,Home.class);
    	startActivity(intent);
    	finish();
    }
    public void installNewApk()
    {
    	if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))//表示SD卡已经挂载
    	{
    		//这里用到一个开源的下载框架 afinal(大神michael)
    		FinalHttp fh = new FinalHttp();
    		
    		fh.download(updateurl, Environment.getExternalStorageDirectory().getAbsolutePath()+
    				"/update.apk", new AjaxCallBack<File>() {

						@Override
						public void onFailure(Throwable t, int errorNo,
								String strMsg) {
							Toast.makeText(SplashActivity.this, "下载失败:"+strMsg, 0).show();
							enterHome();
							super.onFailure(t, errorNo, strMsg);
						}

						@Override
						public void onLoading(long count, long current) {
							long l=current / count ;
							
							super.onLoading(count, current);
						}

						@Override
						public void onSuccess(File t) {
							// 开始安装apk,调用系统的Activity
							Toast.makeText(SplashActivity.this, "下载成功，准备安装", 0).show();
							Intent intent = new Intent();
							intent.setAction("android.intent.action.VIEW");
							intent.addCategory("android.intent.category.DEFAULT");
							//MIME映射关系,其中application/vnd.andorid.package-archive对应的就是.apk文件
							intent.setDataAndType(Uri.fromFile(t), "application/vnd.android.package-archive");
							startActivity(intent);
							enterHome();
							super.onSuccess(t);
						}  			
					});
    	}else
    	{
    		Toast.makeText(SplashActivity.this, "SD卡已经拔出，请检查", 0).show();
    		enterHome();
    	}
    }
}
