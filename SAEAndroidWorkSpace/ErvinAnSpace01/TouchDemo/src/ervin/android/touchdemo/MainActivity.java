package ervin.android.touchdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {

	private Button btn;
	private ImageView img;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		btn = (Button)findViewById(R.id.btn);
		img = (ImageView)findViewById(R.id.img);
		
		btn.setOnTouchListener(new OnTouchListener() {		
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				System.out.println("btn--ontouch"+event);
				return false;
			}
		});
		btn.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				System.out.println("btn--click");
				
			}
		});
		
		
		img.setOnTouchListener(new OnTouchListener() {		
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				System.out.println("img--ontouch"+event);
				return false;
			}
		});
		img.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				System.out.println("img--click");
				
			}
		});
		
		

	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		System.out.println("dispatchTouchEvent");
		return super.dispatchTouchEvent(ev);
	}
}
