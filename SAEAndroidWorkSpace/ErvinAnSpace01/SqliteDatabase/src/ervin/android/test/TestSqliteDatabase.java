package ervin.android.test;

import java.util.List;

import ervin.android.SqliteHelper.SqliteHelper;
import ervin.android.model.Person;
import ervin.android.operatedatabase.OperatorSqliteDatabase;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

public class TestSqliteDatabase extends AndroidTestCase {
	
	public void testCreateDatabase()
	{
		SqliteHelper sqHelper = new SqliteHelper(getContext());
		SQLiteDatabase db = sqHelper.getReadableDatabase();
		db.close();
	}
	
	public void testAddData()
	{
		OperatorSqliteDatabase operator = new OperatorSqliteDatabase(getContext());
		operator.insertData("lisi", "454");
	}
	
	public void testQueryData()
	{
		OperatorSqliteDatabase operator = new OperatorSqliteDatabase(getContext());
		operator.queryData("lisi");
	}
	
	public void testQueryAllData()
	{
		OperatorSqliteDatabase operator = new OperatorSqliteDatabase(getContext());
		List<Person> persons = operator.queryAllData();
		for (Person person : persons) {
			System.out.println(person.toString());
		}
	}
	
	public void testUpdataVersion()
	{
		SqliteHelper sqHelper = new SqliteHelper(getContext());
		SQLiteDatabase db = sqHelper.getReadableDatabase();
		db.close();
	}
	
	public void testUpdataData()
	{
		OperatorSqliteDatabase operator = new OperatorSqliteDatabase(getContext());
		operator.updataData(2, "3000");
		
	}
	
	/**
	 * 测试事务,
	 * 人为设置一个nullexception,则第一句执行，第二句不会执行
	 */
	String s;
	public void testNOTransaction()
	{
		
		SqliteHelper sqHelper = new SqliteHelper(getContext());
		SQLiteDatabase db = sqHelper.getReadableDatabase();
		db.execSQL("update person set account=account-1000 where id=1");
		s.equals("haha"); 
		db.execSQL("update person set account=account+1000 where id=2");
	}
	public void testTransaction()
	{
		SqliteHelper sqHelper = new SqliteHelper(getContext());
		SQLiteDatabase db = sqHelper.getReadableDatabase();
		db.beginTransaction();
		db.execSQL("update person set account=account-1000 where id=1");
		s.equals("haha"); 
		db.execSQL("update person set account=account+1000 where id=2");
		db.setTransactionSuccessful();//标记事务处理成功
		db.endTransaction();
	}
}
