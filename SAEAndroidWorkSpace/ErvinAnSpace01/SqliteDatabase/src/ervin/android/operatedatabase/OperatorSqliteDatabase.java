package ervin.android.operatedatabase;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import ervin.android.SqliteHelper.SqliteHelper;
import ervin.android.model.Person;

public class OperatorSqliteDatabase {
	private SqliteHelper sqHelper = null;
	public OperatorSqliteDatabase(Context context)
	{
		sqHelper = new SqliteHelper(context);
	}
	/**
	 * 向数据库中插入一条数据
	 * @param name
	 * @param psw
	 */
	public void insertData(String name,String psw)
	{
		SQLiteDatabase db = sqHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("username", name);
		values.put("password", psw);
		db.insert("person", null, values);
		db.close();
		//第二种，需要自己写SQL语句。
//		db.execSQL("insert into person(username,password) values (?,?)", new String[]{name,psw});
//		db.close();
	}
	public void queryData(String name)
	{
		SQLiteDatabase db = sqHelper.getWritableDatabase();
		db.query("person", null, "username=?", new String[]{name}, null, null, null);
		db.close();
	}
	public List<Person> queryAllData()
	{
		List<Person> persons =new ArrayList<Person>();
		Person p = null;
		SQLiteDatabase db = sqHelper.getWritableDatabase();
		Cursor cursor = db.query("person", new String[]{"id","username","password"}, null, null, null, null, null);
		while(cursor.moveToNext())
		{
			int id =cursor.getInt(cursor.getColumnIndex("id"));
			String username = cursor.getString(cursor.getColumnIndex("username"));
			String password = cursor.getString(cursor.getColumnIndex("password"));
			p=new Person(id, username, password);
			persons.add(p);
		}
		db.close();
		cursor.close();
		return persons;
	}
	public void deleteData(String name)
	{
		SQLiteDatabase db = sqHelper.getWritableDatabase();
		db.delete("person", "name=?", new String[]{name});
		db.close();
	}
	
	public void updataData(int id,String account)
	{
		SQLiteDatabase db = sqHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("account", account);
		db.update("person", values, "id=?", new String[]{Integer.toString(id)});
		db.close();
	}
}
