package ervin.android.SqliteHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class SqliteHelper extends SQLiteOpenHelper {

	private static final String Tag = "SqliteHelper";

	public SqliteHelper(Context context) {
		super(context, "mydata.db", null, 2);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table person (id integer primary key autoincrement,username varvchar(10),password varchar(20))");

	}

	@Override
	/**
	 * This method executes within a transaction. If an exception is thrown, all changes will automatically be rolled back.
	 */
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// 当版本号发生改变时会执行该函数
		Log.i(Tag, "版本号发生改变");
		db.execSQL("alter table person add account varchar(20)");

	}

}
