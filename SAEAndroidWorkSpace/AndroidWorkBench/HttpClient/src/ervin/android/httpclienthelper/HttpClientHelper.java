package ervin.android.httpclienthelper;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import ervin.android.inputsteamtools.InputstreamUtils;

public class HttpClientHelper {

	public String get(String path)
	{
		URI uri = URI.create(path);
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(uri);
		String content="";
		try {
			HttpResponse response = client.execute(httpGet);
			int code =response.getStatusLine().getStatusCode();
	
			if(code==200)
			{
				InputStream is =response.getEntity().getContent();
				content = InputstreamUtils.getStringByInputsteam(is);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "请求失败";
		}
		return content;
	}
	
	public String post(String path,String name,String age)
	{
		URI uri = URI.create(path);
		HttpClient client = new DefaultHttpClient();
		HttpPost httpPost = new  HttpPost(uri);
		String content="";
		HttpResponse response;
		try {
			NameValuePair nameValuePair1=new BasicNameValuePair("sex", name);
			NameValuePair nameValuePair2=new BasicNameValuePair("adr", age);//参数名称必须和服务器端的相同
			List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>();
			nameValuePairs.add(nameValuePair1);
			nameValuePairs.add(nameValuePair2);//
			
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			response = client.execute(httpPost);
			int code =response.getStatusLine().getStatusCode();
			if(code==200)
			{
				InputStream is =response.getEntity().getContent();
				content = InputstreamUtils.getStringByInputsteam(is);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "请求失败";
		}
		return content;
	}
}
