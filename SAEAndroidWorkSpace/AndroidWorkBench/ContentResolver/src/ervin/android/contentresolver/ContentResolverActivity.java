package ervin.android.contentresolver;

import android.app.Activity;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class ContentResolverActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    public void query(View view)
    {
    	//获得内容解析者（中间人）
    	ContentResolver resolver = this.getContentResolver();
    	Uri uri = Uri.parse("content://ervin.android.provider/query");
    	resolver.query(uri, null, null, null, null);
    }
}