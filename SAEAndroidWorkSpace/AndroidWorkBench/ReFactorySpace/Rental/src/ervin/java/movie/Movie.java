package ervin.java.movie;

public class Movie {
	public static final int CHILDERN = 2;
	public static final int REGULAR = 0;
	public static final int NEW_MOVIE =1;
	
	private String _title;
	private int _priceCode;
	public Movie(String title, int priceCode) {
		super();
		_title = title;
		_priceCode = priceCode;
	}
	public String get_title() {
		return _title;
	}
	public void set_title(String title) {
		_title = title;
	}
	public int get_priceCode() {
		return _priceCode;
	}
	public void set_priceCode(int priceCode) {
		_priceCode = priceCode;
	}
	
	
} 
