package ervin.java.main;


import ervin.java.customer.Customer;
import ervin.java.movie.Movie;
import ervin.java.rental.Rental;

public class MovieShop {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Customer customer = new Customer("ervin");
		Movie movie= new Movie("shoot", 0);
		Movie movie1 = new Movie("transfer",1);
		//让三个类发生关系
		Rental rental = new Rental(movie, 5);
		Rental rental1= new Rental(movie1,3);
		customer.addRental(rental);
		customer.addRental(rental1);
		
		String result = customer.statement();
		System.out.println(result);

	}

}
