package ervin.android.myprovider;

import ervin.android.SqliteHelper.SqliteHelper;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class MyProvider extends ContentProvider {
	//定义一个匹配URI
	private static UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
	private static final int QUERY = 1;
	static{
		matcher.addURI("ervin.android.provider", "query", QUERY);
		matcher.addURI("ervin.android.provider", "delete", 2);
	}
	SqliteHelper sqHelp = null;
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
		
	}

	@Override
	public boolean onCreate() {
		sqHelp = new SqliteHelper(this.getContext());
		return false;
	}

	@Override
	//发布程序后，别的应用程序根据uri就能访问这个方法返回的数据了
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		if(matcher.match(uri) == QUERY)
		{
			SQLiteDatabase db = sqHelp.getWritableDatabase();
			Cursor cursor = db.query("person", null, null, null, null, null, null);
			while(cursor.moveToNext())
			{
				String name = cursor.getString(cursor.getColumnIndex("username"));
				String psw = cursor.getString(cursor.getColumnIndex("password"));
				System.out.println("username:" + name + "---password" + psw);
			}
			return cursor;
		}
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
