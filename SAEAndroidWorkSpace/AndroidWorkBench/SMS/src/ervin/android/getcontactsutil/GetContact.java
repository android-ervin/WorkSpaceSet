package ervin.android.getcontactsutil;

import java.util.ArrayList;
import java.util.List;

import ervin.android.contactmodel.Contact;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class GetContact {

	public static List<Contact> getContactInfo(Context context)
	{
		//生成一个业务类集合
		List<Contact> contact_list = new ArrayList<Contact>();
		Contact contact_item=null;
		//获得内容解析者
		ContentResolver resolver = context.getContentResolver();
		//此uri是查询获得所有联系人的信息
		Uri uri = Uri.parse("content://com.android.contacts/raw_contacts");
		Cursor cursor = resolver.query(uri, null, null, null, null);
		if (cursor != null) {
			while (cursor.moveToNext()) {
				String id = cursor.getString(cursor.getColumnIndex("contact_id"));//获得联系人id
			    //一个id产生一个业务对象
				contact_item = new Contact();
				//此uri是查询获得联系人详细信息，根据联系人的id
				uri = Uri.parse("content://com.android.contacts/data");
				Cursor cursor_detail = resolver.query(uri, null, "raw_contact_id=?", new String[]{id}, null);
				while(cursor_detail.moveToNext())
				{
					String data1 = cursor_detail.getString(cursor_detail.getColumnIndex("data1"));
    				String mime_id = cursor_detail.getString(cursor_detail.getColumnIndex("mimetype"));
    				
    				System.out.println("data1 = "+ data1);
    				System.out.println("mimetype_id = " + mime_id);	
    				
    				if("vnd.android.cursor.item/name".equals(mime_id))
    				{
    					contact_item.setName(data1);
    				}else if("vnd.android.cursor.item/phone_v2".equals(mime_id))
    				{
    					contact_item.setNumber(data1);
    				}else if("vnd.android.cursor.item/email_v2".equals(mime_id))
    				{
    					contact_item.setEmail(data1);
    				}
    				
				}
				//每一个id的联系人数据全部读取完毕后，将联系人信息保存到集合中
				contact_list.add(contact_item);
				System.out.println("-----------------");
			}
		}
		return contact_list;
	}
}
