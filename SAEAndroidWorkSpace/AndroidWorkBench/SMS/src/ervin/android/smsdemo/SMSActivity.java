package ervin.android.smsdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SMSActivity extends Activity {
    /** Called when the activity is first created. */
	private EditText et1 = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        et1 = (EditText) findViewById(R.id.et1);
    }
    
    public void select(View view)
    {
    	Intent intent = new Intent(this,SelectContact.class);
    	//startActivity(intent);
    	startActivityForResult(intent, 0);
    }
    @Override
    //Called when an activity you launched exits,当联系人页面退出时，调用此方法
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);
		if (data != null) {
			String number = data.getStringExtra("number");
			et1.setText(number);
		}
    }
}