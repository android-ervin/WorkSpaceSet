package ervin.android.smsdemo;

import java.util.List;
import java.util.zip.Inflater;

import ervin.android.contactmodel.Contact;
import ervin.android.getcontactsutil.GetContact;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class SelectContact extends Activity {

	private ListView lv_select ;
	private List<Contact> contacts;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_selectcontact);
		
		lv_select = (ListView) findViewById(R.id.lv_select);
		contacts = GetContact.getContactInfo(this);
		lv_select.setAdapter(new myAdapter());
		
		
		lv_select.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Contact contact_select = contacts.get(position);
				Intent data = new Intent();
				data.putExtra("number", contact_select.getNumber());
				setResult(0, data);
				
				finish();
				
			}
		});
	}
	
	public class myAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return contacts.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Contact contact_item = contacts.get(position);
			View view = View.inflate(SelectContact.this, R.layout.select_items, null);
			
			TextView tv1 = (TextView) view.findViewById(R.id.tv1);
			tv1.setText(contact_item.getName());
			
			TextView tv2 = (TextView) view.findViewById(R.id.tv2);
			tv2.setText(contact_item.getNumber());
			
			TextView tv3 = (TextView) view.findViewById(R.id.tv3);
			tv3.setText(contact_item.getEmail());
			return view;
		}
		
	}
}
