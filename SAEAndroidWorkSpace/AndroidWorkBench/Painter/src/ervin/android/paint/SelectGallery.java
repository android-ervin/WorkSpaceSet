package ervin.android.paint;

import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class SelectGallery extends Activity{

	private ImageView iv_select;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectpic);
		iv_select = (ImageView) findViewById(R.id.iv_select);
		
	}
	public void select(View view)
	{
		Intent intent = new Intent();
    	//设置动作是：Pick an item from the data, returning what was selected. 
    	intent.setAction(intent.ACTION_PICK);
    	//设置类型：从图库应用中选择
    	intent.setType("image/*");
    	
    	//startActivity(intent);
    	startActivityForResult(intent, 0);//需要获取返回值
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		
		if(data!=null)
		{
			Uri uri = data.getData();
			//获得缩略图  
			//data.getParcelableExtra("data");
			//iv_select.setImageURI(uri);
			try {
				InputStream is = getContentResolver().openInputStream(uri);
				Bitmap bitmap = BitmapFactory.decodeStream(is);
				
				//创建一个图片的拷贝
				
				//1,创建一个新的空白的图片,图片大小和原图一样
				Bitmap blankbitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
				//2,得到一个画布，将画布填充
				Canvas canvas = new Canvas(blankbitmap);
				//3，在画布上画画，第一个参数就是参考的原图
				canvas.drawBitmap(bitmap, new Matrix(), new Paint());
				Paint paint = new Paint();
				paint.setTextSize(20);
				paint.setColor(Color.RED);
				canvas.drawText("我是拷贝的图片", 20, 20, paint);
				iv_select.setImageBitmap(blankbitmap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}
	
	public void trans(View view)
	{
		Intent intent = new Intent(this,demo.class);
		startActivity(intent);
	}
}
