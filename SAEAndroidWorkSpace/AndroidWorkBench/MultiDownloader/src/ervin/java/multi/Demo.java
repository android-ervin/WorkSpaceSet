package ervin.java.multi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

public class Demo {

	/**
	 * 多线程点对点下载测试类（类似迅雷下载，同时开启多个线程分段下载）
	 * 断点下载（第一次下载部分文件后，通讯中断，第二次会接着断点继续下载）
	 * @param args
	 */
	private static String path="http://127.0.0.1:81/ie.exe";
	private static int threadCount=3; //同时开启三个线程下载文件
	public static void main(String[] args) {
		// 第一步，获取一个文件，获取该文件的长度，在本地创建一个大小和服务器一样大的临时文件
		URL url;
		try {
			url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5000);
			int code = conn.getResponseCode();
			int length = conn.getContentLength();
			if(code ==200)
			{
				System.out.println("文件长度为" + length);
				//每个线程需要下载的文件大小，以及每个线程下载的开始和结束位置。
				int blocksize = length / threadCount ;
				//在本地创建一个同样大小的临时文件
				RandomAccessFile raf = new RandomAccessFile("setup.exe", "rwd");
				raf.setLength(length);
				raf.close();
				
				for(int threadId = 1 ; threadId<=threadCount ; threadId++)
				{
					int startindex = (threadId -1)*blocksize;
					int endindex = threadId*blocksize - 1;
					if(threadId==threadCount)
					{
						endindex = length;
					}
					System.out.println("线程"+threadId+"-->开始下载位置："+startindex+"--结束位置"+endindex);
					new DownloadThread(threadId, startindex, endindex, path).start();
				}
	
			}else
			{
				System.out.print("获取文件失败");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//下载线程
	public static class DownloadThread extends Thread
	{
		private int threadId;
		private int startindex;
		private int endindex;
		private String path ;
		
		/**
		 * @param threadId     线程ID
		 * @param startindex   线程下载开始位置
		 * @param endindex     线程下载结束位置
		 * @param path         网络路径
		 */
		public DownloadThread(int threadId, int startindex, int endindex,
				String path) {
			super();
			this.threadId = threadId;
			this.startindex = startindex;
			this.endindex = endindex;
			this.path = path;
		}

		@Override
		public void run() {
			try {
//				File filerecord = new File(threadId + ".txt");
//				if(filerecord.exists())
//				{
//					FileInputStream fis = new FileInputStream(filerecord);
//					byte[] b = new byte[1024];
//					int length = fis.read(b);
//					String downloadlen = new String(b,0,length);
//					int downloadcount = Integer.parseInt(downloadlen);
//					startindex = downloadcount ;
//				}
				
				URL url = new URL(path);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(5000);
				//重要：请求服务器下载部分文件，指定文件的起始和终止位置。
				conn.setRequestProperty("Range", "bytes="+startindex+"-"+endindex+"");
				
				int code = conn.getResponseCode(); //从服务器请求全部资源时候200 OK，如果是请求部分资源则是206 OK
				System.out.println("code:" + code);
				InputStream is = conn.getInputStream();
				RandomAccessFile raf = new RandomAccessFile("setup.exe", "rwd");
				raf.seek(startindex); //定义随机文件从哪个位置开始写。
				
				byte[] b = new byte[1024];
				int len = 0;
				//将下载的总大小保存到文件中
				int total =0;
				//File file = new File(threadId+".txt");//这种方式写文件测试不行
				
				while((len =raf.read(b))!=-1)
				{
					//FileOutputStream fos = new FileOutputStream(file);
					RandomAccessFile file = new RandomAccessFile(threadId+".txt", "rwd");
					raf.write(b, 0, len);
					total+=len; //写进了多少数据
					System.out.println("线程" + threadId+"total:"+total);
					
					file.write((""+total).getBytes());
					file.close();
					
					//fos.write((String.valueOf(total)).getBytes());
					//fos.close();
				}
				
				is.close();
				raf.close();
				System.out.println("线程" + threadId+"下载完毕了");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}

}
