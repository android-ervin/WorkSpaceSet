package cn.jiangqq.pps.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.jiangqq.pps.activity.R;

public class MyGridViewAdapter extends BaseAdapter {

	private Context mContext;
	private List<String> mnames;
	private List<Integer> mimages;
	public static final int PAGE_SIZE = 10; // 每一屏幕显示28 Button
/*	private static String[] names = {
        "体温偏低","体温偏高","体温过高",
        "心率极高","心率偏高","心率偏低","心率偏低",
        "心率极低","运动提醒","吃饭提醒","睡眠提醒",
        "久坐提醒","喝水提醒 ","想你了","戳一下","心跳传递","感受我的饭量",
        "感受我的运动"  
	};
	private static int[] images = {
        R.drawable.emotion1,
        R.drawable.emotion2,
        R.drawable.emotion3,
        R.drawable.emotion4,
        R.drawable.emotion5,
        R.drawable.emotion6,
        R.drawable.emotion7,
        R.drawable.emotion8,
        R.drawable.emotion9,
        R.drawable.emotion10,
        R.drawable.emotion11,
        R.drawable.emotion12,
        R.drawable.emotion13,
        R.drawable.emotion14,
        R.drawable.emotion15,
        R.drawable.emotion16,
        R.drawable.emotion17,
        R.drawable.emotion2
       
	};*/
	
	public MyGridViewAdapter(Context pContext, String[] pStrs, int[] iMages ,int page) {
		this.mContext = pContext;
		mnames = new ArrayList<String>();
		mimages = new ArrayList<Integer>();
		int i = page *PAGE_SIZE;
		int end = i +PAGE_SIZE;
		while ((i < pStrs.length) && (i < end)) {
			mnames.add(pStrs[i]);
			mimages.add(iMages[i]);
			i++;
		}
	}
	@Override
	public int getCount() {
		
		return mnames.size();
	}
	@Override
	public Object getItem(int position) {
		
		return mnames.get(position);
	}
	@Override
	public long getItemId(int position) {	
		return position;
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		
		
		 View view = View.inflate(mContext, R.layout.gridview_item, null);
         ImageView iv_item = (ImageView) view.findViewById(R.id.iv_item);
         TextView tv_item = (TextView) view.findViewById(R.id.tv_item);

         iv_item.setImageResource(mimages.get(position));
         tv_item.setText(mnames.get(position));
         return view;
	}

	
}
