package cn.jiangqq.pps.activity;

import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;
import cn.jiangqq.pps.adapter.MyGridViewAdapter;
import cn.jiangqq.pps.adapter.MyViewPagerAdapter;

/**
 * 实现自定义ViewPager+GridView
 * @author jiangqq
 * @time 2013/07/17  17:55 
 */
public class MainActivity extends Activity {
	private MyViewPagerAdapter adapter;
	private List<GridView> mLists;
	private ViewPager mViewPager;
	/*private String[] mStrs = new String[] { "01", "02", "03", "04", "05", "06",
			"07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17",
			"18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
			"29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
			"40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50",
			"51", "52", "53", "54", "55", "56", "57", "58", "59", "60" };*/
	private static String[] names = {
        "体温偏低","体温偏高","体温过高",
        "心率极高","心率偏高","心率偏低","心率偏低",
        "心率极低","运动提醒","吃饭提醒","睡眠提醒",
        "久坐提醒","喝水提醒 ","想你了","戳一下","心跳传递","感受我的饭量",
        "感受我的运动"  
	};
	private static int[] images = {
        R.drawable.emotion1,
        R.drawable.emotion2,
        R.drawable.emotion3,
        R.drawable.emotion4,
        R.drawable.emotion5,
        R.drawable.emotion6,
        R.drawable.emotion7,
        R.drawable.emotion8,
        R.drawable.emotion9,
        R.drawable.emotion10,
        R.drawable.emotion11,
        R.drawable.emotion12,
        R.drawable.emotion13,
        R.drawable.emotion14,
        R.drawable.emotion15,
        R.drawable.emotion16,
        R.drawable.emotion17,
        R.drawable.emotion2
        
	};
	private int index = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
		mViewPager = (ViewPager) findViewById(R.id.myviewpager);
		mViewPager.setOnPageChangeListener(new MyOnPageChanger());
		adapter = new MyViewPagerAdapter(this, mLists);
		mViewPager.setAdapter(adapter);

	}
    
	public void init() {

		final int PageCount = (int) Math.ceil(names.length / 10.0f);
		mLists = new ArrayList<GridView>();

		for (int i = 0; i < PageCount; i++) {
			GridView gv = new GridView(this);
			gv.setAdapter(new MyGridViewAdapter(this, names,images,i));
			gv.setGravity(Gravity.CENTER);
			gv.setClickable(true);
			gv.setFocusable(true);
			gv.setNumColumns(5);
			gv.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					/*Toast.makeText(MainActivity.this,
							"正在播放第" + names[index*28+arg2] + "集",
							Toast.LENGTH_SHORT).show();*/
				}
			});
			mLists.add(gv);
		}
	}
	
	/**
	 * ViewPager页面选项卡进行切换时候的监听器处理
	 * @author jiangqingqing
	 *
	 */
	class MyOnPageChanger implements OnPageChangeListener
	{
		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			
		}
		@Override
		public void onPageSelected(int arg0) {
			
			index=arg0;
			Log.i("jiangqq", "当前在第"+index+"页");
		}
		
	}
}
