package com.anzh.viewpager;


import huahua.viewpager.R;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


@SuppressLint("ValidFragment")
public class fragment1 extends Fragment{
	private View mMainView;
	private TextView tv;
	private Button btn;
	private ProgressBar pro;
	private int mIndex;
	
	public fragment1(int index){
		this.mIndex = index;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		mMainView = inflater.inflate(R.layout.fragment1, (ViewGroup)getActivity().findViewById(R.id.viewpager), false);
		tv = (TextView)mMainView.findViewById(R.id.tv1);
		btn = (Button)mMainView.findViewById(R.id.btn1);
		pro = (ProgressBar) mMainView.findViewById(R.id.pro);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup p = (ViewGroup) mMainView.getParent(); 
        if (p != null) { 
            p.removeAllViewsInLayout(); 
        }
		return mMainView;
	}
    
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(mIndex == 1){
			setUserVisibleHint(true);
		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
    
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		Log.v("anzh", "fragment1-->setUserVisibleHint()============="+isVisibleToUser);
		if(isVisibleToUser){
			if(tv != null && btn != null && pro != null){
				tv.setVisibility(View.VISIBLE);
				btn.setVisibility(View.VISIBLE);
				pro.setVisibility(View.GONE);
			}
		}
		super.setUserVisibleHint(isVisibleToUser);
	}
}
