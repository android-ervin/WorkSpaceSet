package com.haozi.mydemoviewflipper;

import android.app.Activity;
import android.os.Bundle;

import com.haozi.mydemoviewflipper.wiget.ViewFlipperView;

public class Main extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(new ViewFlipperView(this));
	}
}
