package com.neurosky.ble;

import java.util.UUID;

import com.neurosky.chrisbleperiphery.R;

import android.app.Activity;
import android.bluetooth.*;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class BLEPeripheryActivity extends Activity {

	public static String serviceUUID = "039AFFF0-2C94-11E3-9E06-0002A5D5C51B";

	public static String characteristicUUID = "039AFFA1-2C94-11E3-9E06-0002A5D5C51B";

	private BluetoothGattServer server;
	private BluetoothManager manager;
	private BluetoothGattCharacteristic character;
	private BluetoothGattService service;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		character = new BluetoothGattCharacteristic(
				UUID.fromString(characteristicUUID),
				BluetoothGattCharacteristic.PROPERTY_NOTIFY,
				BluetoothGattCharacteristic.PERMISSION_READ);

		service = new BluetoothGattService(UUID.fromString(serviceUUID),
				BluetoothGattService.SERVICE_TYPE_PRIMARY);

		service.addCharacteristic(character);

		manager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		server = manager.openGattServer(this,
				new BluetoothGattServerCallback() {

					@Override
					public void onConnectionStateChange(BluetoothDevice device,
							int status, int newState) {
						Log.d("Chris", "onConnectionStateChange");
						super.onConnectionStateChange(device, status, newState);
					}

					@Override
					public void onServiceAdded(int status,
							BluetoothGattService service) {

						Log.d("Chris", "service added");
						super.onServiceAdded(status, service);
					}

					@Override
					public void onCharacteristicReadRequest(
							BluetoothDevice device, int requestId, int offset,
							BluetoothGattCharacteristic characteristic) {
						Log.d("Chris", "onCharacteristicReadRequest");
						super.onCharacteristicReadRequest(device, requestId,
								offset, characteristic);
					}

					@Override
					public void onCharacteristicWriteRequest(
							BluetoothDevice device, int requestId,
							BluetoothGattCharacteristic characteristic,
							boolean preparedWrite, boolean responseNeeded,
							int offset, byte[] value) {
						Log.d("Chris", "onCharacteristicWriteRequest");
						super.onCharacteristicWriteRequest(device, requestId,
								characteristic, preparedWrite, responseNeeded,
								offset, value);
					}

					@Override
					public void onDescriptorReadRequest(BluetoothDevice device,
							int requestId, int offset,
							BluetoothGattDescriptor descriptor) {
						Log.d("Chris", "onDescriptorReadRequest");
						super.onDescriptorReadRequest(device, requestId,
								offset, descriptor);
					}

					@Override
					public void onDescriptorWriteRequest(
							BluetoothDevice device, int requestId,
							BluetoothGattDescriptor descriptor,
							boolean preparedWrite, boolean responseNeeded,
							int offset, byte[] value) {
						Log.d("Chris", "onDescriptorWriteRequest");
						super.onDescriptorWriteRequest(device, requestId,
								descriptor, preparedWrite, responseNeeded,
								offset, value);
					}

					@Override
					public void onExecuteWrite(BluetoothDevice device,
							int requestId, boolean execute) {
						Log.d("Chris", "onExecuteWrite");
						super.onExecuteWrite(device, requestId, execute);
					}

				});

		server.addService(service);

		this.setContentView(R.layout.ble_periphery);

		super.onCreate(savedInstanceState);
	}

	int i = 0;

	public void onButtonClicked(View v) {
		Log.d("Chris", "XXXX");
		i++;
		character.setValue("index" + i);
		server.addService(service);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

}
