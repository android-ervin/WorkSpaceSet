package com.example.navigatedemo;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class Tools 
{
	/**
	 * @author lianglin
	 * @param act The Activity which invoke this function
	 * @param viewId The view of the activity
	 * @param imageId The guide image
	 * @param preferenceName The sharePreference which save the flag whether first login in
	 * */
	public static void setGuidImage(Activity act,int viewId, int imageId, String preferenceName)
	{
		@SuppressWarnings("static-access")
		SharedPreferences preferences = act.getSharedPreferences(preferenceName,act.MODE_PRIVATE);
		final SharedPreferences.Editor editor = preferences.edit();
		final String key = act.getClass().getName()+"_firstLogin";
		if(!preferences.contains(key))
		{
			editor.putBoolean(key, true);
			editor.commit();
		}
		
		//�ж��Ƿ��״ε�½
		if(!preferences.getBoolean(key, true))
			return;
		
		View view = act.getWindow().getDecorView().findViewById(viewId);
		ViewParent viewParent = view.getParent();
		if(viewParent instanceof FrameLayout)
		{
            final FrameLayout frameLayout = (FrameLayout)viewParent;
            final ImageView guideImage = new ImageView(act.getApplication());
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            guideImage.setLayoutParams(params);
            guideImage.setScaleType(ScaleType.FIT_XY);
                guideImage.setImageResource(imageId);
                guideImage.setOnClickListener(new View.OnClickListener() 
                {
                    @Override
                    public void onClick(View v)
                    {
                        frameLayout.removeView(guideImage);
                        editor.putBoolean(key, false);
                        editor.commit();
                    }
                });
                frameLayout.addView(guideImage);//��������ͼƬ
                
        }
	}
}
