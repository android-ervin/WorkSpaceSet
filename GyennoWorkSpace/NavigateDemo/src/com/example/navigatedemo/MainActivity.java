package com.example.navigatedemo;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity 
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Tools.setGuidImage(this, R.id.r1, R.drawable.x, "FirstLogin");

	}

}
