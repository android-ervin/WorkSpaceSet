package ervin.andorid.chat;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

	protected static final String TAG = "MainActivity";
	private ChattingAdapter chatHistoryAdapter;
	private List<ChatMessage> messages = new ArrayList<ChatMessage>();
	private ListView chatHistoryLv;
	private Button sendBtn;
	private EditText textEditor;
	private Button fl;

	private EmotionDialog menuWindow;
	private int bottonheight;
	private LinearLayout viewcontainer;
	ViewGroup.LayoutParams params;
	private int listItemheight;
	private int emotionid;

    private EmotionView emotionView;
    private static String[] names = {
            "早","开心","上班喽",
            "加油","喝水","在干吗",
            "汗","休息","旅行","马上去"
    };
    private static int[] images = {
            R.drawable.emotion1,
            R.drawable.emotion2,
            R.drawable.emotion3,
            R.drawable.emotion4,
            R.drawable.emotion5,
            R.drawable.emotion6,
            R.drawable.emotion7,
            R.drawable.emotion8,
            R.drawable.emotion9,
            R.drawable.emotion10
    };
    private GridView gv_emotion;
    private Adapter madapter;
    @Override
	public void onCreate(Bundle savedInstanceState) {
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chatting);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.chatting_title_bar);
		chatHistoryLv = (ListView) findViewById(R.id.chatting_history_lv);
		setAdapterForThis();
		sendBtn = (Button) findViewById(R.id.send_button);
		//textEditor = (EditText) findViewById(R.id.text_editor);

		viewcontainer = (LinearLayout)findViewById(R.id.popcontainer);
        emotionView = new EmotionView(this);
        madapter = new Adapter();

		fl = (Button) findViewById(R.id.choosemotion);
		chatHistoryLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
			//	menuWindow.dismiss();
				dismissView();
			}
		});
		//menuWindow = new EmotionDialog(MainActivity.this, itemonclick);
		fl.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {


                View view = View.inflate(MainActivity.this, R.layout.emotionwindow, null);
                viewcontainer.addView(view);
                gv_emotion = (GridView)viewcontainer.findViewById(R.id.emotion);
                gv_emotion.setAdapter(madapter);
                gv_emotion.setOnItemClickListener(itemonclick);
                System.out.println("height："+viewcontainer.getHeight());
				/*System.out.println("????????");
				// TODO Auto-generated method stub	
				if(!menuWindow.isShowing()){
				menuWindow.showAtLocation(
						MainActivity.this.findViewById(R.id.chat_root),
						Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
						0, DensityUtil.dip2px(MainActivity.this, 55)); // ????layout??PopupWindow???????λ??
*/
				//showView();
				//viewcontainer = View.inflate(MainActivity.this, R.layout.emotionwindow, null);
				
				}/*else{
					menuWindow.dismiss();
					dismissView();
				}
			}*/
		});
		sendBtn.setOnClickListener(l);
		
	}
	private void showView() {
		menuWindow.getContentView().measure(0,0);
		int height = menuWindow.getContentView().getMeasuredHeight();
		bottonheight = height+55;
		System.out.println("popwindow的高度："+height);
		params = viewcontainer.getLayoutParams(); 
		params.height = DensityUtil.dip2px(MainActivity.this, bottonheight-230);
		viewcontainer.setLayoutParams(params);
	}
	
	private AdapterView.OnItemClickListener itemonclick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			
			emotionid  = position;
			System.out.println("选择了："+position);
		}
	};
	// ????adapter
	private void setAdapterForThis() {
		initMessages();
		chatHistoryAdapter = new ChattingAdapter(this, messages);
		chatHistoryLv.setAdapter(chatHistoryAdapter);
	}

	// ?listView???????
	private void initMessages() {
		/*messages.add(new ChatMessage(ChatMessage.MESSAGE_FROM, "hello"));
		messages.add(new ChatMessage(ChatMessage.MESSAGE_TO, "hello"));
		messages.add(new ChatMessage(ChatMessage.MESSAGE_FROM, "?????"));
		messages.add(new ChatMessage(ChatMessage.MESSAGE_TO, "?????!"));
		messages.add(new ChatMessage(ChatMessage.MESSAGE_FROM,
				"??????????????http://hi.csdn.net/lyfi01"));
		messages.add(new ChatMessage(ChatMessage.MESSAGE_TO, "????????лл"));
		messages.add(new ChatMessage(ChatMessage.MESSAGE_TO, "????????????????????????????"));*/
	}

	private View.OnClickListener l = new View.OnClickListener() {
		public void onClick(View v) {
				sendMessage();		
			}
		
		// ????????
		private void sendMessage() {
			messages.add(new ChatMessage(ChatMessage.MESSAGE_FROM, emotionid));
			chatHistoryAdapter.notifyDataSetChanged();
		}
	};

	public void setListViewHeightBasedOnChildren(ListView listView) {

		// ???listview????????
		ListAdapter listAdapter = listView.getAdapter(); // item????

		if (listAdapter == null) {

			return;
		}
		int totalHeight = 0;

		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = chatHistoryAdapter.getView(i, null, listView);

			listItem.measure(0, 0); // ????????View ???? //????????????????
			listItemheight = listItem.getMeasuredHeight();
			System.out.println("listview????"+listItemheight);
			totalHeight += DensityUtil.dip2px(getApplicationContext(),
					listItem.getMeasuredHeight()+bottonheight)
					+ listView.getDividerHeight();
			
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();

		params.height = totalHeight;
		listView.setLayoutParams(params);

	}
	private void dismissView() {
		/*params = viewcontainer.getLayoutParams();
		params.height = 0;
		viewcontainer.setLayoutParams(params);*/
        viewcontainer.removeAllViews();
	}

    class Adapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return names.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = View.inflate(MainActivity.this, R.layout.emotion_list_item, null);
            ImageView iv_item = (ImageView) view.findViewById(R.id.iv_item);
            TextView tv_item = (TextView) view.findViewById(R.id.tv_item);

            iv_item.setImageResource(images[position]);
            tv_item.setText(names[position]);
            return view;
        }
    }
}
