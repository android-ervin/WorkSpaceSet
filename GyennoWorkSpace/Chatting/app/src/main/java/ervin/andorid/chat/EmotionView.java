package ervin.andorid.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Ervin on 2015/4/27.
 */
public class EmotionView extends View {

    private View mMenuView;
    public Context context;
    public EmotionView(Context context) {
        super(context);
        this.context = context;

    }
    public View getView()
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.emotionwindow,null);
        return mMenuView;
    }

}
