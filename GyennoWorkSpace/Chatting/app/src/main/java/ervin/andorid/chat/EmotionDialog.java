package ervin.andorid.chat;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;

public class EmotionDialog extends PopupWindow {

	private GridView gv_emotion;
	private View mMenuView;
	private Context context;
	private static String[] names = {
		"早","开心","上班喽",
		"加油","喝水","在干吗",
		"汗","休息","旅行","马上去"
	};
	private static int[] images = {
		R.drawable.emotion1,
		R.drawable.emotion2,
		R.drawable.emotion3,
		R.drawable.emotion4,
		R.drawable.emotion5,
		R.drawable.emotion6,
		R.drawable.emotion7,
		R.drawable.emotion8,
		R.drawable.emotion9,
		R.drawable.emotion10
	}; 
	
	public EmotionDialog(Context context,AdapterView.OnItemClickListener itemonclick) {
		this.context = context ;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mMenuView = inflater.inflate(R.layout.emotionwindow, null);
		
		gv_emotion = (GridView) mMenuView.findViewById(R.id.emotion);
		gv_emotion.setOnItemClickListener(itemonclick);
		gv_emotion.setAdapter(new Adapter());
		
		this.setContentView(mMenuView);
		this.setWidth(LayoutParams.FILL_PARENT);
		this.setHeight(LayoutParams.WRAP_CONTENT);
		/*ColorDrawable dw = new ColorDrawable(R.color.emotion);
		this.setBackgroundDrawable(dw);*/

		//System.out.println("popwindow-->"+this.getHeight());
		mMenuView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int height = mMenuView.findViewById(R.id.pop_layout).getTop();
				System.out.println("height-->"+height);
				int y = (int) event.getY();
				System.out.println("y-->"+y);
				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (y < height) {
						dismiss();
					}
				}
				return true;
			}
		});
	}
	
	class Adapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return names.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(context, R.layout.emotion_list_item, null);
			ImageView iv_item = (ImageView) view.findViewById(R.id.iv_item);
			TextView tv_item = (TextView) view.findViewById(R.id.tv_item);
			
			iv_item.setImageResource(images[position]);
			tv_item.setText(names[position]);
			return view;
		}
		
	}
}
