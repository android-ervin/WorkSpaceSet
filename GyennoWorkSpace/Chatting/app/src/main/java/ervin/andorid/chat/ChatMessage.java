package ervin.andorid.chat;

public class ChatMessage {

	public static final int MESSAGE_FROM = 0;
	 public static final int MESSAGE_TO = 1;
	 private int direction;
	 private String content;
	 private int id;
	 public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ChatMessage(int direction, int id) {
	  super();
	  this.direction = direction;
	  this.id = id;
	 }
	 public int getDirection() {
	  return direction;
	 }
	 public void setDirection(int direction) {
	  this.direction = direction;
	 }
	 public void setContent(String content) {
	  this.content = content;
	 }
	 public CharSequence getContent() {
	  return content;
	 }
}
