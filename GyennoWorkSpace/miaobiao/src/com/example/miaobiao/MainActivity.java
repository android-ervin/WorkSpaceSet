package com.example.miaobiao;

import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.Inflater;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	int min = 0;
	int sec = 0;
	private long mlCount = 0;
	private long mlTimerUnit = 1000;
	private TextView tvTime;
	private Button btnStartPause;
	private Button btnStop;
	private Timer timer = null;
	private TimerTask task = null;
	private Handler handler = null;
	private Message msg = null;
	private boolean bIsRunningFlg = false;
	private static final String MYTIMER_TAG = "MYTIMER_LOG";
	private boolean setTime = false;

	// menu item
	private static final int EXIT_ID = Menu.FIRST;
	private static final int SETTINGTIME_ID = Menu.FIRST + 1;// 设置时间

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		tvTime = (TextView) findViewById(R.id.tvTime);
		btnStartPause = (Button) findViewById(R.id.btnStartPaunse);
		btnStop = (Button) findViewById(R.id.btnStop);

		SharedPreferences sharedPreferences = getSharedPreferences(
				"mytimer_unit", Context.MODE_PRIVATE);
		// getString()第二个参数为缺省值，如果preference中不存在该key，将返回缺省值
		mlTimerUnit = sharedPreferences.getLong("time_unit", 100);
		Log.i(MYTIMER_TAG, "mlTimerUnit = " + mlTimerUnit);

		tvTime.setText(R.string.init_time_100millisecond);

		// Handle timer message
		handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					mlCount++;
					int totalSec = 0;
					int yushu = 0;

					totalSec = (int) (mlCount / 10);
					yushu = (int) (mlCount % 10);

					// Set time display
					min = (totalSec / 60);
					sec = (totalSec % 60);

					try {
						System.out.println("case 1 mlCount = " + mlCount);

						tvTime.setText(String.format("%1$02d:%2$02d:%3$d", min,
								sec, yushu));

					} catch (Exception e) {
						tvTime.setText("" + min + ":" + sec + ":" + yushu);
						e.printStackTrace();
						Log.e("MyTimer onCreate", "Format string error.");
					}
					break;
				case 2:
					Bundle b = msg.getData();
					min = b.getInt("min");
					sec = b.getInt("sec");
					mlCount = min * 600 + sec * 10;
					System.out.println("case 2 mlCount = " + mlCount);
					tvTime.setText(String.format("%1$02d:%2$02d:%3$d", min,
							sec, 0));
					break;

				case 3:
					if (mlCount > 0) {
						mlCount--;
						int yushu2 = (int) (mlCount % 10);

						// Set time display
						int min = (((int) (mlCount / 10)) / 60);
						int sec = (((int) (mlCount / 10)) % 60);
						tvTime.setText(String.format("%1$02d:%2$02d:%3$d", min,
								sec, yushu2));
					} else
						setTime = false;

					break;
				default:
					break;
				}

				super.handleMessage(msg);
			}

		};

		btnStartPause.setOnClickListener(startPauseListener);
		btnStop.setOnClickListener(stopListener);
	}

	// Start and pause
	View.OnClickListener startPauseListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			Log.i(MYTIMER_TAG, "Start/Pause is clicked.");

			if ((null == timer) && !setTime) {
				if (null == task) {
					task = new TimerTask() {

						@Override
						public void run() {
							if (null == msg) {
								msg = new Message();
							} else {
								msg = Message.obtain();
							}
							msg.what = 1;
							handler.sendMessage(msg);
						}

					};
				}

				timer = new Timer(true);
				timer.schedule(task, mlTimerUnit, mlTimerUnit); // set timer
																// duration
			}
			if (setTime) {
				if (null == task) {
					task = new TimerTask() {

						@Override
						public void run() {
							if (null == msg) {
								msg = new Message();
							} else {
								msg = Message.obtain();
							}
							msg.what = 3;
							handler.sendMessage(msg);
						}
					};
					timer = new Timer(true);
					timer.schedule(task, mlTimerUnit, mlTimerUnit);
				}
			}

			// start
			if (!bIsRunningFlg) {
				bIsRunningFlg = true;
				btnStartPause.setText("暂停");
			} else { // pause
				try {

					bIsRunningFlg = false;
					task.cancel();
					task = null;
					timer.cancel(); // Cancel timer
					timer.purge();
					timer = null;
					handler.removeMessages(msg.what);
					btnStartPause.setText("开始");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	};

	// Stop
	View.OnClickListener stopListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			Log.i(MYTIMER_TAG, "Stop is clicked.");

			if (null != timer) {
				task.cancel();
				task = null;
				timer.cancel(); // Cancel timer
				timer.purge();
				timer = null;
				handler.removeMessages(msg.what);
			}

			setTime = false;
			mlCount = 0;
			bIsRunningFlg = false;
			btnStartPause.setText("开始");
			System.out.println("min = " + min + "sec = " + sec + "timer = "
					+ timer);
			tvTime.setText(R.string.init_time_100millisecond);
		}

	};
	private LayoutInflater inflater;

	// Menu
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);

		Log.i(MYTIMER_TAG, "Menu is created.");

		// Stop timer
		if (null != task) {
			task.cancel();
			task = null;
		}
		if (null != timer) {
			timer.cancel(); // Cancel timer
			timer.purge();
			timer = null;
			handler.removeMessages(msg.what);
		}

		bIsRunningFlg = false;
		mlCount = 0;

		btnStartPause.setText("开始");

		menu.add(0, SETTINGTIME_ID, 1, R.string.menu_settingtime);
		// 退出
		menu.add(0, EXIT_ID, 2, R.string.menu_exit);

		return true;
	}

	// Menu item
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.i(MYTIMER_TAG, "Menu item is selected.");

		switch (item.getItemId()) {
		case SETTINGTIME_ID:
			AlertDialog.Builder builder = new Builder(MainActivity.this);
			builder.setTitle("设置时间");
			final EditText minText;
			final EditText sevText;
			inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.settingtime, null);
			minText = (EditText) view.findViewById(R.id.min);
			sevText = (EditText) view.findViewById(R.id.sec);

			builder.setView(view);
			builder.setPositiveButton("确定", new OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					setTime = true;
					min = Integer.parseInt(minText.getText().toString());
					sec = Integer.parseInt(sevText.getText().toString());
					if ((min > 59 || min < 0) || (sec > 59 || sec < 0)) {
						Toast.makeText(getApplicationContext(), "请输入正确的时间", 0)
								.show();
					} else {
						//Message message = Message.obtain();
						msg = Message.obtain();
						Bundle b = new Bundle();
						b.putInt("min", min);
						b.putInt("sec", sec);
						msg.setData(b);
						msg.what = 2;
						handler.sendMessage(msg);
					}
				}
			});
			builder.setNegativeButton("取消", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();

				}
			});
			AlertDialog alert = builder.create();
			alert.show();

			break;

		case EXIT_ID:
			finish(); // Exit application
			break;
		default:
			Log.i(MYTIMER_TAG, "Other menu item...");
			break;
		}

		// Save timer unit
		try {
			SharedPreferences sharedPreferences = getSharedPreferences(
					"mytimer_unit", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPreferences.edit();// 获取编辑器
			editor.putLong("time_unit", mlTimerUnit);
			editor.commit();// 提交修改
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(MYTIMER_TAG, "save timer unit error.");
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (KeyEvent.KEYCODE_MENU == keyCode) {
			super.openOptionsMenu(); // 调用这个，就可以弹出菜单

			Log.i(MYTIMER_TAG, "Menu key is clicked.");

			// Stop timer
			if (null != task) {
				task.cancel();
				task = null;
			}
			if (null != timer) {
				timer.cancel(); // Cancel timer
				timer.purge();
				timer = null;
				handler.removeMessages(msg.what);
			}

			bIsRunningFlg = false;
			mlCount = 0;
			btnStartPause.setText("开始");

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

}