package com.peter.ble;

import java.util.UUID;

import com.neurosky.chrisbleperiphery.R;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;



public class BLEPeripheryActivity extends Activity {

	public static String serviceUUID = "039AFFF0-2C94-11E3-9E06-0002A5D5C51B";
	public static String characteristicUUID = "039AFFA1-2C94-11E3-9E06-0002A5D5C51B";
	public static String characteristicUUIDWrite = "039AFFA3-2C94-11E3-9E06-0002A5D5C51B";
	
	private BluetoothGattServer server;
	private BluetoothManager manager;
	private BluetoothGattCharacteristic character;
	private BluetoothGattCharacteristic character2;
	private BluetoothGattService service; 
	
	private TextView myTextView;
	private TextView tv1;

	public static final String ACTION = "com.ervin.ble.data";
	MyBroadCast receiverdata;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		
		
		character = new BluetoothGattCharacteristic(
				UUID.fromString(characteristicUUID),
				BluetoothGattCharacteristic.PROPERTY_NOTIFY,
				BluetoothGattCharacteristic.PERMISSION_READ);
		service = new BluetoothGattService(UUID.fromString(serviceUUID),
				BluetoothGattService.SERVICE_TYPE_PRIMARY);
		service.addCharacteristic(character);
		
		character2 = new BluetoothGattCharacteristic(UUID.fromString(characteristicUUIDWrite),
				BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
				BluetoothGattCharacteristic.PERMISSION_WRITE);
		
		service.addCharacteristic(character2);
		

		//注册广播
		bundReceiverdata();
		
		manager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		server = manager.openGattServer(this,
				new BluetoothGattServerCallback() {

					@Override
					public void onConnectionStateChange(BluetoothDevice device,
							int status, int newState) {
						Log.d("Chris", "onConnectionStateChange");
						super.onConnectionStateChange(device, status, newState);
					}

					@Override
					public void onServiceAdded(int status,
							BluetoothGattService service) {

						Log.d("Chris", "service added");
						super.onServiceAdded(status, service);
					}

					@Override
					public void onCharacteristicReadRequest(
							BluetoothDevice device, int requestId, int offset,
							BluetoothGattCharacteristic characteristic) {
						Log.d("Chris", "onCharacteristicReadRequest");
						super.onCharacteristicReadRequest(device, requestId,
								offset, characteristic);
					}

					@Override
					public void onCharacteristicWriteRequest(
							BluetoothDevice device, int requestId,
							BluetoothGattCharacteristic characteristic,
							boolean preparedWrite, boolean responseNeeded,
							int offset, final byte[] value) {
					
							String text = byte2hex(value);
								
							Log.d("Chris", "onCharacteristicWriteRequest<--->"+text);
							
							System.out.println("写的值为:"+text);
							//发广播
							Intent intent = new Intent();
							intent.setAction(ACTION);
							intent.putExtra("value", text);
							sendBroadcast(intent);// 传递过去
							System.out.println("成功发送数据广播");
							myTextView.setText(text);
								
							/*	Toast.makeText(BLEPeripheryActivity.this
										, text, Toast.LENGTH_LONG).show();*/
								
						super.onCharacteristicWriteRequest(device, requestId,
								characteristic, preparedWrite, responseNeeded,
								offset, value);
					}

					@Override
					public void onDescriptorReadRequest(BluetoothDevice device,
							int requestId, int offset,
							BluetoothGattDescriptor descriptor) {
						Log.d("Chris", "onDescriptorReadRequest");
						super.onDescriptorReadRequest(device, requestId,
								offset, descriptor);
					}

					@Override
					public void onDescriptorWriteRequest(
							BluetoothDevice device, int requestId,
							BluetoothGattDescriptor descriptor,
							boolean preparedWrite, boolean responseNeeded,
							int offset, byte[] value) {
						Log.d("Chris", "onDescriptorWriteRequest");
						super.onDescriptorWriteRequest(device, requestId,
								descriptor, preparedWrite, responseNeeded,
								offset, value);
					}

					@Override
					public void onExecuteWrite(BluetoothDevice device,
							int requestId, boolean execute) {
						Log.d("Chris", "onExecuteWrite");
						super.onExecuteWrite(device, requestId, execute);
					}

				});

		server.addService(service);

		this.setContentView(R.layout.ble_periphery);
		myTextView = (TextView)findViewById(R.id.myTextView);
		tv1 = (TextView) findViewById(R.id.tv1);

		super.onCreate(savedInstanceState);
	}

	int i = 0;

	public void onButtonClicked(View v) {
		Log.d("Chris", "XXXX");
		i++;
		character.setValue("index" + i);
		character2.setValue("index" + i);
		server.addService(service);
	}
	
	private static String byte2hex(byte[] buffer) {
		String h = "";

		for (int i = 0; i < buffer.length; i++) {
			String temp = Integer.toHexString(buffer[i] & 0xFF);
			if (temp.length() == 1) {
				temp = "0" + temp;
			}
			h = h + temp;
		}
		return h;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unbunReceiverdata();
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	public class MyBroadCast extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String str = intent.getStringExtra("value");
			System.out.println("传过来的值为："+str);
			/*myTextView.setText(str);*/
			tv1.setText(str);
			Toast.makeText(BLEPeripheryActivity.this
					, str, Toast.LENGTH_LONG).show();
		}	
	}
	public void bundReceiverdata()
	{
		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION); // 只有持有相同的action的接受者才能接收此广播
		if(receiverdata==null)
			receiverdata = new MyBroadCast();
		registerReceiver(receiverdata,filter);
	}
	public void unbunReceiverdata()
	{
		if (receiverdata!=null)
		{
			unregisterReceiver(receiverdata);
			receiverdata = null;
		}
		
	}
}
