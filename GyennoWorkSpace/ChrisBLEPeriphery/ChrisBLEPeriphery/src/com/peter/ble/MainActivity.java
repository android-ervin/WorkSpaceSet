package com.peter.ble;


import com.neurosky.chrisbleperiphery.R;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;


public class MainActivity extends Activity {
	
	public static final String ACTION = "com.ervin.ble.data";
	MyBroadCast receiverdata;
	
	private ImageView iv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		//开启服务
		Intent intent = new Intent(MainActivity.this,BLEPeripheralService.class);
		startService(intent);
		
		setContentView(R.layout.show1);
		iv = (ImageView) findViewById(R.id.iv);
		bundReceiverdata();
	}
	
	class MyBroadCast extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String str = intent.getStringExtra("value");
			System.out.println("Activity传过来的值为："+str);
			switch (Integer.valueOf(str)) {
			case 1:
				iv.setBackgroundResource(R.drawable.one);
				break;
			case 0:
				iv.setBackgroundResource(R.drawable.eight);
				break;
			case 2:
				iv.setBackgroundResource(R.drawable.two);
				break;
			case 3:
				iv.setBackgroundResource(R.drawable.three);
				break;
			case 4:
				iv.setBackgroundResource(R.drawable.four);
				break;
			case 5:
				iv.setBackgroundResource(R.drawable.five);
				break;
			case 6:
				iv.setBackgroundResource(R.drawable.six);
				break;
			case 7:
				iv.setBackgroundResource(R.drawable.seven);
				break;
			default:
				break;
			}
		}	
	}
	
	public void bundReceiverdata()
	{
		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION); // 只有持有相同的action的接受者才能接收此广播
		if(receiverdata==null)
			receiverdata = new MyBroadCast();
		registerReceiver(receiverdata,filter);
		System.out.println("注册广播成功");
	}
	public void unbunReceiverdata()
	{
		if (receiverdata!=null)
		{
			unregisterReceiver(receiverdata);
			receiverdata = null;
		}		
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unbunReceiverdata();
	}

}
