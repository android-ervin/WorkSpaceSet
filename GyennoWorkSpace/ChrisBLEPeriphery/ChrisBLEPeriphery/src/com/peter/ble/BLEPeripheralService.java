package com.peter.ble;

import java.util.UUID;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;

public class BLEPeripheralService extends Service {

	
	public static String serviceUUID = "039AFFF0-2C94-11E3-9E06-0002A5D5C51B";
	public static String characteristicUUID = "039AFFA1-2C94-11E3-9E06-0002A5D5C51B";
	public static String characteristicUUIDWrite = "039AFFA3-2C94-11E3-9E06-0002A5D5C51B";
	
	private BluetoothGattServer server;
	private BluetoothManager manager;
	private BluetoothGattCharacteristic character;
	private BluetoothGattCharacteristic character2;
	private BluetoothGattService service; 
	
	public static final String ACTION = "com.ervin.ble.data";
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		character = new BluetoothGattCharacteristic(
				UUID.fromString(characteristicUUID),
				BluetoothGattCharacteristic.PROPERTY_NOTIFY,
				BluetoothGattCharacteristic.PERMISSION_READ);
		service = new BluetoothGattService(UUID.fromString(serviceUUID),
				BluetoothGattService.SERVICE_TYPE_PRIMARY);
		service.addCharacteristic(character);
		
		character2 = new BluetoothGattCharacteristic(UUID.fromString(characteristicUUIDWrite),
				BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
				BluetoothGattCharacteristic.PERMISSION_WRITE);
		
		service.addCharacteristic(character2);
	
		manager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		server = manager.openGattServer(this,
				new BluetoothGattServerCallback() {

					@Override
					public void onConnectionStateChange(BluetoothDevice device,
							int status, int newState) {
						Log.d("Chris", "onConnectionStateChange");
						super.onConnectionStateChange(device, status, newState);
					}

					@Override
					public void onServiceAdded(int status,
							BluetoothGattService service) {

						Log.d("Chris", "service added");
						super.onServiceAdded(status, service);
					}

					@Override
					public void onCharacteristicReadRequest(
							BluetoothDevice device, int requestId, int offset,
							BluetoothGattCharacteristic characteristic) {
						Log.d("Chris", "onCharacteristicReadRequest");
						super.onCharacteristicReadRequest(device, requestId,
								offset, characteristic);
					}

					@Override
					public void onCharacteristicWriteRequest(
							BluetoothDevice device, int requestId,
							BluetoothGattCharacteristic characteristic,
							boolean preparedWrite, boolean responseNeeded,
							int offset, final byte[] value) {
					
							String text = byte2hex(value);
								
							Log.d("Chris", "onCharacteristicWriteRequest<--->"+text);
							
							System.out.println("写的值为:"+text);
							//发广播
							Intent intent = new Intent();
							intent.setAction(ACTION);
							intent.putExtra("value", text);
							sendBroadcast(intent);// 传递过去
							System.out.println("成功Activity发送数据广播");
							/*	Toast.makeText(BLEPeripheryActivity.this
										, text, Toast.LENGTH_LONG).show();*/
							BluetoothGattCharacteristic firstServiceChar = new BluetoothGattCharacteristic(
							        UUID.fromString(characteristicUUID),BluetoothGattCharacteristic.PROPERTY_NOTIFY, BluetoothGattCharacteristic.PERMISSION_READ );
							server.notifyCharacteristicChanged(device, firstServiceChar, false);
		
						super.onCharacteristicWriteRequest(device, requestId,
								characteristic, preparedWrite, responseNeeded,
								offset, value);
					}

					@Override
					public void onDescriptorReadRequest(BluetoothDevice device,
							int requestId, int offset,
							BluetoothGattDescriptor descriptor) {
						Log.d("Chris", "onDescriptorReadRequest");
						super.onDescriptorReadRequest(device, requestId,
								offset, descriptor);
					}

					@Override
					public void onDescriptorWriteRequest(
							BluetoothDevice device, int requestId,
							BluetoothGattDescriptor descriptor,
							boolean preparedWrite, boolean responseNeeded,
							int offset, byte[] value) {
						Log.d("Chris", "onDescriptorWriteRequest");
						super.onDescriptorWriteRequest(device, requestId,
								descriptor, preparedWrite, responseNeeded,
								offset, value);
					}

					@Override
					public void onExecuteWrite(BluetoothDevice device,
							int requestId, boolean execute) {
						Log.d("Chris", "onExecuteWrite");
						super.onExecuteWrite(device, requestId, execute);
					}

				});

		
		server.addService(service);	
		System.out.println("服务开启了");
	}

	private static String byte2hex(byte[] buffer) {
		String h = "";

		for (int i = 0; i < buffer.length; i++) {
			String temp = Integer.toHexString(buffer[i] & 0xFF);
			if (temp.length() == 1) {
				temp = temp;
			}
			h = h + temp;
		}
		return h;
	}

	
	@Override
	public void onDestroy() {
		//unbunReceiverdata();
		super.onDestroy();
	}
}
